## Introducción

Los contenidos de este blog se corresponden con los de la UF 1844 Desarrollo de aplicaciones web en el entorno servidor, incardinada en el MF 0492_3 Programación web en el entorno servidor, del certificado IFCD0210 Desarrollo de aplicaciones con tecnologías web regulado por el [RD 1531/2011](https://www.boe.es/buscar/doc.php?id=BOE-A-2011-19503), de 31 de octubre, y modificado por el [RD 628/2013](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2013-9707), de 2 de agosto.

[IFCD0210.MF0492_3.UF1844.pdf - Lanbide](https://apps.lanbide.euskadi.net/descargas/egailancas/certificados/UF/IFCD0210.MF0492_3.UF1844.pdf)
