---
title: Tipos de requisitos (ingeniería de requisitos)
subtitle: Análisis y especificación de requisitos
date: 2021-12-12
draft: false
description: Análisis y especificación de requisitos
tags: ["UF1844","Requisitos","Análisis","Requisitos - Tipos"]
---

<!-- vscode-markdown-toc -->
* 1. [Requisito funcional](#Requisitofuncional)
* 2. [Requisito no funcional](#Requisitonofuncional)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Los requerimientos para un sistema son **descripciones de lo que el sistema debe hacer**: el servicio que ofrece y las restricciones en su operación. Tales requerimientos reflejan las necesidades de los clientes por un sistema que atienda cierto propósito, como sería controlar un dispositivo, colocar un pedido o buscar información. Al proceso de descubrir, analizar, documentar y verificar estos servicios y restricciones se le llama [**ingeniería de requerimientos (IR)**](https://es.wikipedia.org/wiki/Ingenier%C3%ADa_de_requisitos).

Se pueden establecer numerosas categorizaciones de requisitos, así es frecuente oir sobre requisitos de implementación, requisitos de rendimiento, requisitos de usabilidad... Si bien la categorización más frecuente es la que divide los requisitos según su característica de **funcional o de no funcional**, así:

##  1. <a name='Requisitofuncional'></a>Requisito funcional

Los [**Requisitos funcionales**](https://es.wikipedia.org/wiki/Requisito_funcional) son enunciados acerca de servicios que el sistema 
debe proveer, de cómo debería reaccionar el sistema a entradas particulares y de cómo debería comportarse el sistema en situaciones específicas. En algunos casos, los requerimientos funcionales también explican lo que no debe hacer el sistema.

##  2. <a name='Requisitonofuncional'></a>Requisito no funcional

Un [**requisito no funcional**](https://es.wikipedia.org/wiki/Requisito_no_funcional) o **atributo de calidad** especifica criterios que pueden usarse para juzgar la operación de un sistema en lugar de sus comportamientos específicos, ya que estos corresponden a los requisitos funcionales. Por tanto, se refieren a todos los requisitos que no describen información a guardar, ni funciones a realizar, sino características de funcionamiento. Por esto, suelen denominarse atributos de calidad de un sistema.  Son las restricciones o condiciones que impone el cliente al programa que necesita, por ejemplo, el tiempo de entrega del programa, el lenguaje o la cantidad de usuarios, medidas de rendimiento de un programa, etc.

Se suelen clasificar en:

* Requisitos de calidad de ejecución, que incluyen seguridad, usabilidad y otros medibles en tiempo de ejecución.
* Requisitos de calidad de evolución, como testeabilidad, extensibilidad o escalabilidad, que se evalúan en los elementos estáticos del sistema software.2

**Ejemplos**: Rendimiento, disponibilidad, durabilidad, estabilidad, accesibilidad, documentación, mantenibilidad, etc.

---

En realidad, **la distinción entre los diferentes tipos de requerimientos no es tan clara como sugieren estas definiciones sencillas**. Un requerimiento de un usuario interesado por la seguridad, como el enunciado que limita el acceso a usuarios autorizados, parecería un requerimiento no funcional. Sin embargo, cuando se desarrolla con más detalle, este requerimiento puede generar otros requerimientos que son evidentemente funcionales, como la necesidad de incluir facilidades de autenticación en el sistema.

Esto muestra que **los requerimientos no son independientes** y que un requerimiento genera o restringe normalmente otros requerimientos. Por lo tanto, los requerimientos del sistema no sólo detallan los servicios o las características que se requieren del mismo, sino también especifican la funcionalidad necesaria para asegurar que estos servicios y características se entreguen de manera adecuada.







