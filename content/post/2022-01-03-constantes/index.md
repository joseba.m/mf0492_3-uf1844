---
title: Constantes en PHP
subtitle: 
date: 2022-01-03
draft: false
description: 
tags: ["PHP", "PHP - Operadores"]
---

<!-- vscode-markdown-toc -->
* 1. [Constantes](#Constantes)
	* 1.1. [Constantes predefinidas](#Constantespredefinidas)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Constantes'></a>Constantes

 Una constante es un identificador (nombre) para un valor simple. Como el nombre sugiere, **este valor no puede variar durante la ejecución del script**, por convención los identificadores se declaran en mayúsculas para que sean fácil de reconocer en el código. El nombre de una constante sigue las mismas reglas que las variables.


```php
// Nombres de constantes correctos
define("FOO",     "something");
define("FOO2",    "something else");
define("FOO_BAR", "something more");

# Para leer el contenido de una constante en una variable
$var1 = FOO;
echo FOO;
# FOO = "other" # Prohibido "PHP Parse error:  syntax error, unexpected token "=" in"....

// Nombres de constantes incorrectos
define("2FOO",    "something");

// Esto es válido, pero debe evitarse:
// PHP podría cualquier día proporcionar una constante mágica 
// que rompiera el script
define("__FOO__", "something"); 
```

###  1.1. <a name='Constantespredefinidas'></a>Constantes predefinidas

Hay nueve constantes mágicas que cambian dependiendo de dónde se emplean. Por ejemplo, el valor de __LINE__ depende de la línea en que se use en el script.

![](img/01.png)


```php
<?php  
    # línea nº 2
    echo __LINE__."<br>"; # 3
    echo "This is line " . __LINE__ . " of file " . __FILE__;
?>
```