---
title: Modelos del ciclo de vida del software
subtitle: El proceso de desarollo de software
date: 2021-12-04
draft: false
description: El proceso de desarollo de software
tags: ["UF1844","Modelos del ciclo de vida"]
---

Durante décadas se ha perseguido la meta de encontrar **procesos reproducibles y predecibles que mejoren la productividad y la calidad**. Algunas de estas soluciones **intentan sistematizar o formalizar** la aparentemente desorganizada tarea de desarrollar software. Otros aplican **técnicas de gestión de proyectos** para la creación del software. **Sin una gestión del proyecto, los proyectos de software corren el riesgo de demorarse o consumir un presupuesto mayor que el planeado**. Dada la cantidad de proyectos de software que no cumplen sus metas en términos de funcionalidad, costes o tiempo de entrega, una gestión de proyectos efectiva es algo imprescindible.

El **ciclo de vida de un proyecto software comprende las etapas por las que pasa un proyecto**, desde la concepción de la idea que lanza la necesidad de diseñar el sistema, pasando por el análisis, implementación y mantenimiento del software, y que termina cuando el sistema muere al ser sustituido por otro sistema.

El ciclo de vida del software es un intervalo de tiempo que se inicia al concebir el sistema y acaba al retirar de operación (y/o del mercado) el sistema.

**No hay un ciclo de vida único para el software**, la elección de uno u otro dependerá de las necesidades del software, de la estructura / organización de la empresa que lo crea, o del propio criterio del equipo, cada modelo sigue un enfoque diferente para las diferentes actividades que tienen lugar durante el proceso.

A grandes rasgos, y de una forma común, podemos afirmar que:

* A partir de una **Necesidad** se crean unas **Especificaciones** (definir la necesidad de forma explicita).
* A partir de unas Especificaciones se crea un **Producto Software** (resolver la necesidad).
* A partir del Producto se crea una nueva necesidad (ampliación).

Todo proyecto de ingeniería tiene un propósito relacionado con la producción de un producto, proceso o servicio que debe ser generado a través de diversas actividades, algunas de estas actividades pueden agruparse en **fases** para obtener un **producto intermedio**, el cual es necesario para alcanzar el producto final y facilitar la gestión de proyectos. La totalidad de las fases utilizadas se denomina ciclo de vida.

En cambio, para agrupar las actividades, objetivos de las fases, los tipos de productos intermedios generados, etc, pueden ser distintos dependiendo del producto o proceso a generar y las tecnologías usadas.

La **complejidad de las relaciones entre actividades crece exponencialmente con el tamaño**, que se convertirá rápidamente intratable si no fuera por la técnica divide y vencerás. Así, la división de los proyectos en fases es un primer paso  para reducir la complejidas, en el caso de la elección de las partes de modo que las relaciones entre ellos sean tan simples como sea posible.

La definición de un ciclo de vida facilita el control sobre cuándo es necesario aplicar los recursos (personas, equipos, suministros, etc.) para el proyecto. El proyecto a menudo puede incluir la subcontratación de algunas partes del sistema. El control de calidad también será facilitado si la separación de fases se asigna a los puntos de control que se deben verificar (a través de los controles de los productos parciales obtenidos).

De la misma manera, la práctica adquirida en el diseño de ciclo de vida para una variedad de situaciones nos permite beneficiarnos de la experiencia obtenida con el método que mejor se adapte a nuestras necesidades.

**Un ciclo de vida de un proyecto está compuesto por fases correlativas compuestas por tareas planificables**. De acuerdo con el modelo de ciclo de vida, la secuencia de fases se puede ampliar con bucles de realimentación sobre el mismo, lo que se considera conceptualmente la misma fase se puede ejecutar más de una vez; y el proyecto recibirá en cada pasada las contribuciones de cada ejecución de los resultados intermedios que se producen (esto es lo que se conoce como retroalimentación).

## Elementos de un ciclo de vida

Los distintos elementos que integran un ciclo de vida son:

* **Fases**: Una fase es un conjunto de actividades con un objetivo en el desarrollo del proyecto. Se construye al agrupar tareas (actividades elementales) que pueden compartir un determinado tramo de la vida de un proyecto. Las tareas pueden agruparse de forma temporal, lo que implica que se creen requisitos temporales para poder asignar los recursos (humanos, económicos y materiales).

Cuanto más complejo y de mayor volumen sea un proyecto, será necesario un mayor detalle en la creación de las fases para conseguir que su contenido sea manejable. Así cada fase se considera un pequeño proyecto en sí mismo, compuesto por pequeñas fases.

* **Entregables**: Se trata de los productos intermedios a generar en las fases. Serán materiales (equipos, componentes) o inmateriales (software, documentos, soportes digitales). Los entregables servirán para evaluar el proceso del proyecto al comprobar su bondad con los requisitos funcionales y condiciones de realización. Estas evaluaciones pueden servir para la toma de decisiones en el desarrollo del proyecto.

Los ciclos de vida del proyecto definen:

* El trabajo técnico que se debe realizar en cada fase.
* Quién involucra cada fase.
* Cuándo generar los productos entregables en cada fase y cómo revisarlos, verificarlos y validar cada entregable.
* Cómo aprobar y comprobar cada fase.

Se puede especificar cada fase del ciclo de vida de forma muy general, o bien hacerlo muy detalladamente. Al hacerlo detallado se incluirán diagramas, formularios, listas de control para dar estructura y control.

## Características comunes

Los ciclos de vida de proyectos tienen ciertas características comunes, como son:

* De forma general las **fases son secuenciales** y, suelen estar definidas por alguna **transferencia** de información técnica o componentes técnicos.
* El **nivel de incertidumbre es muy alto** y así se amplía el riesgo de no cumplir los objetivos en relación con el inicio del proyecto. 
* El coste y nivel de personal es bajo al principio, alcanzando su nivel máximo en fases intermedias para luego caer rápidamente al aproximarse a su conclusión. Una de las principales razones de este fenómeno es que aumenta el coste de los cambios y de la corrección de errores al avanzar el proyecto.
* Los interesados en el proyecto pueden influir en las características finales del producto que se espera del proyecto.

**Hay pocos proyectos con ciclos de vida idénticos**, incluso teniendo nombres de fases y productos de entrega similares.

Algunos ciclos de vida tienen cuatro o cinco fases, pero también los hay con diez o más. El ciclo de vida del desarrollo de software de una empresa puede tener una única fase de diseño, y otro puede tener fases separadas para los diseños aquitectónico y detallado. Los sub-proyectos podrán también tener ciclos de vida de proyectos diferentes.
















