---
title: Desarrollo rápido (RAD)
subtitle: Modelos del ciclo de vida del software
date: 2021-12-09
draft: false
description: Modelos del ciclo de vida del software
tags: ["UF1844","Modelos del ciclo de vida","RAD","Desarrollo rápido"]
---

El [Desarrollo Rápido de Aplicaciones](https://es.wikipedia.org/wiki/Desarrollo_r%C3%A1pido_de_aplicaciones) (DRA) (en inglés RAD - Rapid Application Development) es un modelo de desarrollo del software que enfatiza un **ciclo de desarrollo extremandamente corto**. DRA es una adaptación a gran velocidad en el que se logra el desarrollo rápido usando un enfoque de construcción basado en componentes. Si se comprenden los requisitos y se limita el ámbito del proyecto, el proceso DRA permite crear un sistema totalmente funcional en periodos cortos de tiempo.

## Fases

Cuando se usa principalmente para aplicaciones de sistemas de información, el enfoque DRA tendrá las siguientes fases:

* **Modelado de datos**: el flujo de información que definió como parte de la fase de modelado de gestión se refina como un conjunto de objetos de datos requeridos para apoyar la empresa. Se definen los atributos de cada objeto y las relaciones entre ellos.
* **Modelado de gestión**: el flujo de información entre las funciones de gestión hace que responda a las siguientes preguntas: ¿Qué información se genera? ¿Qué información conduce al proceso de gestión? ¿Quién la genera? ¿Quién la procesó? ¿A dónde va la información?.
* **Modelado de proceso**: los objetos de datos definidos en la fase de modelado de datos se transforman para lograr el flujo de información necesario para usar una función de gestión. Las descripciones del proceso se crean para modificar, suprimir, añadir, o recuperar un objeto de datos. Es la comunicación entre objetos.
* **Generación de aplicaciones**: El DRA asume el uso de técnicas de 4º generación. En lugar de crear software con lenguajes de programación de la tercera generación, el proceso DRA se ocupa de rehusar componentes de programas ya existentes, cuando esto sea posible, o crear componentes reutilizables, cuando se requiera. En todos los casos se usarán herramientas automáticas que facilitarán la construcción de software.
* **Pruebas de entrega**: Como el proceso DRA enfatiza la reutilización, ya se comprobaron muchos de los componentes de los programas. Esto reduce el tiempo destinado a pruebas. Pero se deben probar todos los componentes nuevos y deben ejercitarse todos los interfaces a fondo.   

**RECORDATORIO**: Este modelo, como su nombre indica, es útil para lograr un desarrollo rápido de la aplicación web.

## Principios básicos

[[Fuente](https://es.wikipedia.org/wiki/Desarrollo_r%C3%A1pido_de_aplicaciones)]

* Objetivo clave es para un **rápido desarrollo y entrega de una alta calidad** en un sistema de relativamente bajo coste de inversión.
* Intenta **reducir los riesgos inherentes del proyecto partiéndolo en segmentos más pequeños y proporcionar más facilidad de cambio** durante el proceso de desarrollo.
* Orientación dedicada a producir sistemas de alta calidad con rapidez, principalmente mediante el uso de **iteración por prototipos** (en cualquier etapa de desarrollo), promueve la participación de los usuarios y el uso de herramientas de desarrollo computarizadas. Estas herramientas pueden incluir constructores de Interfaz gráfica de usuario (GUI), Computer Aided Software Engineering (CASE) las herramientas, los sistemas de gestión de bases de datos (DBMS), lenguajes de programación de cuarta generación 4GL (Visual Studio, Delphi), generadores de código, y técnicas orientada a objetos.
* Hace especial hincapié en el cumplimiento de la necesidad comercial, mientras que la ingeniería tecnológica o la excelencia es de menor importancia.
* Control de proyecto implica el desarrollo de prioridades y la definición de los plazos de entrega. Si el proyecto empieza a aplazarse, se hace hincapié en la reducción de requisitos para el ajuste, no en el aumento de la fecha límite.
* La participación activa de los usuarios es imprescindible.
* Iterativamente realiza la producción de software, en lugar de enfocarse en un prototipo.
* Produce la documentación necesaria para facilitar el futuro desarrollo y mantenimiento.


## Enlaces externos

* [https://es.wikipedia.org/wiki/Desarrollo_r%C3%A1pido_de_aplicaciones](https://es.wikipedia.org/wiki/Desarrollo_r%C3%A1pido_de_aplicaciones).
* [https://velneo.es/desarrollo-low-code-lenguajes-de-programacion-de-cuarta-generacion-y-herramientas-de-desarrollo-de-aplicaciones-rapidas/](https://velneo.es/desarrollo-low-code-lenguajes-de-programacion-de-cuarta-generacion-y-herramientas-de-desarrollo-de-aplicaciones-rapidas/).
* [https://www.incentro.com/es-es/blog/stories/metodologia-rad-desarrollo-rapido-aplicaciones/](https://www.incentro.com/es-es/blog/stories/metodologia-rad-desarrollo-rapido-aplicaciones/).
* [http://metodologiarad.weebly.com/](http://metodologiarad.weebly.com/).
