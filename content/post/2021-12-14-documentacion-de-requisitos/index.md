---
title: Documentación de requisitos
subtitle: Análisis y especificación de requisitos
date: 2021-12-14
draft: false
description: Análisis y especificación de requisitos
tags: ["UF1844","Requisitos","Análisis","Requisitos - Documentación"]
---

El documento de [requisitos de software](https://es.wikipedia.org/wiki/Especificaci%C3%B3n_de_requisitos_de_software) (llamado algunas veces especificación de requerimientos de software,ERS o SRS del inglés) es una descripción completa del sistema que se va a desarrollar. Incluye tanto los **requerimientos del usuario** para un sistema, como una especificación detallada de los **requerimientos del sistema**. En ocasiones, los requerimientos del usuario y del sistema se integran en una sola descripción. En otros casos, los requerimientos del usuario se definen en una introducción a la especificación de requerimientos del sistema. Si hay un gran número de requerimientos, los requerimientos del sistema detallados podrían presentarse en un documento aparte.

Incluye un conjunto de [**casos de uso**](https://es.wikipedia.org/wiki/Casos_de_uso) que describe todas las interacciones que tendrán los usuarios con el software. Los casos de uso también son conocidos como requisitos funcionales. 

Además de los casos de uso, la ERS también contiene requisitos no funcionales (complementarios). Los requisitos no funcionales son requisitos que imponen restricciones en el diseño o la implementación, como, por ejemplo, restricciones en el diseño o estándares de calidad.

Está **dirigida tanto al cliente como al equipo de desarrollo**. El lenguaje utilizado para su redacción debe ser informal, de forma que sea fácilmente comprensible para todas las partes involucradas en el desarrollo.

Posible organización para un documento de requerimientos basada en un estándar del IEEE para documentos de requerimientos (IEEE, 1998). Este estándar es genérico y se adapta a usos específicos. 

| Capítulo | Descripción |
|------------------|---------------------------------------|
| Prefacio | Debe definir el número esperado de lectores del documento, así como describir su historia de versiones, incluidas las causas para la creación de una nueva versión y un resumen de los cambios realizados en cada versión |
| Introducción | Describe la necesidad para el sistema. Debe detallar brevemente las funciones del sistema y explicar cómo funcionará con otros sistemas. También tiene que indicar cómo se ajusta el sistema en los objetivos empresariales o estratégicos globales de la organización que comisiona el software |
| Glosario | Define los términos técnicos usados en el documento. |
| Definición de requerimientos del usuario | Aquí se representan los servicios que ofrecen al usuario. También, en esta sección se describen los requerimientos no funcionales del sistema. Esta descripción puede usar lenguaje natural, diagramas u otras observaciones que sean comprensibles para los clientes. Deben especificarse los estándares de producto y proceso que tienen que seguirse. |
| Arquitectura del sistema | Este capítulo presenta un panorama de alto nivel de la arquitectura anticipada del sistema, que muestra la distribución de funciones a través de los módulos del sistema. Hay que destacar los componentes arquitectónicos que sean de reutilización. |
| Especificación de requerimientos del sistema | Debe representar los requerimientos funcionales y no funcionales con más detalle. Si es preciso, también pueden detallarse más los requerimientos no funcionales. Pueden definirse las interfaces a otros sistemas. |
| Modelos del sistema | Pueden incluir modelos gráficos del sistema que muestren las relaciones entre componentes del sistema, el sistema y su entorno. Ejemplos de posibles modelos son los modelos de objeto, modelos de flujo de datos o modelos de datos semánticos. |
| Evolución del sistema | Describe los supuestos fundamentales sobre los que se basa el sistema, y cualquier cambio anticipado debido a evolución de hardware, cambio en las necesidades del usuario, etc. Esta sección es útil para los diseñadores del sistema, pues los ayuda a evitar decisiones de diseño que restringirían probablemente futuros cambios al sistema. |
| Apéndices | Brindan información específica y detallada que se relaciona con la aplicación a desarrollar; por ejemplo, descripciones de hardware y bases de datos. Los requerimientos de hardware definen las configuraciones, mínima y óptima, del sistema. Los requerimientos de base de datos delimitan la organización lógica de los datos usados por el sistema y las relaciones entre datos. |
| Índice | Pueden incluirse en el documento varios índices. Así como un índice alfabéticoo normal, uno de diagramas, un índice de funciones, etcétera |

## Enlaces externos

* [http://www.jroliva.com/fernando/An%C3%A1lisis/Teoria/Tema3.pdf](http://www.jroliva.com/fernando/An%C3%A1lisis/Teoria/Tema3.pdf).
* [http://flanagan.ugr.es/docencia/2005-2006/2/apuntes/tema2.pdf](http://flanagan.ugr.es/docencia/2005-2006/2/apuntes/tema2.pdf).
* [https://blogs.ead.unlp.edu.ar/ingenieriasoft1/files/2014/10/ING-I-2014-Clase-6-Requerimientos-V-DFD-DFC.pdf](https://blogs.ead.unlp.edu.ar/ingenieriasoft1/files/2014/10/ING-I-2014-Clase-6-Requerimientos-V-DFD-DFC.pdf).







