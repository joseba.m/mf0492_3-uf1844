<?php //printf.php

// ---------------------------------------------------------------

$precio = 5; $impuesto = 0.16;
printf('La cena cuesta $%.2f', $precio * (1 + $impuesto));

// ---------------------------------------------------------------

$codPostal = '6520';
$mes = 2;
$dia = 6;
$año = 2007;
// Código postal es 06520 y fecha es 02/06/2007
printf("Código postal es %05d y fecha es %02d/%02d/%d", $codPostal, $mes, $dia, $año);

// ---------------------------------------------------------------

$min = -40;
$max = 40;
// La computadora puede operar entre -40 ay +40 grados.
printf("La computadora puede operar entre %+d ay %+d grados.", $min, $max);

?>