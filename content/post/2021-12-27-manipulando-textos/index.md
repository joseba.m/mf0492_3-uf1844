---
title: Manipulando textos
subtitle: Introducción básica
date: 2021-12-27
draft: false
description: Introducción básica
tags: ["PHP","PHP - Cadenas", "PHP - Variables","PHP - Texto","PHP - Strings"]
---

<!-- vscode-markdown-toc -->
* 1. [Validando cadenas](#Validandocadenas)
* 2. [Formateando texto](#Formateandotexto)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

PHP ofrece numerosas funciones propias para trabajar con cadenas. En este artículo nos centraremos en las funciones más útiles para dos tipos de tareas más comunes: validar y formatear.

La entrada ["Cadenas de caracteres (Strings)"](https://www.php.net/manual/es/language.types.string.php) de la documentación oficial (php.net) contiene información adicional ampliada.

##  1. <a name='Validandocadenas'></a>Validando cadenas

La función [`trim`](https://www.php.net/manual/es/function.trim) elimina los espacios espacios iniciales y finales de una cadena. Combinado con [`strlen`](https://www.php.net/manual/es/function.strlen), que obtiene la longitud de una cadena, podemos usar la combinación de ambas funciones para validar un formulario donde un usuario introduce un código postal por ejemplo.

[src/cod-postal.php](src/cod-postal.php).

```php
<?php //cod-postal.php
// Aún no hemos abordado los formularios Web, más adelante cuanto aprendamos a crearlos recibiremos el valor usando $_POST['codPostal'] o similar.
$codPostal=trim(" 48001  ");

print "codPostal es:'$codPostal'.";

$codPostalLong = strlen($codPostal);

print "codPostalLong es:'$codPostalLong'.";

if ($codPostalLong != 5) {
    print "Introduce un código postal válido";
} else {
    print "Correcto!";
}
?>
```

A veces usuarios maliciosos introducen caracteres extra con intenciones dañinas.

Una expresión más compacta del ejemplo anterior:

```php
if(strlen(trim(" 211233   ")) != 5 ) {
    print "Introduce un código postal válido";
}
```

A pesar de que veremos más adelante las sentencias condicionales `if` y los operadores, para comparar dos cadenas usamos el operador `==`.

```php
$email = 'iker.l@fptxurdinaga.com';
// PHP es case sensitive, no son iguales
if ($email == 'Iker.l@FPtxurdinaga.com') {
    print "Son iguales";
}
```

Para comparar dos cadenas sin importar que sean mayúsculas o minúsculas usaremos [`strcasecmp`](https://www.php.net/manual/es/function.strcasecmp).

```php
strcasecmp(string $str1, string $str2): int
```

Devuelve < 0 si str1 es menor que str2; > 0 si str1 es mayor que str2 y 0 si son iguales.

```php
<?php
$var1 = "Hello";
$var2 = "hello";
if (strcasecmp($var1, $var2) == 0) {
    echo '$var1 is equal to $var2 in a case-insensitive string comparison';
}
?>
```

##  2. <a name='Formateandotexto'></a>Formateando texto

La función [`printf`](https://www.php.net/manual/es/function.printf) ofrece más control (respecto a [`print`](https://www.php.net/manual/es/function.print)) respecto al formato de salida. A [`printf`](https://www.php.net/manual/es/function.printf) le pasas como parámetros una cadena con el formato esperado en la salida y a continuación los elementos que queremos imprimir. Cada "regla" en la cadena con el formato se reemplaza por un elemento (normalmente una variable).

[src/printf.php](src/printf.php)

```php
<?php
$precio = 5; $impuesto = 0.16;
printf('La cena cuesta $%.2f', $precio * (1 + $impuesto));
?>
```

Imprime lo siguiente:

`La cena cuesta $5.80`

La regla de formato `%.2f` es reemplaza por el valor de `$precio * (1 + $impuesto)` y es formateada para mostrar sólo dos dígitos decimales.

Las cadenas con reglas de [formato](https://www.php.net/manual/es/function.sprintf.php) comienzan con `%` y a continuación le siguen algunos modificadores que afectan a lo que hace la regla:

1. Un **especificador de signo** opcional que fuerza a usar un signo (- o +) en un número. Por defecto, sólo el signo - se utiliza en un número si es negativo. Esta especificación fuerza números positivos para que también tengan adjunto el signo +
2. Un **especificador** de relleno opcional que indica qué carácter se utiliza para rellenar el resultado hasta el tamaño justo del string. Este puede ser un carácter de espacio o un 0 (el carácter cero). El valor por defecto es rellenar con espacios. Un carácter de relleno alternativo se puede especificar prefijándolo con una comilla simple (').
3. Un **especificador de alineación** opcional que indica si el resultado debe ser alineado a la izquierda o a la derecha. El valor por defecto es justificado a la derecha, un carácter - lo justificará a la izquierda.
4. Un número opcional, un **especificador** de ancho que indica de cuántos caracteres (mínimo) resultará esta conversión.
5. Un **especificador de precisión** opcional en la forma de un punto (.) seguido de un string opcional de dígitos decimales que indica cuántos dígitos decimales deben mostrarse para los números de punto flotante. Cuando se utiliza este especificador con un string, actúa como un punto de corte, estableciendo un límite máximo de caracteres al string. Además, el carácter para empleado cuando se rellena un número podría especificarse opcionalmente entre el punto y el dígito.
6. Un **especificador de tipo** que indica con qué tipo deben ser tratados los datos del argumento. Los tipos posibles son:
    * % - un carácter de porcentaje literal. No se requiere argumento.
    * b - el argumento es tratado como un valor de tipo integer y presentado como un número binario.
    * c - el argumento es tratado como un valor de tipo integer y presentado como el carácter con ese valor ASCII.
    * d - el argumento es tratado como un valor de tipo integer y presentado como un número decimal (con signo).
    * e - el argumento es tratado con notación científica (e.g. 1.2e+2). El especificador de precisión indica el número de dígitos después del punto decimal a partir de PHP 5.2.1. En versiones anteriores, se tomó como el número de dígitos significativos (menos uno).
    * E - como %e pero utiliza la letra mayúscula (e.g. 1.2E+2).
    * f - el argumento es tratado como un valor de tipo float y presentado como un número de punto flotante (considerando la configuración regional).
    * F - el argumento es tratado como un valor de tipo float y presentado como un número de punto flotante (no considerando la configuración regional).
    * g - lo mismo que %e y %f.
    * G - lo mismo que %E y %f.
    * o - el argumento es tratado como un valor de tipo integer y presentado como un número octal.
    * s - el argumento es tratado y presentado como un string.
    * u - el argumento es tratado como un valor de tipo integer y presentado como un número decimal sin signo.
    * x - el argumento es tratado como un valor de tipo integer y presentado como un número hexadecimal (con las letras en minúsculas).
    * X - el argumento es tratado como un valor de tipo integer y presentado como un número hexadecimal (con las letras en mayúsculas).

Las variables serán forzadas por el especificador a un tipo adecuado:

![](img/01.png)

Ejemplo, relleno con 0 hasta longitud 5 para el código postal y 2 para la fecha. El código postal es tratado como un entero a pesar de declararse como una cadena.

[src/printf.php](src/printf.php)

```php
$codPostal = '6520';
$mes = 2;
$dia = 6;
$año = 2007;
// Código postal es 06520 y fecha es 02/06/2007
printf("Código postal es %05d y fecha es %02d/%02d/%d", $codPostal, $mes, $dia, $año);
```

Imprime: `Código postal es 06520 y fecha es 02/06/2007`

El especificador de signo es de ayuda para mostrar números negativos o positivos de forma explicita. 

```php
$min = -40;
$max = 40;
// La computadora puede operar entre -40 ay +40 grados.
printf("La computadora puede operar entre %+d ay %+d grados.", $min, $max);
```

Salida: `La computadora puede operar entre -40 ay +40 grados.`

Otra forma de manipular los textos es cambiar las mayúsculas o minúsculas. Las funciones [`strtolower`](https://www.php.net/manual/es/function.strtolower) convierte un string a minúsculas, [`strtoupper`](https://www.php.net/manual/es/function.strtoupper) a su vez convierte un string a mayúsculas.

[src/case-change.php](src/case-change.php)

```php
// ternera, pollo, cerdo, pato
print strtolower('Ternera, POLLO, Cerdo, paTO');

// TERNERA, POLLO, CERDO, PATO
print strtoupper('Ternera, POLLO, Cerdo, paTO');
```

La función [`ucwords`](https://www.php.net/manual/es/function.ucwords) convierte a mayúsculas el primer carácter de cada palabra de una cadena. Es de especial utilidad combinado con [`strtolower`](https://www.php.net/manual/es/function.strtolower) para producir nombres propios por ejemplo cuando son suministrados todo en mayúsculas.

[src/case-change.php](src/case-change.php)

```php
// Iker Landajuela
print ucwords(strtolower('IKER LANDAJUELA'));
```

Con la función [`substr`](https://www.php.net/manual/es/function.substr) se puede extraer una parte de una cadena.

```php
// Si st
print substr("Si start no es negativo, la cadena devuelta comenzará en el start de la posición del string empezando desde cero", 0, 5);
```

Parámetros:

* string: La cadena de entrada. Debe ser de almenos de un caracter.
* start: 
    * Si start no es negativo, la cadena devuelta comenzará en el start de la posición del string empezando desde cero. Por ejemplo, en la cadena 'abcdef', el carácter en la posición 0 es 'a', el carácter en la posición 2 es 'c', y así sucesivamente.
    * Si start es negativo, la cadena devuelta empezará en start contando desde el final de string.
    * Si la longitud del string es menor que start, la función devolverá false.

```php
<?php
$rest = substr("abcdef", -1);    // devuelve "f"
$rest = substr("abcdef", -2);    // devuelve "ef"
$rest = substr("abcdef", -3, 1); // devuelve "d"
?> 
```

En lugar de extraer una cadena, la función [`str_replace`](https://www.php.net/manual/es/function.str-replace) reemplaza todas las apariciones del string buscado con el string de reemplazo. Puede ser útil por ejemplo para personalizar una [plantilla HTML](https://docstore.mik.ua/orelly/webprog/php/ch13_02.htm).

```php
$html = '<span class="{class}">Pollo frito<span>
<span class="{class}">Lubina al horno</span>';

print str_replace('{class}',$my_class,$html);
```





