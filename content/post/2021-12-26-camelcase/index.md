<!-- vscode-markdown-toc -->
* 1. [Notación húngara](#Notacinhngara)
* 2. [camelCase](#camelCase)
* 3. [Pascal](#Pascal)
* 4. [Snake](#Snake)
* 5. [Kebab](#Kebab)
* 6. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->---
title: Escritura Camel, Pascal, Snake, and Kebab Case
subtitle: Introducción básica
date: 2021-12-26
draft: false
description: Introducción básica
tags: ["PHP","PHP - Estilo", "PHP - camelCase"]
---


En programación software, una [convención de nombres](https://es.wikipedia.org/wiki/Convenci%C3%B3n_de_nombres_(programaci%C3%B3n)) es un conjunto de reglas para la elección de la secuencia de caracteres que se utilice para los identificadores que denoten variables, tipos, funciones y otras entidades en el código fuente y la documentación.

Algunas de las razones para utilizar una convención de nombres (en lugar de permitir a los programadores elegir cualquier secuencia de caracteres) son:

* Reducir el esfuerzo necesario para leer y entender el código fuente.
* Mejorar la apariencia del código fuente, por ejemplo, al no permitir nombres excesivamente largos o abreviaturas poco claros.

##  1. <a name='Notacinhngara'></a>Notación húngara

Tal vez la más conocida es la [**notación húngara**](https://es.wikipedia.org/wiki/Notaci%C3%B3n_h%C3%BAngara), que **codifica ya sea el propósito** ("Aplicaciones de Hungría") o el tipo ("Sistemas de Hungría") de una variable en su nombre.4​ Por ejemplo, el prefijo "sz" para el szName variable indica que la variable es una cadena de cero (es decir null-) terminado.

Esta convención es muy utilizada por los programadores de Microsoft y, en particular, en la programación del sistema operativo Windows.

Ejemplo declaración función en C++ para crear una ventana en M$ Win:

![](img/01.png)

Hoy en día existen más detractores que impulsores de la notación húngara. De hecho, se suele calificar de una técnica que a la larga provoca más complejidad que ayuda a la mantenibilidad. 

##  2. <a name='camelCase'></a>camelCase

##  3. <a name='Pascal'></a>Pascal

##  4. <a name='Snake'></a>Snake

##  5. <a name='Kebab'></a>Kebab

##  6. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.php.net/manual/en/userlandnaming.rules.php](https://www.php.net/manual/en/userlandnaming.rules.php).
* [https://raw.githubusercontent.com/php/php-src/master/CODING_STANDARDS.md](https://raw.githubusercontent.com/php/php-src/master/CODING_STANDARDS.md).
* [https://es.wikipedia.org/wiki/Camel_case](https://es.wikipedia.org/wiki/Camel_case).
* [https://betterprogramming.pub/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841](https://betterprogramming.pub/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841).