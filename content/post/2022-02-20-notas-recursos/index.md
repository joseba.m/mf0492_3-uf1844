---
title: Recursos
subtitle: Desarrollo de aplicaciones Web en el entorno servidor
date: 2022-01-17
draft: true
description: Desarrollo de aplicaciones Web en el entorno servidor
tags: ["UF1844","Recursos","Enlaces","Bibliografía","Herramientas"]
---

<!-- vscode-markdown-toc -->
* 1. [Diagramas](#Diagramas)
	* 1.1. [Diagrama Flujo de Datos (DFD)](#DiagramaFlujodeDatosDFD)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Diagramas'></a>Diagramas

* [https://www.diagrams.net/](https://www.diagrams.net/).

###  1.1. <a name='DiagramaFlujodeDatosDFD'></a>Diagrama Flujo de Datos (DFD)

* [https://www.udemy.com/course/programacion-desde-cero-con-diagramas-de-flujo/](https://www.udemy.com/course/programacion-desde-cero-con-diagramas-de-flujo/).
* [Free DFD](https://dfd.es.download.it/) para Windows. DFD es un programa con el que podrás crear y editar diagramas de flujo. También te permitirá expresar gráficamente tus algoritmos, así como ejecutarlos y depurar todos los errores que detecte


[https://www.klariti.com/es/software-development/plantilla-de-especificaci%C3%B3n-de-requisitos-de-software/](https://www.klariti.com/es/software-development/plantilla-de-especificaci%C3%B3n-de-requisitos-de-software/)

[https://www.areatecnologia.com/informatica/ejemplos-de-diagramas-de-flujo.html](https://www.areatecnologia.com/informatica/ejemplos-de-diagramas-de-flujo.html).


TEST