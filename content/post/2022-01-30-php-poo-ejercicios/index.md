---
title: Ejercicios clases 
subtitle: POO PHP
date: 2022-01-30
draft: false
description: POO PHP
tags: ["PHP","PHP - POO","PHP - Clases","PHP - Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [Clase simple](#Clasesimple)
* 2. [Scott](#Scott)
* 3. [Factorial](#Factorial)
* 4. [Calculadora](#Calculadora)
* 5. [Empleado](#Empleado)
* 6. [Persona](#Persona)
* 7. [Ingredientes](#Ingredientes)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Clasesimple'></a>Clase simple

Crea una clase básica que muestre la siguiente cadena en el constructor sobrecargado: 'MyClass class has initialized !'.

```php
<?php
class MyClass {
    public function __construct()  {
        echo 'MyClass class has initialized !'."\n";
    }
}
$userclass = new MyClass;
?>
```

##  2. <a name='Scott'></a>Scott

Crea una clase que muestre un mensaje de bienvenida como  "Hello All, I am Scott", donde "Scott" es un argumento de un método.

```php
<?php
class user_message {
    public $message = 'Hello All, I am ';
    
    public function introduce($name) {
        return $this->message.$name;
    }
}

$mymessage = New user_message();
echo $mymessage->introduce('Scott')."\n";
?>
```

##  3. <a name='Factorial'></a>Factorial

Crea una clase que calcule el factorial de un número.

```php
<?php
class factorial_of_a_number
{
  protected $_n;
  public function __construct($n)
   {
     if (!is_int($n))
	   {
	      throw new InvalidArgumentException('Not a number or missing argument');
       }
    $this->_n = $n;
	}
  public function result()
    {
     $factorial = 1;
     for ($i = 1; $i <= $this->_n; $i++)
	  {
	    $factorial *= $i;
      }
	   return $factorial;
	 }
 }

$newfactorial = New factorial_of_a_number(5);
echo $newfactorial->result();
?>
```

## Cuadrado mágico

```php
<?php
class CuadradoMagico {
    // atributo un cuadrado mágico
    // matriz (array de arrays) de dos dimensiones = 3x3
    private $cuadrado_magico;

    // metodo sobrecargado constructor que acepte como parámetro un cuadrado mágico
    public function __construct($cuadrado) {
        $this->cuadrado_magico=$cuadrado;
        //print_r($this->cuadrado_magico);
    }

    // metodo que retorne una cadena con la representación del cuadrado magico en una tabla HTML
    public function crea_cuadrado() {
        $table="<table>";
        for($contFilas=0;$contFilas<3;$contFilas++) {
            $table.="<tr>";
            for($contCols=0;$contCols<3;$contCols++) {             
                $table.="<td>".$this->cuadrado_magico[$contFilas][$contCols]."</td>";
            }            
            $table.="</tr>";
        }
        $table.="</table>";
        return $table;        
    }

    private function suma_fila($numfila) {
        $suma=0;
        for($i=0;$i<3;$i++) {
            $suma+=$this->cuadrado_magico[$numfila][$i];    
        }
        return $suma;
    }

    private function suma_col($numcol) {
        $suma=0;
        for($i=0;$i<3;$i++) {
            $suma+=$this->cuadrado_magico[$i][$numcol];    
        }
        return $suma;
    }

    private function suma_dia($num) {
        if($num==1) {
            return ($this->cuadrado_magico[0][0] + $this->cuadrado_magico[1][1] + $this->cuadrado_magico[2][2]);
        } elseif ($num==2) {
            return ($this->cuadrado_magico[0][2] + $this->cuadrado_magico[1][1] + $this->cuadrado_magico[2][0]);
        }
    }


    // metodo q retorna un booleano indicando si es un cuadrado magico (true) o no (false)
    public function esCuadradoMagico() {
        $esCuadrado=false;  
        $sumaTest = $this->suma_fila(0);
        if( ($sumaTest==$this->suma_fila(1) ) && 
            ($sumaTest==$this->suma_fila(2)) && 
            ($sumaTest==$this->suma_col(0)) &&
            ($sumaTest==$this->suma_col(1)) && 
            ($sumaTest==$this->suma_col(2)) && 
            ($sumaTest==$this->suma_dia(1)) && 
            ($sumaTest==$this->suma_dia(2)) 
        ) {
            $esCuadrado = true;
        }
        return $esCuadrado;
    }
    
    
}

$cuadrado = array( 
    array(4,9,2),
    array(3,5,7),
    array(8,1,6)
 );

$obj = new CuadradoMagico($cuadrado);
echo $obj->crea_cuadrado();
if ($obj->esCuadradoMagico()) {
    echo "es magia!<br>";
} else {
    echo "vaya mierda<br>";
}

?>
```

##  4. <a name='Calculadora'></a>Calculadora

Crea una clase que acepta dos valores numéricos en el constructor, crear métodos para sumar, restar, multiplicar o dividir.

```php
<?php
class MyCalculator {
    private $_fval, $_sval;

    public function __construct( $fval, $sval ) {
        $this->_fval = $fval;
        $this->_sval = $sval;
    }

    public function add() {
        return $this->_fval + $this->_sval;
    }

    public function subtract() {
        return $this->_fval - $this->_sval;
    }

    public function multiply() {
        return $this->_fval * $this->_sval;
    }

    public function divide() {
        return $this->_fval / $this->_sval;
    }
}

$mycalc = new MyCalculator(12, 6); 
echo $mycalc-> add()."\n"; // Displays 18 
echo $mycalc-> multiply()."\n"; // Displays 72
echo $mycalc-> subtract()."\n"; // Displays 6
echo $mycalc-> divide()."\n"; // Displays 2
?>
```

##  5. <a name='Empleado'></a>Empleado

* Confeccionar una clase Empleado, definir como atributos su nombre y sueldo.
* Definir un método inicializarlo para que lleguen como dato el nombre y sueldo.
* Plantear un segundo método que imprima el nombre y un mensaje si debe o no pagar impuestos (si el sueldo supera a 3000 paga impuestos)

```php
<?php
/**
* Empleado
* Confeccionar una clase Empleado, definir como atributos su nombre y sueldo.
* Definir un método inicializarlo para que lleguen como dato el nombre y sueldo.
* Plantear un segundo método que imprima el nombre y un mensaje si debe o no pagar impuestos (si el sueldo supera a 3000 paga impuestos)
 */

class Empleado {
    private $nombre,$sueldo;

    public function establecer_nombre($nombre) {
        $this->nombre = $nombre;
    }

    public function establecer_sueldo($sueldo) {
        $this->sueldo = $sueldo;
    }

    public function establecer_datos($nombre,$sueldo) {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }

    public function imprime_datos() {
        if($this->sueldo>3000) {
            echo "Hola ".$this->nombre." debes pagar impuestos puto jeta";
        } else {
            echo "Hola ".$this->nombre." eres mileurista, no impuestos"; 
        }
    }
}

$emp = new Empleado();
$emp->establecer_nombre('Iker');
$emp->establecer_sueldo(1400);
$emp->imprime_datos();
?>
```

##  6. <a name='Persona'></a>Persona 

Crea una clase Persona con los siguientes atributos: nombre, apellidos y edad.

Crea su constructor y get y set.

Crear las siguientes funciones:
– mayorEdad: indica si es o no mayor de edad.
– nombreCompleto: devuelve el nombre mas apellidos.

##  7. <a name='Ingredientes'></a>Ingredientes




