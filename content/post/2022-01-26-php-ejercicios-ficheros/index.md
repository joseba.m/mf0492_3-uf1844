---
title: Ejercicios ficheros
subtitle: Entrada-salida de datos
date: 2022-01-26
draft: false
description: Entrada-salida de datos
tags: ["PHP","PHP - Ficheros","PHP - Datos","PHP - Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [Contando direcciones](#Contandodirecciones)
* 2. [Tabla HTML de CSV](#TablaHTMLdeCSV)
* 3. [Función leerContenidoFichero](#FuncinleerContenidoFichero)
* 4. [Formulario lectura ficheros](#Formulariolecturaficheros)
* 5. [Pares e impares](#Pareseimpares)
* 6. [escribirNumerosMod](#escribirNumerosMod)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Contandodirecciones'></a>Contando direcciones

Crear de forma manual que contenga algunas direcciones de correo electrónico, una por línea. Asegurate de que algunas dirección aparezcan más de una vez. Dale al fichero el nombre "direcciones.txt". Escribe un programa en PHP que lee cada línea del fichero y cuenta cuantas veces aparece cada dirección. Para cada dirección distinta en "direcciones.txt" tu programa debería crear escribir una línea en otro fichero "direcciones-contador.txt". Cada línea de contendrá el número de apariciones de una dirección, una coma, y la dirección de correo electrónico.

```php
<?php

$dirLst = array();

foreach (file('direcciones.txt') as $dir) {
    $dir = trim($dir);        
    $existe=false;
    for($i=0;$i<count($dirLst);$i++) {
        if($dirLst[$i][0]==$dir) {
            //echo "Direccion ".$dir."== ".$dirLst[$i][0]." ya existe!<br>";
            $existe=true; 
            $dirLst[$i][1]+=1;                       
            break; 
        } // fin if
    } // fin for

    if (!$existe) {
        //echo "$dir no encontrada en fichero. añadimos<br>";
        $dirLst[]=array($dir,1);
    }
}

$fp = fopen('direcciones-contador.txt', 'w');
for($i=0;$i<count($dirLst);$i++) {
    fwrite($fp, $dirLst[$i][1].",".$dirLst[$i][0]."\n");
}
fclose($fp);
?>
```

##  2. <a name='TablaHTMLdeCSV'></a>Tabla HTML de CSV

Muestra un fichero CSV como una tabla HML. 

Ejemplo:

```
"Fish Ball with Vegetables",4.25,0
"Spicy Salt Baked Prawns",5.50,1
"Steamed Rock Cod",11.95,0
"Sauteed String Beans",3.15,1
"Confucius ""Chicken""",4.75,0
```

Resolución:

```php
<table>
<?php
$file = fopen("platos.csv","r");
while(!feof($file)) {    
    $csvRecord = fgetcsv($file);
    $filaTabla = <<<TABLE
    <tr>
        <td>$csvRecord[0]</td>    
        <td>$csvRecord[1]</td>
        <td>$csvRecord[2]</td>
    </tr>
    TABLE;
    echo $filaTabla;
}
?>
</table>
```

##  3. <a name='FuncinleerContenidoFichero'></a>Función leerContenidoFichero

Una función (tipo procedimiento, no hay valor devuelto) denominada leerContenidoFichero que
reciba como parámetro la ruta del fichero y muestre por pantalla el contenido de cada una de las líneas
del fichero. 

```php
<?php
/**
 * Una función (tipo procedimiento, no hay valor devuelto) denominada leerContenidoFichero que reciba como parámetro la ruta del fichero y muestre por pantalla el contenido de cada una de las líneas del fichero.
 */

function leerContenidoFichero($fileInput) {
    foreach (file($fileInput) as $line) {        
        print $line."<br>";
    }
}

$fileName = 'fichero.txt';
leerContenidoFichero($fileName);
?>
```

Alternativa:

```php
function abrirfichero ($fichero){
    if (!($handleFile = fopen($fichero,"r"))) {
        die("Error abriendo fichero");
    }

    echo fread($handleFile,filesize($fichero));
    
    fclose($handleFile);
}

abrirfichero('fichero.txt');
```

##  4. <a name='Formulariolecturaficheros'></a>Formulario lectura ficheros 

Escribe un programa en PHP que muestre un formulario que permite introducir el nombre de un archivo que este alojado en el propio servidor web. Si el fichero existe en el servidor, es legible, y está alojado dentro del raíz del servidor web, entonces muestra sus contenidos. 

```php
<?php
if ( (isset($_POST['submit'])) && ($_POST['fname']!="") && (file_exists($_POST['fname'])) ) {
    //echo "Pulsado boton submit! Voy a leer el fichero ".$_POST['fname']."<br>";
    echo file_get_contents($_POST['fname']);
}

?>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
   <input type="text" name="fname" value="" required> 
   <input type="submit" name="submit" value="Leer">
</form>
```

##  5. <a name='Pareseimpares'></a>Pares e impares

Disponemos de un fichero de texto llamado numeros.txt que contiene números enteros. Los números aparecen de manera que cada línea del fichero contiene un número y sólo uno. Se desea dividir el fichero en dos de manera que se creen dos ficheros de texto: uno llamado pares.txt que contendrá los números pares del fichero numeros.txt y otro con los impares denominado impares.txt. Escribir el programa en php que resuelvan el problema. Considerar el caso en el que el fichero de entrada esté vacio.


##  6. <a name='escribirNumerosMod'></a>escribirNumerosMod

Una función (tipo procedimiento, no hay valor devuelto) denominada escribirNumerosMod que reciba dos parámetros: un array de valores enteros y una cadena de texto que puede ser "sobreescribir" ó "ampliar". La función debe proceder a: escribir cada uno de los números que forman el contenido del array en una línea de un archivo datosEjercicio.txt usando el modo de operación que se indique con el otro parámetro. Si el archivo no existe, debe crearlo

Ejemplo: El array que se pasa es $numeros = array(5, 9, 3, 22); y la invocación que se utiliza es
escribirNumerosMod($numeros, "sobreescribir"); En este caso, se debe eliminar el contenido que
existiera previamente en el archivo y escribir en él 4 líneas, cada una de las cuales contendrá los
números 5, 9, 3 y 22. 






