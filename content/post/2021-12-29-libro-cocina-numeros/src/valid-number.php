<?php
echo "is_numeric(5)=".is_numeric(5).".<br>";  // true
echo "is_numeric('5')=".is_numeric('5').".<br>";  // true
echo "is_numeric(\"05\")=".is_numeric("05").".<br>";  // true
echo "is_numeric('cinco')=".is_numeric('cinco').".<br>";  // false

echo "is_numeric(0xDECAFBAD)=".is_numeric(0xDECAFBAD).".<br>";  // true
echo "is_numeric(\"10e200\")=".is_numeric("10e200").".<br>";  // true

$number = '1.000.000';
echo "number is numeric=".is_numeric(str_replace('.','',$number))."<br>.";

?>