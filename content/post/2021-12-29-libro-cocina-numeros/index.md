---
title: Números
subtitle: Cookbook
date: 2021-12-29
draft: false
description: Introducción básica
tags: ["PHP", "PHP - Variables","PHP - Números","PHP - Introducción","PHP - Cookbook"]
---

<!-- vscode-markdown-toc -->
* 1. [Comprobando si una variable contiene un número válido](#Comprobandosiunavariablecontieneunnmerovlido)
	* 1.1. [Problema](#Problema)
	* 1.2. [Solución](#Solucin)
	* 1.3. [Discusión](#Discusin)
* 2. [Generar un número aleatorio dentro de un rango](#Generarunnmeroaleatoriodentrodeunrango)
	* 2.1. [Problema](#Problema-1)
	* 2.2. [Solución](#Solucin-1)
* 3. [Formateando números](#Formateandonmeros)
	* 3.1. [Problema](#Problema-1)
	* 3.2. [Solución](#Solucin-1)
* 4. [Imprimiendo moneda](#Imprimiendomoneda)
	* 4.1. [Problema](#Problema-1)
	* 4.2. [Solución](#Solucin-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Comprobandosiunavariablecontieneunnmerovlido'></a>Comprobando si una variable contiene un número válido

###  1.1. <a name='Problema'></a>Problema 

Quieres comprobar que una variable contenga un número, incluso representado como una cadena.

###  1.2. <a name='Solucin'></a>Solución 

[src/valid-number.php](src/valid-number.php)

```php
<?php
echo "is_numeric(5)=".is_numeric(5).".<br>";  // true
echo "is_numeric('5')=".is_numeric('5').".<br>";  // true
echo "is_numeric(\"05\")=".is_numeric("05").".<br>";  // true
echo "is_numeric('cinco')=".is_numeric('cinco').".<br>";  // false

echo "is_numeric(0xDECAFBAD)=".is_numeric(0xDECAFBAD).".<br>";  // true
echo "is_numeric(\"10e200\")=".is_numeric("10e200").".<br>";  // true
?>
```

###  1.3. <a name='Discusin'></a>Discusión

Los números se pueden representar en diferentes formas y tamaños. No puedes asumir que no es un número porqué no contenga los dígitos de 0 al 9. ¿Qué pasa por ejemplo con los números en coma flotante con decimales o los de signo negativo? Y están además las notaciones en hexadecimal o binario por ejemplo.

En lugar de crearte tu propia función, puedes usar [`is_numeric`](https://www.php.net/manual/es/function.is-numeric) para comprobar si una variable contiene algo que represente un número.

![](img/01.png)

Para separar el molesto "." de los separados de millar de tu número antes de llamar a  [`is_numeric`](https://www.php.net/manual/es/function.is-numeric):  

```php
$number = '1.000.000';
echo "number is numeric=".is_numeric(str_replace('.','',$number))."<br>.";
```

Para determinar si un número es de un tipo específico, tenemos una serie de funciones específicas, sus propios nombres aclaran su uso: [`is_float`](https://www.php.net/manual/es/function.is-float), [`is_integer`](https://www.php.net/manual/es/function.is-integer), etc.

##  2. <a name='Generarunnmeroaleatoriodentrodeunrango'></a>Generar un número aleatorio dentro de un rango

###  2.1. <a name='Problema-1'></a>Problema

Quieres generar un número aleatorio dentro de un rango.

###  2.2. <a name='Solucin-1'></a>Solución

[`rand`](https://www.php.net/manual/es/function.rand) — Genera un número entero aleatorio.

[src/random.php](src/random.php)

```php
<?php //random.php

echo rand() . "\n"; // Si se invoca sin los argumentos opcionales min y max, rand() devuelve un entero pseudoaleatorio entre 0 y getrandmax(). 
echo rand() . "\n";

echo rand(5, 15)."\n";

?>
```

##  3. <a name='Formateandonmeros'></a>Formateando números 

###  3.1. <a name='Problema-1'></a>Problema

Tienes un número y lo quieres imprimir con separadores de millares y de decimales (Ejemplo: "1.234,65").

###  3.2. <a name='Solucin-1'></a>Solución

Usa la función [`number_format`](https://www.php.net/manual/es/function.number-format.php).

```php
<?php
$number = 1234.56;
print number_format($number,2,",","."); // 1.234,56
?>
```


##  4. <a name='Imprimiendomoneda'></a>Imprimiendo moneda

###  4.1. <a name='Problema-1'></a>Problema 

Tienes un número que quieres imprimir con separadores de millar y decimales. Además quieres imprimir el símbolo de la moneda local.

**Nota**: No funciona en todas las máquinas.

###  4.2. <a name='Solucin-1'></a>Solución

```php
<?php
$number = 1234.56;
if(!setlocale(LC_MONETARY, 'es_ES')) {
	echo 'error';
}
print money_format('%.2n', $number); // $1,234.56
?>
```
















