---
title: CodeKata
subtitle: Introducción a PHP
date: 2022-01-02
draft: true
description: Desarrollo de aplicaciones Web en el entorno servidor
tags: ["UF1844","TOC","Contenidos","Resumen","Índice"]
---

<!-- vscode-markdown-toc -->
* 1. [Cuestionario](#Cuestionario)
* 2. [¡Hola mundo!](#Holamundo)
	* 2.1. [Enunciado](#Enunciado)
	* 2.2. [Solución](#Solucin)
* 3. [Encuentra los errores en el siguiente programa](#Encuentraloserroresenelsiguienteprograma)
	* 3.1. [Enunciado](#Enunciado-1)
	* 3.2. [Solución](#Solucin-1)
* 4. [Factura restaurante](#Facturarestaurante)
	* 4.1. [Enunciado](#Enunciado-1)
	* 4.2. [Solución](#Solucin-1)
* 5. [Restaurante mejorado](#Restaurantemejorado)
	* 5.1. [Enunciado](#Enunciado-1)
	* 5.2. [Solución](#Solucin-1)
* 6. [Calculadora](#Calculadora)
	* 6.1. [Enunciado](#Enunciado-1)
	* 6.2. [Solución](#Solucin-1)
* 7. [Ruta de una imagen](#Rutadeunaimagen)
	* 7.1. [Enunciado](#Enunciado-1)
	* 7.2. [Solución](#Solucin-1)
* 8. [Configuración PHP](#ConfiguracinPHP)
	* 8.1. [Enunciado](#Enunciado-1)
	* 8.2. [Solución](#Solucin-1)
* 9. [Nombre](#Nombre)
	* 9.1. [Enunciado](#Enunciado-1)
	* 9.2. [Solución](#Solucin-1)
* 10. [Longitud](#Longitud)
	* 10.1. [Enunciado](#Enunciado-1)
	* 10.2. [Solución](#Solucin-1)
* 11. [Incrementos](#Incrementos)
	* 11.1. [Enunciado](#Enunciado-1)
	* 11.2. [Solución](#Solucin-1)
* 12. [Comentarios](#Comentarios)
	* 12.1. [Enunciado](#Enunciado-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Cuestionario'></a>Cuestionario

* ¿Cuáles son las etiquetas que se usan para comenzar y finalizar un script en PHP?
* ¿Cuáles son los dos tipos de comentarios? Usa un ejemplo de cada uno.
* ¿Que carácter se debe usar al final de cada expresión?
* ¿Qué símbolo se usa para al comienzo de un nombre de una variable?
* ¿Cuál es la diferencia entre `$variable` = 1 y  `$variable == 1`?
* ¿Los nombres de las variables distinguen mayúsculas de minúsculas?
* ¿Se pueden usar espacios en los nombres de variables?
* Muestra como se convierte un tipo de variable en otra, por ejemplo de cadena a número.
* ¿Cual es la diferencia entre `++$` y `$j++`?
* ¿Son los operadores `and` e `&&` intercambiables?
* ¿Cual es el resultado de sumar una cadena y un número que representa un número? Por ejemplo `echo "34"+12;`.

##  2. <a name='Holamundo'></a>¡Hola mundo!

###  2.1. <a name='Enunciado'></a>Enunciado

Muestra la cadena de texto “¡Hola mundo!” por pantalla con un salto de línea usando HTML entre las dos palabras. Salida:

![](img/01.png)

###  2.2. <a name='Solucin'></a>Solución 

```php
<?php
echo "¡Hola<br>mundo!";
?>
```

Si estamos ejecutando PHP como modulo del servidor HTTP (Apache por ejemplo) la salida del `echo` se manda al navegador cliente que sólo “entiende” HTML, por eso usamos la etiqueta HTML `<br>`, sin embargo si estamos ejecutando el script PHP por línea de comandos usamos el `\n`.

##  3. <a name='Encuentraloserroresenelsiguienteprograma'></a>Encuentra los errores en el siguiente programa

###  3.1. <a name='Enunciado-1'></a>Enunciado

```php
<? php
print 'Dime tu nombre';
print 'Mi nombre es 'Iker.';
??>
```

###  3.2. <a name='Solucin-1'></a>Solución

**Pendiente**.

##  4. <a name='Facturarestaurante'></a>Factura restaurante

###  4.1. <a name='Enunciado-1'></a>Enunciado

Escribe un programa que calcule el precio final de una comida de restaurante. Dos platos combinados de 5,50, una cerveza por 2,30, un refresco por 1,80. El impuesto es del 10%.

###  4.2. <a name='Solucin-1'></a>Solución

**Pendiente**.


##  5. <a name='Restaurantemejorado'></a>Restaurante mejorado

###  5.1. <a name='Enunciado-1'></a>Enunciado

Modifica la solución del ejercicio previo para que imprima la factura con un formato concreto siempre. Para cada elemento de la consumición imprime la descripción, la cantidad y el total (ambas con dos decimales de precisión siempre). A continuación muestra el IVA aplicado sobre el total y al final en otra línea el precio total. Hazlo con una tabla HTML.

###  5.2. <a name='Solucin-1'></a>Solución

**Pendiente**.

##  6. <a name='Calculadora'></a>Calculadora

###  6.1. <a name='Enunciado-1'></a>Enunciado

Crea dos variables llamadas x e y con valores de 4 y 10 respectivamente y muestra el resultado de las siguientes operaciones aritméticas: suma, resta, multiplicación, división, módulo. Un resultado por línea.

###  6.2. <a name='Solucin-1'></a>Solución

```php
<?php
$x = 10;
$y = 4;

echo $x + $y."<br>";
echo $x - $y."<br>";
echo $x * $y."<br>";
echo $x / $y."<br>";
echo $x % $y."<br>";
?>
```

Usamos los operadores aritméticos más comunes para realizar operaciones matemáticas.

##  7. <a name='Rutadeunaimagen'></a>Ruta de una imagen

###  7.1. <a name='Enunciado-1'></a>Enunciado

Cógete cualquier imagen, descargala en tu servidor local y muestrala usando la etiqueta HTML y una variable PHP con la ruta de la imagen como atributo.

###  7.2. <a name='Solucin-1'></a>Solución

```php
<?php
    $ruta = "img/imagen.jpg";
?>
 
<img src=" <?php echo $ruta; ?> ">
```

##  8. <a name='ConfiguracinPHP'></a>Configuración PHP

###  8.1. <a name='Enunciado-1'></a>Enunciado

Muestra la información de configuración de PHP por pantalla

###  8.2. <a name='Solucin-1'></a>Solución

```php
<?php
phpinfo();
?>
```

##  9. <a name='Nombre'></a>Nombre

###  9.1. <a name='Enunciado-1'></a>Enunciado

Guarda tu nombre y apellidos como cadena de texto en una variable llamada "usuario" y muestra por pantalla **dentro de un párrafo HTML** (`<p>`). Ejemplo de la salida HTML:

```php
Hola “Iker Landajuela”, bienvenido.
```

###  9.2. <a name='Solucin-1'></a>Solución 

```php
<?php
$usuario = "Iker Landajuela";
echo "<p> Hola \"$usuario\", bienvenido</p>";
?>
```

##  10. <a name='Longitud'></a>Longitud

###  10.1. <a name='Enunciado-1'></a>Enunciado

Muestra por pantalla la longitud de esta cadena:

"Yo he visto cosas que vosotros no creeríais. Naves de ataque en llamas más allá del hombro de Orión. He visto rayos-C brillar en la oscuridad cerca de la Puerta de Tannhäuser. Todos esos momentos se perderán en el tiempo, como lágrimas en la lluvia. Hora de morir."

###  10.2. <a name='Solucin-1'></a>Solución

**Pendiente**.

##  11. <a name='Incrementos'></a>Incrementos

###  11.1. <a name='Enunciado-1'></a>Enunciado

Escribe un programa que use el operador de incremento (`++`) y el operador de multiplicación combinado (`*=`) para imprimir los números del 11 al 17 y las potencias de 2 de 1 hasta 3.

###  11.2. <a name='Solucin-1'></a>Solución

**Pendiente**.

##  12. <a name='Comentarios'></a>Comentarios

###  12.1. <a name='Enunciado-1'></a>Enunciado

Añade comentarios a todos los ejercicios explicando su funcionamiento. Trata de usar comentarios de una línea y multilinea.   




