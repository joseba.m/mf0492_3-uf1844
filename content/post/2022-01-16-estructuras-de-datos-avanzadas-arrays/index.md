---
title: Arrays
subtitle: Estructuras avanzadas de datos
date: 2022-01-06
draft: false
description: Estructuras avanzadas de datos
tags: ["PHP", "PHP - Arrays","Array"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción básica](#Introduccinbsica)
* 2. [Acceso básico](#Accesobsico)
* 3. [Arrays asociativos](#Arraysasociativos)
* 4. [El bucle foreach](#Elbucleforeach)
* 5. [Arrays multidimensionales](#Arraysmultidimensionales)
	* 5.1. [Creando un array multidimensional numérico](#Creandounarraymultidimensionalnumrico)
* 6. [Operadores para trabajar con arrays](#Operadoresparatrabajarconarrays)
	* 6.1. [Ejemplos](#Ejemplos)
* 7. [Funciones útiles para arrays](#Funcionestilesparaarrays)
	* 7.1. [is_array](#is_array)
	* 7.2. [count](#count)
	* 7.3. [sort](#sort)
	* 7.4. [shuffle](#shuffle)
	* 7.5. [explode](#explode)
	* 7.6. [extract](#extract)
	* 7.7. [compact](#compact)
	* 7.8. [reset](#reset)
	* 7.9. [end](#end)
	* 7.10. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->
##  1. <a name='Introduccinbsica'></a>Introducción básica

Los [`arrays`](https://www.php.net/manual/es/language.types.array.php) representan una serie de valores del mismo tipo almacenados de forma contigua, igual que las variables un [`array`](https://www.php.net/manual/es/language.types.array.php) está identificado por un nombre y usamos el operador de asignación para darle unos valores.

![](img/01.png)

Un array puede ser creado con el constructor del lenguaje [`array()`](https://www.php.net/manual/es/function.array.php). Éste toma cualquier número de parejas clave => valor como argumentos.

Ejemplo de un array con 5 `string`, los elementos se separan con `,` (Array indexado sin clave):

```php
$equipo = array('Jon', 'Mary', 'Mike', 'Iker', 'Anne');
```

Podemos hacer lo mismo así:

```php
$fruits[0] = 'Apples';
$fruits[1] = 'Bananas';
$fruits[2] = 'Cantaloupes';
$fruits[3] = 'Dates';
```

O así:

```php
$fruits[] = 'Apples';
$fruits[] = 'Bananas';
$fruits[] = 'Cantaloupes';
$fruits[] = 'Dates';
```

Si queremos obtener el nombre del 4º miembro del equipo:

```php
echo $equipo[3]; // Iker
```

La razón por la que usamos 3, y no 4, es porque el primer elemento de un array es el elemento 0, por lo tanto los miembros del equipo van del 0 al 4.

El manejo de [`arrays`](https://www.php.net/manual/es/language.types.array.php) hace a PHP un lenguaje popular, son fáciles de usar y el lenguaje provee de muchas formas para trabajar con ellos.

##  2. <a name='Accesobsico'></a>Acceso básico

Ejemplo: Añadiendo elementos a un `array`, en este ejemplo cada nuevo elemento rellena la primera localización vacía contigua al último elemento. Usamos la función [`print_r`](https://www.php.net/manual/es/function.print-r.php) para imprimir de forma fácilmente legible con sus claves y elementos:

```php
<?php

$lista_compra[] = 'Manzanas';
$lista_compra[] = 'Alubias';
$lista_compra[] = 'Aceite';
    
print_r($lista_compra);
    
/* Imprime: 
    Array
(
    [0] => Manzanas
    [1] => Alubias
    [2] => Aceite
)
*/
?>
```

Podemos explicitar la posición en la declaración así:

```php
$lista_compra[5] = 'Pan';
```

Si seguimos el ejemplo previo hemos saltado varias posiciones, PHP permite hacer esto.


Es habitual usar un bucle `for` para recorrer los elementos de un `array`:

```php
for ($j = 0 ; $j < 4 ; ++$j) {
    echo "$j: $lista_compra[$j]<br>";
}
```

Podemos usar la función [`count`](https://www.php.net/manual/es/function.count) como en el siguiente ejemplo para obtener el número de elementos:

```php
<?php

$myArray = array("uno","dos","tres");

for ($j = 0 ; $j < count($myArray) ; ++$j) {
    echo "myArray[$j]=$myArray[$j]";
}
?>
```

##  3. <a name='Arraysasociativos'></a>Arrays asociativos

Los `arrays` en PHP también pueden trabajar con claves no numéricas como índice:

```php
<?php
$array = array(
    "foo" => "bar",
    "bar" => "foo",
);

echo $array["bar"]; // foo

// a partir de PHP 5.4
$array = [
    "foo" => "bar",
    "bar" => "foo",
];
?>
```

**¿Qué pasa si dentro del array tenemos dos índices iguales?**

```php
<?php
$array = array(
    "foo" => "bar",
    "bar" => "foo",
    "foo" => "mas"
);

print_r($array);

/* Salida
Array
(
    [foo] => mas
    [bar] => foo
)
*/
?>
```

##  4. <a name='Elbucleforeach'></a>El bucle foreach

Los creadores de PHP han realizado grandes cosas para crear un lenguaje fácil de usar. Además de los bucles ya conocidos crearon uno especial para los arrays, el bucle [`foreach`](https://www.php.net/manual/es/control-structures.foreach.php). Siguiendo el ejemplo previo lo usamos así:

```php
foreach($lista_compra as $elemento) {
    echo "$elemento<br>";
}
```

[`foreach`](https://www.php.net/manual/es/control-structures.foreach.php) toma cada elemento del array en cada iteración y lo almacena en una variable (ojo, aunque sea un array asociativo sólo coge el valor).

```php
$myArray = array("uno"=>1,"dos"=>2,"tres"=>3);
foreach ($myArray as $item) {
    var_dump($item);
}
```

##  5. <a name='Arraysmultidimensionales'></a>Arrays multidimensionales

PHP permite crear arrays de más de una dimensión, de hecho pueden ser de tantas dimensiones como queramos. 

```php
<?php

$productos_oficina = array(
    'papeleria' => array(
            'papel' => 'folio blanco DINA4',
            'boligrafos' => 'BIC'),
    'electronica' => array(
            'impresora' => 'Multifuncion',
            'red' => 'switch 8 puertos'        
            )
);

foreach($productos_oficina as $secciones => $elementos) {
    foreach($elementos as $key => $value) {
        echo "$secciones:\t$key\t($value)\n";
    }
}
?>
```

###  5.1. <a name='Creandounarraymultidimensionalnumrico'></a>Creando un array multidimensional numérico



Ejemplo: Creando un `array` multimensional indexado numéricamente.

```php
<?php
$chessboard = array(
	array('r','n','b','q','k','b','n','r'),
    array('p','p','p','p','p','p','p','p'),
    array(' ',' ',' ',' ',' ',' ',' ',' '),
    array(' ',' ',' ',' ',' ',' ',' ',' '),
    array(' ',' ',' ',' ',' ',' ',' ',' '),
    array(' ',' ',' ',' ',' ',' ',' ',' '),
    array('P','P','P','P','P','P','P','P'),
    array('R','N','B','Q','K','B','N','R')
);

echo "<pre>";
foreach($chessboard as $row) {
	foreach ($row as $piece)
		echo "$piece ";
	echo "<br>";
}
echo "</pre>";

?>
```

En el ejemplo superior representamos un tablero de ajedrez, las letras minúsculas representan las piezas negras, y las mayúsculas las blancas. Las piezas se representan con letras (r = rook, n = knight, b = bishop, k = king, q = queen, and p = pawn). Volvemos a usar dos bucles `foreach` anidados para mostrar el contenido. El bucle superior procesa cada fila en una variable `$row`, que es un array en si mismo, el bucle anidado procesa cada celda de cada fila del tablero.

Podemos acceder directamente a cualquier elemento usando `[]`:

```php
echo $chessboard[7][3];
```

##  6. <a name='Operadoresparatrabajarconarrays'></a>Operadores para trabajar con arrays

En PHP existe un conjunto de operadores que facilitan el trabajo con arrays. Los operadores para
trabajar con arrays en PHP son

![](img/02.png)

###  6.1. <a name='Ejemplos'></a>Ejemplos

**Unión**: El operador + devuelve el array del lado derecho añadido al array del lado izquierdo; para las claves que existan en ambos arrays, serán utilizados los elementos del array de la izquierda y serán ignorados los elementos correspondientes del array de la derecha. 

```php
<?php
$a = array("uno" => "1", "dos" => "2");
$b = array("tres" => "3", "cuatro" => "4", "cinco" => "5");

$c = $a + $b; // Unión de $a y $b
echo "Unión de \$a y \$b: <br>";
print_r($c); // Array ( [uno] => 1 [dos] => 2 [tres] => 3 [cuatro] => 4 [cinco] => 5 ) 

?>
```

**Prueba a ver que pasa si los arrays usados para la unión de arriba coinciden en algún índice**.

##  7. <a name='Funcionestilesparaarrays'></a>Funciones útiles para arrays

###  7.1. <a name='is_array'></a>is_array


```php
<?php
$myArray = array("manzana","platano","fresa");
echo (is_array($myArray)) ? "Is an array" : "Is not an array";
?>
```

###  7.2. <a name='count'></a>count

[`count`](https://www.php.net/manual/es/function.count) Cuenta todos los elementos de un array.

```php
<?php
$a[0] = 1;
$a[1] = 3;
$a[2] = 5;
echo count($a); // 3
var_dump(count($a)); // int(3) 
?>
```

###  7.3. <a name='sort'></a>sort

[`sort`](https://www.php.net/manual/es/function.sort) — Ordena un array.


```php
<?php

$frutas = array("limón", "naranja", "banana", "albaricoque");
sort($frutas);
foreach ($frutas as $clave => $valor) {
    echo "frutas[" . $clave . "] = " . $valor . "<br>";
}

/* Salida: 
frutas[0] = albaricoque
frutas[1] = banana
frutas[2] = limón
frutas[3] = naranja
*/

?>
```

###  7.4. <a name='shuffle'></a>shuffle

[`shuffle`](https://www.php.net/manual/es/function.shuffle) — Mezcla un array.


```php
<?php
$números = range(1, 20);
shuffle($números);
foreach ($números as $número) {
    echo "$número ";
}
?>
```

Retorna `TRUE` si tiene éxito o `FALSE` en caso de error.

###  7.5. <a name='explode'></a>explode

[`explode`](https://www.php.net/manual/es/function.explode) divide un _string_ en varios y guarda los trozos en un array, se debe especificar un delimitador como el espacio o cualquier otro carácter a partir del cual hacer la división.

```php
<?php
$temp = explode(' ', "This is a sentence with seven words");
print_r($temp); // Array ( [0] => This [1] => is [2] => a [3] => sentence [4] => with [5] => seven [6] => words ) 
?>
```

###  7.6. <a name='extract'></a>extract

###  7.7. <a name='compact'></a>compact

###  7.8. <a name='reset'></a>reset

###  7.9. <a name='end'></a>end

###  7.10. <a name='Enlacesexternos'></a>Enlaces externos

* [Arrays](https://www.php.net/manual/es/language.types.array.php).
* [`foreach`](https://www.php.net/manual/es/control-structures.foreach.php).
* [Operadores para arrays](https://www.php.net/manual/es/language.operators.array.php).
* [is_array](https://www.php.net/manual/es/function.is-array).
* [count](https://www.php.net/manual/es/function.count).
* [sort](https://www.php.net/manual/es/function.sort).
* [shuffle](https://www.php.net/manual/es/function.shuffle).