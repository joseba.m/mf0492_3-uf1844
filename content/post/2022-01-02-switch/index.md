---
title: Sentencia switch
subtitle: Control de expresiones y del flujo de ejecución en PHP
date: 2022-01-02
draft: false
description: Control de expresiones y del flujo de ejecución en PHP
tags: ["PHP", "PHP - Switch","PHP - Condicionales"]
---

<!-- vscode-markdown-toc -->
* 1. [switch](#switch)
* 2. [default, acción por defecto](#defaultaccinpordefecto)
* 3. [Combinando casos](#Combinandocasos)
* 4. [Switch anidados](#Switchanidados)
* 5. [Más ejemplos](#Msejemplos)
* 6. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='switch'></a>switch

La sentencia switch es similar a una serie de sentencias IF, **de tipo condicional**. En muchas ocasiones, es posible que se quiera comparar la misma variable (o expresión) con muchos valores diferentes, y ejecutar una parte de código distinta dependiendo de a que valor es igual. Para esto es exactamente la expresión switch. 

Diagrama de flujo:

![](img/01.png)

Los dos ejemplos siguientes son dos formas diferentes de escribir lo mismo, uno con una serie de sentencias if y elseif, y el otro usando la sentencia switch: 

```php
<?php

$i=1;

if ($i == 0) {
    echo "i es igual a 0<br>";
} elseif ($i == 1) {
    echo "i es igual a 1<br>";
} elseif ($i == 2) {
    echo "i es igual a 2<br>";
}

// cuando queremos evaluar una variable que cumpla una condición entre varias queda más limpio así, sólo usamos la variable una vez VS el ejemplo de arriba con IF
// OJO: no se usan llaves de apertura y cierre de bloque con { y }, en su lugar usamos break

switch ($i) {
    case 0:
        echo "i es igual a 0<br>";
        break;
    case 1:
        echo "i es igual a 1<br>";
        break;
    case 2:
        echo "i es igual a 2<br>";
        break;
}
?>    
```

Ejemplo: Estrutura switch permite el uso de strings

```php
<?php
$i = barra;
switch ($i) {
    case "manzana":
        echo "i es una manzana";
        break;
    case "barra":
        echo "i es una barra";
        break;
    case "pastel":
        echo "i es un pastel";
        break;
}
?>
```

##  2. <a name='defaultaccinpordefecto'></a>default, acción por defecto

Cuando no se cumple ninguna condición usamos `default`.

```php
default:
    echo "opción no reconocida";
    break;
```

##  3. <a name='Combinandocasos'></a>Combinando casos

```php
<?php

$mensaje = '';
$rol = 'autor';

switch ($rol) {
	case 'admin':
		$mensaje = 'Welcome, admin!';
		break;
	case 'editor':
	case 'autor':
		$mensaje = 'Welcome! Do you want to create a new article?';
		break;
	case 'suscriptor':
		$mensaje = 'Welcome! Check out some new articles.';
		break;
	default: // resto de los casos
		$mensaje = 'You are not authorized to access this page';
}

echo $mensaje;

?>
```

##  4. <a name='Switchanidados'></a>Switch anidados

```php
<?php      
	$coche = "Honda";                   
	$modelo = "City";    
	switch( $coche ) {    
		case "Honda":    
			switch( $modelo ) {    
            
            	case "Amaze":
                	echo "Honda Amaze precio 12.000.";   
					break;    
                    
				case "City":    
					echo "Honda City precio 9.000";    
					break;     
			}    
           	break;    

			case "Renault":    
				switch( $modelo ) {    
					case "Duster":    
						echo "Renault Duster precio 7.500.";  
					break;    
					
                    case "Kwid":    
						echo "Renault Kwid precio 6.000.";  
					break;    
				}    
				break;    
  
	}  
?>    
```


##  5. <a name='Msejemplos'></a>Más ejemplos

Usando la función [`date`](https://www.php.net/manual/es/function.date) de PHP escribir por la salida el día de la semana:

```php
<?php
$today = date("D");
//echo $today; // Mon Tue....

switch($today){
    case "Mon":
        echo "Hoy es Lunes";
        break;
    case "Tue":
        echo "Hoy es Martes";
        break;
    case "Wed":
        echo "Hoy es Miércoles";
        break;
    case "Thu":
        echo "Hoy es Jueves";
        break;
    case "Fri":
        echo "Hoy es Viernes";
        break;
    case "Sat":
        echo "Hoy es Sábado";
        break;
    case "Sun":
        echo "Hoy es Domingo";
        break;
    default:
        echo "No se reconoce el día de la semana";
        break;
}

?>
```

##  6. <a name='Enlacesexternos'></a>Enlaces externos

* [switch](https://www.php.net/manual/es/control-structures.switch.php).
* [https://www.php.net/manual/es/function.date](https://www.php.net/manual/es/function.date)
* [https://www.w3schools.com/php/php_switch.asp](https://www.w3schools.com/php/php_switch.asp).


