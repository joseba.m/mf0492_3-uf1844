---
title: Ejercicios funciones
subtitle: Programación modular
date: 2022-01-18
draft: false
description: Programación modular
tags: ["PHP", "PHP - Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [Función hola nombre apellido](#Funcinholanombreapellido)
* 2. [Área rectángulo](#rearectngulo)
* 3. [Buscador](#Buscador)
* 4. [Función factorial](#Funcinfactorial)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Funcinholanombreapellido'></a>Función hola nombre apellido

**Función que pasandole dos parámetros de entrada de tipo cadena nombre y apellido escriba por la salida las dos cadenas unidas.

Ejemplo de parámetros de entrada: "iker" y "landajuela"
Salida por pantalla: "Hola Iker Landajuela"

```php
<?php
  
function hola_usuario($nombre, $apellido){
  echo "Hola $nombre $apellido";
}    

$nombre = 'Paula';
$apellido = 'Palmero';

hola_usuario($nombre,$apellido);
?>
```

Hacer lo mismo usando parámetros opcionales, cuando no se introduzca ningún parámetro imprimir "Hola nombre apellido".

```php
<?php
  
function hola_usuario($nombre = "nombre", $apellido = "apellido"){
  echo "Hola $nombre $apellido<br>";
}    

$nombre = 'Paula';
$apellido = 'Palmero';

hola_usuario($nombre,$apellido);

hola_usuario();
?>
```

##  2. <a name='rearectngulo'></a>Área rectángulo

Función que pasando dos parámetros, altura y anchura de un rectángulo, calcular y **retornar** su área.

```php
<?php
function area_rectangulo($altura,$base) {
  return $altura * $base;
}
echo area_rectangulo(2,4);
?>
```

##  3. <a name='Buscador'></a>Buscador

Función que recibe como entrada dos parámetros, un array con números y otro parámetro es un número, retornar el número de apariciones del número dentro del array.

```php
<?php
/*Función que recibe como entrada dos parámetros, un array con 2 números y otro parámetro es un número,
retornar el número de apariciones del número dentro del array.
*/
function buscador($arrayaBuscar, $numBuscar){
    for($cont=0; $cont<count($arrayaBuscar); $cont++) {
        $numAparece = 0;
            if ($arrayaBuscar[$cont] == $numBuscar ) {
                //Version resumida $numAparece+=1;
                $numAparece = $numAparece + 1;
            }
    }
    return $cont;
}
$arrayaBuscar = array(1,1);
$numBuscar = 1;
$numAparece = buscador($arrayaBuscar,$numBuscar);
echo "El $numBuscar aparece $numAparece en array <br>";
?>
```

##  4. <a name='Funcinfactorial'></a>Función factorial

Función que recibe un número como parámetro de entrada y retorna su factorial.

5! = 5*4*3*2*1 = 120

```php
<?php
function factorial($numero){
  $resultado=1;
  for ($i = $numero; $i > 0; $i--){        
    $resultado*= $i;
  }
  return $resultado;
} // Fin funcion factorial

//------------------------------------

$resultado=factorial(3);
echo "El numero es $resultado";
?>
```

# Primo fácil

Función que recibe un parámetro como integer y retorna un booleano, TRUE si es primo, FALSE si no lo es.

```php
<?php

function esPrimo($num){

    for($i = 2; $i< $num; $i++){
        if ($num%$i == 0){
            return FALSE;            
        }
    }
    return TRUE;
}

if(!esPrimo(6)) {
    echo "no es primo";
} else {
    echo "es primo";
}


?>
```













