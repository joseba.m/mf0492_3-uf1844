<?php
// Cargamos el fichero con la plantilla
$page = file_get_contents('page-template.html');
// Insertamos el título de nuestra página
$page = str_replace('{page_title}', 'Welcome', $page);
// Fondo de página azul por la tarde y verde por la mañana
if (date('H' >= 12)) {
    $page = str_replace('{color}', 'blue', $page);
} else {
    $page = str_replace('{color}', 'green', $page);
}
$page = str_replace('{name}', 'Iker', $page);
// Imprimimos los resultados 
print $page;
?>