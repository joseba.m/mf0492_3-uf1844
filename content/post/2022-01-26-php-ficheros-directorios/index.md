---
title: Trabajando con ficheros y directorios
subtitle: Entrada-salida de datos
date: 2022-01-26
draft: false
description: Entrada-salida de datos
tags: ["PHP","PHP - Ficheros","PHP - Datos"]
---

<!-- vscode-markdown-toc -->
* 1. [Leyendo y escribiendo el contenido completo de un fichero](#Leyendoyescribiendoelcontenidocompletodeunfichero)
	* 1.1. [Leyendo un fichero](#Leyendounfichero)
	* 1.2. [Escribiendo un fichero](#Escribiendounfichero)
* 2. [Leyendo y escribiendo partes de un fichero](#Leyendoyescribiendopartesdeunfichero)
* 3. [Trabajando con ficheros CSV](#TrabajandoconficherosCSV)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

El destino por excelencia para el almacenamiento de datos de una aplicació Web son las bases de datos. Eso no quiere decir que no vas a trabajar nunca con ficheros de texto plano, aún son una forma útil de intercambio de información y ampliamente usados en algunas situaciones.

Un caso de uso son ficheros HTML con plantillas para nuestra web, los ficheros también son buenos para intercambiar datos entre nuestro programa y una hoja de cálculo (normalmente en formato [CSV](https://es.wikipedia.org/wiki/Valores_separados_por_comas)).

##  1. <a name='Leyendoyescribiendoelcontenidocompletodeunfichero'></a>Leyendo y escribiendo el contenido completo de un fichero

A continuación veremos como trabajar con un fichero completo de una vez, en oposición a manipular sólo algunas líneas del archivo.

###  1.1. <a name='Leyendounfichero'></a>Leyendo un fichero

Para leer el contenido de un fichero en un _string_ usamos [`file_get_contents`](https://www.php.net/manual/es/function.file-get-contents.php). Le pasamos como parámetro el nombre del fichero, **nos devuelve una cadena con los contenidos**. En el siguiente ejemplo hemos creado un sistema de plantillas básico usando [`str_replace`](https://www.php.net/manual/es/function.str-replace).

[src/page-template.html](src/page-template.html)

```php
<html>

<head>
    <title>{page_title}</title>
</head>

<body bgcolor="{color}">
    <h1>Hello, {name}</h1>
</body>

</html>
```

[src/template-system.php](src/template-system.php)

```php
<?php
// Cargamos el fichero con la plantilla
$page = file_get_contents('page-template.html');
// Insertamos el título de nuestra página
$page = str_replace('{page_title}', 'Welcome', $page);
// Fondo de página azul por la tarde y verde por la mañana
if (date('H' >= 12)) {
    $page = str_replace('{color}', 'blue', $page);
} else {
    $page = str_replace('{color}', 'green', $page);
}
$page = str_replace('{name}', 'Iker', $page);
// Imprimimos los resultados 
print $page;
?>
```

###  1.2. <a name='Escribiendounfichero'></a>Escribiendo un fichero

La operación "opuesta" a leer los contenidos de un fichero es escribir el contenido de un _string_ en un fichero con [`file_put_contents`](https://www.php.net/manual/es/function.file-put-contents). Siguiendo con el ejemplo previo en lugar de escribir el HTML lo guardaremos en un nuevo fichero.

El siguiente ejemplo escribe el contenido de `$page` con el HTML en un fichero llamado "page.html". 

[src/template-system-ext.php](src/template-system-ext.php)

```php
<?php
$page = file_get_contents('page-template.html');
$page = str_replace('{page_title}', 'Welcome', $page);
if (date('H' >= 12)) {
    $page = str_replace('{color}', 'blue', $page);
} else {
    $page = str_replace('{color}', 'green', $page);
}
$page = str_replace('{name}','Iker', $page);
// Escribimos los resultados en page.html
file_put_contents('page.html', $page);
?>
```

##  2. <a name='Leyendoyescribiendopartesdeunfichero'></a>Leyendo y escribiendo partes de un fichero

Las funciones [`file_get_contents`](https://www.php.net/manual/es/function.file-get-contents.php) y [`file_put_contents`](https://www.php.net/manual/es/function.file-put-contents) están bien cuando queremos trabajar con un fichero completo de una vez. Cuando necesitamos un trabajo más preciso usaremos la función [`file`](https://www.php.net/manual/es/function.file) para acceder a cada línea del fichero.

El siguiente ejemplo lee un fichero que contiene por cada línea una dirección de correo electrónico y un nombre separados con "|", escribe los resultados como una lista HTML.  

[`file`](https://www.php.net/manual/es/function.file) **Devuelve el fichero a un array. Cada elemento del array se corresponde con una línea del fichero**. Usamos [`trim`](https://www.php.net/manual/es/function.trim) para eliminar los espacios en blanco del inicio y el final de la cadena. [`explode`](https://www.php.net/manual/es/function.explode) divide un string y retorna las piezas como arrays.

```php
<?php
foreach (file('people.txt') as $line) {
    $line = trim($line);
    $info = explode('|', $line);
    print '<li><a href="mailto:' . $info[0] . '">' . $info[1] ."</li>\n";
}
?>
```

Ejemplo [src/people.txt](src/people.txt):

alice@example.com|Alice Liddell
bandersnatch@example.org|Bandersnatch Gardner
charles@milk.example.com|Charlie Tenniel
dodgson@turtle.example.com|Lewis Humbert

Genera una salida como esta:

```html
<li><a href="mailto:alice@example.com">Alice Liddell</li>
<li><a href="mailto:bandersnatch@example.org">Bandersnatch Gardner</li>
<li><a href="mailto:charles@milk.example.com">Charlie Tenniel</li>
<li><a href="mailto:dodgson@turtle.example.com">Lewis Humbert</li>
```

La función [`file`](https://www.php.net/manual/es/function.file) puede ser problemática con el consumo de memoria cuando son ficheros muy grandes ya que debe construir un array de líneas. En ese caso necesitas leer el fichero línea a línea usando las funciones [`fopen`](https://www.php.net/manual/es/function.fopen), [`feof`](https://www.php.net/manual/es/function.feof), [`fgets`](https://www.php.net/manual/es/function.fgets) y [`fclose`](https://www.php.net/manual/es/function.fclose).


```php
$fh = fopen('people.txt','rb');
while ((! feof($fh)) && ($line = fgets($fh))) {
    $line = trim($line);
    $info = explode('|', $line);
    print '<li><a href="mailto:' . $info[0] . '">' . $info[1] ."</li>\n";
}
fclose($fh);
```

La función [`fopen`](https://www.php.net/manual/es/function.fopen) abre una conexión con el fichero y retorna una variable (_handle_) que usaremos para acceder al fichero más adelante. El segundo parámetro de la función (_mode_) especifica el tipo de acceso que se necesita para el flujo. En el ejemplo: Apertura para sólo lectura; coloca el puntero al fichero al principio del fichero, el _flag_ "b" especifica que queremos leer en modo binario el archivo.

![](img/01.png)

![](img/02.png)

![](img/03.png)

La función [`fgets`](https://www.php.net/manual/es/function.fgets) obtiene una línea y la devuelve como cadena mientras no lleguemos al final del fichero, lo comprobamos con [`feof`](https://www.php.net/manual/es/function.feof) ('eof' significa 'end of file'). La función [`fclose`](https://www.php.net/manual/es/function.fclose) cierra el archivo apuntado por _handle_ `$fh`.

El bucle [`while`](https://www.php.net/manual/es/control-structures.while.php) continua su ejecución mientras se cumplan dos premisas (ambas condiciones sean `true`):

* `feof($fh)` retorne `false`.
* El valor de `$line` que retorna `fgets($fh)` sea `true`.

Cada ejecución de `fgets($fh)` retorna una línea y avanza el puntero. 

**NOTA**: En el ejemplo el fichero está ubicado en la misma carpeta del programa, cuando debamos manejar rutas relativas o absolutas debemos tener en cuenta si el SO es Linux o M$ Win.

Apertura de un fichero en Windows:

```php
$fh = fopen('c:/windows/system32/settings.txt','rb');
```



