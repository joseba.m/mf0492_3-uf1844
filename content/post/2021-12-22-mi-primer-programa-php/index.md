---
title: Mi primer programa
subtitle: Introducción a PHP
date: 2021-12-22
draft: false
description: Mi primer script
tags: ["PHP","PHP - Introducción", "PHP - echo","PHP - phpinfo","PHP - Hola Mundo"]
---

<!-- vscode-markdown-toc -->
* 1. [Hola mundo!](#Holamundo)
* 2. [Delimitación del código PHP](#DelimitacindelcdigoPHP)
* 3. [Delimitación de sentencias](#Delimitacindesentencias)
* 4. [Emisión de resultados](#Emisinderesultados)
* 5. [Hola mundo en HTML](#HolamundoenHTML)
	* 5.1. [Cómo funciona](#Cmofunciona)
* 6. [ Obtener información de la configuración](#Obtenerinformacindelaconfiguracin)
* 7. [ Generar HTML con PHP](#GenerarHTMLconPHP)
* 8. [Consideraciones con HTML dentro de PHP](#ConsideracionesconHTMLdentrodePHP)
* 9. [Cadenas de texto](#Cadenasdetexto)
	* 9.1. [Secuencias de escape](#Secuenciasdeescape)
	* 9.2. [Concatenar textos](#Concatenartextos)
	* 9.3. [La  sintaxis heredoc](#Lasintaxisheredoc)
* 10. [Comentarios](#Comentarios)
* 11. [Variables de entorno](#Variablesdeentorno)
* 12. [Variables propias](#Variablespropias)
* 13. [ Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Si no tienes un servidor Web a mano se puden probar los ejercicios con [PHP Online Compiler de W3Schools](https://www.w3schools.com/php/php_compiler.asp) o este mismo [Paiza.io](https://paiza.io/es).

##  1. <a name='Holamundo'></a>Hola mundo!

El objetivo para nuestro primer script es muy humilde, enviar desde un programa PHP que se ejecuta en el servidor Web  (Apache, NginX..) un contenido simple al cliente que es un navegador Web (Firefox, Chrome, Edge...). 

Crea un archivo de nombre **hola-mundo.php** que al ejecutarse genere un documento HTML que muestre el mensaje "Hola Mundo". Para ello escribimos dentro de hola.php:

[src/hola-mundo.php](src/hola-mundo.php).

```php
<?php
    echo "Hola Mundo!";
?>
```

[`echo`](https://www.php.net/manual/es/function.echo.php) no es realmente una función, **es una construcción del lenguaje**, por eso no requiere el uso de paréntesis.

Se puede reducir la expresión y hacer lo mismo con este truco:

```php
<?= "Hola Mundo!"; ?>
```

##  2. <a name='DelimitacindelcdigoPHP'></a>Delimitación del código PHP

En un mismo documento es posible incluir más de un bloque de código PHP, aunque en el ejemplo previo, por su sencillez, este caso no se haya dado. Cada uno de esos bloques debe estar delimitado con unas marcas reconocidas por el intérprete de PHP.

##  3. <a name='Delimitacindesentencias'></a>Delimitación de sentencias

Al igual que en otros muchos lenguajes de programación, en PHP no existe una equivalencia directa entre sentencias lógicas y líneas físicas de código. Una sentencia puede ocupar tantas líneas como se necesite y, de manera análoga, nada nos impide introducir más de una sentencia en una misma línea física.

```php
<?php
echo "Sed eget malesuada mauris. 
Proin sollicitudin sodales dui, non tristique tellus cursus non. Suspendisse et blandit nulla. 
Nam tincidunt bibendum porta."
?>
```

El delimitador de fin de línea de sentencia en PHP no es el retorno de carro o el avance línea, sino el punto y coma, **las expresiones finalizan siempre con `;`**. Podemos introducir más de una expresión en una línea gracias a ello:

```php
<?php
    echo "Hola "; echo "mundo!";
?>
```

##  4. <a name='Emisinderesultados'></a>Emisión de resultados

Uno de los recursos que tiene PHP para generar resultados en el documento que procesa el navegador es la emisión directa de contenido, utilizando para ello la orden [`echo`](https://www.php.net/manual/es/function.echo.php), o bien mediante la función [`print`](https://www.php.net/manual/es/function.print). En el código anterior se puede sustituir todas las apariciones de [`echo`](https://www.php.net/manual/es/function.echo.php) por [`print`](https://www.php.net/manual/es/function.print) y verificar que el resultado es el mismo, por eso en ocasiones surge la pregunta de cual es el sentido y la utilidad de que existan dos medios para hacer lo mismo.

Lo cierto es que, aunque sutiles, hay algunas diferencias entre [`echo`](https://www.php.net/manual/es/function.echo.php) y [`print`](https://www.php.net/manual/es/function.print). La primera, y quizá la más importante, es que [`print`](https://www.php.net/manual/es/function.print) puede actuar como una función aunque, en el fondo, sea una construcción intrínseca (es un constructor de lenguaje) de PHP y no una función en sí (ver [lista de palabras reservadas](https://www.php.net/manual/es/reserved.keywords.php)). ¿Qué significa esto?, pues que a diferencia de [`echo`](https://www.php.net/manual/es/function.echo.php), los argumentos entregados a [`print`](https://www.php.net/manual/es/function.print) pueden ir entre paréntesis y, además, [`print`](https://www.php.net/manual/es/function.print) puede formar parte de una expresión más compleja. Las sentencias siguientes son equivalentes:

```php
<?php
echo "Hola PHP";
print "Hola PHP";
print("Hola PHP");
?>
```

##  5. <a name='HolamundoenHTML'></a>Hola mundo en HTML

Ahora introduce el siguiente programa en el editor de texto y guárdalo como hola-mundo-html.php:

[src/hola-mundo-html.php](src/hola-mundo-html.php)

```php
<HTML>

<HEAD>
    <TITLE>Mi primera Web con PHP</TITLE>
</HEAD>

<BODY>
    <?php
    echo "Hola mundo!";
    ?>
</BODY>

</HTML>
```

Cuando es ejecutado en un servidor HTTP con PHP habilitado la pantalla resultante será similar a la ilustrada abajo:

![](img/01.png)

A continuación examinaremos el código HTML (atajo de teclado CTRL+U o botón derecho del ratón "Ver código fuente de la página") para ver lo que ha sucedido con las partes PHP del mismo. Como se puede comprobar, la parte PHP del código ha desaparecido, dejando únicamente el código HTML resultante.

![](img/02.png)

Ahora añade una segunda línea para ver cómo se analiza el código PHP:

[src/hola-mundo-html-2.php](src/hola-mundo-html-2.php)

```php
<HTML>

<HEAD>
    <TITLE>Mi primera Web con PHP</TITLE>
</HEAD>

<BODY>
    <?php
    echo "Hola mundo!";
    echo "loco";
    ?>
</BODY>

</HTML>
```

Como se puede apreciar, la línea discurre sin salto de línea, aunque el código PHP está en dos líneas diferentes.

###  5.1. <a name='Cmofunciona'></a>Cómo funciona

Cuando el navegador invoca un programa PHP, el servidor HTTP primero busca por el código línea por línea para localizar todas las secciones PHP (encerradas entre las correspondientes etiquetas `<?php ... ?>`) y, tras ello, las procesa una a una. Para el servidor, todo el código PHP se trata como una sola línea, motivo por el que las dos líneas de código se han representado como una línea continua en la pantalla. Una vez interpretado o ejecutado correctamente el código PHP, el servidor recopila el resto de HTML y lo envía al navegador, incluyendo las secciones PHP. 

La extensión .php indica al servidor que tiene que buscar e interpretar cualquier código PHP que encuentre dentro. 

##  6. <a name='Obtenerinformacindelaconfiguracin'></a> Obtener información de la configuración 

PHP ofrece la **función** [`phpinfo`](https://www.php.net/manual/es/function.phpinfo.php) — Muestra información sobre la configuración de PHP:

[src/my-phpinfo.php](src/my-phpinfo.php)

```php
<?php
    phpinfo();
?>
```

![](img/03.png)

Esta función es de gran utilidad cuando tenemos algún problema, puede suceder que algún modulo o librería presente problemas en una versión específica, o que no esté directamente incluido.

##  7. <a name='GenerarHTMLconPHP'></a> Generar HTML con PHP

**Importante**. Si dentro del código del documento NO aparecen las etiquetas <?php y ?> la extensión del documento debe de ser .html pero si en algún punto del código del documento aparecen las etiquetas <?php y ?> la extensión del documento debe de ser .php Esto es así porque los documentos .html se ejecutan en el cliente pero los documentos .php se ejecutan en el servidor. Para incluir código HTML dentro de un documento PHP debemos ir a la zona del código dónde queremos colocar el código HTML y escribir entre las etiquetas <?php y ?> el código PHP que genere las etiquetas HTML que queramos. Debemos recordar que este código PHP sólo se ejecutará si abrimos el documento en el servidor y el documento tiene la extensión .php.

Con este sencillo ejemplo veremos cómo podemos escribir código HTML que interpreta el navegador Web del cliente.

[src/genera-html.php](src/genera-html.php)

```php
<!DOCTYPE html>
<html>
    <head>
        <title>Ejemplo</title>
    </head>
    <body>

        <?php            
            echo "<h1>Hola son un encabezado h1</h1>";
            echo "<p>Esto es un párrafo HTML</p>";
        ?>

    </body>
</html>
```

Verás una pantalla similar a esta:

![](img/04.png)

##  8. <a name='ConsideracionesconHTMLdentrodePHP'></a>Consideraciones con HTML dentro de PHP

A continuación analizaremos algunos de los inconvenientes habituales que surgen al añadir HTML dentro de PHP.

* **Comprobar la presencia de comillas dobles**: Como habrás comprobado en los ejemplos anteriores, el uso de la función echo implica la utilización de comillas dobles (se pueden usar comillas simples aunque no funcionan exactamente igual). Como HTML también emplea comillas dobles, hay dos formas de evitar problemas:
    * Usar comillas simples en el código HTML.
    * Añadir una barra invertida a las comillas dobles, como en este ejemplo: `echo "<font size=\"2\">"`. Resulta especialmente útil si se quiere mostrar comillas dobles en el texto, ejemplo: `echo "Mi nombre es \"Iker\" amigos"`.

* **No conviene añadir demasiado código HTML a una sección PHP**: Si en una parte PHP de tu programa el código HTML a generar es demasiado complejo y extenso, prueba a finalizar la sección PHP y programar directamente en HTML. 

Veamos los siguientes ejemplos, en lugar de hacer esto:

```php
<?php
echo "<table>";
echo "  <thead>";
echo "    <tr>";
echo "      <th>Nombre</th>";
echo "      <th>Apellido</th>";
echo "    </tr>";
echo "  </thead>"; 
echo "  <tbody>";
echo "    <tr>";
echo "      <td>Iker</td>";
echo "      <td>Landajuela</td>";
echo "    </tr>";
echo "    <tr>";
echo "      <td>Paul</td>";
echo "      <td>McCartney</td>";
echo "    </tr>";    
echo "  </tbody>";
echo "</table>";
?>
```

¿Es más legible y amable este código no?

```php
<table>
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Apellido</th>
    </tr>
  </thead> 
  <tbody>
    <tr>
      <td><?php echo "Iker" ?></td>
      <td><?php echo "Landajuela" ?></td>
    </tr>
    <tr>
      <td>Paul</td>
      <td>McCartney</td>
    </tr>    
  </tbody>
</table>
```

Aún no hemos estudiado las variables, pero si quisiéramos mostrar un nombre guardado en una variable sólo necesitamos PHP para mostrar para obtener el valor del nombre y para mostrarlo en pantalla. El resto del código es HTML. En estos casos, bastaría con utilizar HTML y recurrir a la PHP cuando sea necesario, en lugar de crear código HTML dentro de PHP.

##  9. <a name='Cadenasdetexto'></a>Cadenas de texto

En PHP las cadenas de texto pueden usar tanto comillas simples como comillas dobles. Sin embargo hay una diferencia importante entre usar unas u otras. Cuando se pone  una variable dentro de unas comillas dobles, se procesa  y  se  sustituye  por  su  valor.  

```php
<?php
    $modulo="DWES";
    print "<p>Módulo: $modulo</p>"
?>
```

La  variable  `$modulo`  se  reconoce  dentro  de  las  comillas  dobles,  y  se  sustituye  por  el  valor  "DWES" antes  de  generar  la  salida.  Si  esto  mismo  lo  hubieras  hecho  utilizando  comillas  simples,  no  se realizaría sustitución alguna.

###  9.1. <a name='Secuenciasdeescape'></a>Secuencias de escape

Cuando  se  usan  comillas  simples,  sólo  se  realizan  dos  sustituciones  dentro  de  la  cadena:  cuando  se encuentra  la  secuencia  de  caracteres  `\'`,  se  muestra  en  la  salida  una  comilla  simple;  y  cuando  se encuentra la secuencia  `\\`, se muestra en la salida una barra invertida. Estas  secuencias  se  conocen  como  secuencias  de  escape.  

En  las  cadenas  que  usan  comillas  dobles, además  de  la  secuencia  `\\`,  se  pueden  usar  algunas  más,  pero  no  la  secuencia  `\'`.  En  esta  tabla puedes ver las secuencias de escape que se pueden utilizar, y cuál es su resultado.

![](img/07.png)

###  9.2. <a name='Concatenartextos'></a>Concatenar textos

En  PHP  tienes  dos  operadores  exclusivos  para  trabajar  con  cadenas  de  texto.  Con  el  operador  de concatenación  punto  `.`  puedes  unir  las  dos  cadenas  de  texto  que  le  pases  como  operandos.  El operador de asignación y concatenación `.=` concatena al argumento del lado izquierdo la cadena del lado derecho.

```php
<?php
    $a = "Módulo ";
    $b = $a . "DWES"; // ahora $b contiene "Módulo DWES"
    $a .= "DWES"; // ahora $a también contiene "Módulo DWES"
?>
```

###  9.3. <a name='Lasintaxisheredoc'></a>La  sintaxis heredoc

En  PHP  tienes  otra  alternativa  para  crear  [cadenas](https://www.php.net/manual/es/language.types.string.php):  la  sintaxis heredoc.  Consiste  en  poner el  operador `<<<`  seguido  de  un  identificador  de  tu  elección,  y  a  continuación  y  empezando  en  la  línea  siguiente  la cadena  de  texto,  sin  utilizar  comillas.  La  cadena  finaliza  cuando  escribes  ese  mismo  identificador  en una  nueva  línea.  Esta  línea  de  cierre  no  debe  llevar  más  caracteres,  ni  siquiera  espacios  o  sangría, salvo quizás un punto y coma después del identificador. 

```php
<?php
$a = <<<MICADENA
Desarrollo de Aplicaciones Web<br />
Desarrollo Web en Entorno Servidor
MICADENA;
print $a;
?>
```

El  texto  se  procesa  de  igual  forma  que  si  fuera  una  cadena  entre  comillas  dobles,  sustituyendo
variables  y  secuencias  de  escape.  Si  no  quisieras  que  se  realizara  ninguna  sustitución,  debes  poner  el identificador de apertura entre comillas simples.

```php
$a = <<<'MICADENA'
... 
MICADENA; 
```

##  10. <a name='Comentarios'></a>Comentarios

Los [comentarios](https://www.php.net/manual/es/language.basic-syntax.comments.php) no son interpretados por PHP, son 

```php
<?php
    echo 'Esto es una prueba'; // Esto es un comentario de una sola línea
    /* Esto es un comentario multilínea
       y otra lína de comentarios */
    echo 'Esto es otra prueba';
    echo 'Una prueba final'; # Esto es un comentario al estilo de consola de una sola línea
?>
```

¿Qué salida produce este programa?

```php
<h1>Esto es un <?php # echo 'simple';?> ejemplo</h1>
```

Puedes usar los comentarios para ordenar secciones del código:

```php
<?php

//======================================================================
// CATEGORY LARGE FONT
//======================================================================

//-----------------------------------------------------
// Sub-Category Smaller Font
//-----------------------------------------------------

/* Title Here Notice the First Letters are Capitalized */

# Option 1
# Option 2
# Option 3

/*
* This is a detailed explanation
* of something that should require
* several paragraphs of information.
*/

// This is a single line quote.
?>
```

##  11. <a name='Variablesdeentorno'></a>Variables de entorno

El código PHP que introducimos en las páginas se ejecuta en un determinado contexto que, en su mayor parte, viene dado por el servidor Web que está atendiendo la solicitud del cliente. Los datos relativos a ese contexto son accesibles a través de unas variables de entorno y, en el caso concreto de PHP, estas se agrupan en una matriz llamada [`$_SERVER`](https://www.php.net/manual/es/reserved.variables.server).

![](img/05.png)

Las matrices son variables que contienen múltiples elementos, identificándose cada uno de ellos bien sea por un índice numérico o un nombre. Como todas las variables en PHP, deben ir precedidas por el símbolo `$` y, por ser matrices, de unos corchetes en cuyo interior se indicará el índice o nombre del elemento que se quiere leer o modificar.

Todo esto lo veremos más adelante, por ahora es suficiente con un pequeño ejemplo:

[src/vars-entorno.php](src/vars-entorno.php)

```php
<?php
echo $_SERVER["REQUEST_URI"]; 
echo $_SERVER["HTTP_USER_AGENT"];
?>
```

Variables de entorno usando la función `phpinfo()`:

![](img/06.png)

##  12. <a name='Variablespropias'></a>Variables propias

A diferencia de lo que ocurre con la mayoría de los lenguajes de programación, PHP no nos exige que declaremos las variables antes de comenzar a utilizarlas. Asumirá que una variable existe a partir del punto que aparezca por primera vez. Además, en PHP las variables no tienen asociado un tipo que limite los valores que pueden contener.

Las variables se identifican rápidamente en el código por el hecho, de ir siempre precedidas por un signo `$`. La primera vez que se usa una variable lo habitual es que sea en una expresión de asignación, ya que de lo contrario no tendría un contenido que poder obtener. Ese contenido puede ser una cadena de caracteres, un número entero o con parte decimal, un valor booleano, una referencia a un objeto, etc.

Ademas de las variables simples, que contienen un único valor, también podemos crear matrices. Todos estos son temas que iremos abordando a medida que sea necesario.

##  13. <a name='Enlacesexternos'></a> Enlaces externos

* [¿Qué es PHP?](https://www.php.net/manual/es/intro-whatis.php#example-1).

Manual de PHP:

* [echo](https://www.php.net/manual/es/function.echo.php).
* [phpinfo](https://www.php.net/manual/es/function.phpinfo.php).
* [Comentarios](https://www.php.net/manual/es/language.basic-syntax.comments.php).
* [Cadenas de caracteres (Strings)](https://www.php.net/manual/es/language.types.string.php)

Interpretes PHP en línea:

* [https://www.w3schools.com/php/php_compiler.asp](https://www.w3schools.com/php/php_compiler.asp).
* [https://paiza.io/es/projects/new](https://paiza.io/es/projects/new).

Adicional:

* [Run PHP from command line in windows and xampp](https://fellowtuts.com/php/run-php-from-command-line-in-windows-and-xampp/).
