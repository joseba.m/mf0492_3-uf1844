---
title: Condicionales if
subtitle: Control de expresiones y del flujo de ejecución en PHP
date: 2022-01-01
draft: false
description: Control de expresiones y del flujo de ejecución en PHP
tags: ["PHP", "PHP - Condicionales","PHP - if"]
---

Las sentencias condicionales alteran el flujo de ejecución del programa, permiten preguntar sobre una determinada condición y actuar en consecuencia de diferentes formas.  

La estructura de una condicional sencilla en PHP es:

```php
if (condición) {
    // código si se cumple la condición
}
```

En muchas ocasiones queremos ejecutar un código si se cumple la condición y otro código si no se cumple. Esta estructura es del tipo if else. En PHP la estructura if else es:

```php
<?php
$edad = 22;
define("EDAD",18);
if ($edad < EDAD) {
	echo "es menor de edad";
} else {
	echo "es mayor de edad";
}
?>
```

Cuando tenemos varias opciones cuyas condiciones dependen unas de otras podemos agruparlas usando la estructura elseif que es:

```php
$a = 12;
$b = 12;

if ($a > $b) {
    echo "a es mayor que b";
} elseif ($a == $b) { 
    # código si NO se cumple la condición de arriba pero se cumple esta
    echo "a es igual que b";
} else {
    # código si NO se cumple ninguna de las condiciones
    echo "a es menor que b";
}
```

## Enlaces externos

* [elseif/else if](https://www.php.net/manual/es/control-structures.elseif.php).