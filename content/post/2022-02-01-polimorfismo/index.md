---
title: Polimorfismo y enlace dinámico (dynamic binding)
subtitle: Principios de la orientación a objetos - Polimorfismo
date: 2022-02-01
draft: false
description: Principios de la orientación a objetos - Polimorfismo
tags: ["Herencia","POO","Objetos","Polimorfismo"]
---

La definición de polimorfismo que encontramos en el diccionario es la siguiente:

> **Polimorfismo**. Propiedad de los cuerpos que pueden cambiar de forma sin variar su naturaleza.

En nuestro caso, el concepto de polimorfimo engloba distintas posibilidades utilizadas habitualmente para conseguir que un mismo elemento software adquiera varias formas simultáneamente.

1. El concepto de **genericidad** es una manera de lograr que un elemento genérico pueda adquirir distintas formas cuando se particulariza su utilización.
2. El concepto de polimorfismo está muy unido al concepto de herencia. Las estructuras y operaciones heredadas se pueden adaptar a las necesidades concretas del elemento "hijo": no es lo mismo rotar ELIPSES que CÍRCULOS. Por tanto, la operación rotar tiene distintas formas según el tipo de figura a la que se destina y es en el momento de la ejecución del programa cuando se utiliza una forma u otra de rotación. Este tipo de **polimorfismo se conoce como de Anulación**, dado que la rotación específica para los círculos anula la más general para las elipses. En algunos casos, no hay anulación real dado que no tiene sentido diseñar e implementar una operación general que abarque todas las posibles situaciones que se pueden plantear con todos los posibles descendientes. Para estos casos se plantea un **polimorfismo Diferido** en el cual se plantea la necesidad de la operación para el elemento "padre", pero su concreción se deja diferida para que cada uno de los elementos "hijos" concrete su forma específica. Este es el caso de la operación Rotar FIGURAS que resultaría muy compleja y además probablemente inutil.
3. Por último, existe otra posibilidad de polimorfismo que se conoce como **sobrecarga**. En este caso quienes adquieren múltiples formas son los operadores, funciones y procedimientos. El ejemplo claro de polimorfismo por sobrecarga lo constituyen los operadores matemáticos: +,* y /. Estos operadores son similares para operaciones entre enteros, reales, conjuntos o matrices. Sin embargo, en cada caso el tipo de operación que se invoca es distinto: la suma entre enteros es mucho más sencilla y rápida que la suma entre reales. En el diseño de esta sobrecarga de operadores, a pesar de que nos resulta tan familiar, se ha utilizado el concepto de polimorfismo. Este mismo se debe aplicar cuando se diseñan las operaciones a realizar entre elementos del sistema que se trata de desarrollar. Por ejemplo, se puede utilizar el operador + para unir cadenas u otros propósitos. El polimorfismo por sobrecarga también se puede utilizar con funciones o procedimientos. Así, podemos tener la función pintar para FIGURAS y diseñar también una función pintar para representar en una gráfica los valores de una tabla o rellenar con distintos trazos una región de un dibujo, etc.

Todas estas posibilidades de polimorfismo redundan en una mayor facilidad para realizar software reutilizable y mantenible. Los elementos que se propongan deberán resultar familiares al mayor número de usuarios potenciales.

Al igual que la herencia, el concepto de polimorfismo está ligado a las metodologías orientadas a objetos y para simplificar la implementación es conveniente utilizar algún lenguaje orientado a objetos.

