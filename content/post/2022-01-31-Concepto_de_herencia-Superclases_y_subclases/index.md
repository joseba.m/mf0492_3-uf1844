---
title: Concepto de herencia, Superclases y subclases
subtitle: Principios de la orientación a objetos - Herencia
date: 2022-01-31
draft: false
description: Principios de la orientación a objetos - Herencia
tags: ["Herencia","POO","Objetos"]
---

Otro enfoque posible del diseño cuando hay elementos con características comunes es establecer una clasificación o jerarquía entre esos elementos del sistema partiendo de un elemento "padre" que posee una estructura y operaciones básicas. 

Los elementos "hijos" heredan del "padre" su estructura y operaciones para ampliarlos, mejorarlos o simplemente adaptarlos a sus necesidades. A su vez los elementos "hijos" pueden tener otros "hijos" que heredan de ellos de una forma semejante. De manera consecutiva se puede continuar con los siguientes descendientes hasta donde sea necesario. Esta es la idea fundamental en la que se basa el concepto de herencia. 

El mecanismo de herencia permite reutilizar una gran cantidad de software ya desarrollado. Habitualmente, en un nuevo proyecto siempre se parte de otros proyectos ya realizados de los que se toman todos aquellos elementos que nos pueden resultar útiles bien directamente, o con pequeñas modificaciones, o de forma combinada entre varios, etc. Facilitar esa labor es uno de los objetivos fundamentales del concepto de herencia.

Si tratamos de realizar un software para dibujo asistido por computador, las figuras que se manejan se podrían clasificar del siguiente modo:

FIGURAS

```
ABIERTAS
    TRAZO RECTO
        LÍNEA RECTA
    TRAZO CURVO
        SEGMENTO DE CIRCUNFERENCIA
CERRADAS
    ELIPSES
        CÍRCULOS
    POLÍGONOS
        TRIANGULOS
            EQUILATEROS        
        RECTANGULOS
```

Aquí el elemento "padre" será FIGURAS. Las operaciones básicas con cualquier tipo de figura podrían ser:

* Desplazar
* Rotar
* Pintar

Aunque estas operaciones las heredan todos los tipos de figura, normálmente deberán ser adaptadas en cada caso. Así, rotar los CÍRCULOS significa dejarlos tal cual están.

Por otro lado, al concretar los elementos "hijos" aparecen nuevas operaciones que no sentido en el elemento "padre". Así, la operación de calcular el perímetro sólo tiene sentido para figuras CERRADAS. De forma semejante sólo los POLÍGONOS tendrán una operación específica para determinar la posición de sus vértices. Sólo en los rectangulos se puede hablar de las longitudes de sus lados.

El concepto de herencia está muy ligado a las metodologías de análisis y diseño de software orientado a objetos. Aunque en este apartado sólo se ha mencionado la herencia simple entre un "hijo" y un único "padre", es posible realizar diseños en los que se tengan en cuenta herencias múltiples de varios "padres". En esencia se trata de aprovechar elementos ya desarrollados para producir otro nuevo que combine simultaneamente las estructuras y propiedades de más de un elemento anterior.

La aplicación del concepto de herencia en la fase de diseño es posible sin mayor problema. Sin embargo, para trasladar de una manera directa y sencilla los resultados del diseño a la codificación es aconsejable utilizar un lenguaje de programación orientado o objetos. Algunos de estos lenguajes son los siguientes: PHP, C++, Java.