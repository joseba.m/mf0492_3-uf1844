---
title: Ejercicios básicos
subtitle: bucles...
date: 2022-01-12
draft: false
description: bucles...
tags: ["PHP", "PHP - Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [Films](#Films)
* 2. [A00](#A00)
* 3. [30](#)
* 4. [Contador secuencia](#Contadorsecuencia)
* 5. [Deten secuencia](#Detensecuencia)
* 6. [Amstrong](#Amstrong)
* 7. [Fibonacci](#Fibonacci)
* 8. [Fibonacci recursivo](#Fibonaccirecursivo)
* 9. [Suma primos](#Sumaprimos)
* 10. [Búsqueda en array](#Bsquedaenarray)
* 11. [Factorial](#Factorial)
* 12. [Tablero ajedrez](#Tableroajedrez)
* 13. [Arbolito navidad](#Arbolitonavidad)
* 14. [Rectangulo](#Rectangulo)
* 15. [Sumatorio](#Sumatorio)
* 16. [FizzBuzz](#FizzBuzz)
* 17. [Radar de velocidad](#Radardevelocidad)
* 18. [Cuadrado mágico](#Cuadradomgico)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Films'></a>Films

**Dada una variable llamada “films” que contiene una matriz con información de películas, recorrerla usando un bucle y mostrarla en una tabla HTML.**

```php
$productos = array(	
	array(
		'marca' => "Bic",
		'precio'  => "0.75€",
		'referencia'  => "552BIC12"
	),
	array(
		'marca' => "Pritt",
		'precio'  => "1.75€",
		'referencia'  => "567PRI13"
	)
);
```

##  2. <a name='A00'></a>A00

**Dada la variable $d = 'A00' , realizar un bucle for para aplicar las operaciones aritméticas necesarias para mostrar la salida:**

A01                                                         
A02                                                         
A03                                                         
A04                                                         
A05

```php
<?php
$d = 'A00';
for ($n=0; $n<5; $n++) {
      echo ++$d ."\n";
}
?>
```

##  3. <a name=''></a>30

**Dada una variable llamada “num” comprueba si es mayor que 30, de 20 o de 10. Usar sentencias condicionales if.**

##  4. <a name='Contadorsecuencia'></a>Contador secuencia

**Usando un bucle for escribe en una línea la siguiente serie “-5 -4 -3 -2 -1 0 1 2 3 4 5”.**

```php
<?php
for ($i=-5;$i<=5;$i++) {
     echo "$i ";
}
?>
```

##  5. <a name='Detensecuencia'></a>Deten secuencia

**Realiza un bucle for que recorra la secuencia de 1 a 100, declara una variable que contenga un número cualquiera dentro de ese rango, cuando el bucle llegue a ese número detendrá su ejecución.**

```php
<?php
$num = 50;

for($i=1;$i<=100;$i++) {
  echo "i=$i<br>";
  if ($i == $num) {
       echo "me salgo del bucle!<br>";
       break;
  }
}
?>
```

##  6. <a name='Amstrong'></a>Amstrong

**Crea una función que recibe un número como parámetro y  retorne un booleano indicando si es un número Amstrong o no.**

Ejemplo:

Por ejemplo, 153 tiene 3 dígitos y 153 = 1^3 + 5^3 + 3^3, por 153lo tanto, es un número de Armstrong.

Ejemplo:

* 407 = (4*4*4) + (0*0*0) + (7*7*7)  
* 407 = 64 + 0 + 343  
* 407 = 407  

```php
<?php  
$num=407;  
$total=0;  
$x=$num;  
while($x!=0)  {
     $rem=$x%10;  
     $total=$total+$rem*$rem*$rem;  
     $x=$x/10;  
}  

if($num==$total)  {  
     echo "Yes it is an Armstrong number";  
}   else   {  
    echo "No it is not an armstrong number";  
}  
?>  
```

##  7. <a name='Fibonacci'></a>Fibonacci

**Reproduce la Sucesión de Fibonacci usando un bucle for (0 ,1 , 1, 2, 3, 5, 8, 13, 21, 34…).**

```php
<?php
    $f1 = 0;
    $f2 = 1;
    $n = 30;
    echo $f1;
    echo '<br/>';
    echo $f2;
    for($i = 1; $i <= $n; $i++) {
        $f3 = $f1 + $f2;
        $f1 = $f2;
        $f2 = $f3;
        echo $f3 ."<br />"; 
    }
?>
```

##  8. <a name='Fibonaccirecursivo'></a>Fibonacci recursivo

**Escribe una función recursiva que escriba por pantalla los primeros  X números de Fibonacci recibido como parámetro de la función, mostrar en una línea con los términos separados por coma.**

La sucesión comienza con los números 0 y 1,​ y a partir de estos, «cada término es la suma de los dos anteriores»
Ejemplo de la salida de los primeros 5 elementos:  0, 1, 1, 2, 3 

```php
<?php  
// PHP code to get the Fibonacci series
// Recursive function for fibonacci series.
function Fibonacci($number){
      
    // if and else if to generate first two numbers
    if ($number == 0)
        return 0;    
    else if ($number == 1)
        return 1;    
      
    // Recursive Call to get the upcoming numbers
    else
        return (Fibonacci($number-1) + 
                Fibonacci($number-2));
}
  
// Driver Code
$number = 10;
for ($counter = 0; $counter < $number; $counter++){  
    echo Fibonacci($counter),' ';
}
?>
```

##  9. <a name='Sumaprimos'></a>Suma primos

**Escribe un programa que dada una función que recibe como parámetro un número calcule la suma de todos los números primos menores que el número y lo retorne como salida.**

En matemáticas, un número primo es un número natural mayor que 1 que tiene únicamente dos divisores positivos distintos: él mismo y el 1

##  10. <a name='Bsquedaenarray'></a>Búsqueda en array

**Crea una función que reciba dos parámetros: un array de números y el número buscado, retornar el número de apariciones del número buscado dentro del array.**

##  11. <a name='Factorial'></a>Factorial

**Crea un programa que calcule el factorial de un número almacenado en una variable usando un bucle.**

El factorial de un número n, se define como el producto de todos los números enteros positivos desde 1 (es decir, los números naturales) hasta n. Por ejemplo:

5! = 1 x 2 x 3 x 4 x 5 = 120


```php
<?php
$num = 3;
$factorial = 1;

for ($x=$num; $x>=1; $x--) {
    $factorial = $factorial * $x;
}

echo "The factorial of $num is $factorial";
?>
```

##  12. <a name='Tableroajedrez'></a>Tablero ajedrez

**Crea un programa para dibujar un tablero de ajedrez.**

```php
<table width="400px" cellspacing="0px" cellpadding="0px" border="1px">
<?php
for($row=1;$row<=8;$row++)
{
	echo "<tr>";
	for($column=1;$column<=8;$column++)
	{
		$total=$row+$column;
		if($total%2==0)
		{
			echo "<td height=35px width=30px bgcolor=#FFFFFF></td>";
		}
		else
		{
			echo "<td height=35px width=30px bgcolor=#000000></td>";
		}
	}
	echo "</tr>";
}
?>
</table>
```

##  13. <a name='Arbolitonavidad'></a>Arbolito navidad

Dada una variable con la altura de un triangulo (nº de líneas) dibujar su contorno con el carácter “*”.

```php
$num_lineas = 5;

for( $i=0; $i<$num_lineas ; $i++ ) {
    
    for( $k=$num_lineas; $k>$i+1 ; $k-- ) {
        echo "&nbsp;"; // espacio HTML
    }
    
    for( $j=0 ; $j<=$i ; $j++ ) {
        echo "* ";
    }
    
    echo "<br>";
}
```

##  14. <a name='Rectangulo'></a>Rectangulo

Dadas dos variables “alto” y “ancho” de un rectángulo dibujar el contorno usando el carácter “*”. 

```php
$alto = 5;
$ancho = 6;
for($i=0;$i<$alto;$i++) {
    for($j=0;$j<$ancho;$j++) {
            if( $j==0 || $j==$ancho-1 || $i==0 || $i==$alto-1 ) {                
                echo "*";
            } else {
                echo " ";
            }
        }        
        echo "\n";
    }
```

##  15. <a name='Sumatorio'></a>Sumatorio

Crea un script que usando un bucle sume todos los enteros entre 0 y 30 y muestre el total.

```php
<?php
$sum = 0;
for($x=1; $x<=30; $x++) {
    $sum +=$x;
}
echo "La suma de los números de 0 a 30 es: $sum"."<br>"; //  suma de los números de 0 a 30 es: 465
?>
```

##  16. <a name='FizzBuzz'></a>FizzBuzz

Script que recorra los números enteros de 1 a 50. Para múltiplos de 3 imprimir “Fizz” en lugar del propio número, para los múltiplos de 5 imprimir “Buzz”, para los números que son multiplos de 3 y 5 imprimir “FizzBuzz”.

##  17. <a name='Radardevelocidad'></a>Radar de velocidad

Un radar de velocidad registra cada vez 10 mediciones en un array con números enteros positivos, diseñar una función que reciba como parámetro de entrada el array con las mediciones y un número entero con el límite máximo de velocidad, la función debe retornar un nuevo array compuesto con aquellos registros que sobrepasan el límite.

```php
<?php 
function check_valocidad_max($regs_radar,$max_valocidad) {
    foreach ($regs_radar as $reg_elemento) {
        if ($reg_elemento > $max_valocidad) {
            $infraciones[] = $reg_elemento;
        }
    }
    return $infraciones;
}

$registros_velocidad=array(20,130,90,140,120,45,30,135,70,121);
$sobrepsasan = check_valocidad_max($registros_velocidad,120);
print_r($sobrepsasan);
?>
```

##  18. <a name='Cuadradomgico'></a>Cuadrado mágico

Un [cuadrado mágico](https://es.wikipedia.org/wiki/Cuadrado_m%C3%A1gico) es la disposición en las celdas de un cuadrado de una serie de números, de tal modo que, al realizar la suma de cualquier fila o columna, o de cualquiera de las diagonales mayores, obtiene un mismo valor. Dicho valor recibe el nombre de constante mágica.

![](img/01.png)

