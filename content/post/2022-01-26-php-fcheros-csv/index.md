---
title: Trabajando con ficheros CSV
subtitle: Entrada-salida de datos
date: 2022-01-26
draft: false
description: Entrada-salida de datos
tags: ["PHP","PHP - Ficheros","PHP - Datos","CSV","PHP - CSV"]
---

<!-- vscode-markdown-toc -->
* 1. [Trabajando con ficheros CSV](#TrabajandoconficherosCSV)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='TrabajandoconficherosCSV'></a>Trabajando con ficheros CSV

Existe un tipo de fichero que recibe un tratamiento especial en PHP: los ficheros [CSV](https://es.wikipedia.org/wiki/Valores_separados_por_comas). Es un formato muy popular para exportar / importar datos en programas de hoja de cálculo.

La función [`fgetcsv`](https://www.php.net/manual/es/function.fgetcsv) es similar a [`fgets`](https://www.php.net/manual/es/function.fgetcsv) excepto que [`fgetcsv`](https://www.php.net/manual/es/function.fgetcsv) analiza la línea que lee para buscar campos en formato CSV, devolviendo un array que contiene los campos leídos.

Ejemplo: [src/dishes.csv](src/dishes.csv)

"Fish Ball with Vegetables",4.25,0
"Spicy Salt Baked Prawns",5.50,1
"Steamed Rock Cod",11.95,0
"Sauteed String Beans",3.15,1
"Confucius ""Chicken""",4.75,0

[src/dishes.php](src/dishes.php)

```php
<?php
$file = fopen("dishes.csv","r");

while(! feof($file)) {
    print_r(fgetcsv($file));
}

fclose($file);
?>
```

## Escribir CSV

Escribir en un fichero CSV es similar a leer uno. La función [`fputcsv`] toma un _handle_ de un fichero abierto y un array de valores como argumento y los escribe, en formato CSV. 

```php
<?php

$lista = array (
    array('aaa', 'bbb', 'ccc', 'dddd'),
    array('123', '456', '789'),
    array('"aaa"', '"bbb"')
);

$fp = fopen('fichero.csv', 'w');

foreach ($lista as $campos) {
    fputcsv($fp, $campos);
}

fclose($fp);
?>
```

El ejemplo de arriba escribirá lo siguiente en fichero.csv:

```
aaa,bbb,ccc,dddd
123,456,789
"""aaa""","""bbb"""
```

