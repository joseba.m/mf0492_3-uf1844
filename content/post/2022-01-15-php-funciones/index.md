---
title: Funciones definidas por el usuario
subtitle: Programación modular
date: 2022-01-15
draft: false
description: Programación modular
tags: ["PHP", "PHP - Modularidad","PHP - Functions","PHP - Funciones"]
---

<!-- vscode-markdown-toc -->
* 1. [Funciones](#Funciones)
* 2. [Argumentos de una función](#Argumentosdeunafuncin)
* 3. [Retornando un valor](#Retornandounvalor)
* 4. [Parámetros por referencia](#Parmetrosporreferencia)
* 5. [Valores por defecto](#Valorespordefecto)
* 6. [Recursividad](#Recursividad)
* 7. [Ejemplos](#Ejemplos)
* 8. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Funciones'></a>Funciones 

En programación, **una función es un conjunto de instrucciones a la que podemos recurrir siempre que queramos**. Éstas pueden recibir parámetros y  realizar todo tipo de tareas y generar una salida o retorno a al programa principal.

Las funciones **aportan modularidad a las aplicaciones**, es decir, nos permiten dividir la complejidad del código en trozos más sencillos de controlar. Otra característica es que permiten reutilizar de manera eficaz el código ya realizado y probado lo que simplifica la ejecución de aplicaciones posteriores.

La sintaxis básica es la siguiente:

```php
function function_name([parameter [, ...]])
{
    // Statements
}
```

Claves:

* La definición comienza con la **palabra clave `function`**.
* **Nombre de la función** para llamarla en el programa, debe seguir las mismas normas que los nombres de variables, deben empezar con una letra o guion bajo. PHP no previene de que tengas una variable y una función con el mismo nombre, pero deberías evitarlo a ser posible. Muchas cosas con el mismo nombre dificultan la comprensión del programa.

```php
<?php

function myFunc() { echo "Hola mundo<br>"; }

$myFunc = "variable";
myFunc();
?>
```

* Paréntesis apertura y cierre requeridas.
* Puede contener ninguno, uno o más **parámetros** separados por coma.
* No es necesario definir una función antes de que sea referenciada.

Ejemplo de declaración de una función:

```php
<?php
function page_header() {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#ffffff">';
}
?>
```

La función `page_header() ` de arriba puede ser invocada como las funciones nativas de PHP (por ejemplo `phpinfo()`), podemos usar la función para imprimir una página HTML completa.

```php
<?php
function page_header() {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#ffffff">';
}

page_header();
print "Welcome, $user";
print "</body></html>";
?>
```


Ejemplos básicos: 

```php
<?php

hola_mundo(); // EN PHP se permite esto! 

function hola_mundo() {
	echo "hola mundo!";
}

hola_mundo();

echo strlen("12345"); // Funcion propia de PHP para obtener la longitud de una cadena.
?>
```

Las funciones pueden ser definidas o declaradas antes o después de ser invocadas (esto no es así en todos los lenguajes de programación). El intérprete de PHP lee el archivo con el programa entero y identifica las funciones existentes antes de ejecutar las expresiones.  

Ejemplo. Definiendo funciones antes y después de invocarlas.

```php
<?php
function page_header() {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#ffffff">';
}

page_header();
print "Welcome, $user";
page_footer();

function page_footer() {
	print '<hr>Thanks for visiting.';
	print '</body></html>';
}
```

##  2. <a name='Argumentosdeunafuncin'></a>Argumentos de una función 

Mientras que algunas funciones (como `page_header()` en la sección previa) siempre hacen lo mismo, otras funciones pueden alterar su comportamiento basadas en entradas de datos que pueden variar. Los valores de entrada proporcionados a una función se llaman **argumentos**. Los argumentos dan más poder a las funciones ya que las hacen más flexibles. Podemos modificar la función `page_header()` para que acepte un argumento que defina el color de la página.

```php
function page_header2($color) {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#' . $color . '">';
}
```

En la declaración de la función, he añadido `$color` entre paréntesis después del nombre de la función. Esto permite al código dentro de la función usar una variable llamada `$color`, que contiene el valor pasado a la función en la llamada. 

Ejemplo:

```php
page_header2('cc00cc');
```

Establece `$color` a `cc00cc` dentro de `page_header2()`, imprime:

```html
<html><head><title>Welcome to my site</title></head><body bgcolor="#cc00cc">
```

Tomamos como ejemplo una función que pasándole una cadena como argumento retorna la misma cadena con todos los carácteres convertidos a minúsculas.

```php
<?php

function convierte_cadena($str) {
	return strtolower($str);
}

echo convierte_cadena("Hola Mundo!");
?>
```

**Cuando defines una función que recibe argumentos debes pasarle un argumento a la función cuando la llames**, sino producirá un error como este "PHP Fatal error: Uncaught ArgumentCountError: Too few arguments to function page_header2(), 0 passed in...".

Para evitar este problema, define una función que tome un **argumento opcional** por defecto en la declaración de la función. Si se proporciona un valor cuando se llama a la función esta tomará el valor proporcionado. Si no se proporciona un valor cuando se llame a la función, la función usará el valor por defecto.

Ejemplo:

```php
<?php

function page_header3($color = 'cc3399') {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#' . $color . '">';
}

page_header3();

?>
```

Llamar a `page_header3()` produce el mismo resultado que llamar a `page_header2('cc3399')`.

Los valores por defecto para los argumentos deben ser literales, no pueden ser variables, lo siguiente produce un error:

```php
<?php

$my_color = '#000000';

// This is incorrect: the default value can't be a variable
function page_header_bad($color = $my_color) {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#' . $color . '">';
}

?>
```

Para definir una función que acepte **múltiples argumentos**, separa cada argumento con una coma en la declaración de la función.

Ejemplo:

```php
<?php
function page_header4($color, $title) {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
}
?>
```

Ejemplo de llamada:

```php
page_header4('66cc66','my homepage');
```

En el ejemplo previo todos los argumentos son obligatorios, puedes usar la misma sintaxis de antes para definir argumentos por defecto, en cualquier caso los argumentos opcionales siempre deben ir a continuación de los obligatorios:

```php
<?php

// One optional argument: it must be last
function page_header5($color, $title, $header = 'Welcome') {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
	print "<h1>$header</h1>";
}

// Acceptable ways to call this function:
page_header5('66cc99','my wonderful page'); // uses default $header
page_header5('66cc99','my wonderful page','This page is great!'); // no defaults

// Two optional arguments: must be last two arguments
function page_header6($color, $title = 'the page', $header = 'Welcome') {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
	print "<h1>$header</h1>";
}

// Acceptable ways to call this function:
page_header6('66cc99'); // uses default $title and $header
page_header6('66cc99','my wonderful page'); // uses default $header
page_header6('66cc99','my wonderful page','This page is great!'); // no defaults

// All optional arguments
function page_header7($color = '336699', $title = 'the page', $header = 'Welcome') {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
	print "<h1>$header</h1>";
}

// Acceptable ways to call this function:
page_header7(); // uses all defaults
page_header7('66cc99'); // uses default $title and $header
page_header7('66cc99','my wonderful page'); // uses default $header
page_header7('66cc99','my wonderful page','This page is great!'); // no defaults
```

**Todos los argumentos opcionales deben ir al final** de la lista de argumentos para evitar ambiguedades.

Cualquier cambio que realices a una variable pasada como argumento a una función no afecta a la variable fuera de la función.

```php
function countdown($top) {
	while ($top > 0) {
		print "$top..";
		$top--;
	}
	print "boom!\n";
}

$counter = 5;
countdown($counter);
print "Now, counter is $counter";
```

Imprime:

```
5..4..3..2..1..boom!
Now, counter is 5
```

##  3. <a name='Retornandounvalor'></a>Retornando un valor 

Las funciones del capítulo anterior realizan una acción imprimiendo una salida. Además de realizar una acción como imprimir datos o guardar información en una base de datos, las funciones pueden computar un valor, llamado el valor de retorno, que puede ser usado después en el programa principal.

```php
<?php

function suma() {
	return (2+2);
}

$miSuma = suma();
echo $miSuma."<br>";

?>
```


```php

function restaurant_check($meal, $tax, $tip) {
	$tax_amount = $meal * ($tax / 100);
	$tip_amount = $meal * ($tip / 100);
	$total_amount = $meal + $tax_amount + $tip_amount;
	return $total_amount;
}

// Find the total cost of a $15.22 meal with 8.25% tax and a 15% tip
$total = restaurant_check(15.22, 8.25, 15);
print 'I only have $20 in cash, so...';
if ($total > 20) {
	print "I must pay with my credit card.";
} else {
	print "I can pay with cash.";
}
```

Ejemplo. Retornando un array de una función.

```php
function restaurant_check2($meal, $tax, $tip) {
	$tax_amount = $meal * ($tax / 100);
	$tip_amount = $meal * ($tip / 100);
	$total_notip = $meal + $tax_amount;
	$total_tip = $meal + $tax_amount + $tip_amount;
	return array($total_notip, $total_tip);
}

$totals = restaurant_check2(15.22, 8.25, 15);
if ($totals[0] < 20) {
	print 'The total without tip is less than $20.';
}

if ($totals[1] < 20) {
	print 'The total with tip is less than $20.';
}
```

A pesar de que sólo puedes retornar un valor con una expresión `return`, puedes tener más de una expresión `return` dentro de una función. La primera sentencia `return` que encuentre mientras ejecuta la función provocará que la función deje de ejecutarse y retorne un valor.

```php
function payment_method($cash_on_hand, $amount) {
	if ($amount > $cash_on_hand) {
		return 'credit card';
	} else {
		return 'cash';
	}
}

$total = restaurant_check(15.22, 8.25, 15);
$method = payment_method(20, $total);
print 'I will pay with ' . $method;
```

Los valores de retorno de una función se pueden tratar como cualquier otro valor, puedes aprovechar esto para usar una función dentro de una sentencia condicional `if()` o cualquier otra estructura de control del flujo de ejecución.

```php
if (restaurant_check(15.22, 8.25, 15) < 20) {
	print 'Less than $20, I can pay cash.';
} else {
	print 'Too expensive, I need my credit card.';
}
```

##  4. <a name='Parmetrosporreferencia'></a>Parámetros por referencia

```php
<?php
function parameters_by_reference(&$num) {
	$num = 5;
}

$n = 1;
echo "Valor inicio es: $n<br>";

parameters_by_reference($n);

echo "Ahora es: $n<br>";
?>
```

##  5. <a name='Valorespordefecto'></a>Valores por defecto

Ejemplo. Uso de parámetros predeterminados en funciones

```php
<?php
function hacer_café($tipo = "capuchino")
{
    return "Hacer una taza de $tipo.\n";
}
echo hacer_café();
echo hacer_café(null);
echo hacer_café("espresso");
?>
```

##  6. <a name='Recursividad'></a>Recursividad



##  7. <a name='Ejemplos'></a>Ejemplos

Función que pasando dos operandos como argumentos y la operación a realizar como cadena ("suma" o "resta") retorne el resultado de la operación.

```php
<?php 
function calculadora($n1, $n2, $operacion) {

	$resultado = 0;
	switch($operacion) {
    	case "suma":
        	$resultado = $n1 + $n2;
        	break;
        case "resta":
        	$resultado = $n1 - $n2;
            break;
        default:
        	echo "Operación no reconocida";
        	break;
    }
    
    return $resultado;
}

echo calculadora(1,2,"suma");

?>
```

Una función que reciba cinco números enteros como parámetros y muestre por pantalla el resultado de sumar los cinco números (tipo procedimiento, no hay valor devuelto).

```php
<?php

function suma_5_nums($n1,$n2,$n3,$n4,$n5) {
	echo "suma es: ".($n1+$n2+$n3+$n4+$n5)."<br>";
}

suma_5_nums(1,2,3,4,5);

?>
```

Una función que reciba cinco números enteros como parámetros y devuelva el resultado de sumar los cinco números (tipo función, hay un valor devuelto). Asigna el resultado de una invocación a la función con los números 2, 5, 1, 8, 10 a una variable de nombre $tmp y muestra por pantalla el valor de la variable.


Una función que reciba como parámetros el valor del radio de la base y la altura de un cilindro y devuelva el volumen del cilindro, teniendo en cuenta que el volumen de un cilindro se calcula como Volumen = númeroPi * radio * radio * Altura siendo númeroPi = 3.1416 aproximadamente.


##  8. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.php.net/manual/es/language.functions.php](https://www.php.net/manual/es/language.functions.php).
* [https://www.php.net/manual/es/functions.arguments.php](https://www.php.net/manual/es/functions.arguments.php).
* [https://www.tutorialspoint.com/php/php_functions.htm](https://www.tutorialspoint.com/php/php_functions.htm): Excelente artículo para seguir profundizando.
* [https://www.php.net/manual/es/functions.arrow.php](https://www.php.net/manual/es/functions.arrow.php) Funciones flecha!.