<?php
$cadena = '10 es < que 21, y 10 es > que 4, "estoy entre comillas dobles"';
echo $cadena;
echo "<br>";
echo htmlspecialchars($cadena);

$cadena = "tom \"&\" 'Jerry'";
 //Escapa las comillas dobles
echo htmlspecialchars($cadena, ENT_COMPAT); 
 //Escapa comillas simples y dobles
echo htmlspecialchars($cadena, ENT_QUOTES); 
 //No escapa comillas simples y dobles
echo htmlspecialchars($cadena, ENT_NOQUOTES);
?>