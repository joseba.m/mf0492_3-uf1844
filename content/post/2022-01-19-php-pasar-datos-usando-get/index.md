---
title: Pasar variables por URL usando GET
subtitle: PHP Web
date: 2022-01-19
draft: false
description: PHP Web
tags: ["PHP","PHP - GET","PHP - HTTP"]
---

<!-- vscode-markdown-toc -->
* 1. [Métodos HTTP](#MtodosHTTP)
* 2. [Códigos de estado de respuesta HTTP](#CdigosdeestadoderespuestaHTTP)
* 3. [Traspaso de variables por enlaces usando GET](#TraspasodevariablesporenlacesusandoGET)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='MtodosHTTP'></a>Métodos HTTP

HTTP define un conjunto de métodos de petición para indicar la acción que se desea realizar para un recurso determinado, estos métodos de solicitud a veces son llamados HTTP _verbs_.

* **GET**: El método GET  solicita una representación de un recurso específico. Las peticiones que usan el método GET sólo deben recuperar datos.
* **POST**: El método POST se utiliza para enviar una entidad a un recurso en específico, causando a menudo un cambio en el estado o efectos secundarios en el servidor.

![](img/01.jpg)

##  2. <a name='CdigosdeestadoderespuestaHTTP'></a>Códigos de estado de respuesta HTTP

Los códigos de estado de respuesta HTTP indican si se ha completado satisfactoriamente una solicitud HTTP específica:

![](img/02.jpg)

##  3. <a name='TraspasodevariablesporenlacesusandoGET'></a>Traspaso de variables por enlaces usando GET

[`$_GET`](https://www.php.net/manual/es/reserved.variables.get.php) es un array asociativo con el contenido de los parámetros pasados por la URL (también conocida como cadena de consulta).

Este es un ejemplo de como una serie de pares nombre variable y valor se codifican en una URL:

```
http://bieberfanclub.com/topfan.php?name=Ron&rank=cheerleader
```

Las variables comienza a partir del carácter de interrogación, se puede enviar más de una variable usando el carácter & como delimitador. Cada variable es una pareja de nombre-valor con el signo de igual.

PHP de forma automática permite acceder a esas variables en la URL usando la variable superglobal [`$_GET`](https://www.php.net/manual/es/reserved.variables.get.php) que contiene un array asociativo. 

Archivo [src/pasa-get.php](src/pasa-get.php):

```php
<HTML>
<HEAD>
<TITLE>origen.html</TITLE>
</HEAD>
<BODY>
<a href="recibe-get.php?saludo=hola&texto=Esto es una variable texto">Paso variables saludo y texto a la página destino.php</a>
</BODY>
</HTML>
```

Archivo [src/recibe-get.php](src/recibe-get.php):

```php
<HTML>
<HEAD>
<TITLE>destino.php</TITLE>
</HEAD>
<BODY>

  Variable "saludo": <?php echo $_GET["saludo"]; ?> 
  <br>
  Variable "texto" <?php echo $_GET["texto"]; ?>
 
</BODY>
</HTML>
```

Es posible pasar arrays con GET usando esta sintaxis:

``` 
http://example.com/users.php?sort[col]=name&sort[order]=desc
```

Que se reciben así:

```php
<?php
echo $_GET['sort']['col'];
echo $_GET['sort']['order'];
?>
```

Analizar llamadas usando alguna extensión como [Quick HTTP Inspector](https://chrome.google.com/webstore/detail/quick-http-inspector/holkomabobpkbfdfnjomglmcgaeeojpg/related) o las _devtools_ para Chrome / Firefox.

![](img/03.png)

## Enviando datos con GET

PHP incluye un función que permite de forma fácil construir la cadena URL:

```php
<?php
$data = array('foo'=>'bar',
              'baz'=>'boom',
              'cow'=>'milk',
              'php'=>'hypertext processor');

echo http_build_query($data) . "<br>";

echo http_build_query($data, 'myvar_');
?>
```

El resultado sería:

```php
0=foo&1=bar&2=baz&3=boom&cow=milk&php=hypertext+processor
myvar_0=foo&myvar_1=bar&myvar_2=baz&myvar_3=boom&cow=milk&php=hypertext+processor
```

## htmlspecialchars

Los parámetros obtenidos con GET deberían ser pasados por la función [`htmlspecialchars`](https://www.php.net/manual/es/function.htmlspecialchars.php) para convertir caŕacteres especiales en HTML.

![](img/04.png)

```php
<?php
echo '¡Hola ' . htmlspecialchars($_GET["saludo"]) . '!';
?>
```

Prueba el siguiente ejemplo para comprobar su uso, inspecciona el código fuente para ver las diferencias entre las dos líneas.

Archivo [src/htmlspecialchars.php](src/htmlspecialchars.php):

```php 
<?php
$cadena = '10 es < que 21, y 10 es > que 4, "estoy entre comillas dobles"';
echo $cadena;
echo "<br>";
echo htmlspecialchars($cadena);
?>
```

htmlspecialchars_decode() hace justo lo contrario que [`htmlspecialchars`](https://www.php.net/manual/es/function.htmlspecialchars.php).

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [https://developer.mozilla.org/es/docs/Web/HTTP/Methods](https://developer.mozilla.org/es/docs/Web/HTTP/Methods).
* [https://developer.mozilla.org/es/docs/Web/HTTP/Status](https://developer.mozilla.org/es/docs/Web/HTTP/Status).
* [PHP: $_GET - Manual](https://www.php.net/manual/es/reserved.variables.get.php).
* [PHP $_GET - W3Schools](https://www.w3schools.com/PHP/php_superglobals_get.asp).
* [htmlspecialchars - Manual - PHP](https://www.php.net/manual/es/function.htmlspecialchars.php)  — Convierte caracteres especiales en entidades HTML.