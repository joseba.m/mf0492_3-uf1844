---
title: Ventajas e incovenientes. Pautas para la selección de la metodología más adecuada
subtitle: Modelos del ciclo de vida del software
date: 2021-12-10
draft: false
description: Modelos del ciclo de vida del software
tags: ["UF1844","Modelos del ciclo de vida","Comparativa"]
---

Las principales diferencias entre los modelos de ciclo de vida son:

* Las características (contenidos) de las fases que incluye el ciclo. Esto dependerá del propio tema relacionado con el proyecto (no es lo mismo las tareas que se deben realizar para proyectar un puente que un avión), o de la organización (interés por reflejar en la división de fases aspectos relativos a la división interna o externa del trabajo).
* El alcance del ciclo de vida, que depende de hasta donde llegue el correspondiente proyecto. Un proyecto comprometerá un simple estudio de viabilidad para el desarrollo de un producto o toda la historia del producto con su fabricación, desarrollo, y modificaciones posteriores que llegan hasta su retirada del mercado.
* La estuctura de la sucesión de etapas, habiendo realimentación, y si se puede repetir.

En todos los modelos de Ciclo de vida se debe contar con **el riesgo que supone adoptarlo**; con riesgo se entiende la probabilidad que hay que retomar una de las etapas anteriores (lo que supone un esfuerzo en tiempo y dinero).

Hay que tener claro que ninguno de los anteriormente citados ciclos de vida evita los riesgos que aparecerán durante el proyecto; siempre va a existir una **intertidumbre por el cambio, errores o cualquier otra eventualidad**. 

## Enlaces externos

* [https://www.affde.com/es/software-development-life-cycle-models.html](https://www.affde.com/es/software-development-life-cycle-models.html).

