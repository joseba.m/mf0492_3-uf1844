---
title: Sesiones
subtitle: Información persistente
date: 2022-01-25
draft: false
description: Información persistente
tags: ["PHP","PHP - Sesiones"]
---

<!-- vscode-markdown-toc -->
* 1. [Sesiones](#Sesiones)
	* 1.1. [Iniciando una sesión](#Iniciandounasesin)
	* 1.2. [Almacenando y obteniendo información](#Almacenandoyobteniendoinformacin)
		* 1.2.1. [Contando los accesos a una página](#Contandolosaccesosaunapgina)
		* 1.2.2. [Salvando los datos de un formulario en una sesión](#Salvandolosdatosdeunformularioenunasesin)
		* 1.2.3. [Autenticación HTTP con base de datos](#AutenticacinHTTPconbasededatos)
	* 1.3. [Identificador de sesión](#Identificadordesesin)
	* 1.4. [Obteniendo las variables de sesión](#Obteniendolasvariablesdesesin)
	* 1.5. [Finalizando una sesión](#Finalizandounasesin)
	* 1.6. [Configuración](#Configuracin)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Sesiones'></a>Sesiones 

Porque tu programa no puede decir qué variables se establecieron en otros programas, o incluso qué valores el mismo programa fijó la vez anterior que se ejecuto, a veces querrás rastrear lo que sus usuarios están haciendo de una página web a otra. Puedes hacerlo estableciendo campos ocultos en un formulario, pero PHP proporciona una solución mucho más poderosa, más segura y más simple en forma de sesiones.

Las sesiones son grupos de variables que **se almacenan en el servidor pero se relacionan solo con el usuario actual**. Para garantizar que las variables correctas se apliquen a los usuarios correctos, **PHP guarda una cookie en los navegadores web de los usuarios para identificarlos de manera única**.

Esta cookie tiene significado solo para el servidor web y no puede usarse para verificar cualquier información sobre un usuario.

Cada usuario distinto de un sitio web tiene su propia información de sesión. Para distinguir una
sesión de otra se usan los identificadores de sesión (SID). Un SID es un atributo que se asigna a cada
uno de los visitantes de un sitio web y lo identifica. De esta forma, si el servidor web conoce el SID de
un usuario, puede relacionarlo con toda la información que posee sobre él, que se mantiene en la
sesión del usuario. Esa información se almacena en el servidor web, generalmente en ficheros
aunque también se pueden utilizar otros mecanismos de almacenamiento como bases de datos.

Como ya habrás supuesto, la cuestión ahora es: ¿y dónde se almacena ese SID, el identificador de la
sesión, que es único para cada usuario? Pues existen dos maneras de mantener el SID entre las
páginas de un sitio web que visita el usuario:

* Utilizando cookies, tal y como ya viste.
* Propagando el SID en un parámetro de la URL. El SID se añade como una parte más de la URL, de
la forma: http://www.misitioweb.com/tienda/listado.php&PHPSESSID=34534fg4ffg34ty

En el ejemplo anterior, el SID es el valor del parámetro `PHPSESSID`. 

Ninguna de las dos maneras es perfecta. Ya sabes los problemas que tiene la utilización de cookies.
Pese a ello, es el mejor método y el más utilizado. Propagar el SID como parte de la URL conlleva
mayores desventajas, como la imposibilidad de mantener el SID entre distintas sesiones, o el hecho
de que compartir la URL con otra persona implica compartir también el identificador de sesión.

###  1.1. <a name='Iniciandounasesin'></a>Iniciando una sesión

Iniciar una sesión requiere llamar a la función PHP [`session_start`](https://www.php.net/manual/es/function.session-start) (o reanuda una sesión existente) **antes de que cualquier HTML haya sido producido**, de manera similar a cómo se envían las cookies durante los intercambios de encabezado (se puede iniciar de forma automática editando la configuración php.ini el parámetro `session.auto_start = 1`).

Cuando se llama a [`session_start`](https://www.php.net/manual/es/function.session-start) se asigna al usuario un ID de sesión aleatorio único, la función devuelve `true` si una sesión fue iniciada satisfactoriamente, si no, devuelve `false`. Por defecto usará una _cookie_ llamada `PHPSESSID` para almacenar el ID de sesión, cuando iniciamos una sesión en una página PHP comprueba la existencia de esta _cookie_ y la establece sino existe previamente. El valor de la _cookie_ es es una cadena alfanumérica aleatoria. Cada cliente Web recibe un ID de sesión diferente.

```php
<?php
session_start();
// Proporciona el mismo ID
echo session_id().'<br>';
echo $_COOKIE['PHPSESSID'].'<br>';
?>
```

El "dialogo" entre el cliente Web y el servidor cuando se inicia una sesión:

![](img/01.png)

###  1.2. <a name='Almacenandoyobteniendoinformacin'></a>Almacenando y obteniendo información

Los datos de sesión se almacenan en un array global `$_SESSION`. Para **guardar una variable de sesión** usamos el array `$_SESSION`:

```php
$_SESSION['variable'] = $value;
```

Para leer de vuelta su valor:

```php
$variable = $_SESSION['variable'];
```

####  1.2.1. <a name='Contandolosaccesosaunapgina'></a>Contando los accesos a una página

La primera vez que el usuario accede a la página la función `session_start` crea una nueva sesión y envía una _cookie_ PHPSESSID con el nuevo ID de sesión. Cuando se crea la sesión, el array `$_SESSION` comienza vacio. 

```php
<?php
session_start();
if (isset($_SESSION['count'])) {
    $_SESSION['count'] = $_SESSION['count'] + 1;
} else {
    $_SESSION['count'] = 1;
}
print "You've looked at this page " . $_SESSION['count'] . ' times.';
?>
```

####  1.2.2. <a name='Salvandolosdatosdeunformularioenunasesin'></a>Salvando los datos de un formulario en una sesión



####  1.2.3. <a name='AutenticacinHTTPconbasededatos'></a>Autenticación HTTP con base de datos

Ahora vamos a usar el mecanismo de autenticación previo basado en cabeceras HTTP para comprobar el usuario y contraseña contra una tabla de una base de datos ([sql/usuarios.sql](sql/usuarios.sql)) que hemos estado usando en otros artículos previos.

Archivo [src/authenticate2.php](src/authenticate2.php):

```php
<?php // authenticate2.php
require_once 'login.php';

$connection = new mysqli($hn, $un, $pw, $db);
if ($connection->connect_error) die("Fatal Error");

if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $un_temp = mysql_entities_fix_string($connection, $_SERVER['PHP_AUTH_USER']);
    $pw_temp = mysql_entities_fix_string($connection, $_SERVER['PHP_AUTH_PW']);

    $query = "SELECT * FROM usuarios WHERE User='$un_temp'";

    $result = $connection->query($query);

    if (!$result) {
        die("User not found");
    } elseif ($result->num_rows) {
        $row = $result->fetch_array(MYSQLI_NUM);
        $result->close();

        if ( $pw_temp == $row[2] )  {
            session_start();
            $_SESSION['Id'] = $row[0];
            $_SESSION['User'] = $row[1];
            $_SESSION['Password'] = $row[2];
            echo htmlspecialchars("$row[0] $row[1] : Hi you are now logged in as '$row[1]'");
            die ("<p><a href='continue.php'>Click here to continue</a></p>");

        } else die("Invalid password_verify combination. query=".$query);
    }
    else die("Invalid username from query".$query);
} else {
    header('WWW-Authenticate: Basic realm="Restricted Area"');
    header('HTTP/1.0 401 Unauthorized');
    die ("Please enter your username and password");
}

$connection->close();

function mysql_entities_fix_string($connection, $string) {
    return htmlentities(mysql_fix_string($connection, $string));
}

function mysql_fix_string($connection, $string) {
    if (get_magic_quotes_gpc()) $string = stripslashes($string);
    return $connection->real_escape_string($string);
}
?>
```

###  1.3. <a name='Identificadordesesin'></a>Identificador de sesión 

La [constante predefinida SID](https://www.php.net/manual/es/session.constants.php) contiene el nombre de la sesión y el ID de sesión en la forma "name=ID". Se puede usar la función [`session_id`](https://www.php.net/manual/es/function.session-id) con el mismo propósito. 

Una vez iniciada la sesión la variable global `$_SESSION` está disponible en un array asociativo que contiene las variables de sesión.

###  1.4. <a name='Obteniendolasvariablesdesesin'></a>Obteniendo las variables de sesión

Archivo: [src/continue.php](src/continue.php):

```php
<?php // continue.php
session_start();
if (isset($_SESSION['User'])) {
    $user = htmlspecialchars($_SESSION['User']);    
    echo "Welcome back $user.<br>";
} else { 
    echo "Please <a href=authenticate2.php>click here</a> to log in.";
}
?>
```

###  1.5. <a name='Finalizandounasesin'></a>Finalizando una sesión

Cuando llega el momento de finalizar un sesión, por ejemplo cuando le usuario solicita hacer un _log out_ de nuestro sitio, puedes usar la función [`session_destroy`](https://www.php.net/manual/es/function.session-destroy.php).

El siguiente ejemplo es una función útil para destruir una sesión, hacer un _log out_ y resetear las variables de sesión:

```php
<?php
function destroy_session_and_data() {
	session_start();
	$_SESSION = array();
	setcookie(session_name(), '', time() - 2592000, '/');
	session_destroy();
}
?>
```

###  1.6. <a name='Configuracin'></a>Configuración 

Por defecto, PHP incluye soporte de sesiones incorporado. Sin embargo, antes de utilizar sesiones en
tu sitio web, debes configurar correctamente PHP utilizando las siguientes directivas en el fichero
php.ini según corresponda.

![](img/02.png)

La función phpinfo, de la que ya hablamos con anterioridad, te ofrece información sobre la configuración actual de las directivas de sesión.

En la documentación de PHP tienes información sobre éstas y otras directivas que permiten configurar el manejo de sesiones. [https://www.php.net/manual/es/session.configuration.php](https://www.php.net/manual/es/session.configuration.php).

