---
title: Bucles For
subtitle: Control de expresiones y del flujo de ejecución en PHP
date: 2022-01-12
draft: false
description: Control de expresiones y del flujo de ejecución en PHP
tags: ["PHP", "PHP - Condicionales","PHP - For","PHP - Bucles"]
---

<!-- vscode-markdown-toc -->
* 1. [Ejercicios](#Ejercicios)
	* 1.1. [Fibonacci](#Fibonacci)
	* 1.2. [Triangulo ASCII](#TrianguloASCII)
	* 1.3. [ Rectangulo ASCII](#RectanguloASCII)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

EL bucle [`for`](https://www.php.net/manual/es/control-structures.for.php) es otra estructura iterativa al igual que el bucle `while`. 

```php
for (expr1; expr2; expr3)
    sentencia
```

* La primera expresión (expr1) es evaluada (ejecutada) una vez incondicionalmente al comienzo del bucle.
* En el comienzo de cada iteración, se evalúa expr2. Si se evalúa como true, el bucle continúa y se ejecutan la/sy sentencia/s anidada/s. Si se evalúa como false, finaliza la ejecución del bucle.
* Al final de cada iteración, se evalúa (ejecuta) expr3. 

Representación con un diagrama de flujo:

![](img/01.png)

Ejemplos básicos:

```php
/* ejemplo basico */

for ($i = 1; $i <= 10; $i++) {
    echo $i." ";
}

/* ejemplo break */

for ($i = 1; ; $i++) {
    if ($i > 10) {
        break;
    }
    echo $i."<br>";
}


/* ejemplo contador decremento */

for( $x=5; $x>=1; $x--) {
    echo $x."<br>";
}

/* más ejemplos */

$total = 0;

for ($i = 1; $i <= 10; $i++) {
    $total += $i;
}

echo $total; // 55
```

Se usa a menudo para recorrer los elementos de un arreglo o _array_.

##  2. <a name='Enlacesexternos'></a>Enlaces externos

* [for](https://www.php.net/manual/es/control-structures.for.php).
* [PHP for Loop](https://www.w3schools.com/php/php_looping_for.asp).
* [https://www.tutorialspoint.com/php/php_loop_types.htm](https://www.tutorialspoint.com/php/php_loop_types.htm).
* [https://www.jquery-az.com/php-for-loop-explained-with-6-examples/](https://www.jquery-az.com/php-for-loop-explained-with-6-examples/).
* [https://www.studentstutorial.com/php/php-for-loop.php](https://www.studentstutorial.com/php/php-for-loop.php).
* [https://www.educba.com/patterns-in-php/](https://www.educba.com/patterns-in-php/).