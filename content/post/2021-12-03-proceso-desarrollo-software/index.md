---
title: El proceso de desarrollo de software
subtitle: El proceso del desarrollo de software
date: 2021-12-03
draft: false
description: El proceso del desarrollo de software
tags: ["UF1844","Introducción","Proceso desarrollo"]
---

<!-- vscode-markdown-toc -->
* 1. [Sistema](#Sistema)
	* 1.1. [Componentes básicos de un sistema](#Componentesbsicosdeunsistema)
	* 1.2. [Características y tipos de sistemas](#Caractersticasytiposdesistemas)
* 2. [Proceso de desarrollo software](#Procesodedesarrollosoftware)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Sistema'></a>Sistema

Un [**sistema**](https://es.wikipedia.org/wiki/Sistema) es un conjunto de elementos relacionados entre sí con un propósito o objetivo común, que actuan en un cierto entorno y que tienen capacidad de autocontrol.

El desarrollo de un sistema de software está enmarcado por los **recursos**, el **tiempo** y un conjunto de **requerimientos** sobre lo que debe hacer. 

![](img/04.png)

###  1.1. <a name='Componentesbsicosdeunsistema'></a>Componentes básicos de un sistema

Los **componentes básicos de un sistema** son:

* **Entradas**: Datos, información, insumos que regresan al sistema.
* **Procesos**: Cambios que se producen sobre las entradas para generar salidas, resultados del sistema.
* **Salidas**: Resultados entregados producto de los procesos internos realizados por el sistema.  

![](img/02.png)

[[Fuente](https://padakuu.com/article/2-system-definition-and-concepts-characteristics-and-types-of-system)].

También podemos identificar otros elementos además de los arriba descritos.

* **Interfaz**: punto de contacto donde el sistema se encuentra con el entorno o donde los subsistemas interactuan entre sí.
* **Propósito***: el objetibo global del sistema. 
* **Entorno**: cualquier cosa externa al sistema y que interactua sobre él.

###  1.2. <a name='Caractersticasytiposdesistemas'></a>Características y tipos de sistemas

* **Organización**: estructura y orden. Algunos ejemplos, la organización jerarquica de una empresa, una computadora formada por componentes como dispositivos de entrada, salidas, almacenamiento o CPU.
* **Interacción**: Entre los subsistemas o componentes.
* **Interdepencia**: Entre componentes, enlace entre ellos.
* **Integración**: Cómo se integran los sistemas para lograr el objetivo.
* **Objetivo o propósito**: Debería conocerse en las fases iniciales del análisis.

##  2. <a name='Procesodedesarrollosoftware'></a>Proceso de desarrollo software

> "El conjunto de actividades de ingeniería de software necesarias para transformar los requerimientos del usuario en software" [[Humphrey](https://es.wikipedia.org/wiki/Watts_Humphrey)].

El [**proceso de desarrollo software**](https://es.wikipedia.org/wiki/Proceso_para_el_desarrollo_de_software), también conocido como ciclo de vida del desarrollo del software, es una parte de la Ingeniería del Software, consiste en un conjunto de actividades y buenas prácticas que culminan con la creación de uno o más productos con una calidad determinada para un cliente o usuario final. **Debe tener un comienzo y un final** bien definidos (y unas etapas intermedias normalmente) y que se desarrolla típicamente dentro de un plan de desarrollo. 

![](img/01.png)

En algunas ocasiones, y para proyectos pequeños, estas actividades serán únicamente un desarrollo software desde cero en cierto lenguaje de programación, pero este modelo está cada vez más obsoleto, ya que el software se crea reutilizando código existente, modificando y ampliando un código previamente creado, consiguiendo así una optimización en los recursos existentes y un descenso en el coste del proyecto (plazos más cortos de desarrollo).

Los procesos de desarrollo software son procesos complejos que dependen de las decisiones que toman ciertas personas, de ahí la conveniencia de la definición de **roles** en la toma de decisiones.

Pese a la diversidad de procesos software, hay una serie de actividades comunes, que son:

* **Especificación del software**: su misión es definir la funcionalidad y la misión del software a crear.
* **Diseño del software**: a partir de una especificación bien definida, se debe definir la estructura del mismo.
* **Implementación del software**: tras el diseño y con este mismo y las especificaciones, los desarrolladores han de implementarlos.
* **Verificación y validación** del software: su misión es la prueba del software para comprobar que el sistema funciona correctamente (sin fallos) y que hace lo que se dice en las especificaciones del sistema.
* **Mantenimiento del software**: en esta fase se recoge la corrección de errores posteriores, así como la evolución del programa (nuevos requisitos añadidos según las nuevas necesidades del cliente).

A continuación estudiaremos las diferentes tareas y los roles necesarios para una adecuada elaboración de un proyecto software.








