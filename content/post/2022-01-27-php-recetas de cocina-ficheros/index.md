---
title: Recetas de cocina - ficheros
subtitle: Entrada-salida de datos
date: 2022-01-27
draft: false
description: Entrada-salida de datos
tags: ["PHP","PHP - Ficheros","PHP - Datos"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Creando o abriendo un fichero local](#Creandooabriendounficherolocal)
	* 2.1. [Problema](#Problema)
	* 2.2. [Solución](#Solucin)
* 3. [Creando un fichero temporal](#Creandounficherotemporal)
	* 3.1. [Problema](#Problema-1)
	* 3.2. [Solución](#Solucin-1)
* 4. [Abriendo un fichero remoto](#Abriendounficheroremoto)
	* 4.1. [Problema](#Problema-1)
	* 4.2. [Solución](#Solucin-1)
* 5. [Leyendo un fichero en un string](#Leyendounficheroenunstring)
	* 5.1. [Problema](#Problema-1)
	* 5.2. [Solución](#Solucin-1)
* 6. [Contando líneas, párrafos o registros](#Contandolneasprrafosoregistros)
	* 6.1. [Problema](#Problema-1)
	* 6.2. [Solución](#Solucin-1)
* 7. [Procesando cada palabra de un fichero](#Procesandocadapalabradeunfichero)
	* 7.1. [Problema](#Problema-1)
	* 7.2. [Solución](#Solucin-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Introducción

Las entradas-salidas de información en una aplicación Web normalmente fluyen entre el navegador, el servidor y la base de datos, pero hay circunstancias donde se involucran ficheros. 

##  2. <a name='Creandooabriendounficherolocal'></a>Creando o abriendo un fichero local

###  2.1. <a name='Problema'></a>Problema 

Queremos abrir un fichero local para leer y escribir en él.

###  2.2. <a name='Solucin'></a>Solución

Usamos [`fopen`](https://www.php.net/manual/es/function.fopen):

```php
<?php
$fh = fopen('file.txt','r') or die("can't open file.txt: $php_errormsg");
?>
```

El primer argumento de [`fopen`](https://www.php.net/manual/es/function.fopen) es el nombre del fichero, el segundo argumento es el modo en el que se abre.

![](img/01.png)

[`$php_errormsg`](https://www.php.net/manual/es/reserved.variables.phperrormsg.php) es una variable que contiene el texto del último mensaje de error generado por PHP. Esta variable solo estará disponible dentro del ámbito donde ocurrió el error.

##  3. <a name='Creandounficherotemporal'></a>Creando un fichero temporal

###  3.1. <a name='Problema-1'></a>Problema 

Necesitamos un fichero para almacenar información de forma temporal.

###  3.2. <a name='Solucin-1'></a>Solución

Usa [`tmpfile`](https://www.php.net/manual/es/function.tmpfile) si sólo necesitas que el fichero exista durante la ejecución del script.

```php
<?php
$temp_fh = tmpfile();
// write some data to the temp file
fputs($temp_fh,"The current time is ".strftime('%c'));
// the file goes away when the script ends
exit(1);
?>
```

Si necesitas que el fichero dure más, genera un nombre de fichero con [`tempnam`](https://www.php.net/manual/es/function.tempnam.php), crea un fichero con un nombre de fichero único, a continuación usa [`fopen`](https://www.php.net/manual/es/function.fopen).

```php
<?php
$tempfilename = tempnam('/tmp','data-');
$temp_fh = fopen($tempfilename,'w') or die($php_errormsg);
fputs($temp_fh,"The current time is ".strftime('%c'));
fclose($temp_fh) or die($php_errormsg);
?>
```

##  4. <a name='Abriendounficheroremoto'></a>Abriendo un fichero remoto

###  4.1. <a name='Problema-1'></a>Problema 

Necesitas abrir un fichero accesible por HTTP o FTP.

###  4.2. <a name='Solucin-1'></a>Solución

```php
<?php
$fh = fopen('http://www.example.com/robots.txt','r') or die($php_errormsg);
?>
```

Cuando [`fopen`](https://www.php.net/manual/es/function.fopen) recibe como parámetro un nombre de fichero que comienza con _http://_, obtiene la página con una llamada HTTP/1.0 GET. Sólo se puede acceder al cuerpo de la respuesta usando el manejador (_handle_), no a las cabeceras.  

Cuando se la pasa un nombre de fichero que comienza con _ftp://_, [`fopen`](https://www.php.net/manual/es/function.fopen) retorna un puntero al archivo. Puedes abrir ficheros vía FTP para leer o para escribir, no para ambas cosas.

Para abrir URLs que requieren usuario y contraseña con [`fopen`](https://www.php.net/manual/es/function.fopen), incrustamos la información de autenticación como en el siguiente ejemplo:

```php
<?php
$fh = fopen('ftp://username:password@ftp.example.com/pub/Index','r');
$fh = fopen('http://username:password@www.example.com/robots.txt','r');
?>
```

##  5. <a name='Leyendounficheroenunstring'></a>Leyendo un fichero en un string

###  5.1. <a name='Problema-1'></a>Problema 

Quieres volcar el contenido de un fichero en una variable. Por ejemplo quieres comprobar si el texto del fichero coincide con una expresión regular.

###  5.2. <a name='Solucin-1'></a>Solución

Usa [`file_get_contents`](https://www.php.net/manual/es/function.file-get-contents):

```php
<?php
$people = file_get_contents('people.txt');
if (preg_match('/Names:.*(David|Susannah)/i',$people)) {
    print "people.txt matches.";
}
```

Si quieres almacenar los contenidos de un fichero en una cadena para realizar alguna operación, [`file_get_contents`](https://www.php.net/manual/es/function.file-get-contents) está bien. Pero si sólo quieres imprimir el contenido completo de un fichero, existen formas más sencillas (y más eficientes) que leer el contenido en una cadena y después imprimirla. PHP nos provee de dos funciones con este propósito. 

* [`fpassthru`](https://www.php.net/manual/es/function.fpassthru) Lee hasta EOF en el puntero al archivo dado desde la posición actual y escribe los resultados en el buffer de salida.
* [`readfile`](https://www.php.net/manual/es/function.readfile) Lee un fichero y lo escribe en el búfer de salida.

##  6. <a name='Contandolneasprrafosoregistros'></a>Contando líneas, párrafos o registros 

###  6.1. <a name='Problema-1'></a>Problema 

Queremos contar el número de líneas, párrafos o registros de un fichero

###  6.2. <a name='Solucin-1'></a>Solución

Para contar el número de líneas usa [`fgets`](https://www.php.net/manual/es/function.fgets) porqué lee línea a línea. Puedes contar el número de veces que se le llama antes de llegar al final del fichero.

```php
<?php
$lines = 0;
if ($fh = fopen('orders.txt','r')) {
    while (! feof($fh)) {
        if (fgets($fh)) {
            $lines++;
        }
    }
}
print $lines;
?>
```

Para contar el número de párrafos, incrementa el contador sólo cuando leas una línea en blanco.

```php
<?php
$paragraphs = 0;
if ($fh = fopen('great-american-novel.txt','r')) {
    while (! feof($fh)) {
        $s = fgets($fh);
        if (("\n" == $s) || ("\r\n" == $s)) {
            $paragraphs++;
        }
    }
}
```

Para contar registros, incrementar el contador sólo cuando la línea leida contenga el separador de registros.


```php
<?php
$records = 0;
$record_separator = '--end--';
if ($fh = fopen('great-american-textfile-database.txt','r')) {
    while (!feof($fh)) {
        $s = rtrim(fgets($fh));
        if ($s == $record_separator) {
            $records++;
        }
    }
}
print $records;
?>
```

##  7. <a name='Procesandocadapalabradeunfichero'></a>Procesando cada palabra de un fichero 

###  7.1. <a name='Problema-1'></a>Problema 

Quieres hacer algo con cada palabra de un fichero. Por ejemplo quieres contabilizar las ocurrencias de cada palabra en un fichero para comprobar su similitud con otro fichero.

###  7.2. <a name='Solucin-1'></a>Solución

Leemos cada línea del fichero con [`fgets`](https://www.php.net/manual/es/function.fgets), separamos cada línea en palabras con [`preg_split`](https://www.php.net/manual/es/function.preg-split).

```php
<?php
$fh = fopen('great-american-novel.txt','r') or die($php_errormsg);
while (! feof($fh)) {
    if ($s = fgets($fh)) {
        $words = preg_split('/\s+/',$s,-1,PREG_SPLIT_NO_EMPTY);
        // process words
    }
}
fclose($fh) or die($php_errormsg);
?>