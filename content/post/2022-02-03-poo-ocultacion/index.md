---
title: Ocultación de información (information hiding)
subtitle: Principios de la orientación a objetos - Ocultación
date: 2022-02-03
draft: false
description: Principios de la orientación a objetos - Ocultación
tags: ["Herencia","POO","Objetos","Ocultación"]
---

Para poder utilizar correctamente un programa no es necesario conocer cuál es su estructura interna. Por ejemplo, el usuario de un procesador de textos puede ser cualquier persona que no posea absolutamente ningún conocimiento de informática y mucho menos de la estructura interna del programa. 

De forma semejante se puede pensar respecto a cada uno de los módulos en los que se divide el programa en su diseño. Al programador "usuario" de un módulo desarrollado por otro programador del equipo puede quedarle completamente oculta la organización de los datos internos 