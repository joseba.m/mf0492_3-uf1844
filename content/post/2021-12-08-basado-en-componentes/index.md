---
title: Basado en componentes (CBSE)
subtitle: Modelos del ciclo de vida del software
date: 2021-12-08
draft: false
description: Modelos del ciclo de vida del software
tags: ["UF1844","Modelos del ciclo de vida","CBSE","Basado en componentes"]
---

La ingeniería de software basada en componentes (CBSE, por las siglas de Component-Based Software Engineering) surgió a finales de la década de 1990 como un **enfoque al desarrollo de sistemas de software basado en la reutilización de componentes de software**. Su creación fue motivada por la frustración de los diseñadores al percatarse de que el desarrollo orientado a objetos no conducía a una reutilización extensa, como se había sugerido originalmente. Las clases de objetos individuales eran muy detalladas y específicas y con frecuencia tenían que acotarse con una aplicación al momento de compilar. Para usarlas, se debe tener conocimiento detallado de las clases, y por lo general esto significa 
tener el código fuente del componente. Por consiguiente, era casi imposible vender o distribuir objetos como componentes de reutilización individuales.

**Los componentes son abstracciones de alto nivel en comparación con los objetos y se definen mediante sus interfaces**. Por lo general, son más grandes que los objetos individuales y todos los detalles de implementación se ocultan a otros componentes. La CBSE es el proceso de definir, implementar e integrar o componer los componentes independientes e imprecisos en los sistemas.

Los clientes demandan un software más confiable que se entregue e implemente más rápidamente. **La única forma de enfrentar la complejidad y entregar un mejor software con mayor rapidez es reutilizar en lugar de implementar una vez más los componentes de software**.

Un componente de software individual es un paquete de software, un servicio web, o un [módulo](https://es.wikipedia.org/wiki/Programaci%C3%B3n_modular) que [encapsula](https://es.wikipedia.org/wiki/Encapsulamiento_(programaci%C3%B3n_orientada_a_objetos)) un conjunto de funciones relacionadas (o de datos).

Se consideran los componentes como **parte de la plataforma inicial para la orientación a servicios**. Los componentes juegan este rol, por ejemplo, en servicios de web y, más recientemente, en las [arquitecturas orientadas a servicios](https://es.wikipedia.org/wiki/Arquitectura_orientada_a_servicios) (SOA, siglas del inglés Service Oriented Architecture).

## Los fundamentos de la ingeniería de software basada en componentes

> "Un componente es una unidad de composición de aplicaciones software, que posee un conjunto de interfaces y un conjunto de requisitos, y que ha de poder ser desarrollado, adquirido, incorporado al sistema y compuesto con otros componentes de forma independiente, en tiempo y espacio" [Szyperski]


* **Componentes independientes (aislamiento)** que se especifican por completo mediante sus **interfaces**. Los componentes se comunican uno con el otro por medio de interfaces. Debe existir una separación clara entre la interfaz del componente y su implementación. Esto significa que la implementación de un componente puede sustituirse por otra, sin cambiar otras partes del sistema.
* **Estándares** de componentes que facilitan la integración de éstos. Tales estándares se incrustan en un modelo de componentes. Definen, a un nivel mínimo, cómo deben especificarse las interfaces de componentes y cómo se comunican estos últimos.  Algunos modelos van más allá y definen las interfaces que deben implementarse por  todos los componentes integrantes. Si los componentes se conforman a los estándares, entonces su ejecución es independiente de su lenguaje de programación. Los componentes escritos en diferentes lenguajes pueden integrarse en el mismo sistema. 
* **Middleware** que brinda soporte de software para integración de componentes. Para hacer que componentes independientes distribuidos trabajen en conjunto, es necesario soporte de middleware que maneje las comunicaciones de componentes.

![](img/01.png)

Los componentes pueden producir o consumir eventos y pueden ser usados para las [arquitecturas dirigida por eventos](https://es.wikipedia.org/wiki/Arquitectura_dirigida_por_eventos) (EDA).

* **Recordatorio**. Este modelo de ciclo de vida permite la creación de módulos reutilizablaes.

## Enlaces externos

* [https://es.wikipedia.org/wiki/Ingenier%C3%ADa_de_software_basada_en_componentes](https://es.wikipedia.org/wiki/Ingenier%C3%ADa_de_software_basada_en_componentes).
