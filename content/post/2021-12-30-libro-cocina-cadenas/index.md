---
title: Cadenas
subtitle: Cookbook
date: 2021-12-30
draft: false
description: Introducción básica
tags: ["PHP", "PHP - Variables","PHP - Cadenas","PHP - Introducción","PHP - Cookbook"]
---

<!-- vscode-markdown-toc -->
* 1. [Buscando una subcadena](#Buscandounasubcadena)
	* 1.1. [Problema](#Problema)
	* 1.2. [Solución](#Solucin)
* 2. [Extrayendo subcadenas](#Extrayendosubcadenas)
	* 2.1. [Problema](#Problema-1)
	* 2.2. [Solución](#Solucin-1)
* 3. [Reemplazando subcadenas](#Reemplazandosubcadenas)
	* 3.1. [Problema](#Problema-1)
	* 3.2. [Solución](#Solucin-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Las cadenas son secuencias de bytes, por ejemplo "Guía del autoestopista galáctico" o incluso "111211211". Cuando lees datos de un archivo o escribes HTML en el navegador, tus datos están representados como cadenas.

Las cadenas en PHP son **_binary-safe_** (por ejemplo pueden contener bytes null) y **pueden aumentar o reducir su longitud bajo demanda** (esto no pasa en todos los lenguajes).

```php
<?php
$string1 = "Hello";
$string2 = "Hello\x00World";

// This function is NOT ! binary safe
echo strcoll($string1, $string2); // gives 0, strings are equal.
?>
```

`\x` indica notación hexadecimal (`0x00 = NULL`). Ver: [Cadenas de caracteres (Strings)](https://www.php.net/manual/es/language.types.string.php#language.types.string.syntax.double).

El valor de una variable de tipo cadena se puede inicializar de 3 formas: con comillas simples, comillas dobles o con el formato "heredoc".

Con cadenas con comillas simples los únicos carácteres especiales que debes "escapar" son la propia barra "\" y la comilla simple.

```php
<?php
echo 'Hola Mundo<br>';

$var='12';
echo 'Una varible se interpreta literal, no expande su valor. mira $var<br>';

echo 'Quiero imprimir \\<br>'; // Quiero imprimir \
//echo 'Quiero imprimir \<br>'; // Esto provoca error
echo 'Quiero imprimir "\"<br>'; // Quiero imprimir "\"

//echo 'Quiero imprimir '<br>'; // Error
echo 'Quiero imprimir \'<br>'; // Quiero imprimir '
?>
```

Cuando usamos comillas dobles no es necesario "escapar" la comilla simple. Reconoce la siguiente tabla:

![](img/01.png)

```php
<?php
echo "Hola Mundo<br>";
echo "Hola comilla simple '.<br>"; //Hola comilla simple '.

$var=12;
echo "Dolar $var.<br>"; //Dolar 12.
echo "Dolar \$var.<br>"; //Dolar $var.

echo "Imprimir \"dobles comillas\".<br>"; // Imprimir "dobles comillas".
?>
```

Las cadenas "heredoc" reconocen los mismos carácteres de escape que las cadenas entre con comillas dobles.

```php
<?php
echo <<< CADENA
Esto es una cadena con heredoc<br>
Otra línea.
CADENA;
?>
```

Heredoc es especialmente útil para imprimir HTML interpolando variables, ya que no debes escapar las dobles comillas de los elementos HTML.

```php
<?php

$remaining_cards = 2;

if ($remaining_cards > 0) {
	$url = '/deal.php';
	$text = 'Deal More Cards';
} else {
	$url = '/new-game.php';
	$text = 'Start a New Game';
}

print <<< HTML
There are <b>$remaining_cards</b> left.
<p>
<a href="$url">$text</a>
HTML;
?>
```

Los bytes individuales de una cadena pueden ser referenciados con los corchetes. El primer byte corresponde a la posición 0.

```php
<?php
$nombre = 'Hilda';
print $nombre[3]; // d
?>
```

##  1. <a name='Buscandounasubcadena'></a>Buscando una subcadena

###  1.1. <a name='Problema'></a>Problema

Queremos saber si una cadena contiene una subcadena concreta. Por ejemplo, quiero conocer si una cadena que contiene un correo electrónico contiene "@".

###  1.2. <a name='Solucin'></a>Solución

[`strpos`](https://www.php.net/manual/es/function.strpos) — Encuentra la posición de la primera ocurrencia de un substring en un string, retorna `false` si no la encuentra. Para diferenciar entre los valores 0 y false usamos `===` que veremos más adelante.

```php
<?php
$email = "iker.l@fptxurdinaga.com";
$res = strpos($email, '@');
if ( $res === false) {
	print 'No existe @ en la dirección!<br>';
} else {
   print 'Existe en la posición '.$res.'.<br>'; // Existe en la posición 6.
}
?>
```

##  2. <a name='Extrayendosubcadenas'></a>Extrayendo subcadenas

###  2.1. <a name='Problema-1'></a>Problema

Queremos extraer una parte de una cadena, comenzando desde una parte o posición determinada de la cadena. Por ejemplo quieres obtener los primeros 8 carácteres de una cadena con un nombre y apellidos.

###  2.2. <a name='Solucin-1'></a>Solución

Usa la función [`substr`](https://www.php.net/manual/es/function.substr).

Sintaxis:

```php
substr(string $string, int $start, int $length = ?): string
```

Ejemplo:

```php
<?php
$user = "Iker Landajuela";
echo substr($user,0,8); // Iker Lan
?>
```

Si start no es negativo, la cadena devuelta comenzará en el start de la posición del string empezando desde cero. Por ejemplo, en la cadena 'abcdef', el carácter en la posición 0 es 'a', el carácter en la posición 2 es 'c', y así sucesivamente.

Si start es negativo, la cadena devuelta empezará en start contando desde el final de string.

Si la longitud del string es menor que start, la función devolverá false.

Ejemplo. Usando un start negativo:

```php
<?php
$rest = substr("abcdef", -1);    // devuelve "f"
$rest = substr("abcdef", -2);    // devuelve "ef"
$rest = substr("abcdef", -3, 1); // devuelve "d"
?>
```

##  3. <a name='Reemplazandosubcadenas'></a>Reemplazando subcadenas

###  3.1. <a name='Problema-1'></a>Problema

Quieres reemplazar una subcadena por otra cadena distinta. Por ejemplo para cambiar los digitos intermedios de un teléfono antes de imprimirlo.

###  3.2. <a name='Solucin-1'></a>Solución

[`substr_replace`](https://www.php.net/manual/es/function.substr-replace) — Reemplaza el texto dentro de una porción de un string.

```php
<?php
$telefono="605323214";
$reemplazo="xxxx";
echo substr_replace($telefono,$reemplazo,3,4); // 605xxxx14
?>
```



