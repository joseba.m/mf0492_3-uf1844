---
title: Método en V
subtitle: Modelos del ciclo de vida del software
date: 2021-12-07
draft: false
description: Modelos del ciclo de vida del software
tags: ["UF1844","Modelos del ciclo de vida","Modelo en V"]
---

El [Método-V](https://es.wikipedia.org/wiki/M%C3%A9todo_en_V) fue creado por la Administración Alemana para el desarrollo de software.

Su nombre es una clara referencia a su representación gráfica. Este módelo resume las principales medidas a adoptar para las prestaciones relativas al marco del sistema de validación.

![](img/01.png)

El modelo describe las actividades y resultados que deben producirse en el desarrollo del producto.

* La parte izquierda se corresponde con las necesidades y especificaciones del sistema. 
* La parte derecha se corresponde con la integración de las partes y su verificación.

Además de que el nombre del método se corresponde con su imagen, hace referencia a "Verificación y validación". Es semejante al modelo en cascada clásico, siendo muy rígido y con muchas iteraciones.  

**Recordatorio**: Este modelo define la programación del proyecto asociando cada fase con las pruebas que deben establecerse, siendo bidireccional.

## Enlaces externos

* [https://es.wikipedia.org/wiki/M%C3%A9todo_en_V](https://es.wikipedia.org/wiki/M%C3%A9todo_en_V).
* [https://web.archive.org/web/20081223163420/http://www.the-software-experts.de/e_dta-sw-process.htm](https://web.archive.org/web/20081223163420/http://www.the-software-experts.de/e_dta-sw-process.htm).
* [https://archive.is/20121130001843/ddonofrio.blogspot.com/2010/12/modelos-de-desarrollo-de-software.html](https://archive.is/20121130001843/ddonofrio.blogspot.com/2010/12/modelos-de-desarrollo-de-software.html).