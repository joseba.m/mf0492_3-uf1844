---
title: Buenas prácticas y principios como desarrolladores
subtitle: Implementación. Conceptos generales de desarrollo de software.
date: 2021-12-25
draft: false
description: Implementación. Conceptos generales de desarrollo de software.
tags: ["Desarrollo","Principios","Buenas prácticas"]
---

<!-- vscode-markdown-toc -->
* 1. [RTFM - "Lee el maldito manual"](#RTFM-Leeelmalditomanual)
* 2. [DRY - "No Te Repitas"](#DRY-NoTeRepitas)
* 3. [KISS - "Mantenlo Simple, Estúpido!"](#KISS-MantenloSimpleEstpido)
* 4. [YAGNI - No vas a necesitarlo](#YAGNI-Novasanecesitarlo)
* 5. [Estándar de codificación Zend](#EstndardecodificacinZend)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

**Peor que usar un mal estándar o un estándar incorrecto es no seguir ninguno**, de la misma forma,  lo peor  que  podemos  hacer  es  no  tener  ningún  criterio  para  enfrentar  los  desarrollos.  Para  aumentar nuestra productividad, tanto individualmente como en equipo, debemos siempre seguir estándares y fijar criterios  de desarrollo.  Nuestro objetivo debería ser contar con una “plataforma de desarrollo” que nos evite tener que repensar problemas típicos y cotidianos, y concentrarnos solo en los problemas nuevos.

Empecemos por el principio, por lo más básico y elemental… nuestros principios base.


> "Nadie debe empezar un proyecto grande. Empiezas con uno pequeño y trivial y nunca 
debes esperar que crezca; si lo haces solamente sobre-diseñarás y generalmente pensarás que es más importante de lo que lo es en esta etapa. O peor, puedes asustarte por el tamaño de lo que tu esperas que crezca. Así que empieza pequeño y piensa en los detalles. No pienses acerca de la foto grande y el diseño elegante. Si no resuelve una necesidad inmediata, seguramente está sobre-diseñado. Y no esperes que la gente salte a ayudarte, no es así como estas cosas funcionan. Primero debes tener algo medianamente usable y otros dirán "hey, esto casi funciona para mí" y se involucrarán en el proyecto." - Linus Torvalds.

##  1. <a name='RTFM-Leeelmalditomanual'></a>RTFM - "Lee el maldito manual"

[RTFM](https://es.wikipedia.org/wiki/RTFM) es un acrónimo del inglés significa  “lee  el  maldito  manual” (Read The Fucking Manual),  algo  muy  usado  en  los  foros  como respuesta  hacia  los  novatos  que  lo  último  que  hacen  es  leerlos  (lamentablemente **todo** se encuentra ahí).

##  2. <a name='DRY-NoTeRepitas'></a>DRY - "No Te Repitas"

[“No te repitas”](https://es.wikipedia.org/wiki/No_te_repitas) significa algo muy simple: si cuando desarrollas ves que al programar “copias” un código para “pegarlo en otro lado”, es muy probable que estés haciendo algo mal, ya que ese código debería estar aislado y ser usado a través de parámetros.

Generalmente  no  existe  razón  para  tener  que  duplicar  el  código,  si  estamos  muy  apurados, seguro, lo pagaremos muy caro dentro de poco.

##  3. <a name='KISS-MantenloSimpleEstpido'></a>KISS - "Mantenlo Simple, Estúpido!"

Hay  una  frase  que  dice  “la  mejor  arquitectura  es  la  sencillez”.  La  sencillez  es  escalable,  si resolvemos  pequeños  problemas  y  luego  los  unimos,  será  más  fácil  que  hacer  un  sistema complejo de entrada.

[https://es.wikipedia.org/wiki/Principio_KISS](https://es.wikipedia.org/wiki/Principio_KISS)

##  4. <a name='YAGNI-Novasanecesitarlo'></a>YAGNI - No vas a necesitarlo

En ingeniería de software la filosofía de desarrollo de programas: **No vas a necesitarlo** o [**YAGNI**](https://es.wikipedia.org/wiki/YAGNI) (en inglés You Aren't Gonna Need It) consiste en que no se debe agregar nunca una funcionalidad excepto cuando sea necesaria. La tentación de escribir código que no es necesario, pero que puede serlo en un futuro, tiene las siguientes desventajas:

* Cuando se desarrollan nuevas funcionalidades se suele sacrificar el tiempo que se destinaría para la funcionalidad básica.
* Las nuevas características deben ser depuradas, documentadas y soportadas.
* Una nueva funcionalidad impone límites a lo que podría ser hecho en el futuro, y podría impedir la implementación de una característica necesaria más adelante.
* Hasta que no está definido para qué se puede necesitar es imposible saber qué debe hacer. Puede suceder que cuando se requieran las nuevas funcionalidades, estas no funcionen correctamente.
* Puede derivar en un código inflado: el programa se vuelve grande y complicado pero no proporciona más funcionalidad.
* Excepto que haya especificaciones y algún tipo de control de versiones, estas características adicionales pueden ser desconocidas por los programadores que podrían hacer uso de ellas.
* Puede inducir a que se agreguen nuevas funcionalidades y, como resultado, puede llevar a un efecto 'bola de nieve' que puede consumir tiempo y recursos ilimitados, a cambio de ningún beneficio.



##  5. <a name='EstndardecodificacinZend'></a>Estándar de codificación Zend


