---
title: Gestores y repositorios de paquetes [borrador]
subtitle: Introducción a Composer
date: 2022-02-09
draft: false
description: Introducción a Composer
tags: ["PHP","Paquetes","Composer"]
---

<!-- vscode-markdown-toc -->
* 1. [Instalando Composer](#InstalandoComposer)
* 2. [Uso básico](#Usobsico)
* 3. [Instalando las dependencias](#Instalandolasdependencias)
* 4. [Buscando paquetes en Packagist](#BuscandopaquetesenPackagist)
* 5. [Desinstalar paquetes](#Desinstalarpaquetes)
* 6. [Obtener información de los paquetes instalados](#Obtenerinformacindelospaquetesinstalados)
* 7. [Configuración de un proyecto con Composer](#ConfiguracindeunproyectoconComposer)
	* 7.1. [Validar la configuración](#Validarlaconfiguracin)
	* 7.2. [Configurando nuestro proyecto](#Configurandonuestroproyecto)
	* 7.3. [Opciones extras de los comandos de Composer](#OpcionesextrasdeloscomandosdeComposer)
* 8. [Instalación de otros paquetes](#Instalacindeotrospaquetes)
	* 8.1. [Twig](#Twig)
	* 8.2. [Proyecto CodeIgniter4](#ProyectoCodeIgniter4)
* 9. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Después de entender las ventajas de la programación funcional y modular, la posibilidad de reutilizar el código desarrollado por otras personas, los paquetes desarrollan ese concepto a un nivel superior. 

Si alguna vez has integrado librerías de terceros estarás familiarizado con algunos pasos habituales cuando se realiza de forma manual: descargar el archivo con la librería, descomprimirla, colocar los ficheros resultantes en una ubicación determinada y incluir algún tipo de referencia a la librería en nuestro programa. 

Con [Composer](https://getcomposer.org/), todas las operaciones descritas arriba se reducen a un simple comando. Además, cuando se liberan nuevas versiones (_release_) del paquete, Composer puede actualizar la versión que usamos. 

Si has trabajando con otros lenguajes, [Composer](https://getcomposer.org/) es muy parecido a [npm](https://www.npmjs.com/) de Node.js o a [Bundler](https://bundler.io/) para Ruby. 

Los paquetes son administrados al nivel del proyecto, y aunque se pueden instalar de manera global, la herramienta no es un instalador de paquetes como APT para Debian o Ubuntu.   

En resumen, [Composer](https://getcomposer.org/) nos permite agregar paquetes a nuestros proyectos actuales, también nos permite crear un proyecto nuevo a través de una plantilla existente o un framework como [Laravel](https://laravel.com/), [CodeIgniter](https://codeigniter.com/) o [Symfony](https://symfony.es/), por citar algunos de los más populares. 

##  1. <a name='InstalandoComposer'></a>Instalando Composer

Para el uso de composer unicamente se requiere una [versión de PHP](https://www.php.net/releases/index.php) 5.3.2 o superior.

En M$ Win descarga y ejecuta [Composer-Setup.exe](https://getcomposer.org/Composer-Setup.exe)

![](img/01.png)

Si se ha instalado de forma correcta, lo siguiente es abrir una ventana de línea de comandos y ejecutar "composer" para comprobar si el comando funciona. Con "composer -V" podemos comprobar la versión que tenemos instalada de Composer.

```
$ composer -V
Composer version 2.2.7 2022-02-25 11:12:27
```

##  2. <a name='Usobsico'></a>Uso básico

Para introducirnos en el uso básico de Composer seguiré la documentación de su web ([Basic usage](https://getcomposer.org/doc/01-basic-usage.md)).

El paquete escogido como ejemplo para incluir en nuestro proyecto es monolog ([https://packagist.org/packages/monolog/monolog](https://packagist.org/packages/monolog/monolog)), este paquete permite enviar tus _logs_ a diferentes sitios como un fichero, una base de datos, etc. 

Para empezar a usar Composer en un proyecto necesitas crear un fichero "composer.json", este fichero describe las dependencias de tu proyecto, normalmente se crea en la carpeta superior o raíz del proyecto.

Lo primero que añadiremos el fichero "composer.json" será la clave "require" donde se informa a Composer de las dependencias de nuestro proyecto.

[src/prj01/composer.json](src/prj01/composer.json)

```json
{
    "require": {
        "monolog/monolog": "2.3.*"
    }
}
```

##  3. <a name='Instalandolasdependencias'></a>Instalando las dependencias

Dentro de la carpeta del proyecto donde hemos creado el fichero "composer.json" ejecutamos el comando composer con la opción [update](https://getcomposer.org/doc/03-cli.md#update-u):

```
$ composer update

$ composer update
Loading composer repositories with package information
Updating dependencies
Lock file operations: 2 installs, 0 updates, 0 removals
  - Locking monolog/monolog (2.3.5)
  - Locking psr/log (3.0.0)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 2 installs, 0 updates, 0 removals
  - Installing psr/log (3.0.0): Extracting archive
  - Installing monolog/monolog (2.3.5): Extracting archive
12 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
1 package you are using is looking for funding.
Use the `composer fund` command to find out more!
```

Cuando finaliza la ejecución se han creado una carpeta "vendor" y un nuevo fichero "composer.lock".

El comando realiza dos tareas:

* Resuelve las dependencias en "composer.json" y escribe las versiones de todos los paquetes en el fichero "composer.lock", bloqueando las versiones de los paquetes a una versión específica.
* De forma implícita ejecuta el comando [install](https://getcomposer.org/doc/03-cli.md#install-i). Este comando descarga las dependencias en el directorio "vendor".

Una vez que Composer ha instalado el paquete, lo único que tienes que hacer es que el paquete esté disponible para nuestro programa creando una referencia al fichero autoload de Composer con una simple línea `require 'vendor/autoload.php';`.

[src/prj01/index.php](src/prj01/index.php)

```php
<?php

include __DIR__.'vendor/autoload.php'; 

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('name');
$log->pushHandler(new StreamHandler('mylog.log', Logger::WARNING));

// add records to the log
$log->warning('Foo');
$log->error('Bar');

?>
```

El fichero autoload.php contiene la lógica para realizar una mapeo entre los nombres de clases y sus ficheros.  

##  4. <a name='BuscandopaquetesenPackagist'></a>Buscando paquetes en Packagist

![](img/02.png)

La utilidad de un gestor de paquetes de [Composer](https://getcomposer.org/) está en encontrar paquetes que respondan a necesidades concretas de nuestro proyecto. El repositorio de paquetes para Composer más popular es [Packagist](https://packagist.org/).

Por ejemplo, buscamos una librería gráfica CSS, si usamos el termino CSS en el buscador de la web encontramos una las librerías más populares, [twitter/bootstrap](https://packagist.org/packages/twitter/bootstrap). Si entramos dentro encontramos un montón de información interesante sobre el paquete: la versión actual, número de instalaciones, valoración, etc.

Para añadir este paquete al proyecto que tenemos entre manos en la parte superior nos indican el comando que debemos usar: `composer require twitter/bootstrap`. Al ejecutar este comando dentro de la carpeta del proyecto se añade el nuevo paquete dentro de las dependencias de composer.json.

Para poder usar Bootstrap debemos referenciar las librerías correspondientes en nuestra aplicación:

```
<link href="vendor/twitter/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>        
<script src="vendor/bootstrap/bootstrap/dist/js/bootstrap.min.js"></script>
```

Ahora supongamos que queremos **instalar una versión concreta** de un paquete, por ejemplo 1.26.0 de Monolog, para eso añadimos la versión deseada al final del comando, así: `composer require monolog/monolog "1.26.0"`.

##  5. <a name='Desinstalarpaquetes'></a>Desinstalar paquetes

Desinstalar un paquete usando la línea de comandos es muy similar a la instalación, pero sin especificar ninguna versión, por ejemplo:

```
composer remove twitter/bootstrap
```

Este proceso eliminará tanto los archivos como sus dependencias, ahora puedes comprobar en el fichero "composer.json" que ha desaparecido el paquete.


##  6. <a name='Obtenerinformacindelospaquetesinstalados'></a>Obtener información de los paquetes instalados

Ahora vamos a usar la opción de composer [show](https://getcomposer.org/doc/03-cli.md#show) para **ver los paquetes que tenemos instalados**.

```
$ composer show
monolog/monolog           1.26.0  Sends your logs to files, sockets, inboxes, databases and various web services
psr/log                   1.1.4   Common interface for logging libraries
symfony/polyfill-ctype    v1.25.0 Symfony polyfill for ctype functions
symfony/polyfill-mbstring v1.25.0 Symfony polyfill for the Mbstring extension
twig/twig                 v3.3.8  Twig, the flexible, fast, and secure template language for PHP
```

##  7. <a name='ConfiguracindeunproyectoconComposer'></a>Configuración de un proyecto con Composer

Creo una carpeta vacía para el nuevo proyecto y me desplazo hasta ella por la línea de comandos, una vez allí inicio la configuración del proyecto con `composer init`. El comando nos guía en la configuración del proyecto solicitándonos información sobre el mismo (vendor/name, autor, etc), también nos permite seleccionar los paquetes que necesitamos para nuestro proyecto. Al final el comando genera un fichero composer.json con todos los datos. Un ejemplo de mi prueba:

```json
{
    "name": "iker/test_project",
    "description": "A new test project made with composer",
    "type": "project",
    "require": {
        "monolog/monolog": "^2.3"
    },
    "license": "GPL-3.0-or-later",
    "autoload": {
        "psr-4": {
            "Iker\\TestProject\\": "src/"
        }
    },
    "authors": [
        {
            "name": "iker.l",
            "email": "iker.l@fptxurdinaga.com"
        }
    ]
}
```

La descripción de todos los parámetros se puede encontrar en este enlace [https://getcomposer.org/doc/04-schema.md](https://getcomposer.org/doc/04-schema.md).

Ademas de composer.json se crea una carpeta src para los ficheros fuente de nuestro proyecto, la carpeta vendor con los paquetes y composer.lock que ya hemos mencionado anteriormente. Ya puedo empezar a trabajar en mi nuevo proyecto abriendo toda la carpeta en VSCode con el comando `code .`.

###  7.1. <a name='Validarlaconfiguracin'></a>Validar la configuración

El fichero composer.json está representado en formato [JSON](https://es.wikipedia.org/wiki/JSON) (JavaScript Object Notation) estricto, si el fichero contiene algún error en su sintaxis no funcionará. Probamos a cambiar el fichero de forma manual e introducimos algún error para comprobar con el comando `composer validate` la validez del fichero de configuración.

![](img/05.png)

El comando no sólo comprueba que no hay errores de sintaxis, también puede producir una advertencia si el contenido de algún campo no está entre los parámetros permitidos (la licencia por ejemplo).

###  7.2. <a name='Configurandonuestroproyecto'></a>Configurando nuestro proyecto



###  7.3. <a name='OpcionesextrasdeloscomandosdeComposer'></a>Opciones extras de los comandos de Composer


##  8. <a name='Instalacindeotrospaquetes'></a>Instalación de otros paquetes

###  8.1. <a name='Twig'></a>Twig

[Twig](https://github.com/twigphp/Twig) es otro sistema de plantillas similar a Smarty. 

Para añadir el paquete a nuestro proyecto ejecutamos el siguiente comando dentro de la carpeta de proyecto: `composer require twig/twig`. 

![](img/03.png)

[Twig](https://github.com/twigphp/Twig) forma parte del _framework_ Symfony, durante la instalación del paquete recurre a algunas dependencias que tiene del proyecto principal.


###  8.2. <a name='ProyectoCodeIgniter4'></a>Proyecto CodeIgniter4

```
composer create-project codeigniter4/appstarter "my-project"
```

##  9. <a name='Enlacesexternos'></a>Enlaces externos

* Curso gratuito básico en Udemy [Gestión de dependencias con PHP y Composer](https://www.udemy.com/course/composer-php/), interesante para iniciarte en Composer.
* [https://uniwebsidad.com/libros/composer](https://uniwebsidad.com/libros/composer).
* Vídeotutorial de 28 minutos recomendado por una alumna [Tutorial de PHP y Composer para manejar dependencias](https://youtu.be/xpNr_BS1bPw). Del curso de Platzi sobre Laravel [https://platzi.com/cursos/curso-php-laravel/](https://platzi.com/cursos/curso-php-laravel/).









