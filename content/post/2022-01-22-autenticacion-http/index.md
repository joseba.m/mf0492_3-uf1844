---
title: Autenticación HTTP
subtitle: PHP Web
date: 2022-01-22
draft: false
description: PHP Web
tags: ["PHP","PHP - HTTP"]
---

PHP puede enviar cabeceras HTTP al cliente que provoca que salga una ventana emergente que solicita autenticación. Cuando el usuario rellena el cuadro de dialogo con una usuario y contraseña, la URL del script PHP es invocada de nuevo.

En la segunda llamada PHP tendrá tres variables predefinidas en el array  `$_SERVER`. Las variables son `PHP_AUTH_USER`, `PHP_AUTH_PW`, y `AUTH_TYPE` con el usuario, contraseña y el tipo de autenticación.

```php
<?php
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Text to send if user hits Cancel button';
    exit;
} else {
    echo "<p>Hello {$_SERVER['PHP_AUTH_USER']}.</p>";
    echo "<p>You entered {$_SERVER['PHP_AUTH_PW']} as your password.</p>";
}
```

En el ejemplo hemos mostrado el contenido de las variables en el array `$_SERVER`, pero en la vida real deberiamos realizar algun tipo de proceso de autenticación. A no ser que uses HTTPS este es un método poco seguro, la contraseña es legible para cualquiera que intercepte las llamadas entre cliente servidor.

Documentación adicional del manual de PHP: [Autenticación HTTP con PHP](https://www.php.net/manual/es/features.http-auth.php).



