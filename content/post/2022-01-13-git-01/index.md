---
title: Introducción a Git
subtitle: Control de versiones con Git
date: 2022-01-13
draft: false
description: Control de versiones con Git
tags: ["UF1844","Git","Herramientas","Aplicaciones","CVS","Versiones","Código Fuente","SCM"]
---

<!-- vscode-markdown-toc -->
* 1. [Acerca del Control de Versiones](#AcercadelControldeVersiones)
* 2. [¿Qué es Git?](#QuesGit)
* 3. [Terminología básica](#Terminologabsica)
	* 3.1. [Repositorio](#Repositorio)
	* 3.2. [Revisión o version](#Revisinoversion)
	* 3.3. [Etiquetado](#Etiquetado)
	* 3.4. [Los Tres Estados](#LosTresEstados)
		* 3.4.1. [Publicar en local](#Publicarenlocal)
	* 3.5. [Actualizar repositorio remoto](#Actualizarrepositorioremoto)
	* 3.6. [Branch o rama](#Branchorama)
* 4. [Instalación](#Instalacin)
	* 4.1. [M$ Win](#MWin)
	* 4.2. [Linux](#Linux)
* 5. [Configurando Git por primera vez](#ConfigurandoGitporprimeravez)
	* 5.1. [M$ Win](#MWin-1)
	* 5.2. [Linux](#Linux-1)
	* 5.3. [Tu identidad](#Tuidentidad)
	* 5.4. [Define tu editor preferido](#Definetueditorpreferido)
* 6. [Revisando los ajustes](#Revisandolosajustes)
* 7. [Obteniendo ayuda](#Obteniendoayuda)
* 8. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='AcercadelControldeVersiones'></a>Acerca del Control de Versiones

¿Qué es un control de versiones ([**VCS**](https://es.wikipedia.org/wiki/Control_de_versiones) por sus siglas en inglés), y por qué debería importarte? Un control de versiones es un **sistema que registra los cambios realizados en un archivo o conjunto de archivos a lo largo del tiempo**, de modo que puedas recuperar versiones
específicas más adelante.

Si eres desarrollador y quieres mantener cada versión de un programa (es algo que sin duda vas a querer), usar un sistema de control de versiones es una muy decisión muy acertada. Dicho sistema te permite regresar a versiones anteriores de tus archivos, regresar a una versión anterior del proyecto completo, comparar cambios a lo largo
del tiempo, ver quién modificó por última vez algo que pueda estar causando
problemas, ver quién introdujo un problema y cuándo, y mucho más. Usar un
VCS también significa generalmente que si arruinas o pierdes archivos, será
posible recuperarlos fácilmente. Adicionalmente, obtendrás todos estos beneficios
a un costo muy bajo.

##  2. <a name='QuesGit'></a>¿Qué es Git?

![](img/01.png)

[Git](https://es.wikipedia.org/wiki/Git) es un sistema distribuido de [control de versiones](https://es.wikipedia.org/wiki/Control_de_versiones) (SCM - Source Code Management) gratuito y libre bajo la licencia [GNU General Public License version 2.0](https://opensource.org/licenses/GPL-2.0).

El kernel de Linux es un proyecto de software de código abierto con un alcance
bastante amplio. Durante la mayor parte del mantenimiento del kernel
de Linux (1991-2002), los cambios en el software se realizaban a través de
parches y archivos. En el 2002, el proyecto del kernel de Linux empezó a usar un
DVCS propietario llamado [BitKeeper](https://es.wikipedia.org/wiki/BitKeeper).

En el 2005, la relación entre la comunidad que desarrollaba el kernel de Linux
y la compañía que desarrollaba BitKeeper se vino abajo, y la herramienta
dejó de ser ofrecida de manera gratuita. Esto impulsó a la comunidad de desarrollo
de Linux (y en particular a Linus Torvalds, el creador de Linux) a desarrollar
su propia herramienta basada en algunas de las lecciones que aprendieron
mientras usaban BitKeeper.

Desde su nacimiento en el 2005, Git ha evolucionado y madurado para ser
fácil de usar y conservar sus características iniciales. Es tremendamente rápido,
muy eficiente con grandes proyectos, y tiene un increíble sistema de ramificación
(branching) para desarrollo no lineal.

##  3. <a name='Terminologabsica'></a>Terminología básica

###  3.1. <a name='Repositorio'></a>Repositorio

El **repositorio** es el lugar en el que se almacenan los datos actualizados e históricos de cambios. Existen repositorios remotos (_origin_) alojados en Internet en un servidor como GitLab o GitHub o en un servidor personal. El repositorio local en nuestro equipo es un subdirectorio `.git` que contiene todos los archivos necesarios.

###  3.2. <a name='Revisinoversion'></a>Revisión o version

Una **revisión es una versión determinada** de la información que se gestiona. Git identifica las revisiones mediante un código de detección de modificaciones con [SHA1](https://es.wikipedia.org/wiki/SHA1). A la última versión se le suele identificar de forma especial con el nombre de **HEAD**. Para marcar una revisión concreta se usan los **rótulos o tags**.

Todo en Git es verificado mediante una suma de comprobación (_checksum_ en inglés) antes de ser almacenado, y es identificado a partir de ese momento mediante dicha suma. Esto significa que es imposible cambiar los contenidos de cualquier archivo o directorio sin que Git lo sepa. Esta funcionalidad está integrada en Git al más bajo nivel y es parte integral de su filosofía. No puedes perder información durante su transmisión o sufrir corrupción de archivos sin que Git sea capaz de detectarlo. 

El mecanismo que usa Git para generar esta suma de comprobación se conoce como hash SHA-1. Se trata de una cadena de 40 caracteres hexadecimales (0-9 y a-f), y se calcula en base a los contenidos del archivo o estructura del directorio en Git.

Una suma de comprobación o [_hash_](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash) tiene como entrada un conjunto de elementos, que suelen ser cadenas, y los convierte en un rango de salida finito, normalmente cadenas de longitud fija. SHA-1 produce una salida resumen de 160 bits (20 bytes), prueba con otros como `md5sum` con [MD5](https://es.wikipedia.org/wiki/MD5):

```bash
$ sha1sum README.md 
900b32de056a34638f800528f2619234aeba3e62  README.md
```

En Windows podemos usar [certutil](https://docs.microsoft.com/es-es/windows-server/administration/windows-commands/certutil#:~:text=Certutil.exe%20es%20un%20programa,parte%20de%20Servicios%20de%20certificados.&text=Si%20certutil%20se%20ejecuta%20en%20una%20entidad%20que%20no%20es,forma%20certutil%20%5B%2Ddump%5D%20predeterminada.)

```
certutil -hashfile README.md SHA1
```

Para obtener el _hash_ del último __commit_ en Git podemos hacer:

```bash
$ git rev-parse HEAD
093931d59864bf09541c7e364899ca390721df75
```

_Hash_ SHA256 de las distribuciones [ISO de Linux Mint v20](https://mirrors.evowise.com/linuxmint/stable/20/sha256sum.txt) para verificar la integridad y autenticidad de la descarga:

![](img/08.png)

###  3.3. <a name='Etiquetado'></a>Etiquetado

Las etiquetas son referencias que apuntan a puntos específicos en el historial de Git. Generalmente, el etiquetado **se usa para capturar un punto en el historial que se utiliza para una publicación de versión marcada**, por ejemplo, v1.0.1, una [_release_](https://es.wikipedia.org/wiki/Release_Management) o entrega de software. Se usa el comando [`git tag`](https://git-scm.com/docs/git-tag).

Para comprobar todas las etiquetas en un repositorio usamos `$ git tag` (y opcionalmente `-l` o `--list`).

Crear una etiqueta es muy sencillo:

```
C:\>git tag -a v0.1 -m "version 0.1 de la documentación"

C:\>git tag
v0.1
```

###  3.4. <a name='LosTresEstados'></a>Los Tres Estados

Ahora presta atención. Esto es lo más importante qu debes recordar acerca de
Git. Git tiene **tres estados principales en los que se pueden encontrar tus archivos**:
confirmado (_committed_), modificado (_modified_), y preparado (_staged)_.

* Confirmado significa que los datos están almacenados de manera segura en tu base
de datos local. 
* Modificado significa que has modificado el archivo pero todavía no lo has confirmado a tu base de datos.
* Preparado significa que has marcado un archivo modificado en su versión actual para que vaya en tu próxima confirmación.

Esto nos lleva a las tres secciones principales de un proyecto de Git: El directorio
de Git (Git directory), el directorio de trabajo (working directory), y el área
de preparación (staging area).

![](img/09.png)

El **directorio de Git** es donde se almacenan los metadatos y la base de datos
de objetos para tu proyecto. Es la parte más importante de Git, y es lo que se
copia cuando clonas un repositorio desde otra computadora.

El **directorio de trabajo** es una copia de una versión del proyecto. Estos archivos
se sacan de la base de datos comprimida en el directorio de Git, y se colocan
en disco para que los puedas usar o modificar.

El **área de preparación** es un archivo, generalmente contenido en tu directorio
de Git, que almacena información acerca de lo que va a ir en tu próxima confirmación.

El flujo de trabajo básico en Git es algo así:

1. Modificas una serie de archivos en tu directorio de trabajo.
2. Preparas los archivos, añadiéndolos a tu área de preparación.
3. Confirmas los cambios, lo que toma los archivos tal y como están en el área de preparación y almacena esa copia instantánea de manera permanente en tu directorio de Git.

Si una versión concreta de un archivo está en el directorio de Git, se considera
confirmada (committed). Si ha sufrido cambios desde que se obtuvo del repositorio,
pero ha sido añadida al área de preparación, está preparada (staged). Y si ha sufrido cambios desde que se obtuvo del repositorio, pero no se ha preparado, está modificada (modified).

####  3.4.1. <a name='Publicarenlocal'></a>Publicar en local

El comando [`git commit`](https://git-scm.com/docs/git-commit) captura una instantánea de los cambios preparados en ese momento del proyecto (incluye los cambios en el HEAD, pero aún no en tu repositorio remoto). Antes de la ejecución de git commit, se utiliza el comando git add para pasar o "preparar" los cambios en el proyecto que se almacenarán. Estos dos comandos, git commit y git add, son dos de los que se utilizan más frecuentemente.

![](img/06.png)

###  3.5. <a name='Actualizarrepositorioremoto'></a>Actualizar repositorio remoto

El comando [`git push`](https://git-scm.com/docs/git-push) se usa para cargar contenido del repositorio local a un repositorio remoto. El envío es la forma de transferir commits desde tu repositorio local a un repositorio remoto.

![](img/07.png)

###  3.6. <a name='Branchorama'></a>Branch o rama

Un módulo puede ser **branched** o bifurcado en un instante de tiempo de forma que, **desde ese momento en adelante se tienen dos copias (ramas) que evolucionan de forma independiente** siguiendo su propia línea de desarrollo. El módulo tiene entonces 2 (o más) "ramas". La ventaja es que se puede hacer un "merge" de las modificaciones de ambas ramas, posibilitando la creación de "ramas de prueba" que contengan código para evaluación, si se decide que las modificaciones realizadas en la "rama de prueba" sean preservadas, se hace un "merge" con la rama principal. Son motivos habituales para la creación de ramas la creación de nuevas funcionalidades o la corrección de errores.
- **Integración o fusión ("merge")**: Una integración o fusión une dos conjuntos de cambios sobre un fichero o un conjunto de ficheros en una revisión unificada de dicho fichero o ficheros. 

![](img/03.png)

##  4. <a name='Instalacin'></a>Instalación

###  4.1. <a name='MWin'></a>M$ Win

La forma más oficial está disponible para ser descargada en el sitio web de Git. Solo tienes que visitar [http://git-scm.com/download/win](http://git-scm.com/download/win).

###  4.2. <a name='Linux'></a>Linux

```bash
$ sudo apt install git
```

Comprobar la versión:

```bash
$ git --version
git version 2.20.1
```

##  5. <a name='ConfigurandoGitporprimeravez'></a>Configurando Git por primera vez

###  5.1. <a name='MWin-1'></a>M$ Win

Ahora que tienes Git en tu sistema, vas a querer hacer algunas cosas para personalizar tu entorno de Git. Es necesario hacer estas cosas solamente una vez en tu computadora, y se mantendrán entre actualizaciones. También puedes cambiarlas en cualquier momento volviendo a ejecutar los comandos correspondientes.

Git trae una herramienta llamada `git config` que te permite obtener y establecer
variables de configuración que controlan el aspecto y funcionamiento de Git. Estas variables pueden almacenarse en sitios distintos dependiendo si la configuración se aplica a todos los usuarios del sistema, a un usuario específico (C:\Users\tu_usuario\.gitcongif) o a un repositorio.

Dentro del repositorio:

```
more .git\config
[core]
        repositoryformatversion = 0
        filemode = false
        bare = false
        logallrefupdates = true
        symlinks = false
        ignorecase = true
[remote "origin"]
        url = https://gitlab.com/iker.l/mf0492_3-uf1844.git
        fetch = +refs/heads/*:refs/remotes/origin/*
[branch "main"]
        remote = origin
        merge = refs/heads/main
```



###  5.2. <a name='Linux-1'></a>Linux

Git viene acompañado de [`git config`](https://git-scm.com/docs/git-config) que permite configurar la aplicación. 

```bash
$ man git config
```

La configuración se puede almacenar en sitios diferentes:

1. `/etc/gitconfig`: Contiene los valores para todos los usuarios del sistema y para todos los repositorios. Se usa con el parámetro `--system`.
2. `~/.gitconfig` o `~/.config/git/config`: Específico del usuario con el parámetro `--global`.
3. `.git/config` dentro del propio repositorio con `--local`.

```bash
popu@diablo:~/Documentos/GitLab/penascalf52021$ cat .git/config 
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[remote "origin"]
	url = https://gitlab.com/soka/penascalf52021.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
```

Cada nivel tiene prioridad sobre las variables definidas en el anterior, lo que quiere decir que los valores en `.git/config` tienen más peso que los de `/etc/gitconfig`. 

###  5.3. <a name='Tuidentidad'></a>Tu identidad

Lo primero que deberás hacer cuando instales Git es establecer tu nombre de usuario y dirección de correo electrónico. Esto es importante porque los “commits” de Git usan esta información, y es introducida de manera inmutable en los commits que envías:

```bash
$ git config --global user.name "iker.l "
$ git config --global user.email iker.l@fptxurdinaga.com
```

Lo comprobamos en el archivo:

```bash
$ cat /home/usuario/.gitconfig 
[user]
	name = soka
	email = ikernaix@gmail.com
```

En Win compruebo el archivo .gitconfig en C:\Users\mi_usuario:

![](img/10.png)

De nuevo, solo necesitas hacer esto una vez si especificas la opción --global, ya que Git siempre usará esta información para todo lo que hagas en ese sistema. Si quieres sobrescribir esta información con otro nombre o dirección de correo para proyectos específicos, puedes ejecutar el comando sin la opción --global cuando estés en ese proyecto.

###  5.4. <a name='Definetueditorpreferido'></a>Define tu editor preferido

Ahora que tu identidad está configurada, puedes elegir el editor de texto por
defecto que se utilizará cuando Git necesite que introduzcas un mensaje. Si no
indicas nada, Git usa el editor por defecto de tu sistema. Si quieres usar otro editor de texto como VSCode.

Podemos definir VSCode como nuestro editor (por ejemplo para los _commits_):

```bash
$ git config --global core.editor "code --wait"
```

Ahora puedes ejecutar `git config --global -e` para editar la configuración con VSCode. 

Para ver las diferencias entre versiones añado al fichero de configuración:

```bash
[diff]
    tool = default-difftool
[difftool "default-difftool"]
    cmd = code --wait --diff $LOCAL $REMOTE
```

Ahora lo probamos [`git difftool`](https://git-scm.com/docs/git-difftool).

Por línea de comandos explicitado VSCode: `git difftool -x "code --wait --diff"`.

Hacemos lo mismo para los _merge_:

```bash
[merge]
    tool = code
[mergetool "code"]
    cmd = code --wait $MERGED
```	

##  6. <a name='Revisandolosajustes'></a>Revisando los ajustes

Si quieres comprobar tu configuración, puedes usar el comando `git config --list` para mostrar todas las propiedades que Git ha configurado:

Muestra los ajustes de los tres sitios de configuración:

```bash
$ git config --list
user.name=soka
user.email=ikernaix@gmail.com
core.editor=code --wait
diff.tool=default-difftool
difftool.default-difftool.cmd=code --wait --diff $LOCAL $REMOTE
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
remote.origin.url=https://gitlab.com/soka/penascalf52021.git
remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*
branch.master.remote=origin
branch.master.merge=refs/heads/master
```

También se puede comprobar cada valor:

```bash
$ git config user.name
soka
```

##  7. <a name='Obteniendoayuda'></a>Obteniendo ayuda

```bash
$ git help <verb>
$ git <verb> --help
$ man git-<verb>
```

Por ejemplo:

```bash
$ git help clone
```

No olvides que en la Web de Git puede encontrar una excelente documentación en [https://git-scm.com/doc](https://git-scm.com/doc).

##  8. <a name='Enlacesexternos'></a>Enlaces externos

* [Control de versiones con Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-1/).
* [Uso básico de Git](https://soka.gitlab.io/blog/post/2020-11-07-curso-git-2/)


* [https://git-scm.com/](https://git-scm.com/).
* [https://git-scm.com/docs/](https://git-scm.com/docs/): La documentación oficial.
* [https://www.atlassian.com/es/git/tutorials](https://www.atlassian.com/es/git/tutorials): En castellano y muy bien explicados los comandos.
* [https://es.wikipedia.org/wiki/Control_de_versiones](https://es.wikipedia.org/wiki/Control_de_versiones).
* [https://code.visualstudio.com/docs/editor/versioncontrol](https://code.visualstudio.com/docs/editor/versioncontrol).
* [https://medium.com/faun/using-vscode-as-git-mergetool-and-difftool-2e241123abe7](https://medium.com/faun/using-vscode-as-git-mergetool-and-difftool-2e241123abe7).


* [https://www.linuxjournal.com/content/git-origin-story](https://www.linuxjournal.com/content/git-origin-story).


