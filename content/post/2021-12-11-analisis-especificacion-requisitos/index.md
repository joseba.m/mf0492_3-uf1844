---
title: Análisis y especificación de requisitos. Introducción
subtitle: Análisis y especificación de requisitos
date: 2021-12-11
draft: false
description: Análisis y especificación de requisitos
tags: ["UF1844","Requisitos","Análisis"]
---

<!-- vscode-markdown-toc -->
* 1. [Objetivos del análisis](#Objetivosdelanlisis)
* 2. [Tareas del análisis](#Tareasdelanlisis)
	* 2.1. [Estudio del sistema en su contexto](#Estudiodelsistemaensucontexto)
	* 2.2. [Identificación de necesidades](#Identificacindenecesidades)
	* 2.3. [Establecimiento del modelo del sistema](#Establecimientodelmodelodelsistema)
	* 2.4. [Elaboración del documento de especificación de requisitos](#Elaboracindeldocumentodeespecificacinderequisitos)
	* 2.5. [Análisis de alternativas. Estudio de viabilidad.](#Anlisisdealternativas.Estudiodeviabilidad.)
	* 2.6. [Revisión continuada del análisis](#Revisincontinuadadelanlisis)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/01.jpg)

La **identificación de necesidades** será el primer paso del análisis del sistema.

En este proceso el Analista se reúne con el cliente y/o usuario (un representante departamental, institucional, o un cliente particular), y analizan las perspectivas del cliente, identifican las metas globales, sus necesidades y requerimientos, sobre líneas de mercado / producto, la planificación temporal y presupuestal, y otros puntos que pueden ayudar a la identificación y desarrollo del proyecto.

**Esta etapa se divide en cinco partes**:

1. Reconocimiento del problema.
2. Evaluación y Síntesis.
3. Especificación.
4. Modelado.
5. Revisión.

Antes de la reunión con el analista, el **cliente debe preparar un documento conceptual sobre el proyecto**, aunque es recomendable que este se elabore durante la comunicación Cliente-análista, ya que de hacerlo el cliente solo de todas maneras tendría que ser modificado, durante la identificación de las necesidades.

La etapa de análisis se encuadra en la fase de definición del ciclo de vida. El análisis de requisitos caracteriza el problema que se trata de resolver. Se concretará en la obtención del modelo global que define totalmente el corportamiento requerido del sistema. Un problema planteado al analista es lograr un interlocutor válido para llevar a cabo el análisis. Este interlocutor puede estar constituido por una o varias personas expertas en una parte o todo el trabajo que se pretende automatizar. Hay casos en los que no habrá nadie capaz de asumir el papel de cliente, debido a que nadie conoce exactamente lo que se requiere del sistema o simplemente por lo novedoso de la aplicación. No se debe asociar el cliente con la persona o entidad que financia el proyecto. El cliente, junto con el análista, será el encargado de elaborar las especificaciones del proyecto de software, que posteriormente se encargarán de verificar el cumplimiento de las mismas a modo de contrato. A veces el cliente será el usuario final de la aplicación, y otras coincidirá con el cliente que financia el proyecto, en otros puede ser partede una especificación de otro proyecto de mayor envergadura.

##  1. <a name='Objetivosdelanlisis'></a>Objetivos del análisis

El **objetivo del análisis** es obtener las especificaciones que debe cumplir el sistema a desarrollar. Para lograr esto se debe realizar un modelo válido, necesario y suficiente donde recoger las necesidades y exigencias que el cliente precisa del sistema y además todas aquellas restricciones que el analista considere que debe verificar el sistema. Quedarán descartadas aquellas exigencias del cliente que no se puedan lograr con los recursos puestos a disposición del proyecto. Quedarán convenientemente matizadas cada una de las exigencias y necesidades del sistema para adaptarlas a los medios disponibles para el proyecto. El modelo global del sistema deberá cumplir:

* Ser completo y sin omisiones.
* Ser conciso y sin trivialidades.
* Fácilmente entendible por el cliente.

Importante usar un lenguaje que sea asequible y facilite la colaboración entre analista y cliente. Emplear notaciones gráficas.

* Separar requisitos funcionales y no funcionales.
    * **Requisitos funcionales**. Destinados a establecer el modelo de funcionamiento del sistema y será fruto fundamental de las discusiones entre analista y cliente.
    * **Requisitos no funcionales**. Destinados a encuadrar el sistema dentro de un entorno de trabajo. Tienen un origen más técnico y no tiene tanto interés para el cliente. Deben permanecer separados en el modelo del sistema. Capacidades mínima y máxima. Interfaces con otros sistemas. Recursos que se necesitan. Aspectos de seguridad, fiabilidad, mantenimiento, calidad, etc.

* No tener detalles de diseño o implementación.

El objetivo del análisis es definir **QUÉ** debe hacer el sistema y no el **CÓMO** lo debe hacer. No se debe entrar en ningún detalle del diseño o implementación del sistema en la etapa de análisis.

* No tener ambiguedades.

El modelo resultante del análisis es un contrato con el cliente. Nada debe quedar ambiguo o se producirán fricciones y problemas que son de consecuencias defíciles de prever.

* Fijando los criterios de validación del sistema.

Es importante que en el modelo del sistema queden indicados cuáles serán los criterios de valiación del sistema. Un método para fijar los criterios de validación es realizar con carácter preliminar el Manual de Usuario del sistema, aunque en dicho manual no se recogen todos los aspectos a validar, es un buen punto de partida.

* Dividiendo y jerarquizando el modelo.

Simplificar un modelo complejo del sistema es dividirlo y jerarquizarlo. Esta división dará lugar a sub-modelos. En el modelo global del sistema se debe ir de lo general a lo particular. 

##  2. <a name='Tareasdelanlisis'></a>Tareas del análisis

Pasos para la realización correcta de un análisis de requisitos.

###  2.1. <a name='Estudiodelsistemaensucontexto'></a>Estudio del sistema en su contexto

Conocer **el medio** en el que se va a desenvolver el sistema. Importante, el análisis del dominio de la aplicación. Incide directamente no sólo en la terminología a emplear en la especificación, sino también en la creación de sistemas con una visión más globalizadora y que facilita la reutilización posterior de alguna de sus partes.

###  2.2. <a name='Identificacindenecesidades'></a>Identificación de necesidades

La labor del analista es **concretar las necesidades** que se pueden cubrir con los medios disponibles y dentro del presupuesto y plazos de entrega asignados a la realización del sistema. Fundamental mantener un dialogo constante y fluido con el cliente. El análista **deberá recoger y estudiar sus sugerencias** para elaborar una propuesta que satisfaga a todos. Descartando funciones muy costosas en desarrollar y que no aportan gran cosa al sistema. Para aquellas funciones que tengan un cierto interés y requieran más recursos de los disponibles, el analista deberá aportar una solución que aunque sea incompleta, encaje dentro de los presupuestos del sistema. Las necesidades que finalmente se identifiquen deberán estar **consensuadas** por todos aquellos que junto al analista hayan participado en el análisis. El analista tratará de convencer a todos que la solución adoptada es la mejor con los medios disponibles, nunca tratará de imponer su criterio.

###  2.3. <a name='Establecimientodelmodelodelsistema'></a>Establecimiento del modelo del sistema

El modelo se irá perfilando a medida que se avanza en el estudio de las necesidades y las soluciones adoptadas. Para la elaboración del modelo se utilizarán todos los medios disponibles, procesadores de texto, [herramientas CASE](https://es.wikipedia.org/wiki/Herramienta_CASE)  (_Computer Aided Software Engineering_, Ingeniería de Software Asistida por Computadora) como herramientas de diseño [UML](https://es.wikipedia.org/wiki/UML), herramientas gráficas, y todo aquello que contribuya a facilitar la comunicación entre analista, cliente y diseñador.

###  2.4. <a name='Elaboracindeldocumentodeespecificacinderequisitos'></a>Elaboración del documento de especificación de requisitos

El resultado final del análisis es el **documento de [especificación de requisitos](https://es.wikipedia.org/wiki/Especificaci%C3%B3n_de_requisitos_de_software#:~:text=La%20especificaci%C3%B3n%20de%20requisitos%20de,son%20conocidos%20como%20requisitos%20funcionales.)**. Debe recoger todas las funcionalidades del análisis. Una parte fundamental del documento será el modelo del sistema y es en este documento donde se debe ir perfilando su elaboración a lo largo de todo el análisis. Este documento será el que utilice el diseñador como punto de partida de su trabajo. Este documento es el encargado de **fijar las condiciones de validación** del sistema una vez concluido su desarrollo e implementación. 

###  2.5. <a name='Anlisisdealternativas.Estudiodeviabilidad.'></a>Análisis de alternativas. Estudio de viabilidad.

Existen infinitas soluciones a un mismo problema. Es labor del análista buscar aquella alternativa que cubra las necesidades reales detectadas y que tenga en cuenta su viabilidad tanto técnica como económica. Con cada alternativa presentada es necesario realizar su estudio de viabilidad correspondiente.

###  2.6. <a name='Revisincontinuadadelanlisis'></a>Revisión continuada del análisis

La labor de análisis no acaba al dar por finalizada la redacción del documento de especificación de requisitos. **A lo largo del desarrollo del sistema y según aparezcan problemas en las etapas de diseño e implementación se tendrán que modificar alguno de los requisitos del sistema**. También se suelen modificar requisitos debido a cambios de criterio del cliente.

![](img/02.png)



