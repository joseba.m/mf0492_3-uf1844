---
title: Ejercicios básicos
subtitle: Condicionales...
date: 2022-01-02
draft: false
description: Condicionales...
tags: ["PHP", "PHP - Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [Edad](#Edad)
* 2. [Par o impar](#Paroimpar)
* 3. [30 o suma 30](#osuma30)
* 4. [Día de la semana](#Dadelasemana)
* 5. [Factura de la luz](#Facturadelaluz)
* 6. [Switch calculadora](#Switchcalculadora)
* 7. [Positivo o negativo](#Positivoonegativo)
* 8. [Cadenas](#Cadenas)
* 9. [Tabla salarial](#Tablasalarial)
* 10. [A00](#A00)
* 11. [Cadena dentro de cadena](#Cadenadentrodecadena)
* 12. [Versión navegador cliente](#Versinnavegadorcliente)
* 13. [Nombre del script](#Nombredelscript)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Edad'></a>Edad

**Dada una variable “edad” con un valor inicial y una constante EDAD que indica mediante un número la edad mínima (18) desarrollar una sentencia condicional if  que comprueba si es menor de edad o mayor y lo muestra por pantalla.**

```php
<?php
$edad = 10;
define(“EDAD”, 18);
if ($edad < EDAD) {
	echo “Es menor de edad”;
} else {
	echo “Es mayor de edad”;
}
?>
```

##  2. <a name='Paroimpar'></a>Par o impar

**Dada una variable entera llamada “num” comprobar con una sentencia condicional if y usando la operación aritmética módulo si es par o impar.**

```php
<?php
$num = 3;
if ($num % 2 == 0) {
	echo “Es par”;
} else {
	echo “Es impar”;
}
?>
```

##  3. <a name='osuma30'></a>30 o suma 30

**Dadas dos variables “x” e “y”, comprobar con una sentencia condicional si una u otra es 30 o la suma de ambos números es 30, mostrar por pantalla cuando se cumpla alguna de las tres condiciones.**

```php
<?php
$x = 20;
$y = 10;
if ($x == 30 || $y == 30 || $x + $y == 30) {
	echo “Cumple una de las condiciones”;
}
```

##  4. <a name='Dadelasemana'></a>Día de la semana

**Programa que escriba el día de la semana (Ejemplo: “Martes”) dada una variable “dia” que contiene un número del 1 al 7. Usar switch/case.**

```php
<?php
$dia = 3;
switch ($dia) {
	case 1:
		echo “Lunes”;
		break;
	case 2:
		echo “Martes”;
		break;
	case 3:
		echo “Miércoles”;
		break;
	case 4:
		echo “Jueves”;
		break;
	case 5:
		echo “Viernes”;
		break;
	case 6:
		echo “Sábado”;
		break;
	case 7:
		echo “Domingo”;
		break;
	default:
		echo ‘Dato incorrecto en la variable $dia.’;
}
?>
```

##  5. <a name='Facturadelaluz'></a>Factura de la luz

**Programa para calcular la factura de la luz usando sentencias condicionales.**

* Las primeras 50 unidades - 3,50 euros unidad
* Siguientes 100 unidades - 4 unidad
* Siguientes 100 unidades - 5,20 unidad
* Por encima de 250 - 6,50 unidad


```php
<?php
$unidades = 125;
if ($unidades <= 50) {
	$total = $unidades * 3.5;
} else if ($unidades > 50 && $unidades <= 150 ) {
	$total = (50*3.5) + (($unidades-50) * 4);
} else if ($unidades > 150 && $unidades <= 250 ) {
	$total = (50*3.5) + (100 * 4) + (($unidades-150)*5.2);
} else if ($unidades > 250){
	$total = (50*3.5) + (100 * 4) + (100*5.2) + (($unidades-250)*6.5);
}
?>
```

##  6. <a name='Switchcalculadora'></a>Switch calculadora 

**Escribe una calculadora sencilla usando switch. El switch evalúa  4 tipos de operaciones pasadas como cadena en una variable $operacion que puede ser: “Suma”, “Resta”, “Multiplicación”, “División”. Definir también dos variables para cada operador, $operador1 y $operador2.**

```php
<?php
$operador1 = 8;
$operador2 = 3;
$operacion = “Resta”;
switch ($operacion){
	case “Suma”:
		echo $operador1 + $operador2;
		break;
	case “Resta”:
		echo $operador1 - $operador2;
		break;
	case “Multiplicación”:
		echo $operador1 * $operador2;
		break;
	case “División”:
		echo $operador1 / $operador2;
		break;
	default:
		echo “Operación no válida”;
}
```

##  7. <a name='Positivoonegativo'></a>Positivo o negativo

**Escribe un programa para evaluar si un número contenido en una variable es: positivo, negativo o cero y lo muestre por pantalla. Usar sentencias condicionales if.**

```php
<?php
$num = -3;
if ($num < 0) {
	echo “Número negativo”;
} elseif ($num == 0) {
	echo “0”;
} elseif ($num > 0) {
	echo “Número positivo”;
}
?>
```

##  8. <a name='Cadenas'></a>Cadenas 

**Escribe un programa que muestre las siguientes cadenas:**

'Tomorrow I \'ll learn PHP global variables.'
'This is a bad command : del c:\\*.*'
Expected Output :
Tomorrow I 'll learn PHP global variables.
This is a bad command : del c:\*.* 

```php
<?php
echo “’Tomorrow I \\’ll learn PHP global variables.’”;
echo “'This is a bad command : del c:\\\*.*'”;
echo “Expected Output :”;
echo “Tomorrow I ’ll learn PHP global variables.”;
echo “This is a bad command : del c:\\*.*”;
?>
```

##  9. <a name='Tablasalarial'></a>Tabla salarial

**Dados 3 valores enteros a, b y c. Escribir una tabla HTML salarial como esta:**

```php
<?php
$salaryA = 1000;
$salaryB = 1200;
$salaryC = 1400;
?>
<html>
	<head>
	</head>
	<body>
		<table>
			<tr><td>Salary of Mr. A is</td><td><?=$salaryA?>$</td></tr>
			<tr><td>Salary of Mr. B is</td><td><?=$salaryB?>$</td></tr>
			<tr><td>Salary of Mr. C is</td><td><?=$salaryC?>$</td></tr>
		</table>
	</body>
</html>
```

##  10. <a name='A00'></a>A00

**Dada la siguiente variable  $d = 'A00'. Escribir lo siguiente usando el operador de incremento:**

A01
A02
A03
A04
A05


```php
<?php
$d = “A00”;
echo “A”.++$d;	// A01
echo “A”.++$d; 	// A02
echo “A”.++$d; 	// A03
echo “A”.++$d; 	// A04
echo “A”.++$d; 	// A05
?>
```

##  11. <a name='Cadenadentrodecadena'></a>Cadena dentro de cadena

**Escribe un programa que compruebe si una cadena contiene otra cadena dentro**

```php
<?php
$texto = “Hola a todos.”;
$buscar = “todos”;
if (str_contains($texto, $buscar) {
	echo “La palabra $buscar sí se encuentra en la frase”;
} else {
	echo “La palabra $buscar no se encuentra en la frase”;
}
?>
```

##  12. <a name='Versinnavegadorcliente'></a>Versión navegador cliente

**Escribe un programa que muestre la versión del navegador usado.**

```php
<?php
	echo $_SERVER[‘HTTP_USER_AGENT’];
?>
```

##  13. <a name='Nombredelscript'></a>Nombre del script

**Escribe un programa que escriba el nombre del archivo del propio script.**

```php
<?php
	echo $_SERVER[‘SCRIPT_FILENAME’];
?>
```












