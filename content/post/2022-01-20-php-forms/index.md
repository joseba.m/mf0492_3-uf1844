---
title: Creando formularios
subtitle: Procesar la información de los usuarios
date: 2022-01-20
draft: false
description: Procesar la información de los usuarios
tags: ["PHP","PHP - Formularios"]
---

La forma más común de interacción de los usuarios con PHP y MySQL es usando formularios HTML. 

## Creando formularios

Manejar formularios es un proceso de varios partes.La primera es la creación de un formulario donde el usuario introduce los datos. Estos datos son enviados al servidor donde son interpretados, normalmente también se realiza una comprobación de errores. Si el script PHP que procesa el formulario detecta que algún campo debe ser introducido de nuevo puede mostrar el formulario de nuevo con un mensaje de error. Cuando se determina que los datos introducidos son correctos normalmente se realiza algún tipo de acción, es muy común que se involucre una BD, por ejemplo introducir un nuevo usuario o un acceso a una aplicación que comprueba que el usuario y contraseña introducidos son correctas.

Para construir un formulario debe tener al menos los siguientes elementos:

* Una etiqueta de apertura `<form>` y de cierre `</form>`.
* El método HTTP que el navegador usa para enviar el formulario. Valores posibles son: [`POST`](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.5) y [`GET`](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html#sec9.3). 
* Uno o más campos o `inputs` de entrada de datos y un botón para desencadenar la acción.
* La URL de destino donde se envían los datos del formulario para ser procesados.

Un formulario sencillo:

```php
<?php // formtest.php
    echo <<<_END
    <html>
        <head>
            <title>Form Test</title>
        </head>
        
        <body>
            <form method="post" action="formtest.php">
                What is your name?
                <input type="text" name="name">
                <input type="submit">
            </form>
        </body>
    </html>
    _END;
?>
```

La primera novedad es que usamos la sintáxis [heredoc](https://www.php.net/manual/es/language.types.string.php#language.types.string.syntax.heredoc) 


## Enlaces externos

* [https://developer.mozilla.org/es/docs/Web/HTML/Element/form](https://developer.mozilla.org/es/docs/Web/HTML/Element/form): El elemento HTML form (`<form>`) representa una sección de un documento que contiene controles interactivos que permiten a un usuario enviar información a un servidor web.