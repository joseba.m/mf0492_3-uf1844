<?php 
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) {
    echo "Fatal Error ".$conn->connect_errno.": ".$conn->connect_error;
} else {
    echo "conectado!";
    echo $conn->host_info . "<br/>";

    $query = "SELECT * FROM usuarios";
    $result = $conn->query($query); //retorna FALSE o objeto con resultado    
    
    if (!$result) { 
        echo "Fatal Error ".$conn->error."<br/>";
    } else {
        //print_r($result);        
        
        echo "Número filas:".$result->num_rows."<br/>";

        $rows = $result->num_rows;

        for ($j = 0 ; $j < $rows ; ++$j) {

            $result->data_seek($j);
            echo 'Id: ' .htmlspecialchars($result->fetch_assoc()['Id']) .'<br>';
            
            $result->data_seek($j);
            echo 'User: ' .htmlspecialchars($result->fetch_assoc()['User']) .'<br>';
            
            $result->data_seek($j);
            echo 'Password: ' .htmlspecialchars($result->fetch_assoc()['Password']) .'<br>';            
        }
       
        // liberar el conjunto de resultados
        $result->close();
    }

    // Cerrar conexión con la BD
    $conn->close();
}
?>