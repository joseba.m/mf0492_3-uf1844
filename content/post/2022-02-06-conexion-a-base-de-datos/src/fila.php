<?php 
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) {
    echo "Fatal Error ".$conn->connect_errno.": ".$conn->connect_error;
} else {
    echo "conectado!";
    echo $conn->host_info . "<br/>";

    $query = "SELECT * FROM usuarios";
    $result = $conn->query($query); //retorna FALSE o objeto con resultado    
    
    if (!$result) { 
        echo "Fatal Error ".$conn->error."<br/>";
    } else {
        $rows = $result->num_rows;
        for ($j = 0 ; $j < $rows ; ++$j) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            echo 'Id: ' . htmlspecialchars($row['Id']) .'<br>';
            echo 'User: ' . htmlspecialchars($row['User']) .'<br>';
            echo 'Password: ' . htmlspecialchars($row['Password']) .'<br>';
        }
        $result->close();
    }
    $conn->close();
}
?>