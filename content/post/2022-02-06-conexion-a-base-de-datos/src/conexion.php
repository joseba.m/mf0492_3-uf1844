<?php 
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) {
    echo "Fatal Error ".$conn->connect_errno.": ".$conn->connect_error;
} else {
    echo "conectado!";
    echo $conn->host_info . "\n";
}

// Cierra una conexión previamente abierta a una base de datos
$conn->close(); 
?>