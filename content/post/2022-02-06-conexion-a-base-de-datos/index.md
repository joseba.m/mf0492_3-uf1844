---
title: Mi primera conexión con una base de datos
subtitle: PHP y Bases de datos MySQL
date: 2022-02-06
draft: false
description: PHP y Bases de datos MySQL
tags: ["PHP","PHP - BD","MySQL","Bases de Datos"]
---

<!-- vscode-markdown-toc -->
* 1. [Conexión con una base de datos](#Conexinconunabasededatos)
* 2. [Configuración](#Configuracin)
* 3. [Conexión](#Conexin)
* 4. [Creando y ejecutando una consulta](#Creandoyejecutandounaconsulta)
* 5. [Obteniendo los resultados](#Obteniendolosresultados)
* 6. [Obteniendo una fila](#Obteniendounafila)
* 7. [Cerrando la conexión](#Cerrandolaconexin)
* 8. [Referencias externas](#Referenciasexternas)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Conexinconunabasededatos'></a>Conexión con una base de datos

Para las primeras pruebas usaré el servicio de [https://app.infinityfree.net/](https://app.infinityfree.net/), este servicio ofrece un _hosting_ Web gratuito con PHP y bases de datos MySQL (con phpMyAdmin incluido para la gestión).

##  2. <a name='Configuracin'></a>Configuración

La mayoría de las Webs desarrolladas en PHP contienen múltiples scripts y programas que suelen requerir acceso a la base de datos, es lógico crear un archivo para almacenar los detalles de la conexión en un único sitio, si quieres usar este código reemplaza los parámetros de conexión por los tuyos.

Archivo [src/login.php](src/login.php):

```php
<?php 
// Me he creado una cuenta en https://app.infinityfree.net/ , ofece alojamiento gratuito con PHP y BD MySQL

// MySQL
$hn = 'sql208.epizy.com';  // Host 
$db = 'epiz_30511443_ctrlaccesos'; // Database
$un = 'epiz_30511443';  // Username
$pw = 'NxA2aVqSMTOxzWY'; // Password

// FTP
// FTP Hostname	ftpupload.net
// FTP Username	epiz_30511443
// FTP Password NxA2aVqSMTOxzWY
?>
```

##  3. <a name='Conexin'></a>Conexión

A continuación creo un nuevo archivo donde se ejecutará la acción de conexión a la base de datos, en primer lugar usaré la sentencia [`require_once`](https://www.php.net/manual/es/function.require-once.php) para incluir los datos de conexión de arriba en mi nuevo archivo.

Para realizar la conexión emplearé la clase [`mysqli`](https://www.php.net/manual/es/class.mysqli), cuando instancio la clase en el [método constructor](https://www.php.net/manual/es/mysqli.construct.php) le paso los parámetros de conexión definidos en el fichero de configuración, la llamada devuelve un objeto `$conn` que representa la conexión al servidor MySQL.

![](img/01.png)

**Compruebo si se ha producido un error en la conexión** consultando la propiedad [`connect_error`](https://www.php.net/manual/es/mysqli.connect-error.php) que devuelve una cadena con la descripción del último error de conexión. [`connect_errno`](https://www.php.net/manual/es/mysqli.connect-errno.php) Devuelve el código de error de la última llamada, prueba a modificar los parámetros de conexión para provocar un error de forma intencionada para comprobar el funcionamiento.

Archivo [src/conexion.php](src/conexion.php):

```php
<?php 
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) {
    echo "Fatal Error ".$conn->connect_errno.": ".$conn->connect_error;
} else {
    echo "conectado!";
    echo $conn->host_info . "\n";
}

// Cierra una conexión previamente abierta a una base de datos
$conn->close(); 
?>
```

Mi script de prueba ya se puede usar en [http://ikerlandajuela.lovestoblog.com/conexion.php](http://ikerlandajuela.lovestoblog.com/conexion.php).

##  4. <a name='Creandoyejecutandounaconsulta'></a>Creando y ejecutando una consulta

Siguiendo con el ejemplo previo voy a realizar una consulta SQL. El método que usaré será [`query`](https://www.php.net/manual/es/mysqli.query.php) que recibe como parámetro una cadena con la consulta SQL.

He creado una tabla a modo de ejemplo para realizar la consulta de arriba, dejo el archivo con la base de datos exportada usando phpMyAdmin: [sql/usuarios.sql](sql/usuarios.sql).

Como se puede ver, la consulta de MySQL se asemeja a lo que se teclearía directamente en la línea de comandos MySQL, excepto que no hay un punto y coma final, ya que no se necesita cuando se accede a MySQL desde PHP (si la pones támpoco pasa nada).

Aquí asigno un _string_ a la variable `$query` que contiene la consulta, después se la paso al método `query` del objeto `$conn`, el resultado devuelto lo guardo en la variable `$result`, si `$result` es `FALSE` quiere decir que se ha producido algún problema, la propiedad `error`del objeto conexión contendrá los detalles (prueba a hacer una consulta con la sintaxis incorrecta). 

Si todo va bien ahora tenemos los datos que retorna MySQL en un objeto `$result` (la clase [mysqli_result](https://www.php.net/manual/es/class.mysqli-result.php)).

Archivo [src/consulta.php](src/consulta.php):

```php
$query = "SELECT * FROM usuarios";
$result = $conn->query($query); //retorna FALSE o objeto con resultado    
    
if (!$result) { 
    echo "Fatal Error ".$conn->error."<br/>";
} else {
    // obtener registros resultado ... 
}
```

##  5. <a name='Obteniendolosresultados'></a>Obteniendo los resultados

Ahora que tiene el resultado de la consulta almacenado en el objeto `$result`, puedo usarlo para extraer los datos que quiera, uno cada vez, usando el método [`fetch_assoc`](https://www.php.net/manual/es/mysqli-result.fetch-assoc.php).  

Archivo [src/consulta.php](src/consulta.php):

```php
<?php 
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) {
    echo "Fatal Error ".$conn->connect_errno.": ".$conn->connect_error;
} else {
    echo "conectado!";
    echo $conn->host_info . "<br/>";

    $query = "SELECT * FROM usuarios";
    $result = $conn->query($query); //retorna FALSE o objeto con resultado    
    
    if (!$result) { 
        echo "Fatal Error ".$conn->error."<br/>";
    } else {
        //print_r($result);        
        
        echo "Número filas:".$result->num_rows."<br/>";

        $rows = $result->num_rows;

        for ($j = 0 ; $j < $rows ; ++$j) {

            $result->data_seek($j);
            echo 'Id: ' .htmlspecialchars($result->fetch_assoc()['Id']) .'<br>';
            
            $result->data_seek($j);
            echo 'User: ' .htmlspecialchars($result->fetch_assoc()['User']) .'<br>';
            
            $result->data_seek($j);
            echo 'Password: ' .htmlspecialchars($result->fetch_assoc()['Password']) .'<br>';            
        }
       
        // liberar el conjunto de resultados
        $result->close();
    }

    // Cerrar conexión con la BD
    $conn->close();
}
?>
```

La salida que genera el script de arriba:

![](img/02.png)

Cada iteración del bucle llamamos al método `fetch_assoc` para obtener el valor almacenado en cada celda y lo mostramos con `echo`.

La función [`htmlspecialchars`](https://www.php.net/manual/es/function.htmlspecialchars) convierte caracteres especiales en entidades HTML.

Habrás advertido que uso el método [`data_seek`](https://www.php.net/manual/es/mysqli-result.data-seek.php) para moverme por los registros resultantes, debe haber una forma más eficiente de hacer lo mismo.

##  6. <a name='Obteniendounafila'></a>Obteniendo una fila

Ahora **extraigo una fila completa cada iteracción del bucle**, el código es mucho más simple y contiene menos instrucciones. En comparación con el script previo, ahora sólo me posiciono sobre la fila que busco una vez en cada iteración del bucle, obtengo la fila completa usando el método [`fetch_array`](https://www.php.net/manual/es/mysqli-result.fetch-array.php). El método retorna una fila completa de datos como un _array_, que asigno a la variable `$row`.

Archivo: [src/fila.php](src/fila.php).

```php
<?php 
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) {
    echo "Fatal Error ".$conn->connect_errno.": ".$conn->connect_error;
} else {
    echo "conectado!";
    echo $conn->host_info . "<br/>";

    $query = "SELECT * FROM usuarios";
    $result = $conn->query($query); //retorna FALSE o objeto con resultado    
    
    if (!$result) { 
        echo "Fatal Error ".$conn->error."<br/>";
    } else {
        $rows = $result->num_rows;
        for ($j = 0 ; $j < $rows ; ++$j) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            echo 'Id: ' . htmlspecialchars($row['Id']) .'<br>';
            echo 'User: ' . htmlspecialchars($row['User']) .'<br>';
            echo 'Password: ' . htmlspecialchars($row['Password']) .'<br>';
        }
        $result->close();
    }
    $conn->close();
}
?>
```

El método  [`fetch_array`](https://www.php.net/manual/es/mysqli-result.fetch-array.php) puede retornar tres tipos de _array_ dependiendo del valor que le pasemos.

* `MYSQLI_NUM`: Array numérico. Cada columna se ordena de la misma forma en la que creaste la tabla.
* `MYSQLI_ASSOC`: Array asociativo. Cada clave será el nombre de la columna. 
* `MYSQLI_BOTH`: Array numérico y asociativo.

Normalmente usaremos casi siempre arrays asociativos ya que es más sencillo referirse a las columnas por su nombre. 

##  7. <a name='Cerrandolaconexin'></a>Cerrando la conexión

PHP liberará de forma automática la memoria reservada por el objeto cuando finaliza el script, en pequeños scripts normálmente no debes preocuparte de hacerlo tu mismo. Sin embargo, en proyectos donde estés trabajando con grandes conjuntos de datos y/o crees muchos objetos para alojar lo resultados, es una buena idea liberar la memoria para prevenir problemas más adelante. Esto es particularmente importante en sitios con mucho tráfico porque la memoria consumida puede subir rápidamente. 

En los scripts de este artículo hemos usado el método `close` en los objetos `$result` y `$conn` cuando ya no eran necesarios, así:

```php
$result->close();
$conn->close();
```


##  8. <a name='Referenciasexternas'></a>Referencias externas

* [mysqli::__construct](https://www.php.net/manual/es/mysqli.construct.php).
* [require_once](https://www.php.net/manual/es/function.require-once.php).
* [Extensión MySQL mejorada](https://www.php.net/manual/es/book.mysqli.php).
* [mysqli::$connect_error](https://www.php.net/manual/es/mysqli.connect-error.php): Devuelve una cadena con la descripción del último error de conexión-
* [mysqli::$connect_errno](https://www.php.net/manual/es/mysqli.connect-errno.php): Devuelve el código de error de la última llamada.
* [La clase mysqli_result](https://www.php.net/manual/es/class.mysqli-result.php): Representa el conjunto de resultados obtenidos a partir de una consulta en la base de datos.
* [mysqli_result::fetch_assoc](https://www.php.net/manual/es/mysqli-result.fetch-assoc.php): Obtener una fila de resultado como un array asociativo.
* [htmlspecialchars](https://www.php.net/manual/es/function.htmlspecialchars): Convierte caracteres especiales en entidades HTML.
* [mysqli_result::data_seek](https://www.php.net/manual/es/mysqli-result.data-seek.php): Ajustar el puntero de resultado a una fila arbitraria del resultado
* [mysqli_result::fetch_array](https://www.php.net/manual/es/mysqli-result.fetch-array.php): Obtiene una fila de resultados como un array asociativo, numérico, o ambos.
* [mysqli::close](https://www.php.net/manual/es/mysqli.close.php) — Cierra una conexión previamente abierta a una base de datos.