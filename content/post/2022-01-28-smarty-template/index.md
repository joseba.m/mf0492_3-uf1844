---
title: Smarty
subtitle: Template engine
date: 2022-01-28
draft: false
description: Template engine
tags: ["PHP","PHP - Plantillas","PHP - Templates","Smarty"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Smarty](#Smarty)
	* 2.1. [¿Que es Smarty?](#QueesSmarty)
	* 2.2. [Instalación básica](#Instalacinbsica)
	* 2.3. [Instalación con Composer (pendiente)](#InstalacinconComposerpendiente)
	* 2.4. [Comprobando la instalación](#Comprobandolainstalacin)
	* 2.5. [Ejemplo básico](#Ejemplobsico)
	* 2.6. [Generar conjuntos de datos](#Generarconjuntosdedatos)
		* 2.6.1. [Arrays asociativos](#Arraysasociativos)
	* 2.7. [Funciones predefinidas](#Funcionespredefinidas)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


##  1. <a name='Introduccin'></a>Introducción

Un sistema de plantillas (_template engine_) mejora la legibilidad de nuestro código separando la parte visual HTML de la lógica PHP. Hasta el momento hemos trabajado incrustando HTML y PHP en la misma página.

Otra ventaja es que es más fácil realizar modificaciones cuando separamos los componentes o capas de nuestra aplicación.

**¿Cómo funciona un sistema de plantillas?** Básicamente hace algo como lo de abajo, la plantilla es un fichero HTML que se abre desde PHP, con algunas partes donde se reemplaza su contenido de forma dinámica, en el ejemplo inferior, se reemplazan los textos entre "{..}" por el contenido de variables en tiempo de ejecución.

Plantilla:

```
<html>
<head>
    <title>{page_title}</title>
</head>
<body>
    <h1>Hello, {name}</h1>
</body>
</html>
```

Gestor de la plantilla:

```php
<?php
// Cargamos el fichero con la plantilla
$page = file_get_contents('page-template.html');
$page = str_replace('{page_title}', 'Welcome', $page);
$page = str_replace('{name}', 'Iker', $page);
// Imprimimos los resultados 
print $page;
?>
```

##  2. <a name='Smarty'></a>Smarty

###  2.1. <a name='QueesSmarty'></a>¿Que es Smarty?

[Smarty](https://www.smarty.net/) es un motor de plantillas para PHP. Mas específicamente, esta herramienta facilita la manera de **separar la aplicación lógica y el contenido en la presentación**. La mejor descripción esta en una situación donde la aplicación del programador y la plantilla del diseñador juegan diferentes roles, o en la mayoría de los casos no la misma persona.

Fuente: [¿Que es Smarty?](https://www.smarty.net/docsv2/es/what.is.smarty.tpl). Obtenido de la [Web oficial de Smarty](https://www.smarty.net/).

**Algunas de las características de Smarty:**

* Sintaxis de etiquetas delimitadoras para configuración de la plantilla, así lo puede usar {}, {{}}, <!--{}-->, etc.
* Bucles foreach, while.
* Sentencias condicionales if, elseif, else
* Expresiones regulares
* Modificadores de variables (por ejemplo: `{$variable|nl2br}`)
* Funciones creadas por el usuario
* Evaluación de expresiones matemáticas en la plantilla

###  2.2. <a name='Instalacinbsica'></a>Instalación básica 

* Descarga: [https://www.smarty.net/download](https://www.smarty.net/download). El repositorio con el código fuente está alojado en GitHub [https://github.com/smarty-php/smarty](https://github.com/smarty-php/smarty).
* Descomprimimos y copiamos carpeta smarty a C:\xampp (la ruta es opcional pero aconsejable para tener las librerías de Smarty junto con XAMPP en la misma carpeta).
* Comprobar con `phpinfo` la ruta de php.ini (Loaded Configuration File	C:\xampp\php\php.ini).
* Añadir la siguiente línea `include_path = C:\xampp\smarty\libs`.

![](img/01.png)

* El último paso es reiniciar el servidor apache para que tengan efecto los cambios. Lo haremos desde el propio XAMPP haciendo click en «Stop» y luego «Start«.

Información adicional:

* [Instalación Básica](https://www.smarty.net/docsv2/es/installing.smarty.basic).


###  2.3. <a name='InstalacinconComposerpendiente'></a>Instalación con Composer (pendiente)

[https://packagist.org/packages/smarty/smarty](https://packagist.org/packages/smarty/smarty).

![](img/03.png)

###  2.4. <a name='Comprobandolainstalacin'></a>Comprobando la instalación

Instancia de la clase Smarty:

[src/01.php](src/01.php):

```php
<?php \\ 01.php
require_once('Smarty.class.php');
$smarty = new Smarty();
?>
```

Si has configurado correctamente Smarty el script de arriba no debería mostrar un error, en caso de encontrar la ruta a "Smarty.class.php" debería arrojar un error.

###  2.5. <a name='Ejemplobsico'></a>Ejemplo básico

[src/02.php](src/02.php):

```php
<?php //02.php
include('Smarty.class.php');

// create object
$smarty = new Smarty;

// assign some content. This would typically come from
// a database or other source, but we'll use static
// values for the purpose of this example.
$smarty->assign('name', 'george smith');
$smarty->assign('address', '45th & Harris');

// display it
$smarty->display('index.tpl');
?>
```

[src/02.tpl](src/02.tpl):

```
<html>
    <head>
        <title>Info</title>
    </head>
<body>
    <pre>
        User Information:

        Name: {$name}
        Address: {$address}
    </pre>
</body>
</html>
```

Los elementos entre {} como `{$name}` son reemplazados por el contenido de variables.

###  2.6. <a name='Generarconjuntosdedatos'></a>Generar conjuntos de datos

[Smarty](https://www.smarty.net/) es un potente motor de plantillas, además de reemplazar variables en la plantilla, permite generar un conjunto de datos basado en los valores de un array, veamos un ejemplo de como generar un selector HTML.

[src/03.php](src/03.php)

```php
<?php //03.php
include('Smarty.class.php');

// create object
$smarty = new Smarty;

// assign options arrays
$smarty->assign('id', array(1,2,3,4,5));
$smarty->assign('names', array('bob','jim','joe','jerry','fred'));

// display it
$smarty->display('03.tpl');
?>
```

[src/03.tpl](src/03.tpl)

```
<html>
    <head>
        <title>Info</title>
    </head>
    <body>
        <select name=user>
            {html_options values=$id output=$names selected="5"}
        </select>
    </body>
</html>
```

En las opciones del HTML a generar en el select establecemos el valor como el índice del array  y el valor pre-seleccionado con [`html_options`](https://www.smarty.net/docsv2/es/language.function.html.options.tpl).

![](img/02.png)

El HTML resultante es el siguiente:

```html
<html>
    <head>
        <title>Info</title>
    </head>
    <body>
        <select name=user>
            <option value="1">bob</option>
			<option value="2">jim</option>
			<option value="3">joe</option>
			<option value="4">jerry</option>
			<option value="5" selected="selected">fred</option>
        </select>
    </body>
</html>
```

Otro ejemplo obtenido de la documentación [https://www.smarty.net/docsv2/es/language.function.html.options.tpl](https://www.smarty.net/docsv2/es/language.function.html.options.tpl).



####  2.6.1. <a name='Arraysasociativos'></a>Arrays asociativos

###  2.7. <a name='Funcionespredefinidas'></a>Funciones predefinidas



##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.smarty.net/docsv2/es/installing.smarty.basic](https://www.smarty.net/docsv2/es/installing.smarty.basic).
* [https://smarty-php.github.io/smarty/](https://smarty-php.github.io/smarty/)
* [https://smarty-php.github.io/smarty/philosophy.html](https://smarty-php.github.io/smarty/philosophy.html)
* [https://smarty-php.github.io/smarty/designers/language-builtin-functions.html](https://smarty-php.github.io/smarty/designers/language-builtin-functions.html)
* [https://www.smarty.net/crash_course](https://www.smarty.net/crash_course).
* [https://codesamplez.com/development/smarty-basics-tutorial](https://codesamplez.com/development/smarty-basics-tutorial).
* [https://code.tutsplus.com/tutorials/introduction-to-the-smarty-templating-framework--net-14408](https://code.tutsplus.com/tutorials/introduction-to-the-smarty-templating-framework--net-14408).