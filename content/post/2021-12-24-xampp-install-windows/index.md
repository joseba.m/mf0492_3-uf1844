---
title: Instalación XAMPP en Windows
subtitle: Servidor HTTP portable
date: 2021-12-24
draft: false
description: Servidor HTTP portable
tags: ["Windows","XAMPP","Servidor","Web","HTTP","Herramientas","Aplicaciones"]
---

<!-- vscode-markdown-toc -->
* 1. [Instalación de XAMPP](#InstalacindeXAMPP)
* 2. [El panel de control por primera vez](#Elpaneldecontrolporprimeravez)
	* 2.1. [Instalar como servicio del sistema operativo](#Instalarcomoserviciodelsistemaoperativo)
	* 2.2. [Utilidades](#Utilidades)
		* 2.2.1. [Netstat](#Netstat)
		* 2.2.2. [Shell](#Shell)
* 3. [Ajustes principales](#Ajustesprincipales)
	* 3.1. [Panel de control](#Paneldecontrol)
	* 3.2. [Apache](#Apache)
* 4. [Desinstalar XAMPP](#DesinstalarXAMPP)
* 5. [Recursos](#Recursos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[XAMPP](https://www.apachefriends.org/es/about.html) es un paquete de software libre bajo licencia GNU GPL, en el _stack_ de diferentes tecnologías predominan las que usaremos: un SGBD basado en [MySQL](https://www.mysql.com/), un servidor Web HTTP [Apache](https://apache.org/) con el módulo interprete de _scripts_ escritos en [PHP](https://www.php.net/). De echo el nombre XAMPP es un acrónimo: X (multiplataforma para cualquier sistema operativo), Apache, MariaDB/MySQL, PHP y Perl. A partir de la versión 5.6.15 XAMPP cambio la BD basada en MySQL por MariaDB, un _fork_ del primero pero con licencia GPL (MySQL fue comprado por Oracle).

Os dejo por aquí otro manual parecido [doc/Instalación_y_uso_de_XAMPP_en_Windows.pdf](doc/Instalación_y_uso_de_XAMPP_en_Windows.pdf).

##  1. <a name='InstalacindeXAMPP'></a>Instalación de XAMPP

Puedes descargar el paquete usando este enlace [XAMPP download | SourceForge.net](https://sourceforge.net/projects/xampp/), en el momento de escribir este tutorial el archivo instalador para M$ Win es este: "xampp-windows-x64-7.4.25-0-VC15-installer.exe". 

Después de descargar el archivo, ejecutalo para que comience la instalación. Para evitar la elevación de privilegios usando el control de cuentas de usuarios ([UAC](https://docs.microsoft.com/es-es/windows/security/identity-protection/user-account-control/how-user-account-control-works)) seguir la sugerencia de instalación en "C:\xampp".

![](img/05.PNG)

Continuar:

![](img/06.PNG)

Instala todos los componentes predeterminados por defecto (a pesar de que no usaremos algunos de ellos como Perl):

![](img/07.PNG)

Selecciona la carpeta de instalación sugerida:

![](img/08.PNG)

Sólo está traducido al Alemán e Inglés.

![](img/09.PNG)

![](img/10.PNG)

![](img/11.PNG)

![](img/12.PNG)

Cuando finalize la instalación ejecutaremos el panel de administración:

![](img/13.PNG)

##  2. <a name='Elpaneldecontrolporprimeravez'></a>El panel de control por primera vez

Este es el aspecto del panel de control la primera vez que lo ejecutamos:

![](img/14.PNG)

El panel de control de XAMPP se compone de tres áreas principales:

* **Modulos**, para cada módulo XAMPP muestra: si está instalado como servicio del sistema operativo, el nombre del módulo, identificador del proceso (PID), puerto usado y una serie de botones para para arrancar o parar la ejecución, para administrar la aplicación, edición del fichero de configuración y para abrir el registro de _logs_.
* **Notificaciones**, non XAMPP-k egindako ekintzen arrakastaren edo porrotaren berri ematen duen.
* Acceso rápido a **Aplicaciones y utilidades** adicionales,

A continuación arrancaremos las aplicaciones imprescindibles para poder trabajar, Apache y MySQL. 

Una ventana emergente nos advierte que debemos permitir algunas aplicaciones en el cortafuegos de M$ Win.

![](img/15.PNG)

Podemos visualizar las nuevas reglas en el firewall (una para cada protocolo TCP o UDO). Win + R y escribe `control firewall.cpl` para abrir el firewall.

![](img/22.PNG)

"Apache HTTP Server" y "mysqld":

![](img/23.PNG)

Para cerciorarnos que todo esté funcionando correctamente abre la siguiente URL en el navegador [http://localhost/](http://localhost/) (o edo [http://127.0.0.1/](http://127.0.0.1/)), o pulsa el botón "Admin" junto a "Apache" en el panel de control.

![](img/16.PNG)

Desde la página por defecto de XAMPP haz clic en la opción de  [PHPInfo](http://localhost/dashboard/phpinfo.php) para visualizar todos los detalles de la configuración de PHP:

![](img/17.PNG)

Accede también a phpMyAdmin [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/) para comprobar que MySQL funciona.

![](img/19.PNG)

Para abrir el panel de control usa la siguiente aplicación "c:\xampp\xampp-control.exe", puedes crear un acceso directo en el escritorio o donde sea para tenerlo siempre a mano.

Para arrancar o parar los servicios uno a uno:

* Apache & MySQL start: "c:\xampp\xampp_start.exe".
* Apache & MySQL stop: "c:\xampp\xampp_stop.exe".
* Apache start: "c:\xampp\apache_start.bat".
* Apache stop: "c:\xampp\apache_stop.bat".
* MySQL start: "c:\xampp\mysql_start.bat".
* MySQL stop: "c:\xampp\mysql_stop.bat".

###  2.1. <a name='Instalarcomoserviciodelsistemaoperativo'></a>Instalar como servicio del sistema operativo

Si queremos instalar las aplicaciones como servicio (no es obligatorio) para que lancen cuando arrancamos el sistema operativo de forma automática debemos marcar la casilla  Service, para ello debemos arrancar XAMPP con administradores del sistema.

![](img/24.PNG)

Para visualizar los servicios de Win usa: Win+R y escribe `services.msc`.

###  2.2. <a name='Utilidades'></a>Utilidades

####  2.2.1. <a name='Netstat'></a>Netstat

[netstat](https://es.wikipedia.org/wiki/Netstat) (**net**work **stat**istics) es una herramienta de línea de comandos que muestra un listado de las conexiones activas de una computadora, tanto entrantes como salientes.

Si tienes alguna duda sobre el funcionamiento de los servidores Apache y MySQL puedes comprobar que los puertos que usan están abiertos. El servidor HTTP Apache normalmente usa el 80 (se puede cambiar) y MySQL (mysqld.exe) el 3306.

![](img/18.PNG)

####  2.2.2. <a name='Shell'></a>Shell

Interfaz de línea de comandos:

![](img/20.PNG)

Desde aquí también podemos acceder al interprete de comandos de MySQL sin usar phpMyAdmin, recién instalado el usuario es "root" y no tiene contraseña.

```bash
# mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 41
Server version: 10.4.21-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
5 rows in set (0.001 sec)

MariaDB [(none)]>
``` 

##  3. <a name='Ajustesprincipales'></a>Ajustes principales

###  3.1. <a name='Paneldecontrol'></a>Panel de control 

Pulsa el botón "Config" para abrir la ventana de ajustes.

###  3.2. <a name='Apache'></a>Apache

**Apache zerbitzuak funtzionatzen duen portua aldatzea**.

Apache "Config" botoia sakatu, httpd.conf aukeratu editatzeko, bilatu "Listen" ireki berri den testu fitxategian eta nahi duzun portua jarri (8080 ere ohikoa izaten da). Apache zerbitzua geldiarazi eta berriro abiarazi, orain "http://localhost:8080/" zabaldu ondo doala frogatzeko.

![](img/21.PNG)

##  4. <a name='DesinstalarXAMPP'></a>Desinstalar XAMPP

Ejecuta "C:\xampp\uninstall.exe" para comenzar la desinstalación.

![](img/01.PNG)

Si queremos borrar los sitios Web que hayamos creado pulsa "Yes":

![](img/02.PNG)

El desintalador borra todas las entradas de registro de XAMPP y elimina algunos servicios.

![](img/03.PNG)

Fin:

![](img/04.PNG)


##  5. <a name='Recursos'></a>Recursos

* [https://bitnami.com/stack/xampp](https://bitnami.com/stack/xampp).




