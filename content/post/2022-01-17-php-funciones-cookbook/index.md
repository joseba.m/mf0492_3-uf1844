---
title: Funciones. Libro de recetas
subtitle: Programación modular
date: 2022-01-17
draft: false
description: Programación modular
tags: ["PHP", "PHP - Modularidad","PHP - Functions","PHP - Funciones"]
---

<!-- vscode-markdown-toc -->
* 1. [Introducción](#Introduccin)
* 2. [Parámetros con nombre](#Parmetrosconnombre)
	* 2.1. [Problema](#Problema)
	* 2.2. [Solución](#Solucin)
* 3. [Funciones variadic](#Funcionesvariadic)
* 4. [Retornando más de un valor](#Retornandomsdeunvalor)
* 5. [Omitir variables de retorno](#Omitirvariablesderetorno)
* 6. [Retornando fallo de función](#Retornandofallodefuncin)
* 7. [Funciones variables](#Funcionesvariables)
* 8. [Accediendo a una variable global dentro de una función](#Accediendoaunavariableglobaldentrodeunafuncin)
* 9. [Creando funciones dinámicas](#Creandofuncionesdinmicas)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduccin'></a>Introducción

##  2. <a name='Parmetrosconnombre'></a>Parámetros con nombre

###  2.1. <a name='Problema'></a>Problema

Quieres especificar los argumentos de una función por su nombre, no por su posición en la llamada a la función.

###  2.2. <a name='Solucin'></a>Solución

Puedes hacer que la función reciba un sólo parámetro que sea un array asociativo.

```php
function image($img) {
	$tag = '<img src="' . $img['src'] . '" ';
	$tag .= 'alt="' . ($img['alt'] ? $img['alt'] : '') .'">';
	return $tag;
}

$image = image(array('src' => 'cow.png', 'alt' => 'cows say moo'));
$image = image(array('src' => 'pig.jpeg'));
```

Usar parámetros con nombres hace que el código dentro de la función sea más complejo. La llamada a la función es más fácil de leer. Como la declaración de la función reside en una parte pero se puede invocar tantas veces que queramos, en general hace el código más legible.



##  3. <a name='Funcionesvariadic'></a>Funciones variadic 

##  4. <a name='Retornandomsdeunvalor'></a>Retornando más de un valor

##  5. <a name='Omitirvariablesderetorno'></a>Omitir variables de retorno 

##  6. <a name='Retornandofallodefuncin'></a>Retornando fallo de función

##  7. <a name='Funcionesvariables'></a>Funciones variables

##  8. <a name='Accediendoaunavariableglobaldentrodeunafuncin'></a>Accediendo a una variable global dentro de una función

##  9. <a name='Creandofuncionesdinmicas'></a>Creando funciones dinámicas

