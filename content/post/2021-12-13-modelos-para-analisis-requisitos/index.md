---
title: Modelos para el análisis de requisitos
subtitle: Análisis y especificación de requisitos
date: 2021-12-13
draft: false
description: Análisis y especificación de requisitos
tags: ["UF1844","Requisitos","Análisis","Requisitos - Modelos"]
---

<!-- vscode-markdown-toc -->
* 1. [Modelos para el análisis de requisitos](#Modelosparaelanlisisderequisitos)
* 2. [Análisis del dominio](#Anlisisdeldominio)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Modelosparaelanlisisderequisitos'></a>Modelos para el análisis de requisitos

El modelado de los sistemas realizados mediante software también tiene como objetivo entender mejor el funcionamiento requerido y facilitar la comprensión de los problemas planteados. El modelo, es completo y preciso del comportamiento u organización del sistema software. Existen diversas metodologías para realizar el análisis de los requisitos que debe cumplir un proyecto software. Todas tienen un aspecto común: facilitar la obtención de uno o varios modelos que detallen el comportamiento deseado del sistema.

Un [**modelo conceptual**](https://es.wikipedia.org/wiki/Modelo_conceptual) es una representación de un sistema, hecho de la composición de conceptos que se utilizan para ayudar a las personas a conocer, comprender o simular un tema que representa el modelo, incluye las entidades importantes y las relaciones entre ellos. También es un conjunto de conceptos. 

El modelo de un sistema software establece las propiedades y restricciones del sistema. **Visión de alto nivel**. El modelo debe explicar **QUÉ** debe hacer el sistema y **no CÓMO** lo debe de hacer.

**Objetivos** que se deben cubrir con los modelos:

* Facilitar la comprensión del modelo a resolver.
* Establecer un marco para la discusión, que simplifique y sistematice tanto la labor del análisis inicial como las futuras revisiones del mismo.
* Fijar las bases para realizar el diseño.
* Facilitar la verificación del cumplimiento de los objetivos del sistema.


##  2. <a name='Anlisisdeldominio'></a>Análisis del dominio

TODO