---
title: Modelo en cascada (waterfall)
subtitle: Modelos del ciclo de vida del software
date: 2021-12-05
draft: false
description: Modelos del ciclo de vida del software
tags: ["UF1844","Modelos del ciclo de vida","Waterfall","Modelo en cascada"]
---

<!-- vscode-markdown-toc -->
* 1. [Descripción de las fases](#Descripcindelasfases)
* 2. [Reflexiones sobre el modelo](#Reflexionessobreelmodelo)
* 3. [Variantes](#Variantes)
	* 3.1. [Modelo Interactivo](#ModeloInteractivo)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

El [**desarrollo en cascada**](https://es.wikipedia.org/wiki/Desarrollo_en_cascada) (denominado así por la posición de las fases en el desarrollo de esta, el flujo del sistema parece caer en cascada “por gravedad” hacia las siguientes fases), también llamado **secuencial**, de tal forma que **el inicio de cada etapa debe esperar a la finalización de la etapa anterior**. Se trata del primer modelo y que derivó en otros modelos posteriores más complejos o avanzados. 

##  1. <a name='Descripcindelasfases'></a>Descripción de las fases

El **modelo de la cascada**, a veces llamado ciclo de vida clásico, sugiere un enfoque sistemático y secuencial para el desarrollo del software, que comienza con la especificación de los requerimientos por parte del cliente y avanza a través de planeación, modelado, construcción y despliegue, para concluir con el apoyo del software terminado

![](img/04.png)

Se trata de un **modelo líneal** y cuyas etapas se transforman en actividades del desarrollo, y son:

* **Ánalisis y formulación de requisitos**: definiendo los servicios, metas y excepciones del sistema que se pueden acotar tras las consultas con los usuarios / clientes. Se detallán de forma explícita estos requisitos sirviendo como especificaciones del sistema. De esta fase surge una memoria llamada SRD (documento de especificación de requisitos), que contiene la especificación completa de lo que debe hacer el sistema sin entrar en detalles internos. Es importante señalar que en esta etapa **se debe consensuar todo lo que se requiere del sistema** y será aquello lo que seguirá en las siguientes etapas, no pudiéndose requerir nuevos resultados a mitad del proceso de elaboración del software de una manera.
* **Diseño del sistema**: Diferencia entre los requisitos software y hardware. Formará una arquitectura completa del sistema, identificando y formulando las abstracciones fundamentales del sistema software, así como las relaciones del mismo. Como resultado surge el SDD ([Descripción del diseño del software](https://es.wikipedia.org/wiki/Descripci%C3%B3n_del_dise%C3%B1o_del_software)), que contiene la descripción de la estructura relacional global del sistema y la especificación de lo que debe hacer cada una de sus partes, así como la manera en que se combinan unas con otras. Es conveniente **distinguir entre diseño de alto nivel o arquitectónico y diseño detallado**. El primero de ellos tiene como objetivo definir la estructura de la solución (una vez que la fase de análisis ha descrito el problema) identificando grandes módulos (conjuntos de funciones que van a estar asociadas) y sus relaciones. Con ello se define la arquitectura de la solución elegida. El segundo define los algoritmos empleados y la organización del código para comenzar la implementación.
* **Implementación y pruebas unitarias**: en esta etapa se realizan las pruebas como un conjunto o unidades del programa. Las pruebas unitarias implican el **verificar que cada unidad cumpla con sus especificaciones o especificaciones asociadas**.
* **Integración y prueba del sistema en su conjunto**: en esta etapa los programas se integran para formar el sistema completo, y así **probar las funcionalidades generales del sistema**. En esta etapa se asegura la validación completa de los requisitos del software.

Tras estas pruebas se entrega al cliente.

* **Puesta en servicio (despliegue) y Mantenimiento**: suele ser la fase más larga. El sistema se instala (despliegue en producción) y se pone en funcionamiento. El proceso de mantenimiento implica corregir errores no descubiertos en las etapas previas.

![](img/01.svg)

##  2. <a name='Reflexionessobreelmodelo'></a>Reflexiones sobre el modelo

Como puede apreciarse, se trata de un modelo totalmente secuencial, en el que ha de terminar una fase para iniciar la siguiente. **No establece la retroalimentación entre fases y la redefinición de las anteriores**.

De esta forma, **cualquier error de diseño detectado en la etapa de prueba conduce necesariamente al rediseño y nueva programación del código afectado, aumentando los costos del desarrollo**. La palabra _cascada_ sugiere, mediante la metáfora de la fuerza de la gravedad, el esfuerzo necesario para introducir un cambio en las fases más avanzadas de un proyecto.

Si bien ha sido ampliamente criticado desde el ámbito académico y la industria, **sigue siendo el paradigma más seguido al día de hoy por su sencillez**, es fácil de implementar y entender. 

Este modelo promueve una metodología de trabajo efectiva: Definir antes que diseñar, diseñar antes que codificar. 

**Desventajas**:

* En la vida real, **un proyecto rara vez sigue una secuencia lineal**, esto crea una mala implementación del modelo, lo cual hace que lo lleve al fracaso.
* El proceso de creación del software tarda mucho tiempo ya que debe pasar por el proceso de prueba y **hasta que el software no esté completo no se opera**. Esto es la base para que funcione bien.
* Cualquier **error de diseño detectado en la etapa de prueba** conduce necesariamente al rediseño y nueva programación del código afectado, aumentando los costos del desarrollo.
* Una etapa determinada del proyecto no se puede llevar a cabo a menos de que se haya culminado la etapa anterior.

![](img/03.png)

##  3. <a name='Variantes'></a>Variantes

###  3.1. <a name='ModeloInteractivo'></a>Modelo Iterativo

Una variante de este modelo es el denominado "Modelo en Cascada Retroalimentado", que permite la retroalimentación y anidación entre fases. Pasa a ser un modelo interativo, y no líneal.

![](img/02.png)


##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [https://web.archive.org/web/20131203234610/http://sistemas.uniandes.edu.co/~isis2603/dokuwiki/lib/exe/fetch.php?media=principal:isis2603-modelosciclosdevida.pdf](https://web.archive.org/web/20131203234610/http://sistemas.uniandes.edu.co/~isis2603/dokuwiki/lib/exe/fetch.php?media=principal:isis2603-modelosciclosdevida.pdf).



