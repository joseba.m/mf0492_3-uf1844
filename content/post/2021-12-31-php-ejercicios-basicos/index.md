---
title: Ejercicios básicos
subtitle: Variables, operadores...
date: 2021-12-31
draft: false
description: Variables, operadores...
tags: ["PHP", "PHP - Ejercicios"]
---

<!-- vscode-markdown-toc -->
* 1. [Inicio y fin script](#Inicioyfinscript)
* 2. [Comentarios código fuente](#Comentarioscdigofuente)
* 3. [ Final de expresión](#Finaldeexpresin)
* 4. [Declaración de variables](#Declaracindevariables)
* 5. [Operadores](#Operadores)
* 6. [Case sensitive](#Casesensitive)
* 7. [Nombres de variables no permitidas](#Nombresdevariablesnopermitidas)
* 8. [Casting](#Casting)
* 9. [Operadores pre y post incremento](#Operadorespreypostincremento)
* 10. [Operadores](#Operadores-1)
* 11. [Suma cadena y número](#Sumacadenaynmero)
* 12. [Hola mundo HTML](#HolamundoHTML)
* 13. [Errores](#Errores)
* 14. [Precio comida](#Preciocomida)
* 15. [Operaciones](#Operaciones)
* 16. [Información PHP](#InformacinPHP)
* 17. [Usuario](#Usuario)
* 18. [Longitud cadena](#Longitudcadena)
* 19. [Potencias](#Potencias)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Inicioyfinscript'></a>Inicio y fin script

**¿Cuáles son las etiquetas que se usan para comenzar y finalizar un script en PHP? Realiza un ejemplo de un programa que escriba tu nombre, Ejemplo “Hola Igotz Etxebarria”.**

```php
<?php
	echo “Hola Iker Landajuela”;
?>
```

##  2. <a name='Comentarioscdigofuente'></a>Comentarios código fuente

**¿Cuáles son los dos tipos de comentarios? Usa un ejemplo de cada uno.**

```php
<?php
// Comentario de una línea

/* Comentario
de varias
líneas */
?>
```

##  3. <a name='Finaldeexpresin'></a> Final de expresión

**¿Qué carácter se debe usar al final de cada expresión?**

El punto y coma “;”.

##  4. <a name='Declaracindevariables'></a>Declaración de variables 

**¿Qué símbolo se usa siempre para al comienzo de un nombre de una variable?**

El símbolo del dolar “$”.

##  5. <a name='Operadores'></a>Operadores 

**¿Cuál es la diferencia entre $variable = 1 y $variable == 1?**

```php
<?php
    $variable = 1 // es una asignación, el valor de $variable ahora es 1.
    $variable == 1 // es una comparación, comprueba si $variable es igual a 1.
?>
```

##  6. <a name='Casesensitive'></a>Case sensitive

**¿Los nombres de las variables distinguen mayúsculas de minúsculas? (Sí/No)**

Sí, es case sensitive.

##  7. <a name='Nombresdevariablesnopermitidas'></a>Nombres de variables no permitidas

**¿Se pueden usar espacios en los nombres de variables?**

No, sólo letras, números y guión bajo, pero el primero sólo letras o guión bajo.

##  8. <a name='Casting'></a>Casting

**Muestra cómo se convierte un tipo de variable en otra, por ejemplo de cadena a número usando una función de conversión**

```php
<?php
    $var = “2”;
    $var = (integer)$var;
?>
```

##  9. <a name='Operadorespreypostincremento'></a>Operadores pre y post incremento

**¿Cual es la diferencia entre ++$j y $j++?**

```php
++$j // primero incrementa y después se accede al valor

$j++ // accede al valor y después incrementa
```

##  10. <a name='Operadores-1'></a>Operadores

**¿Son los operadores or e && intercambiables?**

NO

##  11. <a name='Sumacadenaynmero'></a>Suma cadena y número

**¿Cuál es el resultado de sumar una cadena y un número que representa un número?  Ejemplo echo "34"+12**

La suma se ejecuta como si fuesen los dos de tipo integer, y el resultado es de tipo integer también.

```php
<?php
	echo “34”+12; //Muestra 46 de tipo entero
?>
```

##  12. <a name='HolamundoHTML'></a>Hola mundo HTML

**Muestra la cadena de texto “¡Hola mundo!” por pantalla con un salto de línea usando HTML entre las dos palabras**

```php
<?php
	echo “¡Hola <br>mundo!”;
?>
```

##  13. <a name='Errores'></a>Errores

**Encuentra los errores del siguiente programa. Indica el número de las líneas de error y la razón de tu respuesta**

```php
<? php
    print 'Dime tu nombre';
    print 'Mi nombre es 'Iker.';
??>
```

Error en la línea 3: No se reconoce la palabra Iker. La cadena termina con la segunda comilla simple, después del ‘es’. Iker no es parte de la cadena y php no sabe interpretarlo porque no es ni una función ni una palabra reservada.

Error en la línea 4: El cierre del scrip de PHP está mal escrito. Sería ?>.

##  14. <a name='Preciocomida'></a>Precio comida

**Escribe un programa que calcule el precio final de una comida de restaurante. Dos platos combinados de 5,50, una cerveza por 2,30, un refresco por 1,80. El impuesto es del 10%.**

```php
<?php
	$precioPlatoCombinado = 5.50;
	$cantidadPlatoCombinado = 2;
	$precioCerveza = 2.30;
	$cantidadCerveza = 1;
	$precioRefresco = 5.50;
	$cantidadRefresco = 1;
	$porcentajeImpuesto = 10;
	
	$totalPedido = $precioPlatoCombinado * $cantidadPlatoCombinado + $precioCerveza * $cantidadCerveza + $precioRefresco * $cantidadRefresco;
	$iva = $totalPedido * $porcentajeImpuesto / 100;
	$precioFinal = $totalPedido + $iva;
?>
```

##  15. <a name='Operaciones'></a>Operaciones

**Crea dos variables llamadas x e y con valores de 4 y 10 respectivamente y muestra el resultado de las siguientes operaciones aritméticas: suma, resta, multiplicación, división, módulo. Un resultado por línea**

```php
<?php
	$x = 4;
	$y = 10;

	echo $x + $y; echo “<br>”;
	echo $x - $y; echo “<br>”;
	echo $x * $y; echo “<br>”;
	echo $x / $y; echo “<br>”;
	echo $x % $y; echo “<br>”;
?>
```

##  16. <a name='InformacinPHP'></a>Información PHP

**Muestra la información de configuración de PHP por pantalla**

```php
<?php
	phpinfo();
?>
```

##  17. <a name='Usuario'></a>Usuario

**Guarda tu nombre y apellidos como cadena de texto en una variable llamada "usuario" y muestra por pantalla dentro de un párrafo HTML**

```php
<?php
	$usuario = “Aimar Lauzirika Zamakola”;
?>
<html>
	<head>
	</head>
	<body>
		<p><?= $usuario;?></p>
	</body>
</html>
```

##  18. <a name='Longitudcadena'></a>Longitud cadena

**Muestra por pantalla la longitud de esta cadena**

_"Yo he visto cosas que vosotros no creeríais. Naves de ataque en llamas más allá del hombro de Orión. He visto rayos-C brillar en la oscuridad cerca de la Puerta de Tannhäuser. Todos esos momentos se perderán en el tiempo, como lágrimas en la lluvia. Hora de morir."_

##  19. <a name='Potencias'></a>Potencias

**Escribe un programa que use el operador de incremento (++) y el operador de multiplicación combinado (*=) para imprimir los números del 11 al 17 y las potencias de 2 de 1 hasta 3.**


