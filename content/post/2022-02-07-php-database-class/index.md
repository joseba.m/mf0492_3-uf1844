---
title: Proyecto creación clase PHP acceso a una BD MySQL
subtitle: PHP y Bases de datos
date: 2022-02-07
draft: false
description: PHP y Bases de datos
tags: ["PHP","PHP - Clases","POO","MySQL","Bases de datos","PHP - BD"]
---

<!-- vscode-markdown-toc -->
* 1. [Estructura del proyecto](#Estructuradelproyecto)
* 2. [Base de datos](#Basededatos)
* 3. [Configuración](#Configuracin)
* 4. [Clase Database](#ClaseDatabase)
* 5. [Pruebas](#Pruebas)
* 6. [Conclusiones](#Conclusiones)
* 7. [Herramientas y utilidades](#Herramientasyutilidades)
* 8. [Síntaxis y ayuda](#Sntaxisyayuda)
* 9. [Enlaces internos](#Enlacesinternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Estructuradelproyecto'></a>Estructura del proyecto

Comenzaré creando unas carpetas para ordenar mis archivos en previsión de que el proyecto crezca y poco a poco añada nuevas cosas, a mi me gusta configurar mi proyecto así, siéntete libre de hacerlo como te guste.

```
index.php
/conf
    config.php
/inc
    /class
        database.php
```

He subido la carpeta entera con los archivos junto con este artículo a este mismo blog basado en GitLab, el proyecto está en la carpeta src (en su lugar puedes darle el nombre del proyecto a la carpeta) y dentro contiene la estructura que menciono arriba.

* [src/index.php](src/index.php)
* [src/conf/config.php](src/conf/config.php)
* [src/inc/class/database.php](src/inc/class/database.php)

Por el momento tendré el script principal "**index.php**" en el raíz, se ejecutará cuando introduzca la URL base de mi dominio, es donde probaré mi nueva clase para manejar la BD. Estoy usando un [alojamiento gratuito](https://infinityfree.net/) y mi sitio Web de pruebas es: [http://ikerlandajuela.lovestoblog.com](http://ikerlandajuela.lovestoblog.com).

Para subir los archivos uso [Filezilla](https://filezilla-project.org/) como cliente FTP gráfico en Debian (`sudo apt install filezilla`).

La carpeta "**conf**" contiene un fichero "**config.php**" con información de configuración global de todo mi proyecto, por el momento sólo contendrá los datos de conexión a mi BD pero es probable que en el futuro añada nuevos parámetros. Los mantengo centralizados en un archivo fácil de localizar y modificar cualquier ajuste sin andar buscando por todos los archivos.

La carpeta "**inc**" (de _include_) es donde guardaré ficheros con utilidades que deben incluirse en otros script para ser ejecutados, sólo contiene a su vez una subcarpeta "**class**" con declaraciones de clases, por el momento tengo sólo "**database.php**" que contiene una clase básica para manejar bases de datos.

Ya que el código fuente irá creciendo necesitamos adoptar un **sistema de generación de la documentación a partir de los comentarios del código fuente de forma automática**, hay diversos sistemas, yo usaré [http://www.doxygen.org/](http://www.doxygen.org/).

##  2. <a name='Basededatos'></a>Base de datos




##  3. <a name='Configuracin'></a>Configuración

Archivo: [src/conf/config.php](src/conf/config.php).

```php
<?php
// Database connection parameters
define ( 'DB_HOST', 'remotemysql.com' );
define ( 'DB_USER', 'Xgl65VVZ0W' );
define ( 'DB_PASSWORD', 'TtSCOfYD0P' );
define ( 'DB_DB', 'Xgl65VVZ0W' );
define ( 'DB_PORT', 3306 );
?>
```
##  4. <a name='ClaseDatabase'></a>Clase Database

En cualquier aplicación implementada en PHP que requiera de acceso a una base de datos, podremos utilizar dos procedimientos principalmente: el manejo de MySQLi, en su variante de programación orientada a objetos o en la variante de programación estructurada, y el uso de la clase PDO (PHP Data Object) que proporciona una capa de abstracción de acceso a datos. Usaremos MySQLi, mediante POO.

La [clase mysqli](https://www.php.net/manual/es/class.mysqli.php) representa una conexión entre PHP y una base de datos MySQL. El método constructor realizará la conexión con la BD. El atributo de la clase [mysqli_connect_errno](https://www.php.net/manual/es/mysqli.connect-errno.php) Devuelve el código de error de la última llamada si ha fallado. cero significa que no ha ocurrido ningún error.

Archivo: [src/inc/class/database.php](src/inc/class/database.php). 

```php
<?php 
 
class Database {
    private $host, $database, $username, $password  = '';
    private $connection;
    private $port = 3306;

    private $error_num = 0;
     
    function __construct($host, $username, $password, $database, $port = 3306, $autoconnect = true) {
        echo "construct<br>";

        $this->error_num=0;

        $this->host = $host;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
        $this->port = $port;
        if($autoconnect) $this->open();
    }
   
    // https://www.php.net/manual/en/mysqli.construct.php
    function open() {
        
        echo "open<br>";
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database, $this->port);

        if ($this->connection->connect_errno) { // gestion error conexion
            echo "connection error<br>";
            $this->error_num+=1;
        } else {
            echo "connection ok!<br>";
        }

    }

    function close() {
        $this->connection->close();
    }

    function get_error_num() {
        return $this->error_num;
    }

    function query($query) {
        return $this->connection->query($query);
    }
   
}
 
?>
```


##  5. <a name='Pruebas'></a>Pruebas

Archivo: [src/index.php](src/index.php).

```php
<?php
require_once(dirname(__FILE__) . '/conf/config.php'); // fichero configuración con parámetros de conexión a base de datos
require_once(dirname(__FILE__) . '/inc/class/database.php'); // clase básica para manejar bases de datos


$db = new Database(DB_HOST,DB_USER,DB_PASSWORD,DB_DB,DB_PORT,FALSE); // Instancia de clase Database

echo "abriendo conexión a base de datos<br>";
$db->open();

if(!$db->is_error()) { // compruebo error de conexión

    echo "realizando consulta tabla usuarios<br>";
    $query = $db->query("SELECT * FROM usuarios;"); // consulta a tabla

    foreach ($query as $row) { // recorro registros de resultado
        echo "user:".$row['user']."<br>";    
    }

    echo "cerrando conexión a base de datos<br>";
    $db->close();
} else {
    echo "Error conectando a base de datos, revise configuración<br>";
}
?>
```

##  6. <a name='Conclusiones'></a>Conclusiones



##  7. <a name='Herramientasyutilidades'></a>Herramientas y utilidades

* [Free Remote MySQL](https://remotemysql.com/): Permite crear una BD gratuita.
* [Free and Unlimited Web Hosting with PHP and MySQL - InfinityFree](https://infinityfree.net/): Alojamiento gratuito PHP y BD, yo uso la BD de [https://remotemysql.com/](https://remotemysql.com/)) pero podría usar esta perfectamente.

##  8. <a name='Sntaxisyayuda'></a>Síntaxis y ayuda


##  9. <a name='Enlacesinternos'></a>Enlaces internos


