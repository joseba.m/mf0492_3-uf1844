<?php 

/**
 * Clase Database para facilitar la conexión con la base de datos y permite realizar consultas básicas.
 */
class Database {
    private $host, $database, $username, $password  = '';
    private $connection;
    private $port = 3306;

    private $error_num = 0;
     
    /**
     * Constructor de la clase.
     * Almacena los datos de conexión pasados como parámetros al método y los almacena como propiedades internas de la clase.
     * @param $host Cadena con nombre del host o máquina que alberga la BD.
     * @param $username Cadena con nombre de usuario
     */
    function __construct($host, $username, $password, $database, $port = 3306, $autoconnect = true) {
        $this->error_num=0;
        $this->error_str;

        $this->host = $host;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
        $this->port = $port;
        if($autoconnect) $this->open();
    }
   
    
    function open() {
                
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database, $this->port);

        if ($this->connection->connect_errno) {  // error de conexión!            
            // TODO guardar string $connect_error; 
            //$this->error_str[$this->error_num] = $this->connection->connect_error
            $this->error_num+=1;
        } 
    }

    function close() {
        $this->connection->close();
    }

    function get_error_num() {
        return $this->error_num;
    }

    function is_error() {
        if ($this->error_num>0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function query($query) { // todo: se debería contemplar error o falta de resultados dentro del método de alguna manera?
        return $this->connection->query($query);
    }
   
} // fin clase Database
 
?>