<?php
require_once(dirname(__FILE__) . '/conf/config.php'); // fichero configuración con parámetros de conexión a base de datos
require_once(dirname(__FILE__) . '/inc/class/database.php'); // clase básica para manejar bases de datos


$db = new Database(DB_HOST,DB_USER,DB_PASSWORD,DB_DB,DB_PORT,FALSE); // Instancia de clase Database

echo "abriendo conexión a base de datos<br>";
$db->open();

if(!$db->is_error()) { // compruebo error de conexión

    echo "realizando consulta tabla usuarios<br>";
    $query = $db->query("SELECT * FROM usuarios;"); // consulta a tabla

    foreach ($query as $row) { // recorro registros de resultado
        echo "user:".$row['user']."<br>";    
    }

    echo "cerrando conexión a base de datos<br>";
    $db->close();
} else {
    echo "Error conectando a base de datos, revise configuración<br>";
}

// Probar siempre errores posibles
//$db = new Database('192.168.200.155',DB_USER,DB_PASSWORD,DB_DB,DB_PORT,TRUE); 

?>