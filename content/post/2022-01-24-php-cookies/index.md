---
title: Cookies y autenticación
subtitle: Información persistente
date: 2022-01-24
draft: false
description: Información persistente
tags: ["PHP","PHP - Cookies","PHP - Sesiones","PHP - Autenticación"]
---

<!-- vscode-markdown-toc -->
* 1. [Cookies en PHP](#CookiesenPHP)
	* 1.1. [Estableciendo una cookie](#Estableciendounacookie)
	* 1.2. [Accediendo a una Cookie](#AccediendoaunaCookie)
	* 1.3. [Comprobar si las cookies están habilitadas](#Comprobarsilascookiesestnhabilitadas)
	* 1.4. [Destruir una cookie](#Destruirunacookie)
* 2. [Autenticación HTTP](#AutenticacinHTTP)
	* 2.1. [Autenticación comprobando entradas](#Autenticacincomprobandoentradas)
* 3. [Enlaces internos](#Enlacesinternos)
* 4. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

A medida que tus proyectos web crecen y se complican, te verás en la necesidad de llevar un registro de sus usuarios. Incluso si no estás ofreciendo logins y contraseñas, a menudo tendrás que almacenar detalles sobre la sesión actual de un usuario y posiblemente también reconocerlos cuando regresen a tu sitio.

HTTP es un protocolo sin estado (_stateless protocol_), lo que significa que la conexión entre el cliente (navegador web) y el servidor se pierde una vez finaliza la transacción.

Varias tecnologías respaldan este tipo de interacción, que van desde simples **cookies de navegador** hasta el manejo de **sesiones y autenticación HTTP**. Entre ellos, ofrecen la oportunidad de configurar tu sitio a las preferencias de sus usuarios y garantizar una transición suave y agradable a través de él.

Las sesiones son un mecanismo para que el servidor guarde de forma persistente el estado de la aplicación a través de peticiones consecutivas de un visitante.

##  1. <a name='CookiesenPHP'></a>Cookies en PHP

Una **_cookie_ es un elemento de datos que un servidor web guarda en el disco duro de tu computadora a través de un navegador web**. Puede contener casi cualquier información alfanumérica y puede ser recuperado de tu computadora y devuelto al servidor. Los usos comunes incluyen el seguimiento de sesiones, el mantenimiento de datos a través de varias visitas, la retención de contenido del carrito de compras, el almacenamiento de datos de inicio de sesión, y más.

Debido a sus implicaciones de privacidad, **las cookies solo se pueden leer desde el dominio de emisión**. En otras palabras, si una cookie es emitida por, por ejemplo, oreilly.com, solo puede ser recuperada por un servidor web usando ese dominio. Esto impide que otros sitios web tengan acceso a detalles para los que no están autorizados.

Debido a la forma en que funciona Internet, múltiples elementos en una página web pueden estar integrados desde múltiples dominios, cada uno de los cuales puede emitir sus propias cookies. Cuando esto sucede, se denominan **cookies de terceros** (_third-party cookies_). Más comúnmente, estas son creadas por empresas publicitarias con el fin de rastrear a los usuarios en varios sitios web.

Debido a esto, la mayoría de los navegadores permiten a los usuarios deshabilitar las cookies ya sea para el dominio del servidor actual, servidores terceros, o ambos. Afortunadamente, la mayoría de las personas que desactivan las cookies lo hacen solo para sitios web de terceros.

Si usas Firefox como navegador puedes instalar la extensión Web Developer para obtener
información sobre las páginas web que visitas. Entre sus características te permite consultar y editar
las cookies almacenadas en el mismo [https://addons.mozilla.org/es/firefox/addon/web-developer/](https://addons.mozilla.org/es/firefox/addon/web-developer/). Para Chrome puede usar [CookieManager - Cookie Editor](https://chrome.google.com/webstore/detail/cookiemanager-cookie-edit/hdhngoamekjhmnpenphenpaiindoinpo/related).

**Las cookies se intercambian durante la transferencia de encabezados**, antes de enviar el HTML real de una página web, y **es imposible enviar una cookie una vez que se haya transferido cualquier HTML** (Esto incluye cualquier tipo de etiqueta html, echo, print, etc. ). Por lo tanto, **es importante una planificación cuidadosa del uso de cookies**.

![](img/01.png)

La imagen de arriba muestra el intercambio de información cuando nuestro navegador solicita dos páginas.

1. Nuestro navegador solicita la página principal index.html, en el servidor http://www.webserver.com. 
2. Cuando el servidor web recibe este par de encabezados, devuelve algunos propios. El segundo encabezado define el tipo de contenido a enviar (texto/html), y el tercero envía una cookie del nombre y con el valor. Solo entonces se transfieren los contenidos de la página web. 
3. Una vez que el navegador ha recibido la cookie, la devolverá con cada futura petición hecha al servidor emisor hasta que la cookie expire o se borre. Así, cuando el navegador solicita la nueva página /news.html, también devuelve el nombre de la cookie con el valor.
4. Debido a que la cookie ya ha sido configurada, cuando el servidor recibe la solicitud de enviar /news.html, no tiene que reenviar la cookie, sino que solo devuelve la página solicitada.


###  1.1. <a name='Estableciendounacookie'></a>Estableciendo una cookie

Configurar una cookie en PHP es un asunto simple. **Mientras no se haya transferido ningún HTML**, puedes llamar a la función [`setcookie`](https://www.php.net/manual/es/function.setcookie), que tiene la siguiente sintaxis:

```php
setcookie(name, value, expire, path, domain, secure, httponly);
```

| Parámetro      | Descripción        | Ejemplo         |
| -------------- | ------------------ |-----------------|
| name           | El nombre de la cookie. Será el nombre que usará el servidor para acceder a la cookie en llamadas posteriores del navegador | usuario |
| value | El valor de la cookie. Puede contener 4KB de texto alfanumérico | iker.l@fptxurdinaga.com |
| expire | (opcional). Cuando expira. Si no se establece expira cuando se cierra el navegador | time() + 3600 |
| path | (opcional) La ruta de la cookie en el servidor. Si es "/" la cookie está accesible para todo el dominio. Si es un subdirectorio, sólo estará accesible desde el mismo. | / |
| domain | (Opcional) Dominio (con o sin subdominio)| webserver.com |
| secure | (Opcional) Si la cookie debe usar una conexión segura (https://) | FALSE |
| httponly | (Opcional) La cookie debe usar o no el protocolo HTTP. Si es TRUE lenguajes de script como JavaScript no podrán acceder. | FALSE |

Ejemplo:

```php
setcookie('userid','ralph');
```

Establece una _cookie_ llamada "userid" con el valor "ralph". 

Entonces, para crear una _cookie_ con el nombre `usuario` y el valor `iker.l@fptxurdinaga.com` accesible en todo el servidor web en el dominio actual, y que será eliminada del cache del navegador en 1 hora.

Archivo [src/cookie.php](src/cookie.php):

```php
setcookie('usuario', 'iker.l@fptxurdinaga.com', time()+3600, '/'); /* expira en 1 hora */
```

* **Nota**: La función [`setcookie`](https://www.php.net/manual/es/function.setcookie) debe ir siempre antes del tag <html>.

**El valor proporcionado a [`setcookie`](https://www.php.net/manual/es/function.setcookie) puede ser un _string_ o un número. No puede ser un array u otra estructura de datos compleja**.

Las _cookies_ sólo pueden contener valores escalares, pero puedes usar una sintaxis como la siguiente:

```php
<?php
setcookie("user[name]", "Alice");
setcookie("user[email]", "alice@example.com");

print_r($_COOKIE); // Array ( [user] => Array ( [name] => Alice [email] => alice@example.com ) )
?>
```

Cuando llamas a [`setcookie`](https://www.php.net/manual/es/function.setcookie), la respuesta que prepara PHP para mandar de vuelta al cliente web incluye una cabecera especial que informa al navegador sobre una nueva _cookie_. En llamadas posteriores, el cliente web envía el nombre de la _cookie_ y su valor de vuelta al servidor: 

![](img/02.png)

###  1.2. <a name='AccediendoaunaCookie'></a>Accediendo a una Cookie

Leer el valor de una cookie es tan sencillo como acceder al array `$_COOKIE`. Por ejemplo, para comprobar si existe una cookie llamada `usuario` y leer su valor:

```php
if (isset($_COOKIE['usuario'])) $usuario = $_COOKIE['usuario'];

echo "usuario es: ".$usuario."<br>";
```

**Nota**: En el script de arriba la primera vez que lo ejecutemos la salida mostrará que no existe la cookie, el servidor manda al navegador que cree una cookie en el sistema de archivos local con `setcookie` pero aún no puede leerla hasta que el navegador a su vez mande la información al servidor, si volvemos a ejecutar el mismo script una vez más entonces mostrará el valor de la cookie correctamente. Puedes usar cualquier addon o extensión como [Cookie Manager (Firefox)](https://addons.mozilla.org/en-US/firefox/addon/a-cookie-manager/) para modificar el valor de las cookies.

Otro ejemplo otenido de [W3Schools](https://www.w3schools.com/php/php_cookies.asp), archivo [src/modify_cookie.php](src/modify_cookie.php):

```php
<?php
$cookie_name = "usuario";
$cookie_value = "iker.l@fptxurdinaga.com";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
?>
<html>
<body>

<?php
if(!isset($_COOKIE[$cookie_name])) {
  echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
  echo "Cookie '" . $cookie_name . "' is set!<br>";
  echo "Value is: " . $_COOKIE[$cookie_name];
}
?>

</body>
</html> 
```

###  1.3. <a name='Comprobarsilascookiesestnhabilitadas'></a>Comprobar si las cookies están habilitadas

Un pequeño script de test para comprobar si las cookies están habilitadas (tomado de W3Schools).

Archivo [test_cookies.php](test_cookies.php):

```php
<?php
setcookie("test_cookie", "test", time() + 3600, '/');
?>
<html>
<body>

<?php
if(count($_COOKIE) > 0) {
  echo "Cookies are enabled.";
} else {
  echo "Cookies are disabled.";
}
?>

</body>
</html> 
```

###  1.4. <a name='Destruirunacookie'></a>Destruir una cookie

Para eliminar una cookie debes hacer la misma llamada pero poniendo una fecha en el pasado:

```php
setcookie('usuario', 'iker.l@fptxurdinaga.com', time()-3600, '/');
```

También puedes proporcionar una cadena vacia al valor (o FALSE) y PHP le pondrá una fecha en el pasado por ti.

##  2. <a name='AutenticacinHTTP'></a>Autenticación HTTP

La autenticación HTTP usa el servidor Web (Apache, Nginx...) para gestionar los usuarios y contraseñas. Se puede usar **para una aplicación sencilla que solicita un usuario y contraseña**, cuando se trate de un proyecto normalmente necesitamos un formulario con requisitos más avanzados o medidas de seguridad más estrictas.

Para usar la autenticación HTTP, PHP envía una petición en la cabecera HTTP para iniciar un cuadro de dialogo de autenticación con el navegador cliente, el servidor debe tener habilitada esta característica, pero lo más normal es que así sea.

Archivo: [src/auth.php](src/auth.php).

```php
<?php
if (isset($_SERVER['PHP_AUTH_USER']) &&  isset($_SERVER['PHP_AUTH_PW'])) {
    echo "Welcome User: " . htmlspecialchars($_SERVER['PHP_AUTH_USER']) .
        " Password: "
        . htmlspecialchars($_SERVER['PHP_AUTH_PW']);
} else {
    header('WWW-Authenticate: Basic realm="Restricted Area"');
    header('HTTP/1.0 401 Unauthorized');
    die("Please enter your username and password");
}
?>
```

Cuando visitemos la página de arriba el usuario verá una ventana emergente con el título "Authentication Required”, solicitando un usuario y una contraseña. 

En nuestro ejemplo no realizamos ninguna comprobación excepto que introduzca algo en ambos campos usando [`isset`](https://www.php.net/manual/es/function.isset) y el operador lógico AND.

Deberíamos aplicar la función [`htmlspecialchars`](https://www.php.net/manual/es/function.htmlspecialchars.php) sobre los campos, eso es porqué son introducidos por el usuario y debemos evitar ataques estilo [_cross-site scripting_ o XSS](https://es.wikipedia.org/wiki/Cross-site_scripting).

Si el valor no existe, el usuario aún no se ha autenticado y le muestras el cuadro de dialogo mandando la siguiente cabecera:

```php
header('WWW-Authenticate: Basic realm="Restricted Area"');
```

La función [`header`](https://www.php.net/manual/es/function.header) envía una cabecera HTTP pura o sin formato.

**Nota**: Recuerde que **header() debe ser llamado antes de mostrar nada por pantalla**, etiquetas HTML, líneas en blanco desde un fichero o desde PHP. Es un error muy común leer código con funciones como include o require, u otro tipo de funciones de acceso de ficheros que incluyen espacios o líneas en blanco que se muestran antes de llamar a la función header().

Si el usuario rellena los campos, el script PHP se vuelve a ejecutar desde arriba. Pero si pulsa el botón para cancelar, el programa sigue con las siguientes líneas, que manda una cabecera y un mensaje de error.

```php
header('HTTP/1.0 401 Unauthorized');
die("Please enter your username and password");
```

###  2.1. <a name='Autenticacincomprobandoentradas'></a>Autenticación comprobando entradas

Archivo: [src/auth_check.php](src/auth_check.php).

```php
<?php
$username = 'admin';
$password = 'letmein';

if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    if ($_SERVER['PHP_AUTH_USER'] === $username && $_SERVER['PHP_AUTH_PW'] === $password)
        echo "You are now logged in";
    else die("Invalid username/password combination");
} else {
    header('WWW-Authenticate: Basic realm="Restricted Area"');
    header('HTTP/1.0 401 Unauthorized');
    die ("Please enter your username and password");
}
?>
```


##  3. <a name='Enlacesinternos'></a>Enlaces internos

* [Ejemplo práctico - Interacción con MySQL usando formularios y PHP](https://iker.l.gitlab.io/iaw_2as3_blog/post/2021-11-29-php-mysql-ejemplo-practico/).

##  4. <a name='Enlacesexternos'></a>Enlaces externos

* [setcookie](https://www.php.net/manual/es/function.setcookie) — Enviar una cookie.
* [$_COOKIE](https://www.php.net/manual/es/reserved.variables.cookies.php):  Una variable tipo array asociativo de variables pasadas al script actual a través de Cookies HTTP. 
* [PHP Cookies - W3Schools](https://www.w3schools.com/php/php_cookies.asp).
* [$_SERVER](https://www.php.net/manual/en/reserved.variables.server.php).
* [header](https://www.php.net/manual/es/function.header) — Enviar encabezado sin formato HTTP.
* [die](https://www.php.net/manual/es/function.die) — Equivalente a exit.
* [exit](https://www.php.net/manual/es/function.exit.php) — Imprime un mensaje y termina el script actual]
* [session_start](https://www.php.net/manual/es/function.session-start) — Iniciar una nueva sesión o reanudar la existente.
* [session_destroy](https://www.php.net/manual/es/function.session-destroy.php) — Destruye toda la información registrada de una sesión.





