<?php // authenticate2.php
require_once 'login.php';

$connection = new mysqli($hn, $un, $pw, $db);
if ($connection->connect_error) die("Fatal Error");

if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $un_temp = mysql_entities_fix_string($connection, $_SERVER['PHP_AUTH_USER']);
    $pw_temp = mysql_entities_fix_string($connection, $_SERVER['PHP_AUTH_PW']);

    $query = "SELECT * FROM usuarios WHERE User='$un_temp'";

    $result = $connection->query($query);

    if (!$result) {
        die("User not found");
    } elseif ($result->num_rows) {
        $row = $result->fetch_array(MYSQLI_NUM);
        $result->close();

        if ( $pw_temp == $row[2] )  {
            session_start();
            $_SESSION['Id'] = $row[0];
            $_SESSION['User'] = $row[1];
            $_SESSION['Password'] = $row[2];
            echo htmlspecialchars("$row[0] $row[1] : Hi you are now logged in as '$row[1]'");
            die ("<p><a href='continue.php'>Click here to continue</a></p>");

        } else die("Invalid password_verify combination. query=".$query);
    }
    else die("Invalid username from query".$query);
} else {
    header('WWW-Authenticate: Basic realm="Restricted Area"');
    header('HTTP/1.0 401 Unauthorized');
    die ("Please enter your username and password");
}

$connection->close();

function mysql_entities_fix_string($connection, $string) {
    return htmlentities(mysql_fix_string($connection, $string));
}

function mysql_fix_string($connection, $string) {
    if (get_magic_quotes_gpc()) $string = stripslashes($string);
    return $connection->real_escape_string($string);
}
?>


