---
title: Incluir Código de otros Documentos PHP. include y require
subtitle: PHP Web
date: 2022-01-23
draft: false
description: PHP Web
tags: ["PHP","PHP - Include","PHP - Require"]
---

PHP permite incluir en un documento PHP código de otro documento PHP creado anteriormente.

Para cargar el código de un documento PHP dentro de otro se puede usar la sentencia [`include`](https://www.php.net/manual/es/function.include.php) o la sentencia [`require`](https://www.php.net/manual/es/function.require.php) la diferencia entre ellas es que si se produce un error al cargar el documento PHP la sentencia include genera un warning y continúa la ejecución mientras que la sentencia require produce un error del tipo `E_COMPILE_ERROR` que detiene la ejecución.

La sintaxis de include en PHP para cargar el documento PHP de nombre 'codigo.php' es:

```php
include('codigo.php');
```

La sintaxis de require en PHP para cargar el documento PHP de nombre 'codigo.php' es:

```php
require('codigo.php');
```

La sentencia `include_once` es idéntica a include excepto que PHP verificará si el archivo ya ha sido incluido y si es así, no se incluye de nuevo.

La sentencia `require_once` es idéntica a require excepto que PHP verificará si el archivo ya ha sido incluido y si es así, no se incluye (require) de nuevo.


