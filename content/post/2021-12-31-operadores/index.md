---
title: Operadores
subtitle: Aritméticos, asignación, comparación y lógicos
date: 2021-12-31
draft: false
description: Aritméticos, asignación, comparación y lógicos
tags: ["PHP", "PHP - Operadores"]
---

<!-- vscode-markdown-toc -->
* 1. [Operadores aritméticos binarios](#Operadoresaritmticosbinarios)
	* 1.1. [Asignación por referencia](#Asignacinporreferencia)
* 2. [Operadores aritméticos unarios](#Operadoresaritmticosunarios)
* 3. [Operadores unarios de incremento/decremento](#Operadoresunariosdeincrementodecremento)
* 4. [Operador de asignación](#Operadordeasignacin)
	* 4.1. [Operadores combinados](#Operadorescombinados)
* 5. [Comparación](#Comparacin)
* 6. [Lógicos](#Lgicos)
* 7. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Un [operador](https://www.php.net/manual/es/language.operators.php) es algo que toma uno más valores (o expresiones, en la jerga de programación) y produce otro valor.

##  1. <a name='Operadoresaritmticosbinarios'></a>Operadores aritméticos binarios

Deberías reconocer las siguientes operaciones aritméticas:

![](img/08.png)

Los operadores aritméticos cogen dos argumentos, por eso se llaman [**operadores binarios**](https://es.wikipedia.org/wiki/Operaci%C3%B3n_binaria). Se usan para realizar operaciones matemáticas.

**Operadores aritméticos**:

![](img/01.png)

```php
echo (5 % 3)."\n";           // muestra 2
echo (5 % -3)."\n";          // muestra 2
echo (-5 % 3)."\n";          // muestra -2
echo (-5 % -3)."\n";         // muestra -2

$x = 20;
$y = 10;

echo $x + $y;  // 30
echo $x - $y;  // 10
echo $x * $y;  // 200

$z = $x / $y;

$y = 15;
echo $x % $y; // 5
```

El operador de división ("/") devuelve un valor flotante a menos que los dos operandos sean integers (o strings que se conviertan a integers) y los números sean divisibles, en cuyo caso será devuelto un valor integer.

Los operandos del módulo se convierten en integers (por extracción de la parte decimal) antes del procesamiento.

Observe que la asignación copia la variable original en la nueva (**asignación por valor**), por lo que los cambios en una no afectarán a la otra. 

###  1.1. <a name='Asignacinporreferencia'></a>Asignación por referencia

La asignación por referencia también está soportada, utilizando la sintaxis `$var = &$othervar;`. Asignación por referencia significa que ambas variables terminan apuntando a los mismos datos y nada es copiado en ninguna parte.

```php
<?php
$a = 3;
$b = &$a; // $b es una referencia para $a

print "$a\n"; // muestra 3
print "$b\n"; // muestra 3

$a = 4; // cambia $a

print "$a\n"; // muestra 4
print "$b\n"; // muestra 4 también, dado que $b es una referencia para $a, la cual ha
              // sido cambiada
?>
```

##  2. <a name='Operadoresaritmticosunarios'></a>Operadores aritméticos unarios

Los **operadores unarios** son operadores aritméticos que realizan una acción sobre un solo operando, se posiciones antes o después de la variable, esto condiciona su funcionamiento. Sólo existen dos tipos de operadores unarios en PHP, llamados prefijo (_prefix_) y (_sufijo_), refiriéndose a que el operador se escribe antes o después de la variable sobre la que opera.

Comportamiento de los operadores unarios:

* Prefijo: El interprete PHP primero evalúa la operación y a continuación retorna la variable modificada.
* Sufijo: Primero retorna la variable tal cual está y después aplica la operación.

A continuación puedes observar cómo afectan cuando inicializamos una variable `$a` a 1 y operamos sobre ella:

![](img/09.png)

##  3. <a name='Operadoresunariosdeincrementodecremento'></a>Operadores unarios de incremento/decremento

![](img/07.png)

Ejemplos:

```php
echo "<h3>Postincremento</h3>";
$a = 5;
echo "Debe ser 5: " . $a++ . "<br />\n";
echo "Debe ser 6: " . $a . "<br />\n";

echo "<h3>Preincremento</h3>";
$a = 5;
echo "Debe ser 6: " . ++$a . "<br />\n";
echo "Debe ser 6: " . $a . "<br />\n";

echo "<h3>Postdecremento</h3>";
$a = 5;
echo "Debe ser 5: " . $a-- . "<br />\n";
echo "Debe ser 4: " . $a . "<br />\n";

echo "<h3>Predecremento</h3>";
$a = 5;
echo "Debe ser 4: " . --$a . "<br />\n";
echo "Debe ser 4: " . $a . "<br />\n";
```

Cosas locas:

```php
$a = 1;
$b = 3;
echo $a++ + $b; # 4

$a = 1;
$b = 3;
echo $a + ++$b; # 5

$a = 1;
$b = 3;
echo ++$a + $b++; # 5
```

##  4. <a name='Operadordeasignacin'></a>Operador de asignación

**El operador básico de asignación es "="**. Se podría inclinar a pensar primero que es como un "igual a". No lo es. Realmente significa que el operando de la izquierda se establece con el valor de la expresión de la derecha (es decir, "se define como"). 

El valor de una expresión de asignación es el valor asignado. Es decir, el valor de "$a = 3" es de 3. Esto permite hacer algunas cosas intrincadas:

```php
<?php
$a = ($b = 4) + 5; // ahora $a es igual a 9 y $b se ha establecido en 4.
?>
```

###  4.1. <a name='Operadorescombinados'></a>Operadores combinados 

Además del operador básico de asignación, existen «**operadores combinados**» que permiten usar un valor en una expresión y entonces establecer su valor como el resultado de esa expresión. Por ejemplo: 

![](img/02.png)

```php
$count += 1;
# es lo mismo que
$count = $count + 1;

# más ejemplos
$a = 3;
$a += 5; // establece $a en 8, como si se hubiera dicho: $a = $a + 5;
$b = "Hola ";
$b .= "ahí!"; // establece $b en "Hola ahí!", al igual que $b = $b . "ahí!";
```

##  5. <a name='Comparacin'></a>Comparación

Generalmente se usan dentro de las sentencias condicionales `if` donde necesitas comparar dos elementos. Por ejemplo, si necesitas saber si una variable que has estado incrementando ha alcanzado un valor específico.

![](img/03.png)

**Fijate en la diferencia entre el operador `=` y `==`**. El primero es un operador de asignación, el segundo de comparación.

```php
$count = 17;
$count ++;
echo ($count >= 18); # 1

var_dump(0 == "a"); // 0 == 0 -> true
var_dump("1" == "01"); // 1 == 1 -> true
var_dump("10" == "1e1"); // 10 == 10 -> true
var_dump(100 == "1e2"); // 100 == 100 -> true


// Integers
echo 1 <=> 1; // 0
echo 1 <=> 2; // -1
echo 2 <=> 1; // 1

// Floats
echo 1.5 <=> 1.5; // 0
echo 1.5 <=> 2.5; // -1
echo 2.5 <=> 1.5; // 1

// Strings
echo "a" <=> "a"; // 0
echo "a" <=> "b"; // -1
echo "b" <=> "a"; // 1

echo "a" <=> "aa"; // -1
echo "zz" <=> "aa"; // 1
```

##  6. <a name='Lgicos'></a>Lógicos

![](img/04.png)

```php
echo "true && false=".(true && false)."<br>";
echo "true  || false=".(true  || false)."<br>";
echo "true and false=".(true and false)."<br>";
echo "true  or  false=".(true  or  false)."<br>";
```

A menudo se usan los operadores lógicos para combinar dos o mas resultados de comparaciones que hemos visto arriba.

```php
$hour = 14;
if ($hour > 13 && $hour < 15) echo "a comer!";
```

Fijate que `and`y `&&` son equivalentes, también `or` y `||`.

## Operadores binarios bit a bit

Los operadores bit a bit permiten la evaluación y la manipulación de bits específicos dentro de un integer.

![](img/10.png)

Ver: Manual PHP [Operadores bit a bit](https://www.php.net/manual/es/language.operators.bitwise.php).

##  7. <a name='Enlacesexternos'></a>Enlaces externos 

* [Operadores](https://www.php.net/manual/es/language.operators.php)
* [Constantes](https://www.php.net/manual/es/language.constants.php).
* [Constantes predefinidas](https://www.php.net/manual/es/language.constants.predefined.php).
* [https://www.tutorialspoint.com/php/php_arithmatic_operators_examples.htm](https://www.tutorialspoint.com/php/php_arithmatic_operators_examples.htm).

