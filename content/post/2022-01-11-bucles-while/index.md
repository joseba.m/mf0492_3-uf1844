---
title: Bucles While
subtitle: Control de expresiones y del flujo de ejecución en PHP
date: 2022-01-11
draft: false
description: Control de expresiones y del flujo de ejecución en PHP
tags: ["PHP", "PHP - Condicionales","PHP - While"]
---

<!-- vscode-markdown-toc -->
* 1. [while](#while)
* 2. [do-while](#do-while)
* 3. [Romper la ejecución del bucle con break](#Romperlaejecucindelbucleconbreak)
* 4. [Saltar a la siguiente iteración del bucle con continue](#Saltaralasiguienteiteracindelbucleconcontinue)
* 5. [Ejemplos](#Ejemplos)
	* 5.1. [Gasolina](#Gasolina)
	* 5.2. [Números pares de 0 a 10](#Nmerosparesde0a10)
	* 5.3. [Continue](#Continue)
	* 5.4. [HTML select](#HTMLselect)
* 6. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

![](img/08.jpg)

##  1. <a name='while'></a>while

![](img/03.png)

Los bucles while son el tipo más sencillo de bucle en PHP, la forma más básica:

```php
while (expr)
    sentencia
```

El significado de una sentencia while es simple. Le dice a PHP que ejecute las sentencias anidadas, tanto como la expresión while se evalúe como **true**.

**El valor de la expresión es verificado cada vez al inicio del bucle**, por lo que incluso si este valor cambia durante la ejecución de las sentencias anidadas, la ejecución no se detendrá hasta el final de la iteración.

 A veces, si la expresión while se evalúa como **false** desde el principio, las sentencias anidadas no se ejecutarán ni siquiera una vez.

Contador 1 a 10:

```php
$i = 1;
while ($i <= 10) {
    echo $i++." "; 
}
```

Salida:

![](img/02.PNG)

##  2. <a name='do-while'></a>do-while

La condición se comprueba después de ejecutar el código. De tal modo que el código siempre se ejecuta por lo menos una vez.

![](img/04.png)

Ejemplo:

```php
$i = 0;
do {
    echo $i;
} while ($i > 0);
```

La salida es 0.


##  3. <a name='Romperlaejecucindelbucleconbreak'></a>Romper la ejecución del bucle con break

Usando el comando [`break`](https://www.php.net/manual/es/control-structures.break.php) se puede interrumpir la ejecución de un bucle (no sólo se usa con `while`)

```php
$contador = 0;

while ($contador < 10) {
    if ($contador == 4) {
        echo "saliendo de bucle<br>";
        break;
    }
    $contador++;
}

echo "valor final de contador es:".$contador."<br>";
```

##  4. <a name='Saltaralasiguienteiteracindelbucleconcontinue'></a>Saltar a la siguiente iteración del bucle con continue


```php
$contador = 0;

while ($contador < 10) {
    if ($contador % 2 == 0) {
    	$contador++;
        echo "salto!<br>";
    	continue; # Salta a siguiente iteracion del bucle si es par
    }
    echo "Es impar: $contador<br>";
    $contador++;
}
```

Salida:

![](img/07.PNG)

##  5. <a name='Ejemplos'></a>Ejemplos

###  5.1. <a name='Gasolina'></a>Gasolina

Definir una variable "gasolina" que contenga un valor numérico y una constante "MAX_DEPOSITO" que límite el valor máximo al que se puede llenar el deposito, implementar un bucle While que suma una unidad a la variable "gasolina", el bucle debe parar cuando alcanza el máximo.

```php
<?php
$gasolina = 15;
define("MAX_DEPOSITO",20);
while ($gasolina < MAX_DEPOSITO) {
    $gasolina++;
    echo "Tiene $gasolina despues de recarga <br>";
}

echo "Estado final gasolina:".$gasolina."<br>";
?>
```

Salida:

![](img/01.PNG)


###  5.2. <a name='Nmerosparesde0a10'></a>Números pares de 0 a 10

Usando un bucle `while` muestra los números pares de 0 a 10.

```php
$num = 0;

while ($num <= 10) {
    echo "num es: $num <br>";
    $num+=2;
}
```

Salida:

![](img/05.PNG)


###  5.3. <a name='Continue'></a>Continue

Un ejercicio similar al anterior usando [`continue`](https://www.php.net/manual/es/control-structures.continue.php) para saltar a la siguiente iteracción.

```php
$contador = 0;

while ($contador < 10) {
    $contador += 1;

    if ($contador % 2 == 0) {
        echo "Saltando numero porque es par.<br>";
        continue;
    }

    echo "Ejecutando - contador es $contador.<br>";
}
```

Salida:

![](img/06.PNG)


###  5.4. <a name='HTMLselect'></a>HTML select

Usando [`while`](https://www.php.net/manual/es/control-structures.while.php) crear un menú [`select`](https://www.w3schools.com/tags/tag_select.asp) en HTML.

```php
<?php
$i = 1;
print '<select name="people">';
while ($i <= 10) {
	print "<option>$i</option>\n";
	$i++;
}
print '</select>';
?>
```

Antes de entrar en el bucle [`while`](https://www.php.net/manual/es/control-structures.while.php), se establece la variable `$i` a 1 e imprime la etiqueta de apertura `<select>`. La expresión de test comprueba si `$i` es menor o igual que 10, cuando se cumple ejecuta las dos líneas dentro del bloque. La primera línea dentro del bucle imprime la etiqueta con la opción `<<option>` del `<select>`, la siguiente línea incrementa la variable `$i` con un operador de post decremento. Después de imprimir `<option>10</option>` la línea `$i++;` incrementa su valor a 11, como no se cumple la condición el programa sigue con las expresiones que sigan al bucle más abajo.

* ¿Se puede compactar / optimizar imprimiendo e incrementando `$i` en la misma línea?
* ¿Qué pasa si se nos olvida realizar el incremento de la variable `$i`? (imprimirá `<option>1</option>` hasta el infinito).

##  6. <a name='Enlacesexternos'></a>Enlaces externos

* [while](https://www.php.net/manual/es/control-structures.while.php).
* [do-while](https://www.php.net/manual/es/control-structures.do.while.php).
* [break](https://www.php.net/manual/es/control-structures.break.php).
* [continue](https://www.php.net/manual/es/control-structures.continue.php).
* [PHP while Loop](https://www.w3schools.com/php/php_looping_while.asp).
* [PHP Break and Continue](https://www.w3schools.com/php/php_looping_break.asp)