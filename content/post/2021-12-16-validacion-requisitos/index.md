---
title: Validación de requisitos
subtitle: Análisis y especificación de requisitos
date: 2021-12-16
draft: false
description: Análisis y especificación de requisitos
tags: ["UF1844","Requisitos","Análisis","Requisitos - Validación"]
---

La **validación de requerimientos** es el proceso de verificar que los requerimientos definan realmente el sistema que en verdad quiere el cliente. Se solapa con el análisis, ya que se interesa por encontrar problemas con los requerimientos. La validación de requerimientos es importante porque los errores en un documento de requerimientos pueden conducir a grandes costos por tener que rehacer, cuando dichos problemas se descubren durante el desarrollo del sistema o después de que éste se halla en servicio.

En general, el costo por corregir un problema de requerimientos al hacer un cambio en el sistema es mucho mayor que reparar los errores de diseño o codificación. La razón es que un cambio a los requerimientos significa generalmente que también deben cambiar el diseño y la implementación del sistema. Más aún, el sistema debe entonces ponerse a prueba de nuevo.

Durante el proceso de validación de requerimientos, tienen que realizarse diferentes 
tipos de comprobaciones sobre los requerimientos contenidos en el documento de requerimientos. Dichas comprobaciones incluyen:

* _Comprobaciones de validez_ Un usuario quizá crea que necesita un sistema para 
realizar ciertas funciones. Sin embargo, con mayor consideración y análisis se logra 
identificar las funciones adicionales o diferentes que se requieran. Los sistemas tienen diversos participantes con diferentes necesidades, y cualquier conjunto de requerimientos es inevitablemente un compromiso a través de la comunidad de participantes.
* _Comprobaciones de consistencia_ Los requerimientos en el documento no deben 
estar en conflicto. Esto es, no debe haber restricciones contradictorias o descripciones diferentes de la misma función del sistema.
* _Comprobaciones de totalidad_ El documento de requerimientos debe incluir requerimientos que definan todas las funciones y las restricciones pretendidas por el usuario del sistema.
* _Comprobaciones de realismo_ Al usar el conocimiento de la tecnología existente, 
los requerimientos deben comprobarse para garantizar que en realidad pueden implementarse. Dichas comprobaciones también tienen que considerar el presupuesto y la fecha para el desarrollo del sistema.
* _Verificabilidad_ Para reducir el potencial de disputas entre cliente y contratista, los requerimientos del sistema deben escribirse siempre de manera que sean verificables. Esto significa que usted debe ser capaz de escribir un conjunto de pruebas que demuestren que el sistema entregado cumpla cada requerimiento especificado.