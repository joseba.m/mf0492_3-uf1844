---
title: Presentación Unidad Formativa UF1844
subtitle: Desarrollo de aplicaciones Web en el entorno servidor
date: 2021-12-01
draft: false
description: Desarrollo de aplicaciones Web en el entorno servidor
tags: ["UF1844","Presentación"]
---


Los contenidos de este blog se corresponden con los de la [UF 1844](https://apps.lanbide.euskadi.net/descargas/egailancas/certificados/UF/IFCD0210.MF0492_3.UF1844.pdf) Desarrollo de aplicaciones web en el entorno servidor, incardinada en el MF 0492_3 Programación web en el entorno servidor, del certificado IFCD0210 Desarrollo de aplicaciones con tecnologías web regulado por el [RD 1531/2011](https://www.boe.es/buscar/doc.php?id=BOE-A-2011-19503), de 31 de octubre, y modificado por el [RD 628/2013](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2013-9707), de 2 de agosto.

* Documento base (PDF) [IFCD0210_FIC.pdf](https://apps.lanbide.euskadi.net/descargas/egailancas/certificados/catalogo/IFCD0210_FIC.pdf).
* Datos Identificativos de la Unidad Formativa [IFCD0210.MF0492_3.UF1844.pdf - Lanbide](https://apps.lanbide.euskadi.net/descargas/egailancas/certificados/UF/IFCD0210.MF0492_3.UF1844.pdf)

![](img/01.png)

* Normativa (PDF) [IFCD0210_NOR.pdf](https://apps.lanbide.euskadi.net/descargas/egailancas/certificados/normativa/IFCD0210_NOR.pdf).
* [RD 1531/2011](https://www.boe.es/buscar/doc.php?id=BOE-A-2011-19503), [RD 628/2013](https://www.boe.es/diario_boe/txt.php?id=BOE-A-2013-9707).
* [Módulos y unidades formativas](http://apps.lanbide.euskadi.net/apps/ce_Modulos?LG=C&ML=FORMEN21&MS=Feaba&IDAR=IFCD&IDCE=IFCD0210&ORGN=&SVSG=N&IDUS=).

## Capacidades y criterios de evaluación

**Objetivo general**:

C1: Crear componentes software con tecnologías de **desarrollo orientadas a objetos**.

**Objetivos específicos**:

* CE1.1 En un supuesto práctico en el que se pide gestionar componentes software en el entorno del servidor mediante herramientas de desarrollo y lenguajes de programación contando con documentación de diseño detallado:
    * Crear y archivar componentes software.
    * Modificar y eliminar componentes software.
    * Depurar y verificar los componentes software elaborados.
* CE1.2 Crear objetos, clases y métodos adecuados a la funcionalidad del componente software a desarrollar utilizando lenguajes de programación orientados a objetos.
* CE1.3 Formular estructuras de datos y flujo de control mediante lenguajes de programación orientados a objetos según la funcionalidad del componente software a desarrollar.
* CE1.4 Documentar el componente software desarrollado.
* CE1.5 En un supuesto práctico en el que se pide construir componentes de software mediante herramientas de desarrollo y lenguajesde programación orientados a objetos a partir de documentación de diseño detallado:
    * Integrar componentes software de control del contenido de los documentos ubicados en el servidor para ser utilizados en el entorno del cliente tipo servlet.
    * Integrar gestión de ficheros en el componente software a desarrollar.
    * Integrar gestión de errores en el componente software a desarrollar.
    * Utilizar variables de servidor en el componente software a desarrollar para proporcionar acceso a las propiedades del servidor.
    * Integrar seguimiento de sesiones de usuario y propiedades de la aplicación web a desarrollar en el componente software a construir.
    * Crear componentes software con la funcionalidad de aplicación de cliente para ser utilizado en el entorno cliente tipo applet.
    * Crear componentes software que puedan ofrecer su funcionalidad a otros componentes software del mismo servidor u otros servidores de la red.