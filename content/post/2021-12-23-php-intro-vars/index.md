---
title: Introducción a las variables en PHP
subtitle: Introducción básica
date: 2021-12-23
draft: false
description: Introducción básica
tags: ["PHP","PHP - Introducción", "PHP - Variables","PHP - gettype"]
---

<!-- vscode-markdown-toc -->
* 1. [Declarar una variable en PHP, el símbolo $](#DeclararunavariableenPHPelsmbolo)
* 2. [Reglas para nombrar variables](#Reglasparanombrarvariables)
* 3. [Propiedades de las variables en PHP](#PropiedadesdelasvariablesenPHP)
* 4. [Tipos de variables](#Tiposdevariables)
	* 4.1. [Tipos escalares](#Tiposescalares)
		* 4.1.1. [Variables numéricas](#Variablesnumricas)
		* 4.1.2. [Booleanos](#Booleanos)
		* 4.1.3. [Cadenas](#Cadenas)
	* 4.2. [Tipos compuestos](#Tiposcompuestos)
	* 4.3. [Variables variables](#Variablesvariables)
	* 4.4. [Casting variables](#Castingvariables)
		* 4.4.1. [Operadores de conversión](#Operadoresdeconversin)
		* 4.4.2. [Funciones de conversión](#Funcionesdeconversin)
	* 4.5. [Funciones relacionadas con los tipos de datos](#Funcionesrelacionadasconlostiposdedatos)
		* 4.5.1. [Obtener el tipo de una variable con gettype() y comprobación de tipos con funciones is_*](#Obtenereltipodeunavariablecongettypeycomprobacindetiposconfuncionesis_)
		* 4.5.2. [var_dump, otra función útil para analizar variables](#var_dumpotrafuncintilparaanalizarvariables)
		* 4.5.3. [empty](#empty)
	* 4.6. [NULL](#NULL)
* 5. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Las variables son un contenedor donde guardar un valor que podemos manipular a lo largo de nuestro programa referenciando al nombre de la variable, simplemente imagínate una caja de cerillas que contiene un papel con una **cadena de texto** (_string_) escrita con un nombre.

![](img/02.png)

##  1. <a name='DeclararunavariableenPHPelsmbolo'></a>Declarar una variable en PHP, el símbolo $

![](img/01.jpg)

En PHP el nombre de variable debe venir siempre precedido por el carácter `$`.

```php
<?php
    $username = "Iker Landajuela";
?>
```

Las comillas que encierran el texto indican que es una cadena de caractéres, se pueden usar también las comillas simples (`$username = 'Iker Landajuela';`), aunque se parecen **tiene algunas diferencias hacerlo de una forma u otra**.

Cuando queremos saber que tenemos dentro de la caja podemos mostrar su contenido:

```php
<?php
    echo $username;
?>
```

O puede asignar su valor a otra variable:

```php
<?php
    $current_user = $username;    
?>
```

[src/vars-01.php](src/vars-01.php):

```php
<?php // vars-01.php
    // Variable tipo cadena
    $username = "Iker Landajuela";
    echo $username;
    echo "<br>"; // salto de línea HTML
    // Asignar valor de una variable a otra nueva
    $current_user = $username;
    echo $current_user;
?>
```

Si estas usando un servidor Web local en tu computadora ya puedes ver el resultado introduciendo la siguiente dirección [http://localhost/vars-01.php](http://localhost/vars-01.php) (ojo, la ruta de la URL depende de la carpeta en htdocs de XAMPP donde estés guardando los scripts). 

El resultado de la ejecución es que se mostrarán dos líneas con la cadena _Fred Smith_, resultado de `echo $username` y `echo $current_user`.

**Un nombre de variable válido tiene que empezar con una letra o un carácter de subrayado (underscore)**, seguido de cualquier número de letras, números y caracteres de subrayado.

[src/vars-02.php](src/vars-02.php).

```php
<?php
    $var = 'Roberto';
    $Var = 'Juan';
    echo "$var, $Var";      // imprime "Roberto, Juan"

    // PHP Parse error:  syntax error, unexpected integer "4", expecting variable or "{" or "$" in /workspace/Main.php on line 6
    $4site = 'aun no';      // inválido; comienza con un número
    $_4site = 'aun no';     // válido; comienza con un carácter de subrayado / guion bajo
?>
```

##  2. <a name='Reglasparanombrarvariables'></a>Reglas para nombrar variables

* Los nombres de variables después del símbolo `$` (obligatorio) deben empezar por una letra del alfabeto o el guion bajo "_".
* Los nombres sólo pueden contener carácteres a-z , A-Z , 0-9 , y _.
* ¡NO puede contener espacios!
* Son sensitivos a mayúsculas y minúsculas (_case-sensitive_). `$PuntuacionMax` y `$puntuacionmax` son variables diferentes (lectura adicional [Camel case (estilizado como camelCase)](https://es.wikipedia.org/wiki/Camel_case)).

##  3. <a name='PropiedadesdelasvariablesenPHP'></a>Propiedades de las variables en PHP

**Los datos NO deben de tener un tipo declarado explícitamente**, no está [fuertemente tipado](https://es.wikipedia.org/wiki/Tipado_fuerte) como en otros lenguajes.

Es de [tipado dinámico](https://es.wikipedia.org/wiki/Tipado_din%C3%A1mico), podemos cambiar el tipo de la variable en tiempo de ejecución:

[src/dynamically_typed.php](src/dynamically_typed.php)

```php
<?php
    $var1 = 1; # De tipo integer o entero
    $var1 = "ahora soy una cadena"; # cadena de texto o
?>
```

_"Se dice de un lenguaje de programación que usa un tipado estático cuando la comprobación de tipificación se realiza durante la compilación, y no durante la ejecución.[...] Comparado con el tipado dinámico, el estático permite que los errores de tipificación sean detectados antes, y que la ejecución del programa sea más eficiente y segura."_ [[Fuente](https://es.wikipedia.org/wiki/Sistema_de_tipos#Tipado_est%C3%A1tico)].

##  4. <a name='Tiposdevariables'></a>Tipos de variables

Como hemos visto PHP es un lenguaje poco tipado (_loosely typed language_). Es importante **no pensar que las variables en PHP no tienen un tipo**. Definitivamente lo tienen, es sólo que **pueden cambiar su tipo en tiempo de ejecución** y no necesitan que sus tipos sean declarados explícitamente cuando se inicializan.

PHP de forma implícita hará una conversión (**_casting_**) de la variable para los datos que requiera. Nos introduciremos en el _casting_ de variables en otra sección más adelante. Por el momento sólo necesitas saber que las variables de PHP sí tienen un tipo, que el tipo puede cambiar, y incluso puedes cambiarlo de forma explícita. 

PHP tiene **tres categorías de variables**: escalares, compuestos y recursos. Un escalar variable es una que solo puede tener un valor a la vez. Las variables compuestas pueden contener varios valores a la vez.

Un valor tipo [resource](https://www.php.net/manual/es/language.types.resource.php) es una variable especial, que contiene una referencia a un recurso externo. Los recursos son creados y usados por funciones especiales. Vea el [apéndice](https://www.php.net/manual/es/resource.php) para un listado de todas estas funciones y los tipos resource correspondientes.

Finalmente, PHP tiene un tipo especial [`NULL`](https://www.php.net/manual/es/language.types.null.php), es usado para variables que no tienen ningún valor asignado. 

###  4.1. <a name='Tiposescalares'></a>Tipos escalares

En PHP existen los siguientes **cuatro tipos de datos primitivos o escalares**.

![](img/03.png)

* `integer`. Para valores de tipo entero positivos o negativos (números sin decimales). Por ejemplo los valores 1, -5, 0
* `float`. Para valores de tipo real (números con decimales). El separador decimal en PHP es el carácter "." Por ejemplo los valores 1.7 , -5.0, 0.3.
* `string`. Para almacenar una cadena de caracteres. Para definir cadenas de caracteres se puede usar como delimitador de inicio y fin el carácter ' o el carácter ". Por ejemplo "Hola", 'hola', "".
* `boolean`. Para variables que solo pueden tener el valor verdadero (`TRUE`) o falso (`FALSE`). Se usan para construir estructuras condicionales.

Algunos tipos tienen un alias, en el siguiente ejemplo usamos `bool` como alias de `boolean`:

[src/boolean.php](src/boolean.php).

```php
<?php
    $a = (boolean)true;
    $b = (bool)true;
    var_dump($a === $b); // bool(true)
?>
```

Las cadenas (_string_) no son simplemente una lista o secuencia de caracteres. Internamente PHP contiene información sobre su longitud y no terminan en `NULL`. Eso significa que una cadena puede contener información binaria, por ejemplo el contenido de un archivo con una imagen leida de disco. En otras palabras, **las cadenas en PHP son _binary safe_** (en [este foro](https://qastack.mx/programming/3264514/in-php-what-does-it-mean-by-a-function-being-binary-safe) se explica en que consisten).

####  4.1.1. <a name='Variablesnumricas'></a>Variables numéricas

Las variables no sólo pueden contener cadenas de texto, también pueden contener números. Los números en PHP se expresan usando una notación familiar.

[src/numbers.php](src/numbers.php)

```php
<?php
print 56;
print 56.3;
print 56.30;
print 0.774422;
print 16777.216;
print 0;
print -213;
print 1298317;
print -9912111;
print -12.52222;
print 0.00;
?>
```

##### Usando diferentes tipos de números

PHP internamente distingue entre números con parte decimal o sin ella. Formalmente los primeros se denominan de coma flotante (_floating-point_) y los segundos como enteros (_integers_). 

```php
<?php
echo 43;
?>
```

Este es un **tipo entero** o _integer_, pueden ser negativos o positivos. Los números con decimales se denominan **flotantes** (_float_), como separador se usa el `.`.:

```php
<?php
    $count = 17.125;
?>
```

####  4.1.2. <a name='Booleanos'></a>Booleanos

Este es el tipo más simple. Un _boolean_ expresa un valor que indica verdad. Puede ser _true_ (verdadero) o _false_ (falso). 

```php
<?php
    $foo = True; // asigna el valor TRUE a $foo
    echo $foo;  // 1
    $foo = TRUE; 
    echo true;  // 1
?>
```

####  4.1.3. <a name='Cadenas'></a>Cadenas

Cuando se usan en aplicaciones, una pieza de texto se llama cadena o _string_ en inglés. Esto es porqué consisten en elementos individuales (caracteres) encadenados entre sí. Las cadenas (_strings_) pueden contener letras, números, espacios o cualquier otro carácter. Algunos ejemplos de cadenas:

[src/cadenas-01.php](src/cadenas-01.php)

```php
<?php
    print 'Quiero un cuenco de sopa.';
    print 'pollo';
    print '06520';
    print 'Estoy cenanando," dijo él.';
?>
```

Una cadena incluso puede almacenar los contenidos de un archivo binario, como una imagen o un archivo de audio. 

**Nota**: Las cadenas en PHP son secuencias de _bytes_, no caracteres. 

Como las cadenas consisten en todo lo que se encierra entre comillas simples, se imprime lo siguiente:

`Quiero un cuenco de sopa.pollo06520Estoy cenanando," dijo él.`

Fijate que la salida de las cuatro sentencias [`print`](https://www.php.net/manual/es/function.print.php) se muestran en una sola línea. [`print`](https://www.php.net/manual/es/function.print.php) no añade ningún salto de línea (con [`echo`](https://www.php.net/manual/es/function.echo) pasa lo mismo).

Las comillas simples del ejemplo no son parte del _string_. Son **delimitadores**, que le dicen al intérprete de PHP donde comienza y finaliza la cadena. Si quieres incluir una comilla simple dentro de la cadena rodeada por comillas simples, introduce una barra invertida `\` delante:

```php
<?php
    print 'Mi amigo \'Mikel\' es el mejor';
?>
```

Genera la siguiente salida: `Mi amigo 'Mikel' es el mejor`.

La barra "\" le dice al interprete de PHP que trate el carácter que viene después ("'") de forma literal como una comilla simple en lugar de fin de cadena. Esto se llama escapar (_scaping_), y la barra "\" se llama [**carácter de escape**](https://www.php.net/manual/es/regexp.reference.escape.php). Un carácter de escape le dice al sistema que trate de forma especial el carácter que viene a continuación.

El propio carácter de escape puede ser escapado. Para incluir de forma literal la barra "\" ponemos ese mismo carácter por delante.

```php
<?php
    print 'Usa un \\ para escapar una cadena';
?>
```

Produce la salida: `Usa un \ para escapar una cadena`.

Puedes incluir espacios y nuevas líneas en una cadena con las comillas simples:

```php
<?php
print '<ul>
<li>Pure de manzana</li>
<li>Tofu ahumado</li>
<li>Ensalada de marisco</li>
</ul>';
?>
```

Los únicos caracteres que tienen un tratamiento especial dentro de las cadenas con comillas simples son "\" y "'". Cualquier otra cosa se interpreta de forma literal.

También puedes delimitar las cadenas con comillas dobles. Las cadenas con comillas dobles son similares a las cadenas construidas con comillas simples, pero aceptan más caracteres especiales.

![](img/04.png)

**La mayor diferencia entre las cadenas con comillas simples o dobles es cuando incluyes una variable en una cadena con comillas dobles**, el valor de la variable es introducida en la cadena, cosa que no pasa con las cadenas con comillas simples.

```php
<?php
    $num = 123;
    echo 'Valor de $num con comillas simples.';
    echo "Valor de $num con comillas dobles";
?>
```

Produce esta salida: `Valor de $num con comillas simples.Valor de 123 con comillas dobles`.

##### Sintaxis 'here document'

Se pueden definir cadenas usando la sintaxis [_here document_](https://www.php.net/manual/es/language.types.string.php#language.types.string.syntax.heredoc) (abreviado como _heredoc_). Un _heredoc_ comienza con `<<<` y una palabra delimitadora. Finaliza con la misma palabra al comienzo de una nueva línea.

[src/heredoc.php](src/heredoc.php)

```php
<<<HTMLBLOCK
<html>
    <head>
        <title>Menu</title>
    </head>

    <body bgcolor="#fffed9">
        <h1>Cena</h1>
        <ul>
            <li> Ternera
            <li> Patatas fritas
            <li> Noodles
        </ul>
    </body>

</html>
HTMLBLOCK;
```

El texto **_heredoc_ se comporta como un string entre comillas dobles. No es necesario escapar las comillas en un heredoc, pero se pueden seguir empleando los códigos de la tabla de sentencias de escape para caracteres especiales.**

En el ejemplo de arriba la palabra que usamos como delimitador es "HTMLBLOCK". Los delimitadores _heredoc_ pueden contener letras, números y el guion bajo. El primer carácter debe ser una letra o un guion bajo (igual que las variables), es una **buena práctica** usar mayúsculas en el delimitador para facilitar la legibilidad, aunque no es obligatorio. El delimitador que finaliza el _heredoc_ debe estar sólo en una línea. El delimitador no se puede identar y no puede contener espacios en blanco, comentarios o cualquier otro carácter detras.

El siguiente ejemplo muestra la sintaxis para imprimir un _heredoc_.

[src/heredoc.php](src/heredoc.php)

```php
print <<<HTMLBLOCK
<html>
    <head>
        <title>Menu</title>
    </head>

    <body bgcolor="#fffed9">
        <h1>Cena</h1>
        <ul>
            <li> Ternera
            <li> Patatas fritas
            <li> Noodles
        </ul>
    </body>

</html>
HTMLBLOCK;
```

##### Concatenar cadenas

Para concatenar dos cadenas, usa el operador "." de concatenación de cadenas.

[src/str-concat.php](src/str-concat.php)

```php
<?php
print 'pan' . 'fruta';
print "Es un día estupendo " . 'en el vecindario.';
print "El precio es: " . '$3.95';
print 'Inky' . 'Pinky' . 'Blinky';
?>
```

###  4.2. <a name='Tiposcompuestos'></a>Tipos compuestos

Los arreglos (_arrays_) y otras estructuras más complejas del lenguaje PHP como las clases las veremos más adelante.

###  4.3. <a name='Variablesvariables'></a>Variables variables

```php
<?php
  $var1 = 'nombreDeVariable';
  $nombreDeVariable = '¡Este es el valor que quería!';
  echo $$var1; // ¡Este es el valor que quería! 
?>
```

###  4.4. <a name='Castingvariables'></a>Casting variables

PHP de forma implícita realiza una conversión de tipo (_casting_) al tipo necesario para realizar una operación.

Es posible realizar una conversión de tipo de forma explicita usando una de estas opciones:

* Usando un operador de conversión.
* Usando una función PHP.

####  4.4.1. <a name='Operadoresdeconversin'></a>Operadores de conversión

Los operadores de conversión se usan introduciendo el nombre del tipo de dato que al quieres convertir entre paréntesis antes del nombre de la variable. Por ejemplo:

```php
<?php
$a = '123';  // $a es cadena (string)
$a = (int)$a;  // $a ahora es un entero (integer)
$a = (bool)$a;  // $a ahora es un booleano y contiene TRUE
echo $a; // 1 = TRUE

// Prueba con $a = '0'; ---> Sale FALSE al final
?>
```

La conversión a `NULL` está _deprecated_ desde la versión de PHP 7.2

####  4.4.2. <a name='Funcionesdeconversin'></a>Funciones de conversión

También existen funciones en PHP que permiten convertir una variable a un tipo de dato, sus nombres dan fe de la labor que realizan: [`floatval`](https://www.php.net/manual/es/function.floatval.php), [`intval`](https://www.php.net/manual/es/function.intval), [`strval`](https://www.php.net/manual/es/function.strval.php) y [`boolval`](https://www.php.net/manual/es/function.boolval.php).

Se puede usar la función más genérica [`settype`](https://www.php.net/manual/es/function.settype) pasando como parámetros la variable y el tipo al que queremos convertir como segundo argumento ("boolean" o "bool", "integer" o "int", "float" o "double", "string", "array", "object", "null").

```php
<?php
$foo = "5bar"; // cadena
$bar = true;   // booleano

settype($foo, "integer"); // $foo es ahora 5   (entero)
echo $foo; // 5
settype($bar, "string");  // $bar es ahora "1" (cadena)
echo $bar; // 1
?>
```

Existen numerosas reglas que debes recordar sobre como realiza la conversión de tipos PHP. En lugar de mencionar todas las reglas o casos posibles, nos vamos a enfocar en los errores más comunes.

Realizar una conversión de tipo de _float_ a _integer_ no redondea el valor hacia arriba o hacia abajo, simplemente trunca la porción decimal:

```php
<?php
$a = 1234.88;
echo (int)$a;  // 1234 (no 1235)

$a = -1234.99;
echo (int)$a;  // -1234
?>
```

##### Algunas reglas generales para convertir a un tipo Booleano

* Las cadenas y arrays vacíos son convertidos a `false`.

```php
<?php
    $str = "";
    $bool = boolval($str);
    var_dump($bool); // bool(false)
?>
```

* Las cadenas siempre se evaluan como `TRUE` excepto cuando contienen un valor que PHP considera "vacío".

* Las cadenas que representan un número se evaluan como `TRUE` si el número no es cero.

```php
<?php
$bool = boolval("0");
var_dump($bool); // bool(false)

$bool = boolval("99");
var_dump($bool); // bool(true)
?>
```

* Cualquier entero (_integer_) o flotante (_float_) que no sea 0 es `TRUE`, también los números negativos.

```php
<?php
var_dump(boolval(-1)); // bool(true)
?>
```

Manipulación de tipos [https://www.php.net/manual/es/language.types.type-juggling.php](https://www.php.net/manual/es/language.types.type-juggling.php).

Conversión de string a números [https://www.php.net/manual/es/language.types.string.php#language.types.string.conversion](https://www.php.net/manual/es/language.types.string.php#language.types.string.conversion)

##### Floats y integers (pending)

Debes tener cuidado realizando la conversión de tipos entre _float_ e _integer_. 

```php
<?php
echo (int) ( (0.1+0.7) * 10 ); // 7
echo (int) ( (0.1+0.5) * 10); // 6
?>
```

###  4.5. <a name='Funcionesrelacionadasconlostiposdedatos'></a>Funciones relacionadas con los tipos de datos

####  4.5.1. <a name='Obtenereltipodeunavariablecongettypeycomprobacindetiposconfuncionesis_'></a>Obtener el tipo de una variable con gettype() y comprobación de tipos con funciones is_*

[`gettype`](https://www.php.net/manual/es/function.gettype) Devuelve el tipo de la variable. Para la comprobación de tipos, utilice las funciones is_* como abajo.

[gettype.php](src/gettype.php):

```php
<?php //gettype.php
    $username = "Fred Smith";
    echo gettype($username); # string
    echo is_string($username); # 1 (TRUE)

    $count = 17;
    echo gettype($count); # integer
    echo is_int($count); # 1 (TRUE)

    $temperatura = 36.8;
    echo gettype($temperatura); # double
    echo is_float($temperatura); # 1 (TRUE)
?>
```

####  4.5.2. <a name='var_dumpotrafuncintilparaanalizarvariables'></a>var_dump, otra función útil para analizar variables 

[`var_dump`](https://www.php.net/manual/es/function.var-dump) Muestra información sobre una variable.

```php
<?php 
    $name = 'Iker';
    var_dump($name); # string(4) "Iker"
?>
```

Esta función muestra información estructurada sobre una o más expresiones incluyendo su tipo y valor. Las matrices y los objetos son explorados recursivamente con valores sangrados para mostrar su estructura.

Probar también [print_r()](https://www.php.net/manual/es/function.print-r.php).

####  4.5.3. <a name='empty'></a>empty

Determina si una variable está vacía. Tiene la sintaxis:  [`bool empty($var)`](https://www.php.net/manual/es/function.empty).

Una  variable  se  considera  vacía  si  no  existe  si  su  valor  es  igual  a  `FALSE`.  No  genera  una  advertencia  si la variable no existe.


[src/empty.php](src/empty.php)

```php
<?php
$var = 0;

// Se evalúa a true ya que $var está vacia
if (empty($var)) {
    echo '$var es o bien 0, vacía, o no se encuentra definida en absoluto';
}

// Se evalúa como true ya que $var está definida
if (isset($var)) {
    echo '$var está definida a pesar que está vacía';
}

// una cadena vacia 
$cadena = "";
if (empty($var)) {
    echo "La cadena está vacia";
}

if(!$cadena) echo "Una cadena vacia se evalua como false";
?>
```

**Nota**: No se genera una advertencia si la  variable  no  existe.  Esto  significa que empty() es esencialmente el equivalente conciso de `!isset($var) || $var == false`.

Las siguientes expresiones son consideradas como vacías:
* "" (cadena vacía).
* 0 (0 como _integer_ o _float_).
* "0" (como _string_).
* NULL.
* FALSE.
* array() (un array vacío).
* $var; (una variable declarada, pero sin un valor).

###  4.6. <a name='NULL'></a>NULL

El valor especial [`null`](https://www.php.net/manual/es/language.types.null.php) representa una variable sin valor. null es el único valor posible del tipo [`null`](https://www.php.net/manual/es/language.types.null.php). 

No hay más que un valor de tipo null, y es la constante null insensible a mayúsculas/minúsculas. 

```php
$var = NULL;    
    
$var1;
var_dump($var1); // NULL
echo is_null($var1); // 1
```

##  5. <a name='Enlacesexternos'></a>Enlaces externos

* [Conceptos básicos](https://www.php.net/manual/es/language.variables.basics.php).
* [Variables](https://www.php.net/manual/es/language.variables.php).

* [Booleanos](https://www.php.net/manual/es/language.types.boolean.php).

* [echo](https://www.php.net/manual/es/function.echo.php) - Muestra una o más cadenas.
* [print](https://www.php.net/manual/es/function.print).
* [var_dump](https://www.php.net/manual/es/function.var-dump).
* [print_r](https://www.php.net/manual/es/function.print-r).

* [gettype](https://www.php.net/manual/es/function.gettype) - Obtener el tipo de una variable.
* [is_string](https://www.php.net/manual/en/function.is-string). 
