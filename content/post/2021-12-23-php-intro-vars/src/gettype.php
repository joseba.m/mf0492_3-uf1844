<?php //test2.php
    $username = "Fred Smith";
    echo gettype($username); # string
    echo is_string($username); # 1 (TRUE)

    $count = 17;
    echo gettype($count); # integer
    echo is_int($count); # 1 (TRUE)

    $temperatura = 36.8;
    echo gettype($temperatura); # double
    echo is_float($temperatura); # 1 (TRUE)
?>  