<?php
<<<HTMLBLOCK
<html>
    <head>
        <title>Menu</title>
    </head>

    <body bgcolor="#fffed9">
        <h1>Cena</h1>
        <ul>
            <li> Ternera
            <li> Patatas fritas
            <li> Noodles
        </ul>
    </body>

</html>
HTMLBLOCK;

// =======================================================================

// Heredoc con espacios en blanco después de primer delimitador produce error.
// PHP Parse error:  syntax error, unexpected token "<<", expecting end of file in /workspace/Main.php on line XX
<<<'HTMLBLOCK'    
test
HTMLBLOCK;

// Parse error
<<<'HTMLBLOCK' // comentario
test
HTMLBLOCK;

// =======================================================================

print <<<HTMLBLOCK
<html>
    <head>
        <title>Menu</title>
    </head>

    <body bgcolor="#fffed9">
        <h1>Cena</h1>
        <ul>
            <li> Ternera
            <li> Patatas fritas
            <li> Noodles
        </ul>
    </body>

</html>
HTMLBLOCK;


?>