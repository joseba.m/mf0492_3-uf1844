---
title: Concepto de genericidad
subtitle: Principios de la orientación a objetos - Genericidad
date: 2022-02-02
draft: false
description: Principios de la orientación a objetos - Genericidad
tags: ["Herencia","POO","Objetos","Genericidad"]
---

Un posible enfoque de diseño es agrupar aquellos elementos del sistema que utilizan estructuras semejantes o que necesitan de un tratamiento similar. Si se prescinde de los aspectos específicos de cada elemento concreto es bastante razonable diseñar un elemento genérico con las características comunes a todos los elementos a todos los elementos agrupados. Posteriormente, cada uno de los elementos agrupados se pueden diseñar como un caso particular del elemento genérico. Esta es la idea básica que aporta el concepto de genericidad.

Para ilustrar este concepto con un ejemplo, supongamos que se trata de diseñar un sistema operativo multitarea que necesita atender las ordenes que le llegan de los disintos terminales. Una de las ordenes habituales será imprimir por cualquiera de las impresoras disponibles. Por otro lado, es normal que desde varios terminales simultaneamente se necesite acceder a ficheros o bases de datos compartidas. Cada una de estas actividades necesita de un módulo gestor para decidir en que orden se atienden las peticiones. 

La forma más sencilla de gestión es poner en colas las ordenes, peticiones de impresión o peticiones de acceso a ficheros o bases de datos compartidas. Inmediatamente surge la necesidad de una estructura genérica en forma de cola FIFO / LIFO para la que se necesitan también de unas operaciones genéricas tales como poner en cola, sacar el primero, etc.

En cada caso el tipo de información que se guarda en la cola es diferente: tipo de orden a ejecutar, texto a imprimir, dato a consultar, etc. A partir de la cola genérica y con un diseño posterior más detallado se tendrá que decidir la estructura concreta de las distintas colas necesarias y si para alguna de ellas es conveniente utilizar prioridades, lo que daría lugar a operaciones específicas. 

Indudablemente el concepto de genericidad es de gran utilidad en el diseño y da lugar a soluciones simples y fáciles de mantener. Sin embargo, la implementación de los elementos genéricos obtenidos como resultado del diseño puede resultar bastante compleja e incluso desvirtuar el propio concepto de genericidad.

## Enlaces externos

* [https://wiki.php.net/rfc/generics](https://wiki.php.net/rfc/generics).
* [https://stitcher.io/blog/php-generics-and-why-we-need-them](https://stitcher.io/blog/php-generics-and-why-we-need-them).

