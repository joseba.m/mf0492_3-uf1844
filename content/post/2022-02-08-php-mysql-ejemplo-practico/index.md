---
title: Ejemplo práctico
subtitle: Interacción con MySQL usando formularios y PHP 
date: 2022-02-08
draft: false
description: Interacción con MySQL usando formularios y PHP 
tags: ["PHP","PHP - BD","MySQL","Bases de Datos"]
---

<!-- vscode-markdown-toc -->
* 1. [El array $_POST](#Elarray_POST)
* 2. [Borrando un registro](#Borrandounregistro)
* 3. [Visualizando el formulario](#Visualizandoelformulario)
* 4. [Realizando una consulta a la base de datos](#Realizandounaconsultaalabasededatos)
* 5. [Enlaces internos](#Enlacesinternos)
* 6. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Ha llegado el momento de escribir nuestro primer ejemplo para **insertar y borrar registros de una tabla** MySQL. Voy a crear un formulario para insertar nuevos registros en la base de datos que hemos usado en artículos previos ([sql/usuarios.sql](sql/usuarios.sql)), después del formulario de inserción mostraré todos los registros de la tabla y un botón que permite borrar cada uno de ellos.

Archivo [src/sqltest.php](src/sqltest.php)

```php
<?php // sqltest.php
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) die("Fatal Error");

if (isset($_POST['delete']) && isset($_POST['id'])) {
    $id = get_post($conn, 'id');
    $query = "DELETE FROM usuarios WHERE id=$id";
    $result = $conn->query($query);
    if (!$result) echo "DELETE failed<br><br>";    
}

if (isset($_POST['user']) && isset($_POST['password']) ) {
    $user = get_post($conn, 'user');
    $password = get_post($conn, 'password');    
    $query = "INSERT INTO usuarios(User,Password) VALUES('$user', '$password')";        
    $result = $conn->query($query);    
    if (!$result) echo "INSERT failed, query=".$query."<br><br>";    
}

echo <<<_END
<form action="sqltest.php" method="post">
    <pre>
        User <input type="text" name="user">
        Password <input type="text" name="password">
        <input type="submit" value="ADD RECORD">
    </pre>
</form>
_END;

$query = "SELECT * FROM usuarios";
$result = $conn->query($query);
if (!$result) die ("Database access failed");
$rows = $result->num_rows;

for ($j = 0 ; $j < $rows ; ++$j) {
    $row = $result->fetch_array(MYSQLI_NUM);
    $r0 = htmlspecialchars($row[0]);
    $r1 = htmlspecialchars($row[1]);
    $r2 = htmlspecialchars($row[2]);

echo <<<_END
<pre>
    Id $r0
    User $r1
    Password $r2        
</pre>
    
<form action='sqltest.php' method='post'>
    <input type='hidden' name='delete' value='yes'>
    <input type='hidden' name='id' value='$r0'>
    <input type='submit' value='DELETE RECORD'>
</form>
_END;
}

$result->close();
$conn->close();

function get_post($conn, $var) {
    return $conn->real_escape_string($_POST[$var]);
}
?>
```

Mi ejemplo en ejecución en el navegador:

![](img/01.png)

Siguiendo los ejemplos previos los parámetros de conexión con la base de datos estarán alojados en otro archivo ([src/login.php](src/login.php)) que incluiré en el script principal ([src/sqltest.php](src/sqltest.php)).

Primero creo una instancia de la clase [`mysqli`](https://www.php.net/manual/es/book.mysqli.php) **para establecer la conexión con la base de datos** ([mysqli::__construct](https://www.php.net/manual/es/mysqli.construct.php)).

Usando la función [`isset`](https://www.php.net/manual/es/function.isset) compruebo si se ha enviado el formulario para **borrar un registro con POST**. La variable `$_POST['delete']` es un campo oculto dentro del formulario asociado a cada registro de la tabla que permite borrarlos. También compruebo en la misma sentencia condicional (usando un [operador lógico](https://www.php.net/manual/es/language.operators.logical.php) AND) si he recibido la clave primaria del registro "Id" para ejecutar la sentencia SQL [DELETE](https://www.w3schools.com/sql/sql_delete.asp). 

Para **insertar un nuevo registro** en la tabla hago algo similar, compruebo que recibo por POST el usuario y su contraseña para ejecutar la sentencia SQL [INSERT](https://www.w3schools.com/sql/sql_insert.asp).

Al final del script he creado una pequeña función que hace un trabajo esencial, le paso como parámetros la clave del array $_POST del valor que quiero obtener, convierto la cadena usando el método de mysqli [`real_escape_string`](https://www.php.net/manual/en/mysqli.real-escape-string) para convertir los carácteres especiales de escape en una cadena SQL válida (los caracteres codificados son NUL (ASCII 0), \n, \r, \, ', ", y Control-Z.).

##  1. <a name='Elarray_POST'></a>El array $_POST

Ya hemos mencionado antes que el navegador envía las entradas del usuario usando una llamada GET o POST. El método POST es generalmente preferido porque evita colocar los datos de forma visible en la URL. El servidor Web recoge todos los datos de la llamada y los coloca en un _array_ llamado `$_POST`.

**`$_POST` Es un _array_ asociativo**, cada campo del formulario contiene un elemento en el array con su nombre. Si un formulario contiene un campo llamado "Id", el array `$_POST` contendrá un elemento con la clave con la cadena "Id". Nuestro programa PHP puede leer ese campo refiriendose a él como `$_POST['Id']` o `$_POST["Id"]` (las comillas simples o dobles tienen el mismo efecto en este caso).

Si la sintaxis `$_POST` resulta compleja para ti, copia las entradas de usuario en variables y olvidate del `$_POST`. 

Volviendo sobre la función `get_post`, pasa el parámetro recibido a través del método [`real_escape_string`](https://www.php.net/manual/en/mysqli.real-escape-string) del objeto conexión para evitar cualquier comilla que un atacante haya insertado para alterar o romper tu base de datos (los caracteres codificados son NUL (ASCII 0), \n, \r, \, ', ", y Control-Z.).  

```php
function get_post($conn, $var) {
    return $conn->real_escape_string($_POST[$var]);
}
```

Si por ejemplo le pasamos en el formulario como nombre de usuario `"Iker`, la función lo convierte en  `\"Iker` y en la tabla quedará almacenado como `"Iker`.

##  2. <a name='Borrandounregistro'></a>Borrando un registro

Antes de comprobar si hemos recibido nuevos datos de un usuario, el programa comprueba si la variable `$_POST['delete']` contiene algún valor. Si es así, significa que el usuario ha hecho clic en el botón "DELETE RECORD" para borrar un registro. En ese caso también deberiamos recibir el valor de `$Id` en el POST. El "Id" identifica un registro de forma exclusiva (yo he definido la columna como clave primaria única autoincremental).

Si `$_POST['delete']` no está establecido (y por lo tanto no hay registro que borrar), compruebo otras variables como `$_POST['user']`. 

Si recibo el usuario y la contraseña la consulta `$query` será una sentencia SQL [INSERT](https://www.w3schools.com/sql/sql_insert.asp) seguida de los valores de la tabla que quiero insertar. Le paso la cadena con la consulta al método [`query`](https://www.php.net/manual/es/mysqli.query) de la clase mysqli que retorna false en caso de error.

```php
if (!$result) echo "INSERT failed, query=".$query."<br/>";    
```

##  3. <a name='Visualizandoelformulario'></a>Visualizando el formulario

Antes de mostrar el formulario de borrado para cada registro pasamos cada elemento de la fila `$row` de la consulta [SELECT](https://www.w3schools.com/sql/sql_select.asp) por la función [`htmlspecialchars`]https://www.php.net/manual/es/function.htmlspecialchars) que convierte caracteres especiales en entidades HTML, debemos usar esta función cuando queramos escapar carateres que tienen un significado por si solos en HTML, por ejemplo convierte el carácter "<" en "&lt;". 

Prueba este ejemplo:

```php
<?php
$str = "This is some <b>bold</b> text.";
echo htmlspecialchars($str);
?>
```

En el código fuente del navegador (CTRL+U) se ha convertido en:

```
This is some &lt;b&gt;bold&lt;/b&gt; text.
```

Y en el navegador lo veremos así:

```
This is some <b>bold</b> text.
```

Luego sigue la parte donde mostramos la salida usando la estructura `echo <<<_END ... _END;` usando la sintaxis [heredoc](https://www.php.net/manual/es/language.types.string.php) para mostrar varias líneas sin necesidad de hacer un `echo` para cada una.

El formulario HTML establece la acción del form llamandose a si mismo, esto significa que cuando se procese el formulario los campos del formulario se enviarán al archivo "sqltest.php". 

Uso el elemento HTML [<pre>](https://developer.mozilla.org/es/docs/Web/HTML/Element/pre) para representar el texto preformateado exactamente como es mostrado en el archivo (incluyendo los saltos de línea).

##  4. <a name='Realizandounaconsultaalabasededatos'></a>Realizando una consulta a la base de datos

Esta parte debería resultarte familiar, se realiza una consulta MySQL para para obtener todos los registros de la tabla usuarios.

```php
$query = "SELECT * FROM usuarios";
$result = $conn->query($query);
```

Después guardo el número de filas resultado en una variable:

```php
$rows = $result->num_rows;
```

Usando el valor de `$rows` recorro cada fila usando un bucle `for` y obtengo los datos usando el método [`fetch_array`](https://www.php.net/manual/es/mysqli-result.fetch-array), la constante `MYSQLI_NUM` indica que los resultados deben retornarse en un array númerico (en lugar de uno asociativo):

```php
$row = $result->fetch_array(MYSQLI_NUM);
```

Despues de mostrar un registro usando la estructura heredoc hay un segundo formulario que manda el POST a sí mismo (sqltest.php) como antes, esta vez contiene dos campos ocultos (_hidden_): `delete` e `id`. El campo `delete` está establecido a `yes` y el `id` está asociado al valor obtenido en `$row[0]`, que contiene el identificador del registro.

A continuación se muestra un botón (`submit`) "DELETE RECORD" y se cierra el formulario. Usamos la llave de cierre `}` para cerrar el bucle `for`, que continuará hasta que se hayan visualizado todos los registros. Al final llamamos al método `close` para liberar los recursos de los objetos `$result` y `$conn`.

```php
$result->close();
$conn->close();
```

Al final, se muestra la definición de la función `get_post`. 

##  5. <a name='Enlacesinternos'></a>Enlaces internos

* [Creando formularios - Procesar la información de los usuarios](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-20-php-forms/).
* [Mi primera conexión con una base de datos - PHP y Bases de datos MySQL](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-06-conexion-a-base-de-datos/).

##  6. <a name='Enlacesexternos'></a>Enlaces externos

* [die — Equivalente a exit](https://www.php.net/manual/es/function.die).
* [isset](https://www.php.net/manual/es/function.isset)
* [$_POST](https://www.php.net/manual/es/reserved.variables.post): Un array asociativo de variables pasadas al script actual a través del método POST de HTTP.
* [SQL DELETE Statement - W3Schools](https://www.w3schools.com/sql/sql_delete.asp).
* [SQL INSERT INTO Statement - W3Schools](https://www.w3schools.com/sql/sql_insert.asp).
* [mysqli::real_escape_string](https://www.php.net/manual/en/mysqli.real-escape-string).