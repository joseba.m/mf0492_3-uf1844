<?php // sqltest.php
require_once 'login.php';

$conn = new mysqli($hn, $un, $pw, $db);

if ($conn->connect_error) die("Fatal Error");

if (isset($_POST['delete']) && isset($_POST['id'])) {
    $id = get_post($conn, 'id');
    $query = "DELETE FROM usuarios WHERE id=$id";
    $result = $conn->query($query);
    if (!$result) echo "DELETE failed<br><br>";    
}

if (isset($_POST['user']) && isset($_POST['password']) ) {
    $user = get_post($conn, 'user');
    $password = get_post($conn, 'password');    
    $query = "INSERT INTO usuarios(User,Password) VALUES('$user', '$password')";        
    $result = $conn->query($query);    
    if (!$result) echo "INSERT failed, query=".$query."<br/>";    
}

echo <<<_END
<form action="sqltest.php" method="post">
    <pre>
        User <input type="text" name="user">
        Password <input type="text" name="password">
        <input type="submit" value="ADD RECORD">
    </pre>
</form>
_END;

$query = "SELECT * FROM usuarios";
$result = $conn->query($query);
if (!$result) die ("Database access failed");
$rows = $result->num_rows;

for ($j = 0 ; $j < $rows ; ++$j) {
    $row = $result->fetch_array(MYSQLI_NUM);
    $r0 = htmlspecialchars($row[0]);
    $r1 = htmlspecialchars($row[1]);
    $r2 = htmlspecialchars($row[2]);

echo <<<_END
<pre>
    Id $r0
    User $r1
    Password $r2        
</pre>
    
<form action='sqltest.php' method='post'>
    <input type='hidden' name='delete' value='yes'>
    <input type='hidden' name='id' value='$r0'>
    <input type='submit' value='DELETE RECORD'>
</form>
_END;
}

$result->close();
$conn->close();

function get_post($conn, $var) {
    return $conn->real_escape_string($_POST[$var]);
}
?>