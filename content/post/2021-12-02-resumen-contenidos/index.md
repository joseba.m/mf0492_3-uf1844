---
title: UF1844 Resumen de contenidos 
subtitle: Desarrollo de aplicaciones Web en el entorno servidor
date: 2021-12-02
draft: false
description: Desarrollo de aplicaciones Web en el entorno servidor
tags: ["UF1844","TOC","Contenidos","Resumen","Índice"]
---

<!-- vscode-markdown-toc -->
* 1. [El proceso del desarrollo de software](#Elprocesodeldesarrollodesoftware)
	* 1.1. [Modelos del ciclo de vida del software](#Modelosdelciclodevidadelsoftware)
	* 1.2. [Análisis y especificación de requisitos](#Anlisisyespecificacinderequisitos)
	* 1.3. [Diseño](#Diseo)
	* 1.4. [Implementación. Conceptos generales de desarrollo de software.](#Implementacin.Conceptosgeneralesdedesarrollodesoftware.)
	* 1.5. [Validación y verificación de sistemas.](#Validacinyverificacindesistemas.)
	* 1.6. [Pruebas de software](#Pruebasdesoftware)
	* 1.7. [Calidad del software](#Calidaddelsoftware)
	* 1.8. [Herramientas de uso común para el desarrollo de software](#Herramientasdeusocomnparaeldesarrollodesoftware)
	* 1.9. [Gestión de proyectos de desarrollo de software.](#Gestindeproyectosdedesarrollodesoftware.)
* 2. [Introducción a PHP](#IntroduccinaPHP)
	* 2.1. [Programación modular](#Programacinmodular)
	* 2.2. [Trabajando con ficheros y directorios](#Trabajandoconficherosydirectorios)
	* 2.3. [Desarrollo Web con PHP](#DesarrolloWebconPHP)
		* 2.3.1. [Sistema de plantillas](#Sistemadeplantillas)
* 3. [Adicional](#Adicional)
* 4. [La orientación a objetos.](#Laorientacinaobjetos.)
	* 4.1. [Principios de la orientación a objetos. Comparación con la programación estructurada](#Principiosdelaorientacinaobjetos.Comparacinconlaprogramacinestructurada)
	* 4.2. [Clases de objetos](#Clasesdeobjetos)
	* 4.3. [Objetos](#Objetos)
	* 4.4. [Herencia](#Herencia)
	* 4.5. [Modularidad](#Modularidad)
	* 4.6. [Genericidad y sobrecarga](#Genericidadysobrecarga)
	* 4.7. [Desarrollo orientado a objetos](#Desarrolloorientadoaobjetos)
	* 4.8. [Lenguajes de modelización en el desarrollo orientado a objetos](#Lenguajesdemodelizacineneldesarrolloorientadoaobjetos)
* 5. [Arquitecturas web](#Arquitecturasweb)
	* 5.1. [Concepto de arquitectura web.](#Conceptodearquitecturaweb.)
	* 5.2. [El modelo de capas](#Elmodelodecapas)
	* 5.3. [Plataformas para el desarrollo en las capas servidor](#Plataformasparaeldesarrolloenlascapasservidor)
	* 5.4. [Herramientas de desarrollo orientadas a servidor de aplicaciones web](#Herramientasdedesarrolloorientadasaservidordeaplicacionesweb)
* 6. [Lenguajes de programación de aplicaciones web en el lado servidor](#Lenguajesdeprogramacindeaplicacioneswebenelladoservidor)
	* 6.1. [Características de los lenguajes de programación web en servidor](#Caractersticasdeloslenguajesdeprogramacinwebenservidor)
	* 6.2. [Tipos y características de los lenguajes de uso común](#Tiposycaractersticasdeloslenguajesdeusocomn)
	* 6.3. [Criterios en la elección de un lenguaje de programación web en servidor. Ventajas e inconvenientes.](#Criteriosenlaeleccindeunlenguajedeprogramacinwebenservidor.Ventajaseinconvenientes.)
	* 6.4. [Características generales.](#Caractersticasgenerales.)
	* 6.5. [Gestión de la configuración.](#Gestindelaconfiguracin.)
	* 6.6. [Gestión de la seguridad](#Gestindelaseguridad)
	* 6.7. [Gestión de errores](#Gestindeerrores)
	* 6.8. [Transacciones y persistencia](#Transaccionesypersistencia)
	* 6.9. [Componentes en servidor. Ventajas e inconvenientes en el uso de contenedores de componentes](#Componentesenservidor.Ventajaseinconvenientesenelusodecontenedoresdecomponentes)
	* 6.10. [Modelos de desarrollo. El modelo vista controlador](#Modelosdedesarrollo.Elmodelovistacontrolador)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

[Presentación Unidad Formativa UF1844](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-01-presentacion-unidad-formativa/).

##  1. <a name='Elprocesodeldesarrollodesoftware'></a>El proceso del desarrollo de software

[El proceso del desarrollo de software](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-03-proceso-desarrollo-software/). 

###  1.1. <a name='Modelosdelciclodevidadelsoftware'></a>Modelos del ciclo de vida del software

[Modelos del ciclo de vida del software](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-04-modelos-ciclo-vida-software/). 

* [En cascada (waterfall)](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-05-modelo-en-cascada/). 
* [Incremental](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-06-modelo-incremental/)
* [En V](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-07-modelo-v/).
* [Basado en componentes (CBSE)](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-08-basado-en-componentes/)
* [Desarrollo rápido (RAD)](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-09-desarrollo-rapido-rad/).
* [Ventajas e inconvenientes. Pautas para la selección de la metodología más adecuada](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-10-seleccion-metodologia/).

###  1.2. <a name='Anlisisyespecificacinderequisitos'></a>Análisis y especificación de requisitos

[Análisis y especificación de requisitos. Introducción](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-11-analisis-especificacion-requisitos/).

* [Tipos de requisitos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-12-tipos-de-requisitos/)
* [Modelos para el análisis de requisitos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-13-modelos-para-analisis-requisitos/)
* [Documentación de requisitos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-14-documentacion-de-requisitos/).
* [Especificación de requerimientos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-15-especificacion-requerimientos/).
	* Ejercicios prácticos del libro "Aproximación a la ingeniería del software" página 101 - Sistema de gestión de biblioteca. 
* [Validación de requisitos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-16-validacion-requisitos/). [**En desarrollo**]
* Gestión de requisitos.

###  1.3. <a name='Diseo'></a>Diseño

* Modelos para el diseño de sistemas
* Diagramas de diseño. El estándar UML.
* Documentación

###  1.4. <a name='Implementacin.Conceptosgeneralesdedesarrollodesoftware.'></a>Implementación. Conceptos generales de desarrollo de software.

* Principios básicos del desarrollo de software.
* Técnicas de desarrollo de software.

* [Buenas prácticas como desarrolladores](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-25-buenas-pr%C3%A1cticas-principios-desarrolladores/). [**pendiente revisión**]

###  1.5. <a name='Validacinyverificacindesistemas.'></a>Validación y verificación de sistemas.

* Planificación
* Métodos formales de verificación.
* Métodos automatizados de análisis

###  1.6. <a name='Pruebasdesoftware'></a>Pruebas de software

* Tipos
* Pruebas funcionales (BBT)
* Pruebas estructurales (WBT)
* Comparativa. Pautas de utilización
* Diseño de pruebas
* Ámbitos de aplicación.
* Pruebas de Sistemas.
* Pruebas de componentes
* Automatización de pruebas. Herramientas.
* Estándares sobre pruebas de software

###  1.7. <a name='Calidaddelsoftware'></a>Calidad del software

* Principios de calidad del software
* Métricas y calidad del software.
* Concepto de métrica y su importancia en la medición de la calidad.
* Principales métricas en las fases del ciclo de vida software
* Estándares para la descripción de los factores de Calidad
* ISO-9126
* Otros estándares. Comparativa.

* Refactorización [**pendiente crear**]


###  1.8. <a name='Herramientasdeusocomnparaeldesarrollodesoftware'></a>Herramientas de uso común para el desarrollo de software

* Editores orientados a lenguajes de programación.
	* [Introducción a Visual Studio Code](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-28-vscode-1/) [**borrador**].
	* Editores PHP en línea.
* Compiladores y enlazadores.
* Generadores de programas.
* Depuradores
* De prueba y validación de software
* Optimizadores de código
* Empaquetadores
* Generadores de documentación de software
* [Gestores y repositorios de paquetes](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-09-php-gestion-paquetes/). Versionado y control de dependencias. [**borrador**]
* De distribución de software.
* Gestores de actualización de software
* De control de versiones
	* [Introducción a Git](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-13-git-01/). [impartido]
	* [Uso básico de Git](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-14-git-02/). [impartido]
* Entornos integrados de desarrollo (IDE) de uso común.
* VBox.
* Docker [https://soka.gitlab.io/blog/tags/docker/](https://soka.gitlab.io/blog/tags/docker/).

###  1.9. <a name='Gestindeproyectosdedesarrollodesoftware.'></a>Gestión de proyectos de desarrollo de software.

* Planificación de proyecto
* Control de proyectos
* Ejecución de proyectos
* Herramientas de uso común para la gestión de proyectos

##  2. <a name='IntroduccinaPHP'></a>Introducción a PHP

* [Instalación XAMPP en M$ Windows](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-24-xampp-install-windows/). [impartido]
* Instalación XAMPP en Linux Mint.
* PHP-CLI (Win). [**Pendiente creación**]
* [Mi primer programa](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-22-mi-primer-programa-php/). [impartido]
* [Introducción a las variables en PHP](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-23-php-intro-vars/). [impartido]
* [Manipulando textos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-27-manipulando-textos/). [impartido]
* Libro de cocina:
	* [Números](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-29-libro-cocina-numeros/). 
	* [Cadenas](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-30-libro-cocina-cadenas/). 
* [Operadores](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-31-operadores/). [impartido]
* [Ejercicios básicos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-31-php-ejercicios-basicos/).
* [Constantes](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-03-constantes/). [impartido]
* Condicionales 
	* [if](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-01-condicionales-if/). [impartido]
	* [switch](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-02-switch/). [impartido]
* [Case Styles: Camel, Pascal, Snake, and Kebab Case](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2021-12-26-camelcase/). [**Pendiente revisión**].
* [Ejercicios](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-02-php-ejercicios/).
* Arrays - Estructuras avanzadas de datos.
	* [Arrays](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-16-estructuras-de-datos-avanzadas-arrays/). [impartido]
* [Bucles While](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-11-bucles-while/). [impartido]
* [Bucles For](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-12-bucles-for/). [impartido]
* Operador Ternario [**pendiente**]

###  2.1. <a name='Programacinmodular'></a>Programación modular 

* Funciones (programación modular).
	* [Funciones definidas por el usuario](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-15-php-funciones/). [impartido].
	* [Funciones. Libro de cocina](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-18-php-funciones-cookbook/). [**Pendiente revisión**].
	* Funciones flecha.
* [Ejercicios](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-12-php-ejercicios/).
* [Ejercicios funciones](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-18-ejercicios/).
* [Incluir Código de otros Documentos PHP. include y require](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-23-php-include-require/). [impartido]

###  2.2. <a name='Trabajandoconficherosydirectorios'></a>Trabajando con ficheros y directorios

* [Trabajando con ficheros y directorios](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-26-php-ficheros-directorios/).  [impartido]
* [Recetas de cocina - ficheros](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-27-php-recetas-de-cocina-ficheros/).[**pendiente revisión**]
* [Trabajando con ficheros CSV](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-26-php-fcheros-csv/).  [impartido]
* XML 
* JSON
* Funciones del sistema de archivos [**pendiente creación**]
* [Ejercicios ficheros](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-26-php-ejercicios-ficheros/).

###  2.3. <a name='DesarrolloWebconPHP'></a>Desarrollo Web con PHP

* [Carga de Otros Documentos Desde PHP](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-21-php-header-redirigir-a-otro-doc/). [impartido]
* [Autenticación HTTP](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-22-autenticacion-http/). [impartido]
* [Pasar variables por URL usando GET](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-19-php-pasar-datos-usando-get/). [impartido]
* [Formularios y POST](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-20-php-forms/). [impartido]
* [Cookies y autenticación](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-24-php-cookies/). Información persistente entre páginas.[impartido]
* [Sesiones](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-25-php-sesiones/). [impartido]

####  2.3.1. <a name='Sistemadeplantillas'></a>Sistema de plantillas

* Smarty
	* [Introducción a Smarty](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-28-smarty-template/). [**pendiente revisión**]

##  3. <a name='Adicional'></a>Adicional

* El lado oscuro: [código espagueti](https://es.wikipedia.org/wiki/C%C3%B3digo_espagueti) [**Pendiente creación**]

##  4. <a name='Laorientacinaobjetos.'></a>La orientación a objetos.

* [Programación orientada a objetos (|)](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-29-php-poo-1/).
* [Ejercicios clases](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-30-php-poo-ejercicios/).

###  4.1. <a name='Principiosdelaorientacinaobjetos.Comparacinconlaprogramacinestructurada'></a>Principios de la orientación a objetos. Comparación con la programación estructurada

* [Ocultación de información (information hiding)](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-03-poo-ocultacion/).
* [El tipo abstracto de datos (ADT). Encapsulado de datos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-04-poo-abstraccion/).
* Paso de mensajes

###  4.2. <a name='Clasesdeobjetos'></a>Clases de objetos

* Atributos, variables de estado y variables de clase
* Métodos. Requisitos e invariantes.
* Gestión de excepciones
* Agregación de clases.

###  4.3. <a name='Objetos'></a>Objetos

* Creación y destrucción de objetos
* Llamada a métodos de un objeto
* Visibilidad y uso de las variables de estado
* Referencias a objetos
* Persistencia de objetos
* Optimización de memoria y recolección de basura (garbage collection)

###  4.4. <a name='Herencia'></a>Herencia

* [Concepto de herencia. Superclases y subclases](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-01-31-concepto_de_herencia-superclases_y_subclases/). [**pendiente impartir**]
* Herencia múltiple.
* Clases abstractas.
* Tipos de herencia
* [Polimorfismo y enlace dinámico (dynamic binding)](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-01-polimorfismo/). [**pendiente impartir**]
* Directrices para el uso correcto de la herencia.

###  4.5. <a name='Modularidad'></a>Modularidad

* [Modularidad](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-05-poo-modularidad/).
* Librerías de clases. Ámbito de utilización de nombres
* Ventajas de la utilización de modulos o paquetes

###  4.6. <a name='Genericidadysobrecarga'></a>Genericidad y sobrecarga

* [Concepto de genericidad](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-02-concepto_de_genericidad/).
* Concepto de Sobrecarga. Tipos de sobrecarga
* Comparación entre genericidad y sobrecarga.

###  4.7. <a name='Desarrolloorientadoaobjetos'></a>Desarrollo orientado a objetos

* Lenguajes de desarrollo orientado a objetos de uso común
* Herramientas de desarrollo

###  4.8. <a name='Lenguajesdemodelizacineneldesarrolloorientadoaobjetos'></a>Lenguajes de modelización en el desarrollo orientado a objetos

* Uso del lenguaje unificado de modelado (UML) en el desarrollo orientado a objeto
* Diagramas para la modelización de sistemas orientados a objetos.

##  5. <a name='Arquitecturasweb'></a>Arquitecturas web

###  5.1. <a name='Conceptodearquitecturaweb.'></a>Concepto de arquitectura web.

###  5.2. <a name='Elmodelodecapas'></a>El modelo de capas

###  5.3. <a name='Plataformasparaeldesarrolloenlascapasservidor'></a>Plataformas para el desarrollo en las capas servidor

###  5.4. <a name='Herramientasdedesarrolloorientadasaservidordeaplicacionesweb'></a>Herramientas de desarrollo orientadas a servidor de aplicaciones web

* Tipos de herramientas
* Extensibilidad. Instalación de módulos
* Técnicas de configuración de los entornos de desarrollo, preproducción y producción.
* Funcionalidades de depuración.

##  6. <a name='Lenguajesdeprogramacindeaplicacioneswebenelladoservidor'></a>Lenguajes de programación de aplicaciones web en el lado servidor

###  6.1. <a name='Caractersticasdeloslenguajesdeprogramacinwebenservidor'></a>Características de los lenguajes de programación web en servidor

###  6.2. <a name='Tiposycaractersticasdeloslenguajesdeusocomn'></a>Tipos y características de los lenguajes de uso común

* Interpretados orientados a servidor
* Lenguajes de cliente interpretados en servidor
* Lenguajes compilados.

###  6.3. <a name='Criteriosenlaeleccindeunlenguajedeprogramacinwebenservidor.Ventajaseinconvenientes.'></a>Criterios en la elección de un lenguaje de programación web en servidor. Ventajas e inconvenientes.

###  6.4. <a name='Caractersticasgenerales.'></a>Características generales.

* Tipos de datos
* Clases
* Operadores básicos. Manipulación de cadenas de caracteres
* Estructuras de control. Bucles y condicionales.
* Módulos o paquetes.
* Herencia
* Gestión de bibliotecas (libraries)

###  6.5. <a name='Gestindelaconfiguracin.'></a>Gestión de la configuración.

* Configuración de descriptores.
* Configuración de ficheros.

###  6.6. <a name='Gestindelaseguridad'></a>Gestión de la seguridad

* Conceptos de identificación, autenticación y autorización
* Técnicas para la gestión de sesiones

###  6.7. <a name='Gestindeerrores'></a>Gestión de errores

* Técnicas de recuperación de errores.
* Programación de excepciones.

###  6.8. <a name='Transaccionesypersistencia'></a>Transacciones y persistencia

* Acceso a bases de datos. Conectores
	* [Mi primera conexión con una base de datos](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-06-conexion-a-base-de-datos/). [**pendiente impartir**]
	* Ejemplo práctico [Interacción con MySQL usando formularios y PHP](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-08-php-mysql-ejemplo-practico/). [**pendiente impartir**]
	* [Proyecto creación clase PHP acceso a una BD MySQL](https://iker.l.gitlab.io/mf0492_3-uf1844/post/2022-02-07-php-database-class/). [**pendiente impartir**]
* Estándares para el acceso a bases de datos.
* Gestión de la configuración de acceso a bases de datos
* Acceso a directorios y otras fuentes de datos.
* Programación de transacciones.

###  6.9. <a name='Componentesenservidor.Ventajaseinconvenientesenelusodecontenedoresdecomponentes'></a>Componentes en servidor. Ventajas e inconvenientes en el uso de contenedores de componentes

###  6.10. <a name='Modelosdedesarrollo.Elmodelovistacontrolador'></a>Modelos de desarrollo. El modelo vista controlador

* Modelo: programación de acceso a datos
* Vista: Desarrollo de aplicaciones en cliente. Eventos e interfaz de usuario
* Programación del controlador.
* Documentación del software. Inclusión en código fuente. Generadores de documentación.










