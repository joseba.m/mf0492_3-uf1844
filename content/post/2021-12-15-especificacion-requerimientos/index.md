---
title: Especificación de requisitos
subtitle: Análisis y especificación de requisitos
date: 2021-12-15
draft: false
description: Análisis y especificación de requisitos
tags: ["UF1844","Requisitos","Análisis","Requisitos - Especificación"]
---

<!-- vscode-markdown-toc -->
* 1. [Especificación de requisitos](#Especificacinderequisitos)
	* 1.1. [Notaciones para la especificación](#Notacionesparalaespecificacin)
		* 1.1.1. [Especificación en lenguaje natural](#Especificacinenlenguajenatural)
		* 1.1.2. [Especificaciones estructuradas](#Especificacionesestructuradas)
		* 1.1.3. [Diagramas de flujo de datos (DFD)](#DiagramasdeflujodedatosDFD)
		* 1.1.4. [Diagramas de transición de estados](#Diagramasdetransicindeestados)
		* 1.1.5. [Descripciones funcionales. Pseudocódigo](#Descripcionesfuncionales.Pseudocdigo)
		* 1.1.6. [Descripción de datos](#Descripcindedatos)
		* 1.1.7. [Diagramas de modelo de datos](#Diagramasdemodelodedatos)
* 2. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Especificacinderequisitos'></a>Especificación de requisitos

La [**especificación de requerimientos**](https://es.wikipedia.org/wiki/Especificaci%C3%B3n_de_requisitos_de_software#:~:text=La%20especificaci%C3%B3n%20de%20requisitos%20de,son%20conocidos%20como%20requisitos%20funcionales.) es el **proceso de escribir, en un documento de requerimientos, los requerimientos del usuario y del sistema**. De manera ideal, los requerimientos del usuario y del sistema deben ser claros, sin ambigüedades, fáciles de entender, completos y consistentes. Esto en la práctica es difícil de lograr, pues los participantes interpretan los requerimientos de formas diferentes y con frecuencia en los requerimientos hay conflictos e inconsistencias inherentes.

Los requerimientos del usuario para un sistema deben **describir los requerimientos funcionales y no funcionales**, de forma que sean comprensibles para los usuarios del sistema que no cuentan con un conocimiento técnico detallado. De manera ideal, deberían especificar sólo el comportamiento externo del sistema. El documento de requerimientos no debe incluir detalles de la arquitectura o el diseño del sistema. En consecuencia, si usted escribe los requerimientos del usuario, no tiene que usar jerga de software anotaciones estructuradas o formales. Debe escribir los requerimientos del usuario en lenguaje natural, con tablas y formas sencillas, así como diagramas intuitivos.

Los **requerimientos del sistema** son versiones extendidas de los requerimientos del usuario que los ingenieros de software usan como punto de partida para el diseño del sistema. Añaden detalles y explican cómo el sistema debe brindar los requerimientos del usua rio. Se pueden usar como parte del contrato para la implementación del sistema y, por lo tanto, deben ser una especificación completa y detallada de todo el sistema.

Los requerimientos del usuario se escriben casi siempre en lenguaje natural, complementado con diagramas y tablas adecuados en el documento de requerimientos. Los requerimientos del sistema se escriben también en lenguaje natural, pero de igual modo se utilizan otras notaciones basadas en formas, modelos gráficos del sistema o modelos matemáticos del sistema. La tabla de abajo resume las posibles anotaciones que podrían usarse para escribir requerimientos del sistema.

**Formas de escribir una especificación de requerimientos del sistema**:

| Notación | Descripción   |
|----------|---------------|
| Enunciados en lenguaje natural | Los requerimientos se escriben al usar enunciados numerados en lenguaje natural. Cada enunciado debe expresar un requerimiento |
| Lenguaje natural estructurado | Los requerimientos se escriben en lenguaje natural en una forma o plantilla estándar. Cada campo ofrece información de un aspecto del requerimiento |
| Lenguajes de descripción de diseño | Este enfoque usa un lenguaje como un lenguaje de programación, pero con características más abstractas para especificar los requerimientos al definir un modelo operacional del sistema. Aunque en la actualidad este enfoque se usa raras veces, aún tiene utilidad para especificaciones de interfaz |
| Anotaciones gráficas | Los modelos gráficos, complementados con anotaciones de texto, sirven para definir los requerimientos funcionales del sistema; los **casos de uso del UML** y los **diagramas de secuencia** se emplean de forma común |
| Especificaciones matemáticas | Dichas anotaciones se basan en conceptos matemáticos como máquinas o conjuntos de estado finito. Aunque tales especificaciones sin ambigüedades pueden reducir la imprecisión en un documento de requerimientos, la mayoría de los clientes no comprenden una especificación formal. No pueden comprobar que representa lo que quieren y por ello tienen reticencia para aceptarlo como un contrato de sistema |

Los modelos gráficos son más útiles cuando es necesario mostrar cómo cambia un  estado o al describir una secuencia de acciones. Los **gráficos de secuencia UML** y los **gráficos de estado**, que se explican más adelante exponen la secuencia de acciones que ocurren en respuesta a cierto mensaje o evento.

###  1.1. <a name='Notacionesparalaespecificacin'></a>Notaciones para la especificación

####  1.1.1. <a name='Especificacinenlenguajenatural'></a>Especificación en lenguaje natural

Desde los albores de la ingeniería de software, el lenguaje natural se usa para escribir los requerimientos de software. Es expresivo, intuitivo y universal. También es potencialmente vago, ambiguo y su significado depende de los antecedentes del lector. Como resultado, hay muchas propuestas para formas alternativas de escribir los requerimientos. Sin embargo, ninguna se ha adoptado de manera amplia, por lo que el lenguaje natural seguirá siendo la forma más usada para especificar los requerimientos del sistema y del software.

Para minimizar la interpretación errónea al escribir los requerimientos en lenguaje 
natural, se recomienda seguir algunos lineamientos sencillos:

1. Elabore un **formato estándar** y asegúrese de que todas las definiciones de requerimientos se adhieran a dicho formato. Al estandarizar el formato es menos probable cometer omisiones y más sencillo comprobar los requerimientos. El formato que usa el autor expresa el requerimiento en una sola oración.
2. Utilice el lenguaje de manera clara para **distinguir entre requerimientos obligatorios y deseables**. Los primeros son requerimientos que el sistema debe soportar y, por lo general, se escriben en futuro “debe ser”. En tanto que los requerimientos deseables no 
son necesarios y se escriben en tiempo pospretérito o como condicional “debería ser”.
3. Use texto resaltado (negrilla, cursiva o color) para seleccionar partes clave del requerimiento.
4. No deduzca que los lectores entienden el lenguaje técnico de la ingeniería de software. Es fácil que se malinterpreten palabras como “arquitectura” y “módulo”. Por lo tanto, debe **evitar el uso de jerga, abreviaturas y acrónimos**.
5. Siempre que sea posible, asocie una razón con cada requerimiento de usuario. La 
razón debe explicar por qué se incluyó el requerimiento.

####  1.1.2. <a name='Especificacionesestructuradas'></a>Especificaciones estructuradas

El lenguaje natural estructurado es una manera de escribir requerimientos del sistema, donde está limitada la libertad del escritor de requerimientos y todos éstos se anotan en una forma estándar. Aunque este enfoque conserva la mayoría de la expresividad y comprensibilidad del lenguaje natural, asegura que haya cierta **uniformidad sobre la especificación**. Las anotaciones en lenguaje estructurado **emplean plantillas** para especificar requerimientos del sistema.

Ejemplo especificación estructurada de un requerimiento:

| id            | nombre descriptivo                    |
|---------------|---------------------------------------|
| Función       | Una descripción de la función o entidad a especificar. |
| Descripción   | El sistema deberá ... _descripción del requisito general del sistema_ |
| Entradas      | Una descripción de sus entradas y sus procedencias. |
| Fuente        | |
| Salidas       | Una descripción de sus salidas y a dónde se dirigen |
| Destino       | |
| Acción        | Una descripción de la acción que se va a tomar. |
| Requerimientos | |
| Precondición | una precondición establece lo que debe ser verdadero antes de llamar a la función |
| Postcondición | una postcondición especifica lo que es verdadero después de llamar a la función. |
| Efectos colaterales | Una descripción de los efectos colaterales (si acaso hay alguno) de la operación. |

####  1.1.3. <a name='DiagramasdeflujodedatosDFD'></a>Diagramas de flujo de datos (DFD)

Un [**diagrama de flujo de datos**](https://es.wikipedia.org/wiki/Diagrama_de_flujo_de_datos) o **DFD** (sus siglas en español e inglés) es una **representación gráfica del flujo de datos a través de un sistema de información**. El enfoque del análisis estrucurado es considerar que un sistema software es un sistema procesador de datos.  Recibe datos como entrada, los procesa, es decir los ordena, los cuenta, transforma o los usa para calcular expresiones.

Es una práctica común para un diseñador dibujar un contexto a nivel de DFD que primero muestra la interacción entre el sistema y las entidades externas.

Componentes de un Diagrama de Flujo de Datos (DFD) según la notación de Yourdon y DeMarco:

![](img/01.png)

* Una **flecha** indica un flujo de datos en el sentido que apunta la cabeza. Con cada flecha de un DFD se tienen que detallar sus Datos mediante un nombre o una descripción.
* Un proceso o transformación de datos se indica con un **círculo o burbuja**. Dentro del círculo se nombra o describe el proceso que realiza.
* Una **línea doble** indica un almacén de datos. Estos datos almacenados pueden ser guardados por uno o varios procesos y asimismo pueden ser consultados o consumidos por uno o más procesos. Entre las dos líneas se detalla el nombre o descripción de los datos que guarda el almacén.
* Finalmente, un **rectangulo** representa una entidad externa al sistema software (el usuario, otro programa, un dispositivo hardware, etc.), que produce o consume los datos.

##### Ejercicio prático 01

Enunciado:

Se trata de especificar un sistema para el control de acceso a un recinto mediante una tarjeta y una clave de acceso que conoce el usuario. La tarjeta magnética lleva grabada los datos personales del propietario y la clave de acceso (que debe coincidir con la que memoriza el usuario). El sistema dispondrá de un teclado numérico para introducir la clave y un display o pantalla para escribir los mensajes que lee el usuario. El sistema registrará todos los intentos de accesos aunque sean fallidos por no teclear la clave correctamente o por tratar de utilizar un tipo de tarjeta incorrecta. Cuando la clave es correcta el sistema acciona una cerradura electrónica para abrir la puerta.

DFD de contexto para el sistema de control de accesos:

![](img/02.png)

En la figura se muestra el **DFD del sistema global o DFD de contexto**. En este ejemplo, las entidades externas son el lector de tarjetas magnéticas, el teclado para introducir la clave, la pantalla o display y el dispositivo para la apertura de la puerta.

El lector de tarjetas suministra a nuestro sistema los datos grabados en las mismas (por ejemplo: nombre, apellidos, clave de acceso, etc.) y mediante el teclado se lee la clave para la correspondiente comprobación. La pantalla recibe de nuestro sistema los mensajes para poder establecer el diálogo con la persona que trata de acceder al recinto y el dispositivo de apertura de la puerta, las órdenes para abrirla.

El DFD de contexto nunca es suficiente para describir el modelo del sistema que se trata de especificar. Para refinar el modelo, los DFD se usan de forma jerarquizada por niveles. El DFD de contexto se denomina nivel 0. A continuación cada proceso o burbuja se puede "explotar" y mostrar como se organiza internamente, mediante otros DFD. En el caso de un diagrama de contexto, sólo se puede explotar un único proceso que es el proceso global del sistema y su explosión dará lugar al único DFD de nivel 1.

En el ejemplo del sistema para el control de acceso, la siguiente figura muestra el DFD de nivel 1. Como se puede observar, los flujos de entrada y salida al DFD de nivel 1 corresponden exactamente con los del nivel 0. 

![](img/03.png)

El refinamiento del DFD de nivel 1 puede continuar con cada uno de los procesos que en él aparecen.

####  1.1.4. <a name='Diagramasdetransicindeestados'></a>Diagramas de transición de estados

Un **diagrama de transición de estados** muestra el comportamiento dependiente del tiempo de un sistema de información. Representa los estados que puede tomar un componente o un sistema y muestra los eventos que implican el cambio de un estado a otro.

Los diagramas de estado son los más utilizados para representar [**autómatas de estados finitos**](https://es.wikipedia.org/wiki/Aut%C3%B3mata_finito) (autómatas mecánicos, circuitos electrónicos o una aplicación informática). 

![](img/04.png)

##### Ejemplo 01

Implementar el control de acceso usando un diagrama de transión de estados.

![](img/05.png)


####  1.1.5. <a name='Descripcionesfuncionales.Pseudocdigo'></a>Descripciones funcionales. Pseudocódigo

Los requisitos funcionales son una parte muy importante de la especificación de un sistema. Por tanto, es esencial que las descripciones funcionales se realicen empleando la notación precisa y que no se preste a ambiguedades. Como mínimo se deberá emplear un lenguaje natural estructurado y siempre que sea posible es aconsejable utilizar una notación algo más precisa que denominaremos pseudocódigo.

La notación de las estructuras básicas que podemos emplear tendrán los siguientes formatos:

![](img/06.png)

Evidentemente cuando se necesite se pueden anidar varias selecciones unas dentro de otras.

Otras notaciones:

![](img/08.png)

Ejemplo:

![](img/09.png)

Más notaciones:

![](img/10.png)

![](img/11.png)

![](img/12.png)


##### Ejercicio prático 01

Siguiendo el ejemplo del control de accesos:

![](img/07.png)

####  1.1.6. <a name='Descripcindedatos'></a>Descripción de datos

La descripción de los datos constiyue otro aspecto fundamental de una especificación software. Se trata de detallar la estructura interna de los datos que maneja el sistema. Lo mismo sucede con la descripción funcional, cuando se realiza una descripción de datos no se debe descender a detalles de diseño o codificación. Sólo se deben describir aquellos datos que resulten relevantes para entender que debe hacer el sistema.

La notación se conoce como diccionario de datos [Yourdon90]. Está notación es bastante más informal que cualquier definición de tipos de un lenguaje de programación pero con ella se logra una descripción de los datos suficientemente precisa para la especificación de requisitos.

Información a describir para cada dato:

![](img/13.png)

##### Ejemplo práctico 01

Para ilustrar una descripción de datos, a continuación se detallan algunos de los datos del sistema de control de acceso:

![](img/14.png)

####  1.1.7. <a name='Diagramasdemodelodedatos'></a>Diagramas de modelo de datos

Si un sistema maneja una cierta cantidad de datos relacionados entre sí, como sucede habitualmente en los sistemas de información es necesario establecer una organización que facilite las operaciones que se quieren realizar con ellos. Precisamente dicha organización es que la configura la estructura de base de datos que utilizará el sistema.

El siguiente **modelo** se conoce como [**entidad-relación**](https://es.wikipedia.org/wiki/Modelo_entidad-relaci%C3%B3n) (modelo E-R) y permite definir todos los datos que manejará el sistema junto con las relaciones que se desea que existan entre ellos. Aunque el modelo estará sujeto a revisiones durante las fases de diseño y codificación, como sucede con el resto de la especificación, sin embargo, es un punto de partida imprescindible para comenzar cualquier diseño.

Notación básica de diagrama de datos:

![](img/15.png)

Existen diferentes propuestas de notación para realizar los diagramas E-R con pequeñas diferencias. En la figura de arriba se recogen los más habituales. Las **entidades** o datos que se manejan en el diagrama se encierran en un rectángulo. Las **relaciones** se indican mediante un rombo.

Las entidades y la relación que se establece entre ellas indican uniéndolas todas mediante líneas. Opcionalmente, se puede indicar el sentido de la relación con una flecha. Es conveniente utilizar la flecha sobre todo cuando únicamente con el nombre de la relación encerrado en el rombo no queda claro cual es su sentido.

Otro elemento fundamental es la **cardinalidad** de la relación, esto es, entre que valores mínimo y máximo se mueve la relación entre entidades:

![](img/16.png)

##### Ejercicio práctico 01

Dibujar el modelo E-R que ilustre la relación entre una ciudad y un páis, una ciudad puede ser capital de un país como máximo o de ninguno.

![](img/17.png)

Ampliar el modelo para mostrar en que región está una ciudad y que ciudad tiene la embajada.

![](img/18.png)

##### Ejercicios adicionales

* [http://alimentos.web.unq.edu.ar/wp-content/uploads/sites/87/2018/03/Practica-2-MR.pdf](http://alimentos.web.unq.edu.ar/wp-content/uploads/sites/87/2018/03/Practica-2-MR.pdf).
* Ejercicios prácticos del libro "Aproximación a la ingeniería del software" página 101 - Sistema de gestión de biblioteca. 

![](img/19.png)

![](img/20.png)

##  2. <a name='Enlacesexternos'></a>Enlaces externos

* PDF [Especificación de Requisitos según el estándar de IEEE 830](https://www.fdi.ucm.es/profesor/gmendez/docs/is0809/ieee830.pdf) Este documento presenta, en castellano, el formato de Especificación de Requisitos Software (ERS) según la última versión del estándar IEEE 830.
* [Plantilla de Especificación de Requisitos del Sistema de Información](http://www.juntadeandalucia.es/servicios/madeja/contenido/recurso/456) en ODT
* [Especificación de requisitos de software - ocw unican](https://ocw.unican.es/pluginfile.php/1403/course/section/1793/plantilla_formato_ieee830.pdf) (PDF). Este formato es una plantilla tipo para documentos de requisitos del software.Está basado y es conforme con el estándar IEEE Std 830-1998.
* [Introducción a la Ingeniería de Requerimientos](https://www.fceia.unr.edu.ar/~mcristia/publicaciones/ingreq-a.pdf) PDF. Universidad Nacional de Rosario (2014).
* Karl E. Wiegers [Software Requirements Specification](https://www.processimpact.com/process_assets/srs_preview.pdf) (PDF).

* [http://ftpmirror.your.org/pub/wikimedia/images/wikipedia/commons/4/4b/Metodologia_Youdon-DeMarco.pdf](http://ftpmirror.your.org/pub/wikimedia/images/wikipedia/commons/4/4b/Metodologia_Youdon-DeMarco.pdf).

* Vídeo 34 minutos [Principios de la Ingeniería de Requerimientos (01)](https://youtu.be/fLcWpOY3h7k).


* [http://www.vc.ehu.es/jiwotvim/IngenieriaSoftware/Teoria/BloqueII/UML-5.pdf](http://www.vc.ehu.es/jiwotvim/IngenieriaSoftware/Teoria/BloqueII/UML-5.pdf) Digramas de estado UML.

* [https://josejuansanchez.org/bd/unidad-03-teoria/index.html](https://josejuansanchez.org/bd/unidad-03-teoria/index.html) Unidad 03. Del modelo conceptual al modelo relacional.


