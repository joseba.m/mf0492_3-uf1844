---
title: Carga de Otros Documentos Desde PHP con la función Header
subtitle: PHP Web
date: 2022-01-21
draft: false
description: PHP Web
tags: ["PHP","PHP - Header"]
---

<!-- vscode-markdown-toc -->
* 1. [Cabeceras HTTP](#CabecerasHTTP)
* 2. [Enviando cabeceras](#Enviandocabeceras)
* 3. [Cargar otro documento](#Cargarotrodocumento)
* 4. [Tracking headers](#Trackingheaders)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='CabecerasHTTP'></a>Cabeceras HTTP

Las cabeceras HTTP se envían con la petición del navegador cliente al servidor Web HTTP y con la respuesta. Las cabeceras se usan para proporcionar información sobre el mensaje HTTP que va a continuación.

![](img/01.png)

Las cabeceras HTTP toman la forma de pares nombre-valor en una cadena de texto plano. Un retorno de carro (\r) y una nueva línea (\n) acompaña a cada línea de la cabecera. No hay límite de tamaño en la especificación del estándar pero la mayoría de los servidores y clientes navegadores imponen un límite.

##  2. <a name='Enviandocabeceras'></a>Enviando cabeceras

La función [`header`](https://www.php.net/manual/es/function.header) permite mandar cabeceras desde el servidor al navegador cliente. Sólo puedes enviar cabeceras antes de que se envíe cualquier otro contenido normal.

Parámetros:

![](img/02.png)

Existen dos casos especiales para las cabeceras. El primero es para las cabeceras que comienzan con la cadena "HTTP/". Estas pueden ser usadas para explicitar el código de respuesta HTTP. Por ejemplo:

```php
<?php
header("HTTP/1.0 404 Not Found");
exit;
```

El resultado será algo similar a esto:

![](img/03.png)

El segundo caso especial es cuando usamos "Location". Esta cabecera indica al cliente que el documento que buscan está en la localización que especifiques. Veremos a continuación como hacerlo.

##  3. <a name='Cargarotrodocumento'></a>Cargar otro documento

En muchas ocasiones dentro un código PHP debemos cargar otro documento en el navegador. Para ello se usa la función [`header`](https://www.php.net/manual/es/function.header) dentro de la cuál como atributo de Location se especifica la URL del documento que queremos que cargue el navegador.

```php
<?php
header("Location: https://duckduckgo.com/");
exit;
```

Fijate en el uso de [`exit`](https://www.php.net/manual/es/function.exit) después de mandar la cabecera de redirección. Tu código sigue ejecutándose después de enviar las cabeceras a no ser que lo pares.

Recuerde que [`header`](https://www.php.net/manual/es/function.header) **debe ser llamado antes de mostrar nada por pantalla**, etiquetas HTML, líneas en blanco desde un fichero o desde PHP. Es un error muy común leer código con funciones como include o require, u otro tipo de funciones de acceso de ficheros que incluyen espacios o líneas en blanco que se muestran antes de llamar a la función [`header`](https://www.php.net/manual/es/function.header). Sucede el mismo problema cuando se utiliza un solo fichero PHP/HTML.

```php
<html>
<?php
/* Esto producirá un error. Fíjese en el html
 * que se muestra antes que la llamada a header() */
header('Location: http://www.example.com/');
exit;
?>
```

##  4. <a name='Trackingheaders'></a>Tracking headers

La función [`headers_list`](https://www.php.net/manual/es/function.headers-list) Devuelve una lista de encabezados de respuesta enviados (o listos para enviar) en un array. Para determinar si se han enviado o no estos encabezados, use [`headers_sent`](https://www.php.net/manual/es/function.headers-sent.php).