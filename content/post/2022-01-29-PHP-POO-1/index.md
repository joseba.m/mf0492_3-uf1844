---
title: Programación orientada a objetos (|)
subtitle: POO PHP
date: 2022-01-29
draft: false
description: POO PHP
tags: ["PHP","PHP - GET","PHP - POO"]
---

<!-- vscode-markdown-toc -->
* 1. [Conceptos básicos](#Conceptosbsicos)
	* 1.1. [Clase](#Clase)
		* 1.1.1. [Declarando una clase e instanciando objetos](#Declarandounaclaseeinstanciandoobjetos)
	* 1.2. [Atributos o propiedades](#Atributosopropiedades)
	* 1.3. [Visibilidad](#Visibilidad)
	* 1.4. [Métodos de una clase](#Mtodosdeunaclase)
	* 1.5. [Clonando objetos](#Clonandoobjetos)
	* 1.6. [Constructores](#Constructores)
	* 1.7. [Destructores](#Destructores)
	* 1.8. [Set y Get](#SetyGet)
	* 1.9. [Herencia](#Herencia)
		* 1.9.1. [La palabra clave parent](#Lapalabraclaveparent)
		* 1.9.2. [Constructores de clases heredadas](#Constructoresdeclasesheredadas)
		* 1.9.3. [Métodos Final](#MtodosFinal)
	* 1.10. [Serialización de Objetos](#SerializacindeObjetos)
	* 1.11. [Polimorfismo](#Polimorfismo)
	* 1.12. [Propiedades constantes de una clase](#Propiedadesconstantesdeunaclase)
	* 1.13. [Métodos estáticos](#Mtodosestticos)
* 2. [Utilización de objetos](#Utilizacindeobjetos)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

La [**Programación Orientada a Objetos**](https://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos) (POO o OOP en inglés) es un [paradigma dentro de algunos lenguajes de programación](https://es.wikipedia.org/wiki/Paradigma_de_programaci%C3%B3n), hasta el momento hemos usado la [programación funcional](https://es.wikipedia.org/wiki/Programaci%C3%B3n_funcional#Ventajas_de_usar_un_paradigma_funcional) en PHP para agrupar expresiones que nos permite un diseño **modular y reutilizable** (Se denomina "modularidad" a la propiedad que permite subdividir una aplicación en partes más pequeñas (llamadas módulos), cada una de las cuales debe ser tan independiente como sea posible de la aplicación en sí y de las restantes partes). En la POO los objetos se utilizan para representar "entidades" donde los datos y procedimientos están agrupados.

![](img/01.jpg)

El principio No te repitas (en inglés Don't Repeat Yourself o [DRY](https://www.kroatoan.es/articulos/principio-software-dry/), también conocido como Una vez y sólo una) es una filosofía de definición de procesos que promueve la reducción de la duplicación. 

##  1. <a name='Conceptosbsicos'></a>Conceptos básicos 

###  1.1. <a name='Clase'></a>Clase 

Una clase es una especie de "plantilla" en la que se definen los **atributos y métodos** predeterminados de un tipo de objeto. Esta plantilla se crea para poder crear objetos fácilmente. Al método de crear nuevos objetos mediante la lectura y recuperación de los atributos y métodos de una clase se le conoce como **instanciación**.

A la creación de un objeto basado en una clase se le llama instanciar una clase y al objeto obtenido también se le conoce como instancia de esa clase.

Entre los miembros de una clase puede haber:

* **Métodos**. Son los miembros de la clase que contienen el código de la misma. Un método es
como una función. Puede recibir parámetros y devolver valores.
* **Atributos o propiedades**. Almacenan información acerca del estado del objeto al que pertenecen
(y por tanto, su valor puede ser distinto para cada uno de los objetos de la misma clase).

A la creación de un objeto basado en una clase se le llama **instanciar una** clase y al objeto obtenido
también se le conoce como **instancia de esa clase**.

**Los pilares fundamentales de la POO son:**

* **Herencia**. Es el proceso de crear una clase a partir de otra, heredando su comportamiento y características y pudiendo redefinirlos.
* **Abstracción**. Hace referencia a que cada clase oculta en su interior las peculiaridades de su implementación, y presenta al exterior una serie de métodos (interface) cuyo comportamiento está bien definido. Visto desde el exterior, cada objeto es un ente abstracto que realiza un trabajo.
* **Polimorfismo**. Un mismo método puede tener comportamientos distintos en función del objeto
con que se utilice.
* **Encapsulación**. En la POO se juntan en un mismo lugar los datos y el código que los manipula.

**Las ventajas más importantes que aporta la programación orientada a objetos son:**

* **Modularidad**. La POO permite dividir los programas en partes o módulos más pequeños, que son independientes unos de otros pero pueden comunicarse entre ellos.
* **Extensibilidad**. Si se desean añadir nuevas características a una aplicación, la POO facilita esta tarea de dos formas: añadiendo nuevos métodos al código, o creando nuevos objetos que extiendan el comportamiento de los ya existentes.
* **Mantenimiento**. Los programas desarrollados utilizando POO son más sencillos de mantener, debido a la modularidad antes comentada. También ayuda seguir ciertas convenciones al escribirlos, por ejemplo, escribir cada clase en un fichero propio. No debe haber dos clases en un mismo fichero, ni otro código aparte del propio de la clase.

En este módulo no se pretende realizar un estudio profundo de las ventajas y características de la POO, sino simplemente recordar conceptos que ya deberías haber asimilado con anterioridad. Si tienes dudas sobre algo de lo que acabamos de repasar, puedes consultar este tutorial de la web desarrolloweb.com. [https://desarrolloweb.com/articulos/499.php](https://desarrolloweb.com/articulos/499.php)


####  1.1.1. <a name='Declarandounaclaseeinstanciandoobjetos'></a>Declarando una clase e instanciando objetos

Para crear una clase en PHP usamos la palabra reservada del lenguaje `class` y encapsulamos todo el código dentro de las llaves `{ ... }`.

Declaración básica de una clase :

```php
<?php
class ExampleClass {
    // todo el código se introduce aquí dentro
}
?>
```

No es imperativo pero es una **buena práctica de programación que los nombres de las clases comienzen con una letra mayúscula**. Los nombres de las clases siguen las **mismas normas que usamos para nombrar variables**.

Para instanciar un nuevo objeto de una clase, usamos la palabra reservada `new`:

```php
<?php
$exampleObject = new ExampleClass();
// If you are not passing constructor parameters you can omit the brackets if you choose
$anotherObject = new ExampleClass;
?>
```

**La asignación de objetos es siempre por referencia**.

###  1.2. <a name='Atributosopropiedades'></a>Atributos o propiedades

Una clase contiene **atributos o propiedades**, las propiedades representan características de una determinada clase. Cuando se instancia una clase las propiedades son únicas para ese objeto. El sistema operativo reserva un espacio en memoria para guardar las propiedades.

Ejemplo: declaración de una clase con dos atributos.

```php
<?php
class Persona {
    public $nombre;
    private $edad;
}
?>
```

**Las propiedades (y métodos) deben ir antecedidas de una palabra clave para definir su visibilidad**.

###  1.3. <a name='Visibilidad'></a>Visibilidad

La visibilidad de una propiedad, un método o una constante se puede definir anteponiendo a su declaración una de las palabras reservadas,PHP ofrece 3 palabras claves para controlar el [**ámbito**](https://es.wikipedia.org/wiki/%C3%81mbito_(programaci%C3%B3n)) de las propiedades y métodos:

1. `public`: los miembros públicos pueden ser referenciados desde cualquier sitio, incluyendo otras clases e instancias (objeto). Está es la propiedad por defecto para los métodos cuando no se especifica nada. 
2. `protected`: solo desde la misma clase, mediante clases heredadas o desde la clase padre.
3. `private`: únicamente se puede acceder desde la clase que los definió.

**Si no defines de forma explicita la visibilidad esta será pública por defecto**.

_"En programación de computadoras, el ámbito (referido mediante su denominación en inglés scope) es el contexto que pertenece a un nombre dentro de un programa. El ámbito determina en qué partes del programa una entidad puede ser usada. Esto sirve para que se pueda volver a definir una variable con un mismo nombre en diferentes partes del programa sin que haya conflictos entre ellos."_ [[Wikipedia](https://es.wikipedia.org/wiki/%C3%81mbito_(programaci%C3%B3n))].

Ejemplo de una clase instanciada y acceso a sus propiedades:

```php
<?php
class Persona { // Declaración de la clase
    public $nombre;
    private $edad;
}

$objPersona = new Persona(); // instancia de la clase Persona
// Acceso a propiedades del objeto
$objPersona->nombre = "Iker";
//$objPersona->edad = 42; // Error: Cannot access private property Persona

print_r($objPersona);

/*
Persona Object
(
    [nombre] => Iker
    [edad:Persona:private] => 
)
*/

// Podemos instanciar nuevos objetos claro
$objPersona1 = new Persona();
?>
```

Para **crear un nuevo objeto de una clase** se usa el operador **new** que llama al constructor de la clase  para construir el nuevo objeto de manera correcta.

En el ejemplo de arriba hemos definido dos propiedades con diferentes tipos de visibilidad para ilustrar su funcionamiento básico.

**Para acceder desde un objeto a sus atributos o a los métodos de la clase, debes utilizar el operador flecha (fíjate que sólo se pone el símbolo $ delante del nombre del objeto)**.

producto.php:

```php
class Producto {
    private $codigo;
    public $nombre;
    public $PVP;
    
    public function muestra() {
        print "<p>" . $this->codigo . "</p>";
    }
}
```

index.php:

```php
require_once('producto.php');
// Una vez definida la clase, podemos usar la palabra new para instanciar objetos de la siguiente forma
$p = new Producto();

//Para acceder desde un objeto a sus atributos o a los métodos de la clase, debes utilizar el operador
//flecha (fíjate que sólo se pone el símbolo $ delante del nombre del objeto):

$p->nombre = 'Samsung Galaxy S';
$p->muestra();
```

###  1.4. <a name='Mtodosdeunaclase'></a>Métodos de una clase

Algoritmo asociado a un objeto. Desde el punto de vista del comportamiento, **es lo que el objeto puede hacer**. Entre otras cosas un método puede producir un cambio en las propiedades del objeto como en el siguiente ejemplo:

```php
<?php
class Persona { // Declaración de la clase
    public $nombre;
    private $edad;
    
    function estableceEdad($edad) {
        echo "estableceEdad($edad)";
        $this->edad = $edad;
    }
}

$objPersona = new Persona(); // instancia de la clase Persona
// Acceso a propiedades del objeto
$objPersona->nombre = "Iker";
//$objPersona->edad = 42; // Error: Cannot access private property Persona

$objPersona->estableceEdad(42); //llamada a método
?>
```

**Para acceder a un dato de la clase dentro de la misma clase se usa el operador `this`**.

###  1.5. <a name='Clonandoobjetos'></a>Clonando objetos

**Cuando realizamos la asignación de un objeto a otro no se copia todo el objeto sino que se le pasa una referencia**.

```php
<?php

class Usuario {
    public $nombre='';
}

$object1 = new Usuario();
$object1->name = "Alice";
$object2 = $object1;
$object2->name = "Amy";
echo "object1 name = " . $object1->name . "<br>"; // object1 name = Amy
echo "object2 name = " . $object2->name; // object2 name = Amy
?>
```

Para evitar esto podemos usar la función [`clone`](https://www.php.net/manual/es/language.oop5.cloning.php), siguiendo el ejemplo:

```php
$object2 = clone $object1;
``` 

###  1.6. <a name='Constructores'></a>Constructores

Cuando creamos un nuevo objeto podemos pasarle una serie de argumentos, para hacerlo usamos la función `__construct`. 

```php
<?php
class Usuario {
    private $nombre='';
    
    function __construct($nombre) {
        $this->nombre = $nombre;
    }
}

$usuario = new Usuario("Iker");
?>
```

###  1.7. <a name='Destructores'></a>Destructores

También tienes la posibilidad de crear un método _destructor_. Un método destructor puede ser de utilidad por ejemplo para cerrar una conexión con una base de datos o liberar cualquier recurso.

```php
<?php
class Usuario {
    function __destruct() {
        // codigo 
    }
}

$usuario = new Usuario();
?>
```

###  1.8. <a name='SetyGet'></a>Set y Get

Por principios de la programación orientada a objetos, los atributos y métodos de una clase deberían ser en lo posible de tipo private (privados), esto por conceptos de ocultación o encapsulación, con el fin de garantizar la integridad de los datos. Entonces estos atributos no se deberían acceder ni modificar desde otras clases. Entonces ahí aparecen los métodos `__set` y `__get`.

```php
<?php
class Persona {
    private $edad;

    public function __set($name, $value) {
        //echo "Name es: $name y Value: $value";
        if ($value < 0) {
            echo "Imposible";
            $this->edad = 0;
        } else {
            $this->edad = $value;
        }
    }

    public function __get($name) {

        return $this->edad;
    }

}

$persona = new Persona();

$persona->edad = -1;
echo "Edad es: $persona->edad";
?>
```

###  1.9. <a name='Herencia'></a>Herencia

Las clases no se encuentran aisladas, sino que se relacionan entre sí, formando una jerarquía de clasificación. **Los objetos heredan las propiedades y el comportamiento de todas las clases a las que pertenecen**. La herencia organiza y **facilita el polimorfismo y el encapsulamiento**, permitiendo a los objetos ser definidos y creados como tipos especializados de objetos preexistentes (Cuando un objeto 
hereda de más de una clase, se dice que hay herencia múltiple).

En el ejemplo inferior la clase "Suscriptor" es una subclase de "Usuario".

Ejemplo. Heredando y extendiendo una clase:

```php
<?php

$object = new Suscriptor;
$object->nombre = "Iker";
$object->pwd = "pword";
$object->tlfno = "012 345 6789";
$object->email = "iker.l@fptxurdinaga.com";
$object->muestra();

class Usuario {
    public $nombre, $pwd; //nombre y password

    function guardar_usuario() {
        echo "salvar datos de usuarioe";
    }
}

class Suscriptor extends Usuario {
    public $tlfno, $email;
    
    function muestra()  {
        echo "Nombre: " . $this->nombre . "<br>";
        echo "Pass: " . $this->pwd . "<br>";
        echo "Teléfono: " . $this->tlfno . "<br>";
        echo "Email: " . $this->email;
    }
}
?>
```

La clase original tiene dos propiedades, `$nombre` y `$pwd`, y un método para guardar el usuario actual en una base de datos. La clase "Suscriptor" extiende la clase "padre" añadiendo dos propiedades adicionales, además incluye un método para mostrar las propiedades del objeto usando la variable `$this`, que referencia a los valores actuales del objeto procesado.

####  1.9.1. <a name='Lapalabraclaveparent'></a>La palabra clave parent

Si escribes un método en una subclase con el mismo nombre que la clase padre, el nuevo método predomina. A veces no queremos que se comporte así y necesitamos acceder al método del padre, para ello usamos el operador `parent`.

```php
<?php
$object = new Son;
$object->test(); // "[Class Son] I am Luke"
$object->test2(); // "[Class Dad] I am your Father"

class Dad {
    function test() {
        echo "[Class Dad] I am your Father<br>";
    }
}

class Son extends Dad {
    function test() {
        echo "[Class Son] I am Luke<br>";
    }
    
    function test2() {
        parent::test();
    }
}
?>
```

Si quieres asegurarte que estás usando un método de la propia clase puedes usar `self`: `self::method();`.

####  1.9.2. <a name='Constructoresdeclasesheredadas'></a>Constructores de clases heredadas

Extiendes una clase y declaras tu propio constructor, **si quieres inicializar algo en el padre debes llamar explicitamente al constructor del padre**.

```php
<?php
class BaseClass {
   function __construct() {
       print "En el constructor BaseClass<b>";
   }
}

class SubClass extends BaseClass {
   function __construct() {
       parent::__construct();
       print "En el constructor SubClass<br>";
   }
}

class OtherSubClass extends BaseClass {
    // heredando el constructor BaseClass
}

// En el constructor BaseClass
$obj = new BaseClass();

// En el constructor BaseClass
// En el constructor SubClass
$obj = new SubClass();

// In BaseClass constructor
$obj = new OtherSubClass();
?>
```

####  1.9.3. <a name='MtodosFinal'></a>Métodos Final

Si quieres prevenir que una subclase sobrescriba un método de la superclase, puedes usar `final`:

```php
<?php

class SuperClase {
   final public function metodoFinal () {
      echo 'Método final en la superclase';
   }
}

class SubClase extends SuperClase {
   //intento de modificación de la clase final.
   public function metodoFinal () {
      echo 'Método final en la subclase'; // PHP Fatal error:  Cannot override final method SuperClase::metodoFinal()
   }
}

?>
```

###  1.10. <a name='SerializacindeObjetos'></a>Serialización de Objetos

La serialización de objetos es el mecanismo que nos permite almacenar para después recuperar los datos de un objeto.

En PHP la serialización se realiza mediante la función  [`serialize`](https://www.php.net/manual/es/function.serialize.php)  que devuelve un _string_ que contiene un flujo de bytes con la información que deseamos almacenar y la función  unserialize  que restaura 
la información original a partir de dicho _string_.  

Para poder hacer unserialize  a  un objeto, debe de estar definida la clase de ese objeto. Es decir, si se 
tiene un objeto de la clase A, y lo serializamos, se obtendrá un string que haga referencia a la clase  A 
y contenga todas las variables que haya en esta clase. Si se desea deserializar en otro fichero, antes 
debe  estar  presente  la  definición  de  la  clase  A.

```php
<?php
class A {
      var $one = 1;
    
      function show_one() {
          echo $this->one;
      }
  }
  
$a = new A;
$s = serialize($a);
echo $s;
?>
```

###  1.11. <a name='Polimorfismo'></a>Polimorfismo

Comportamientos diferentes, asociados a objetos distintos, pueden compartir el mismo nombre; al llamarlos por ese nombre se utilizará el comportamiento correspondiente al objeto que se esté usando. O, dicho de otro modo, **las referencias y las colecciones de objetos pueden contener objetos de diferentes tipos, y la invocación de un comportamiento en una referencia producirá el comportamiento correcto para el tipo real del objeto referenciado**.

```php
<?php

class Perro {
    
    function ladra() {
        print "guau";
    }
}

class Pajaro {
    function pia() {
        print "piooo";
    }
}

function dameSonido($objeto) {
    if ($objeto instanceof Perro) {
        $objeto->ladra();
    } else if ($objeto instanceof Pajaro) {
        $objeto->pia();
    } else {
        print "Error. El objeto es invalido";
    }
    echo "<br>";
}

dameSonido(new Perro());
dameSonido(new Pajaro());

?>
```

[Operadores de tipo](https://www.php.net/manual/es/language.operators.type.php): `instanceof` se utiliza para determinar si una variable de PHP es un objeto instanciado de una cierta clase: 

###  1.12. <a name='Propiedadesconstantesdeunaclase'></a>Propiedades constantes de una clase 

Hemos visto como se puede definir una constante global así `define("PI",3.14)`, PHP también permite la definición de constantes dentro de una clase 

```php
<?php
class	ColorEnumeration {
	const	YELLOW	=	"Yellow";
	const	ORANGE	=	"Orange";
	const	PINK	=	"Pink";
	
	function showPink() {
		print self::PINK;
	}
}

print ColorEnumeration::YELLOW; // Yellow
$object	= new ColorEnumeration();
$object->showPink(); // Pink

?>
```

Puedes referencias las constantes directamente usando la palabra reservada `self` y el operador `::`. Fijate que esto **llama a la clase directamente, sin crear una instancia previamente**.  

###  1.13. <a name='Mtodosestticos'></a>Métodos estáticos 

Un método puede ser definido como `static`, esto significa que **puede llamarse en la clase, pero no en el objeto**. Un método estático no tiene acceso a las propiedades del objeto.

```php
<?php
Usuario::pwd_string(); // Please enter your password

class Usuario {
    static function pwd_string() {
        echo "Please enter your password";
    }
}
?>
```

Fijate que llamamos a la clase con un método `static` usando `::` en lugar de `->`. 

##  2. <a name='Utilizacindeobjetos'></a>Utilización de objetos

Ya sabes cómo instanciar un objeto utilizando `new`, y cómo acceder a sus métodos y atributos públicos con el operador flecha:

```php
$p = new Producto();
$p->nombre = 'Samsung Galaxy S';
$p->muestra();
```

Una vez creado un objeto, puedes utilizar el operador [`instanceof`](https://www.php.net/manual/es/language.operators.type.php) para comprobar si es o no una instancia de una clase determinada.

```php
if ($p instanceof Producto) {
    // …
}
```

Además, en PHP se incluyen una serie de **funciones útiles para el desarrollo de aplicaciones utilizando POO**.

[`get_class`](https://www.php.net/manual/es/function.get-class) Devuelve el nombre de la clase de un objeto:

```php
<?php

class foo {
    function nombre()
    {
        echo "Mi nombre es " , get_class($this) , "\n";
    }
}

// crear un objeto
$bar = new foo();

// llamada externa
echo "Su nombre es " , get_class($bar) , "\n";  // Su nombre es foo

// llamada interna
$bar->nombre(); // Mi nombre es foo

?>
```

[`class_exists`](https://www.php.net/manual/es/function.class-exists) Devuelve true si la clase está definida o
false en caso contrario.

```php
<?php
// Verificar que la clase exista antes de usarla
if (class_exists('MiClase')) {
    $mi_clase = new MiClase();
}

?>
```

[`get_declared_classes`](https://www.php.net/manual/es/function.get-declared-classes) Devuelve una matriz con los nombres de las clases definidas.

[`class_alias`](https://www.php.net/manual/es/function.class-alias)  Crea un alias para una clase.

```php
<?php

class foo { }

class_alias('foo', 'bar');

$a = new foo;
$b = new bar;

// los objetos son los mismos
var_dump($a == $b, $a === $b);
var_dump($a instanceof $b);

// las clases son las mismas
var_dump($a instanceof foo);
var_dump($a instanceof bar);

var_dump($b instanceof foo);
var_dump($b instanceof bar);

?>
```


[`get_class_methods`](https://www.php.net/manual/en/function.get-class-methods)



##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://www.php.net/manual/es/language.oop5.php](https://www.php.net/manual/es/language.oop5.php).
* [https://www.php.net/manual/es/language.oop5.visibility.php](https://www.php.net/manual/es/language.oop5.visibility.php).
* [https://www.php.net/manual/es/language.oop5.cloning.php](https://www.php.net/manual/es/language.oop5.cloning.php).
* [https://www.php.net/manual/es/function.serialize.php](https://www.php.net/manual/es/function.serialize.php).
* [https://www.php.net/manual/es/language.oop5.magic.php](https://www.php.net/manual/es/language.oop5.magic.php).


* [https://en.wikipedia.org/wiki/Don%27t_repeat_yourself](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).