---
title: Modelo incremental
subtitle: Modelos del ciclo de vida del software
date: 2021-12-06
draft: false
description: Modelos del ciclo de vida del software
tags: ["UF1844","Modelos del ciclo de vida","Modelo incremental"]
---

<!-- vscode-markdown-toc -->
* 1. [Beneficios de este modelo](#Beneficiosdeestemodelo)
* 2. [Ejemplos](#Ejemplos)
* 3. [Enlaces externos](#Enlacesexternos)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Este [modelo](https://en.wikipedia.org/wiki/Incremental_build_model) se basa en **ir incrementando las funcionalidades de un programa**.

De forma **iterativa** se construyen módulos, cada uno de los cuales cumplirá las funciones del sistema. Esto permitirá aumentar de forma gradual las funcionalidades del software.

Este modelo **permite dividir el trabajo entre distintos equipos** para que realice cada uno un módulo.

Este modelo es una evolución del ciclo de vida en cascada que de forma iterativa, ayudará a construir el programa con todas sus funcionalidades.

**Al terminar cada iteración se entrega una versión al cliente** con una nueva funcionalidad. El sistema crea versiones del software mejorando y aprendiendo de las versiones anteriores de forma iterativa. Así habrá entrega en cada iteración y el cliente conocerá la evolución del sistema en todo momento.

Cuando se utiliza un modelo incremental, **el primer incremento es a menudo un producto esencial, sólo con los requisitos básicos**. Este modelo se centra en la entrega de **un producto operativo con cada incremento**. Los primeros incrementos son versiones incompletas del producto final, pero proporcionan al usuario la funcionalidad que precisa y también una plataforma para la evaluación.

![](img/01.jpg)

[[Fuente](https://en.wikipedia.org/wiki/Incremental_build_model#/media/File:Incremental_Model.jpg)]

Este modelo aplica secuencias lineales de forma escalonada mientras progresa el tiempo en el calendario. Cada secuencia lineal produce un incremento del software.


##  1. <a name='Beneficiosdeestemodelo'></a>Beneficios de este modelo

* Permite construir un sistema inicialmente pequeño, lo que conlleva menos riesgos que hacerlo con uno grande. 
* Genera software operativo de forma rápida y en etapas tempranas del ciclo de vida del software.
* Al desarrollar independientemente las distintas funciones, es más sencillo detectar los requisitos de usuario.
* Es más fácil probar y depurar en una iteración más pequeña.
* Si detectamos un error grave, sólo habrá que desechar la última fase del proyecto.
* No se requiere tener los requisitos de todas las funcionalidades al principio del proyecto, facilitando la labor de desarrollo (divide y vencerás).

Este modelo es especialmente útil cuando el cliente precise entregas rápidas aún siendo estas parciales.

**Recordatorio**: La construcción en módulos dotan al sistema de flexibilidad, permitiendo su construcción por fases y su posterior ampliación.

##  2. <a name='Ejemplos'></a>Ejemplos

Supongamos que un ingeniero está pensando en desarrollar un software para procesar textos. El modelo incremental requerirá que el desarrollo se haga por fases y la planificación administrativa y técnica es muy importante. Primero hay que enfocarse en el procesador de textos con sus componentes básicos. Por ejemplo, la creación de un archivo y la edición del mismo.

En segundo lugar, pueden agregar funciones como el corrector ortográfico y gramatical, además de añadir opciones vinculas con el diseño del documento. En tercer lugar, el poder incluir imágenes, cuadros y crear gráficos estadísticos. Y así, hasta que se cumpla con el objetivo central.  

El cliente podrá ir viendo cada avance, corrigiendo y aprobando cada paso. Esto explica el modelo incremental fases.

##  3. <a name='Enlacesexternos'></a>Enlaces externos

* [https://en.wikipedia.org/wiki/Incremental_build_model](https://en.wikipedia.org/wiki/Incremental_build_model).